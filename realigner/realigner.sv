module realigner
(
  input  wire                 areset,
  input  wire                 clk,

  input  wire                 ws_write,
  input  wire [ 3: 0]         ws_be,
  input  wire [ 3: 0][ 7: 0]  ws_d,
  output wire                 ws_full,
  
  input  wire                 rs_read,
  input  wire [ 1: 0]         rs_size,
  output wire [ 3: 0][ 7: 0]  rs_q,
  output wire                 rs_empty
);

  logic                 r_ws_write;
  logic [ 3: 0]         r_ws_be;
  logic [ 3: 0][ 7: 0]  r_ws_d;
  logic                 r_ws_full;
  logic                 n_ws_full;
  
  logic                 r_rs_read;
  logic [ 1: 0]         r_rs_size;
  logic [ 3: 0][ 7: 0]  r_rs_q;
  logic [ 3: 0][ 7: 0]  n_rs_q;
  logic                 r_rs_empty;
  logic                 n_rs_empty;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_ws_write  <= '0;
      r_ws_be     <= '0;
      r_ws_d      <= '0;
      r_ws_full   <= '0;
      
      r_rs_read   <= '0;
      r_rs_size   <= '0;
      r_rs_q      <= '0;
      r_rs_empty  <= '0;
    end
    else begin
      r_ws_write  <= ws_write;
      r_ws_be     <= ws_be;
      r_ws_d      <= ws_d;
      r_ws_full   <= n_ws_full;
      
      r_rs_read   <= rs_read;
      r_rs_size   <= rs_size;
      r_rs_q      <= n_rs_q;
      r_rs_empty  <= n_rs_empty;
    end
  
  assign ws_full  = r_ws_full;
  assign rs_q     = r_rs_q;
  assign rs_empty = r_rs_empty;
  
  /*
  realigner_4x4b
    i0
    (
      .areset   (areset),
      .clk      (clk),
      
      .ws_write (r_ws_write),
      .ws_size  (r_ws_size),
      .ws_d     (r_ws_d),
      .ws_full  (n_ws_full),
      
      .rs_read  (r_rs_read),
      .rs_size  (r_rs_size),
      .rs_q     (n_rs_q),
      .rs_empty (n_rs_empty)
    );
  */
  
  align_pipe_4x1
    ap4x1
    (
      .areset   (areset),
      .clk      (clk),
       
      .ws_write (r_ws_write),
      .ws_be    (r_ws_be),
      .ws_d     (r_ws_d),
      .ws_full  (n_ws_full),
      
      .rs_read  (r_rs_read),
      .rs_q     (n_rs_q[0]),
      .rs_empty (n_rs_empty)
    );

endmodule

module realigner_4x4
(
  input  wire                 areset,
  input  wire                 clk,

  input  wire                 ws_write,
  input  wire [ 1: 0]         ws_size,
  input  wire [ 3: 0][ 7: 0]  ws_d,
  output wire                 ws_full,
  
  input  wire                 rs_read,
  input  wire [ 1: 0]         rs_size,
  output wire [ 3: 0][ 7: 0]  rs_q,
  output wire                 rs_empty
);

  logic [ 7: 0][ 7: 0]  rp;

  logic [ 7: 0][ 7: 0]  nrp;
  logic [ 7: 0]         nre;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rp <= '0;
    else
      for (int i = 0; i < 8; ++ i)
        if (nre[i])
          rp[i] <= nrp[i];
          
  logic [ 3: 0] cnt;
  logic [ 3: 0] ncnt;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cnt <= '0;
    else
      cnt <= ncnt;

  logic [ 2: 0] rs_read_size;
  always_comb begin
    unique case (rs_size)
      2'b00 : rs_read_size = 3'd4;
      2'b01 : rs_read_size = 3'd3;
      2'b10 : rs_read_size = 3'd2;
      2'b11 : rs_read_size = 3'd1;
    endcase
  end
  
  logic [ 2: 0] ws_write_size;
  always_comb begin
    unique case (ws_size)
      2'b00 : ws_write_size = 3'd4;
      2'b01 : ws_write_size = 3'd3;
      2'b10 : ws_write_size = 3'd2;
      2'b11 : ws_write_size = 3'd1;
    endcase
  end
  
  logic rs_can_read;
  logic ws_can_write;
  
  always_comb begin
    ncnt = cnt;
    nrp  = rp;
    nre  = '1;

    rs_can_read = (rs_read_size <= cnt);
    
    if (rs_read) begin
      ncnt = cnt - rs_read_size;
      unique case (rs_size)
        2'b00 : nrp[ 3: 0] = nrp[ 7: 4];
        2'b01 : nrp[ 4: 0] = nrp[ 7: 3];
        2'b10 : nrp[ 5: 0] = nrp[ 7: 2];
        2'b11 : nrp[ 6: 0] = nrp[ 7: 1];
      endcase
    end
    
    ws_can_write = (ws_write_size <= (4'd8 - ncnt));
    if (ws_write) begin
      unique case (ncnt[ 2: 0])
        3'd0 : nrp[ 3: 0] = ws_d[ 3: 0];
        3'd1 : nrp[ 4: 1] = ws_d[ 3: 0];
        3'd2 : nrp[ 5: 2] = ws_d[ 3: 0];
        3'd3 : nrp[ 6: 3] = ws_d[ 3: 0];
        3'd4 : nrp[ 7: 4] = ws_d[ 3: 0];
        3'd5 : nrp[ 7: 5] = ws_d[ 2: 0];
        3'd6 : nrp[ 7: 6] = ws_d[ 1: 0];
        3'd7 : nrp[ 7: 7] = ws_d[ 0: 0];
      endcase

      ncnt = ncnt + ws_write_size;
    end
  end
  
  assign rs_empty = !rs_can_read;
  assign rs_q     = rp[ 3: 0];
  
  assign ws_full  = !ws_can_write;

endmodule

module realigner_4x4b
(
  input  wire                 areset,
  input  wire                 clk,

  input  wire                 ws_write,
  input  wire [ 1: 0]         ws_size,
  input  wire [ 3: 0][ 7: 0]  ws_d,
  output wire                 ws_full,
  
  input  wire                 rs_read,
  input  wire [ 1: 0]         rs_size,
  output wire [ 3: 0][ 7: 0]  rs_q,
  output wire                 rs_empty
);

  logic [ 6: 0][ 7: 0]  rp;

  logic [ 6: 0][ 7: 0]  nrp;
  logic [ 6: 0]         nre;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rp <= '0;
    else
      for (int i = 0; i < 7; ++ i)
        if (nre[i])
          rp[i] <= nrp[i];
          
  logic [ 2: 0] cnt;
  logic [ 2: 0] ncnt;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cnt <= '0;
    else
      cnt <= ncnt;

  logic [ 2: 0] rs_read_size;
  always_comb begin
    unique case (rs_size)
      2'b00 : rs_read_size = 3'd4;
      2'b01 : rs_read_size = 3'd3;
      2'b10 : rs_read_size = 3'd2;
      2'b11 : rs_read_size = 3'd1;
    endcase
  end
  
  logic [ 2: 0] ws_write_size;
  always_comb begin
    unique case (ws_size)
      2'b00 : ws_write_size = 3'd4;
      2'b01 : ws_write_size = 3'd3;
      2'b10 : ws_write_size = 3'd2;
      2'b11 : ws_write_size = 3'd1;
    endcase
  end
  
  logic rs_can_read;
  logic ws_can_write;
  
  always_comb begin
    ncnt = cnt;
    nrp  = rp;
    nre  = '1;

    rs_can_read = (rs_read_size <= cnt);
    
    if (rs_read) begin
      ncnt = cnt - rs_read_size;
      unique case (rs_size)
        2'b00 : nrp[ 2: 0] = nrp[ 6: 4];
        2'b01 : nrp[ 3: 0] = nrp[ 6: 3];
        2'b10 : nrp[ 4: 0] = nrp[ 6: 2];
        2'b11 : nrp[ 5: 0] = nrp[ 6: 1];
      endcase
    end
    
    ws_can_write = (ws_write_size <= (4'd7 - ncnt));
    if (ws_write) begin
      unique case (ncnt[ 2: 0])
        3'd0 : nrp[ 3: 0] = ws_d[ 3: 0];
        3'd1 : nrp[ 4: 1] = ws_d[ 3: 0];
        3'd2 : nrp[ 5: 2] = ws_d[ 3: 0];
        3'd3 : nrp[ 6: 3] = ws_d[ 3: 0];
        3'd4 : nrp[ 6: 4] = ws_d[ 2: 0];
        3'd5 : nrp[ 6: 5] = ws_d[ 1: 0];
        3'd6 : nrp[ 6: 6] = ws_d[ 0: 0];
        3'd7 : ;
      endcase

      ncnt = ncnt + ws_write_size;
    end
  end
  
  assign rs_empty = !rs_can_read;
  assign rs_q     = rp[ 3: 0];
  
  assign ws_full  = !ws_can_write;

endmodule

module align_pipe_4x1
(
  input  wire         areset,
  input  wire         clk,
  
  input  wire         ws_write,
  input  wire [ 3: 0] ws_be,
  input  wire [31: 0] ws_d,
  output wire         ws_full,
  
  input  wire         rs_read,
  output wire [ 7: 0] rs_q,
  output wire         rs_empty
);
  logic [ 3: 0][ 7: 0] d;
  always_comb begin
    d = 'x;
    if      (ws_be[0])
      d[ 3: 0] = ws_d[31: 0];
    else if (ws_be[1])
      d[ 2: 0] = ws_d[31: 8];
    else if (ws_be[2])
      d[ 1: 0] = ws_d[31:16];
    else
      d[ 0: 0] = ws_d[31:24];
  end
  
  logic [ 2: 0] wsize;
  always_comb
    wsize = ws_be[0] + ws_be[1] + ws_be[2] + ws_be[3];

  logic [ 3: 0][ 7: 0]  rp;
  logic [ 3: 0][ 7: 0]  nrp;
  
  logic [ 2: 0] cnt;
  logic [ 2: 0] ncnt;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      rp  <= '0;
      cnt <= '0;
    end
    else begin
      rp  <= nrp;
      cnt <= ncnt;
    end
  
  logic rs_can_read;
  logic ws_can_write;
  
  always_comb begin
    nrp  = rp;
    ncnt = cnt;

    rs_can_read = (cnt != 0);
  
    if (rs_read) begin
      ncnt = ncnt - 1'b1;
      nrp[ 2: 0]  = rp[ 3: 1];
    end
    
    ws_can_write = (wsize <= (3'd4 - ncnt));
    
    if (ws_write) begin
      unique case (ncnt[ 1: 0])
        2'd0 : nrp[ 3: 0] = d[ 3: 0];
        2'd1 : nrp[ 3: 1] = d[ 2: 0];
        2'd2 : nrp[ 3: 2] = d[ 1: 0];
        2'd3 : nrp[ 3: 3] = d[ 0: 0];
      endcase
      ncnt = ncnt + wsize;
    end
  end
  
  assign rs_empty = !rs_can_read;
  assign ws_full  = !ws_can_write;
  
  assign rs_q = rp[0];
  
endmodule

