create_clock -name clk -period "50.0MHz" [get_ports {clk}]

derive_clock_uncertainty
derive_pll_clocks
