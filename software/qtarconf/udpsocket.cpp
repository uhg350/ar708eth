#include "udpsocket.hpp"

namespace udp
{

boost::asio::io_service       socket::io_service_;
boost::asio::io_service::work socket::work_(socket::io_service_);

}
