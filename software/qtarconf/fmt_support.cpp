#include <iostream>
#include <iterator>

#define FMT_HEADER_ONLY
#include "../fmt/fmt/format.h"
#include "../fmt/fmt/printf.h"

#include "fmt_support.hpp"

void hexdump(const void *p, std::size_t size, std::ptrdiff_t offset)
{
  const unsigned char *pu = static_cast<const unsigned char *>(p);
  const std::size_t cols = 16;

  while (size != 0)
  {
    //std::cout << fmt::sprintf("%08X: ", offset);
    fmt::printf("%08X: ", offset);

    std::size_t data_size = std::min(size, cols);
    std::size_t padding_size = cols - data_size;

    std::for_each(pu, pu + data_size, [](unsigned char data){
      //std::cout << fmt::sprintf("%02X ", data);
      fmt::printf("%02X ", data);
    });

    std::fill_n(std::ostream_iterator<char>(std::cout), padding_size * 3, ' ');

    std::for_each(pu, pu + data_size, [](unsigned char data){
      //std::cout << (std::isprint(data) ? static_cast<char>(data) : '.');
      std::putchar(std::isprint(data) ? static_cast<char>(data) : '.');
    });

    std::cout << "\n";

    size -= data_size;
    pu += data_size;
    offset += data_size;
  }
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const version_t& vi)
{
  f.writer().write("{:02d}.{:02d}.{:04d}", vi.major.value(), vi.minor.value(), vi.patch.value());
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const timestamp_t& ts)
{
  f.writer().write(
    "{:04d}/{:02d}/{:02d} {:02d}:{:02d}:{:02d}.{:02d}",
    ts.year.value(), ts.month.value(), ts.day.value(),
    ts.hour.value(), ts.minute.value(), ts.second.value(), ts.cs.value()
  );
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const serial_t& si)
{
  f.writer().write("{:d}", si.number.value());
}
void format_arg(fmt::BasicFormatter<char> &f, const char *&, const ethernet_address_t& ea)
{
  f.writer().write("mac: {} ip: {} port: {} ", ea.mac, ea.ip, ea.port);
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const eth2_mac_t& mac)
{
  for (size_t i = 0; i < 6; ++ i)
  {
    if (i != 0)
      f.writer().write(":");
    f.writer().write("{:02X}", mac.u8[i].value());
  }
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const ipv4_address_t& ip)
{
  for (size_t i = 0; i < 4; ++ i)
  {
    if (i != 0)
      f.writer().write(".");
    f.writer().write("{:d}", ip.u8[i].value());
  }
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const ipv4_port_t& port)
{
  f.writer().write("{:d}", port.u16.value());
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const message_filter_t& mf)
{
  f.writer().write(
    "lines: {:08b}, id: {:03o}, offset: {:d}, size: {:d}",
    mf.lines_mask.value(),
    mf.message_id.value(),
    mf.message_offset.value(),
    mf.message_size.value()
  );
}

void format_arg(fmt::BasicFormatter<char> &f, const char *&, const device_e100mode_t& em)
{
  if (em.bm & (EMAC_PHY_CONFIG_AN100M | EMAC_PHY_CONFIG_AN10M))
  {
    f.writer().write(
      "auto{}{}{}",
      (em.bm & EMAC_PHY_CONFIG_AN100M)      ? "-100M"       : "",
      (em.bm & EMAC_PHY_CONFIG_AN10M)       ? "-10M"        : "",
      (em.bm & EMAC_PHY_CONFIG_HALFDUPLEX)  ? "-halfduplex" : "-fullduplex"
    );
  }
  else
  {
    f.writer().write(
      "force{}{}{}",
      (em.bm & EMAC_PHY_CONFIG_FORCE100M)   ? "-100M"       : "-10M",
      (em.bm & EMAC_PHY_CONFIG_HALFDUPLEX)  ? "-halfduplex" : "-fullduplex",
      (em.bm & EMAC_PHY_CONFIG_MDI_X)       ? "-mdi"        : "-mdix"
    );
  }
}

void dump_data_header(const header_t h)
{
  //const header_t& h = ident.header;
  std::cout << fmt::format("  size              : {}\n", h.size);
  std::cout << fmt::format("  version           : {}\n", h.version);
}

void dump_ident_content(const ident_content_t& d)
{
  //const ident_content_t& d = ident.content;
  std::cout << fmt::format("  timestamp         : {}\n", d.timestamp);
  std::cout << fmt::format("  device version    : {}\n", d.version);
  std::cout << fmt::format("  device serial     : {}\n", d.serial);
  const std::string s(d.user->data());
  std::cout << fmt::format("  user data         : {}\n", s);
}

void dump_ident(const ident_t& ident)
{
  std::cout << "ident header\n";
  dump_data_header(ident.header);
  std::cout << "ident content\n";
  dump_ident_content(ident.content);
}

void dump_config_content(const config_content_t& d)
{
  std::cout << fmt::format("  timestamp         : {}\n", d.timestamp);
  std::cout << fmt::format("  device address    : {}\n", d.device_address);
  std::cout << fmt::format("  ethernet mode     : {}\n", d.device_e100mode);

  for (size_t i = 0; i < CONFIG_MAXIMUM_SINKS; ++ i)
  {
    std::cout << fmt::format("  sink #{:d}\n", i + 1);
    const sink_config_t& s = d.sinks[i];
    if (s.flags & SINK_CONFIG_FLAG_ENABLED)
    {
      if (s.flags & SINK_CONFIG_FLAG_USE_CUSTOM_DST_MAC)
        std::cout << fmt::format("    dst addr      : {}\n", s.dst);
      else
        std::cout << fmt::format("    dst addr      : {}:{} (use arp for mac)\n", s.dst.ip, s.dst.port);
      if (s.flags & SINK_CONFIG_FLAG_USE_CUSTOM_SRC)
        std::cout << fmt::format("    src addr      : {}\n", s.dst);

      for (size_t k = 0; k < CONFIG_MAXIMUM_FILTERS; ++ k)
      {
        const message_filter_t& m = s.messages[k];
        std::cout << fmt::format("    filter {:d}      : ", (k+1));
        if (m.lines_mask == 0)
          std::cout << "disabled\n";
        else
          std::cout << fmt::format("{}\n", m);
      }
    }
    else
    {
      std::cout << "    disabled\n";
    }
  }
}

void dump_config(const config_t& config)
{
  //const header_t& h = config.header;
  std::cout << "config header\n";
  dump_data_header(config.header);

  //const config_content_t& d = config.content;
  std::cout << "config content\n";
  dump_config_content(config.content);
}

