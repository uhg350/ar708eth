TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    arconf.cpp \
    udpsocket.cpp \
    fmt_support.cpp

HEADERS += \
    structs.hpp \
    udpsocket.hpp \
    fmt_support.hpp

unix:LIBS += -lboost_system
win32:LIBS += -lboost_system-mt -lfmt -lws2_32
