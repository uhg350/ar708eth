#ifndef FMT_SUPPORT_HPP
#define FMT_SUPPORT_HPP

#include <cstddef>
#include "structs.hpp"

void hexdump(const void *p, std::size_t size, std::ptrdiff_t offset = 0);
void dump_ident(const ident_t& ident);
void dump_config(const config_t& config);

#endif // FMT_SUPPORT_HPP
