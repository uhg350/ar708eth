#ifndef STRUCTS_HPP
#define STRUCTS_HPP

#include <boost/endian/arithmetic.hpp>

typedef boost::endian::little_uint32_t  le_uint32_t;
typedef boost::endian::little_uint16_t  le_uint16_t;
typedef boost::endian::little_uint8_t   le_uint8_t;

typedef boost::endian::big_uint32_t     be_uint32_t;
typedef boost::endian::big_uint16_t     be_uint16_t;
typedef boost::endian::big_uint8_t      be_uint8_t;

#define CONFIG_MAJOR  1
#define CONFIG_MINOR  0
#define CONFIG_PATCH  0

#ifndef CONFIG_MAJOR
#define CONFIG_MAJOR  1
#endif

#ifndef CONFIG_MINOR
#define CONFIG_MINOR  0
#endif

#ifndef CONFIG_PATH
#define CONFIG_PATCH  0
#endif

#if (CONFIG_MAJOR == 1) && (CONFIG_MINOR == 0) && (CONFIG_PATCH == 0)

  #define CONFIG_MAXIMUM_SINKS    10
  #define CONFIG_MAXIMUM_FILTERS  4

#endif
//------------------------------------------
typedef le_uint32_t checksum_t;

typedef struct
{
  le_uint8_t    major;
  le_uint8_t    minor;
  le_uint16_t   patch;
} version_t;

typedef struct
{
  le_uint32_t   size;
  version_t     version;
} header_t;

template<typename content_t>
struct data_block_t
{
  header_t    header;
  content_t   content;
  checksum_t  checksum;
};

typedef struct
{
  le_uint16_t   year;
  le_uint8_t    month;
  le_uint8_t    day;
  le_uint8_t    hour;
  le_uint8_t    minute;
  le_uint8_t    second;
  le_uint8_t    cs;
} timestamp_t;

typedef struct
{
  le_uint32_t   number;
} serial_t;

typedef struct
{
  timestamp_t     timestamp;
  version_t       version;
  serial_t        serial;
  le_uint8_t      user[256];
} ident_content_t;

typedef data_block_t<ident_content_t> ident_t;

//------------------------------------------------
typedef struct
{
  union
  {
    be_uint32_t u32;  // network byte order (big endian)
    be_uint16_t u16[2];
    be_uint8_t  u8[4];
  };
} ipv4_address_t;

typedef struct
{
  union
  {
    be_uint16_t u16; // network byte order (big endian)
    be_uint8_t  u8[2];
  };
}  ipv4_port_t;

typedef struct
{
  union
  {
    be_uint8_t  u8[6];  // network byte order (big endian)
    be_uint16_t u16[3];
  };
} eth2_mac_t;

typedef struct
{
  eth2_mac_t        mac;
  ipv4_port_t       port;
  ipv4_address_t    ip;
} ethernet_address_t;

typedef struct
{
  le_uint32_t     bm;
} device_e100mode_t;

typedef struct
{
  le_uint32_t     bm;
} device_ar708mode_t;

typedef struct
{
  le_uint8_t       lines_mask;
  le_uint8_t       message_id;
  le_uint8_t       message_offset;
  le_uint8_t       message_size;
} message_filter_t;

typedef struct
{
  ethernet_address_t  src;
  ethernet_address_t  dst;
  le_uint32_t         flags; // 0 bit :  0 - use dev addr, 1 - use addr form config
  message_filter_t    messages[CONFIG_MAXIMUM_FILTERS];
} sink_config_t;

typedef struct
{
  timestamp_t         timestamp;
  ethernet_address_t  device_address;
  device_e100mode_t   device_e100mode;
  device_ar708mode_t  device_ar708mode;
  sink_config_t       sinks[CONFIG_MAXIMUM_SINKS];

} config_content_t;

typedef data_block_t<config_content_t> config_t;

#define SINK_CONFIG_FLAG_ENABLED            (0x00000001)
#define SINK_CONFIG_FLAG_USE_CUSTOM_SRC     (0x00000002)
#define SINK_CONFIG_FLAG_USE_CUSTOM_DST_MAC (0x00000004)

#define EMAC_PHY_CONFIG_AN100M      (0x0001)
#define EMAC_PHY_CONFIG_AN10M       (0x0002)
#define EMAC_PHY_CONFIG_FULLDUPLEX  (0x0004)
#define EMAC_PHY_CONFIG_HALFDUPLEX  (0x0000)
#define EMAC_PHY_CONFIG_FORCE100M   (0x0008)
#define EMAC_PHY_CONFIG_FORCE10M    (0x0000)
#define EMAC_PHY_CONFIG_MDI         (0x0000)
#define EMAC_PHY_CONFIG_MDI_X       (0x0010)

#define EMAC_PHY_CONFIG_AN_10_100_FD  (EMAC_PHY_CONFIG_AN100M | EMAC_PHY_CONFIG_AN10M | EMAC_PHY_CONFIG_FULLDUPLEX)
#define EMAC_PHY_CONFIG_AN_10_100_HD  (EMAC_PHY_CONFIG_AN100M | EMAC_PHY_CONFIG_AN10M | EMAC_PHY_CONFIG_HALFDUPLEX)

#define EMAC_PHY_CONFIG_AN_10_FD      (EMAC_PHY_CONFIG_AN10M | EMAC_PHY_CONFIG_FULLDUPLEX)
#define EMAC_PHY_CONFIG_AN_10_HD      (EMAC_PHY_CONFIG_AN10M | EMAC_PHY_CONFIG_HALFDUPLEX)

#define EMAC_PHY_CONFIG_AN_100_FD     (EMAC_PHY_CONFIG_AN100M | EMAC_PHY_CONFIG_FULLDUPLEX)
#define EMAC_PHY_CONFIG_AN_100_HD     (EMAC_PHY_CONFIG_AN100M | EMAC_PHY_CONFIG_HALFDUPLEX)

#define EMAC_PHY_CONFIG_10_FD         (EMAC_PHY_CONFIG_FORCE10M | EMAC_PHY_CONFIG_FULLDUPLEX)
#define EMAC_PHY_CONFIG_10_HD         (EMAC_PHY_CONFIG_FORCE10M | EMAC_PHY_CONFIG_HALFDUPLEX)

#define EMAC_PHY_CONFIG_100_FD        (EMAC_PHY_CONFIG_FORCE100M | EMAC_PHY_CONFIG_FULLDUPLEX)
#define EMAC_PHY_CONFIG_100_HD        (EMAC_PHY_CONFIG_FORCE100M | EMAC_PHY_CONFIG_HALFDUPLEX)

#define EMAC_PHY_CONFIG_DEFAULT       EMAC_PHY_CONFIG_AN_10_100_FD

#endif // STRUCTS_HPP
