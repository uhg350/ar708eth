#include <iostream>
#include <fstream>
#include <ctime>
#include <chrono>

#include "structs.hpp"
#include "udpsocket.hpp"

#include <boost/crc.hpp>

//#include <cppformat/format.h>
//#include <fmt/format.h>

#define FMT_HEADER_ONLY
#include "../fmt/fmt/format.h"
#include "../fmt/fmt/printf.h"

#include "fmt_support.hpp"


typedef struct
{
  le_uint32_t           tag;
  le_uint32_t           lun;
  le_uint16_t           command;
  le_uint16_t           id;
  le_uint32_t           status;
  le_uint32_t           args[4];
} ctrl_block_header_t;

typedef struct
{
  le_uint8_t            data[1024];
} rw_page_t;

#define CTRL_CB_TAG  0x30314243
#define CTRL_SB_TAG  0x30314253

#define CTRL_CMD_GET_IDENT        0x00
#define CTRL_CMD_GET_CONFIG       0x01
#define CTRL_CMD_SET_IDENT        0x02
#define CTRL_CMD_SET_CONFIG       0x03

#define CTRL_CMD_READ_PAGE_CHUNK  0x04
#define CTRL_CMD_WRITE_PAGE_CHUNK 0x05
#define CTRL_CMD_REBOOT           0x06

#define CTRL_CMD_SEND_AR708_FRAME 0x07

class ar708ctrl
{
private:
  ::udp::socket u_;

  typedef struct
  {
    ctrl_block_header_t hdr;
    uint8_t             data[1024];
  } command_block_t;

  typedef struct
  {
    ctrl_block_header_t hdr;
    uint8_t             data[1024];
  } reply_block_t;

  command_block_t   cb_;
  reply_block_t     sb_;

  le_uint16_t       id_;

  le_uint16_t get_transfer_id()
  {
    return ++ id_;
  }


public:
  using status_t    = ::udp::status_t;
  using endpoint_t  = ::udp::endpoint_t;
  using address_t   = ::udp::address_t;

  static const uint32_t ANY_LUN     = 0xFFFFFFFF;
  static const uint32_t CRC32_MAGIC = 0x2144DF1C;

  status_t transact(const endpoint_t& da, size_t  send_size,
                          endpoint_t& ra, size_t& recv_size)
  {
    status_t status;

    do
    {
      size_t total_send_size = send_size + sizeof(cb_.hdr);
      size_t sent;

      cb_.hdr.id = get_transfer_id();

      status = u_.sendto(da, &cb_, total_send_size, &sent);
      if (status != 0)
        break;

      if (sent != total_send_size)
        break;

      recv_size = 0;
      size_t total_received;
      status = u_.recvfrom(ra, &sb_, sizeof(sb_), &total_received);
      if (status != 0)
        break;

      if (total_received < sizeof(sb_.hdr))
      {
        status = -1; // protocol error: size too small
        break;
      }

      if (sb_.hdr.tag != CTRL_SB_TAG)
      {
        status = -1; // protocol error: tag mismatch
        break;
      }

      if (sb_.hdr.command != cb_.hdr.command)
      {
        status = -1; // procol error: command mismatch
        break;
      }

      if (sb_.hdr.id != cb_.hdr.id)
      {
        status = -1; // protocol error: id seq mismatch
        break;
      }

      if (sb_.hdr.status != 0)
      {
        status = a708error_to_status(sb_.hdr.status);
        break;
      }

      recv_size = total_received - sizeof(sb_.hdr);

    } while (false);

    return status;
  }

  ar708ctrl()
    : id_(0)
  {
    cb_.hdr.tag     = CTRL_CB_TAG;
    cb_.hdr.status  = 0x00000000;
  }

  ~ar708ctrl()
  {
  }

  std::vector<ident_t> enum_devices()
  {
    std::vector<ident_t> rv;

    return rv;
  }

  status_t bind(const endpoint_t& baddr)
  {
    status_t status;
    status = u_.bind(baddr);
    return status;
  }

  status_t a708error_to_status(uint32_t error)
  {
    return static_cast<status_t>(error | 0x0000E000);
  }

  template<typename T>
  checksum_t compute_data_block_crc(const data_block_t<T>& d)
  {
    std::size_t size = sizeof(d) - sizeof(d.checksum);
    const void *p = &d;
    boost::crc_32_type crc;
    crc.process_bytes(p, size);
    return crc.checksum();
  }

  template<typename T>
  status_t get_data_block(const endpoint_t& da, le_uint16_t command, data_block_t<T>& data)
  {
    status_t status;

    typedef data_block_t<T> data_t;

    do
    {
      cb_.hdr.command = command;
      cb_.hdr.lun     = ANY_LUN;

      endpoint_t ra;
      size_t recv_size;
      status = transact(da, 0, ra, recv_size);
      if (status != 0)
        break;

      if (recv_size != sizeof(data_t))
      {
        status = -1; // protocol error: received size mismatch
        break;
      }

      data_t *p = reinterpret_cast<data_t *>(sb_.data);
      if (p->header.size != sizeof(data_t))
      {
        status = -1; // error: header size mismatch
        break;
      }

      if (compute_data_block_crc(*p) != p->checksum)
      {
        status = -1; // error: checksum mismatch
        break;
      }


      data = *p;

    } while (false);

    return status;
  }

  template<typename T>
  status_t set_data_block(const endpoint_t& da, le_uint16_t command, const data_block_t<T>& data)
  {
    status_t status;

    typedef data_block_t<T> data_t;

    do
    {
      cb_.hdr.command = command;
      cb_.hdr.lun     = ANY_LUN;

      static_assert(sizeof(data.content) <= sizeof(cb_.data), "Sizeof data.content is too big");

      data_t *p = reinterpret_cast<data_t *>(cb_.data);

      *p = data;
      p->checksum = compute_data_block_crc(*p);

      endpoint_t ra;
      size_t recv_size;
      status = transact(da, sizeof(data_t), ra, recv_size);
      if (status != 0)
        break;

      if (recv_size != 0)
      {
        status = -1; // protocol error: received size mismatch
        break;
      }

    } while (false);

    return status;
  }

  status_t get_ident(const endpoint_t& da, ident_t& ident)
  {
    return get_data_block(da, CTRL_CMD_GET_IDENT, ident);
  }

  status_t set_ident(const endpoint_t& da, const ident_t& ident)
  {
    return  set_data_block(da, CTRL_CMD_SET_IDENT, ident);
  }

  status_t get_config(const endpoint_t& da, config_t& config)
  {
    return get_data_block(da, CTRL_CMD_GET_CONFIG, config);
  }

  status_t set_config(const endpoint_t& da, const config_t& config)
  {
    return set_data_block(da, CTRL_CMD_SET_CONFIG, config);
  }

  status_t send_ar708_frame(const endpoint_t& da, uint32_t line, const void *data, size_t size)
  {
    status_t status;
    do
    {
      if (size > sizeof(cb_.data))
      {
        status = -1; // frame too big
        break;
      }

      std::memcpy(cb_.data, data, size);

      cb_.hdr.command = CTRL_CMD_SEND_AR708_FRAME;
      cb_.hdr.lun     = ANY_LUN;
      cb_.hdr.args[0] = line;

      endpoint_t ra;
      size_t recv_size;
      status = transact(da, size, ra, recv_size);
      if (status != 0)
        break;

      if (recv_size != 0)
      {
        status = -1; // protocol error: received size mismatch
        break;
      }

    } while (false);

    return status;
  }

  status_t read_page_chunk(const endpoint_t& da, uint32_t offset, rw_page_t& page)
  {
    status_t status;
    do
    {
      cb_.hdr.command = CTRL_CMD_READ_PAGE_CHUNK;
      cb_.hdr.lun     = ANY_LUN;
      cb_.hdr.args[0] = offset;

      endpoint_t ra;
      size_t recv_size;
      status = transact(da, 0, ra, recv_size);
      if (status != 0)
        break;

      if (recv_size != sizeof(rw_page_t))
      {
        status = -1; // protocol error: received size mismatch
        break;
      }

      rw_page_t *p = reinterpret_cast<rw_page_t *>(sb_.data);
      page = *p;

    } while (false);

    return status;
  }

  status_t write_page_chunk(const endpoint_t& da, uint32_t offset, const rw_page_t& page)
  {
    status_t status;
    do
    {
      cb_.hdr.command = CTRL_CMD_WRITE_PAGE_CHUNK;
      cb_.hdr.lun     = ANY_LUN;
      cb_.hdr.args[0] = offset;

      std::memcpy(cb_.data, page.data->data(), sizeof(page.data));

      endpoint_t ra;
      size_t recv_size;
      status = transact(da, sizeof(page.data), ra, recv_size);
      if (status != 0)
        break;

      if (recv_size != 0)
      {
        status = -1; // protocol error: received size mismatch
        break;
      }

    } while (false);

    return status;
  }

  status_t reboot(const endpoint_t& da)
  {
    status_t status;
    do
    {
      cb_.hdr.command = CTRL_CMD_REBOOT;
      cb_.hdr.lun     = ANY_LUN;
      //cb_.hdr.args[0] = MAGIC_REBOOT_VALUE;

      endpoint_t ra;
      size_t recv_size;
      status = transact(da, 0, ra, recv_size);
      if (status != 0)
        break;
      if (recv_size != 0)
      {
        status = -1;
        break;
      }

    } while (false);

    return status;
  }
};

void system_to_timestamp(timestamp_t& ts)
{
  auto now = std::chrono::system_clock::now();
  std::time_t nowt = std::chrono::system_clock::to_time_t(now);
  auto tm = std::gmtime(&nowt);

  ts.year    = static_cast<uint16_t>(tm->tm_year) + 1900;
  ts.month   = static_cast<uint8_t>(tm->tm_mon);
  ts.day     = static_cast<uint8_t>(tm->tm_mday);
  ts.hour    = static_cast<uint8_t>(tm->tm_hour);
  ts.minute  = static_cast<uint8_t>(tm->tm_min);
  ts.second  = static_cast<uint8_t>(tm->tm_sec);
  ts.cs = 0;
}

int main(int argc, char *argv[])
{
  (void)(argc); // eleminate 'unused warning'
  (void)(argv); // eleminate 'unused warning'

  using ::udp::socket;
  using ::udp::endpoint_t;
  using ::udp::address_t;
  using ::udp::status_t;

  status_t status;

  endpoint_t sa(address_t::from_string("192.168.1.12"), 0);
  endpoint_t da(address_t::from_string("192.168.1.101"), 4660);

  ar708ctrl ctrl;
  //ctrl.bind(sa);

  size_t errs = 0;
  for (;;)
  {
#if 1
    // get/set ident check
    ident_t ident;
    status = ctrl.get_ident(da, ident);
    if (status != 0)
      break;

    timestamp_t& ts = ident.content.timestamp;
    system_to_timestamp(ts);

    //status = ctrl.set_ident(da, ident);
    if (status != 0)
      break;

    fmt::printf("ident dump:\n");
    //hexdump(&ident, sizeof(ident));
    dump_ident(ident);
    std::cout << std::endl;
#endif

#if 0
    // get/set config check
    config_t config;
    status = ctrl.get_config(da, config);
    if (status != 0)
      break;

    fmt::printf("config dump:\n");
    dump_config(config);
    fmt::printf("\n");

    // modify some fields
    // for example - sink #5 (zero based indexing)
    auto& sink = config.content.sinks[4];

    // unused, use default source address
    memset(&sink.src, 0, sizeof(sink.src));

    // set destination address
    endpoint_t sink_dst_address(address_t::from_string("192.168.1.143"), 4660);
    sink.dst.ip.u32   = ::htonl(sink_dst_address.address().to_v4().to_ulong());
    sink.dst.port.u16 = ::htons(sink_dst_address.port());
    memset(sink.dst.mac.u8, 0, sizeof(sink.dst.mac.u8)); // unused, mac address would be resolved via ARP

    // enable this sink
    sink.flags = SINK_CONFIG_FLAG_ENABLED;

    // configure message filters for this sink

    // zero all fields
    memset(sink.messages, 0, sizeof(sink.messages));

    // capture 055 messages from any line
    sink.messages[0].lines_mask     = 0x03; // message can be captured from line 1 or 2
    sink.messages[0].message_id     = 055;  // message id to be captured
    sink.messages[0].message_offset = 0;    // from begining of message
    sink.messages[0].message_size   = 200;  // till end (max message size is 200 octets)
                                            // if received message is shorter (in case of error)
                                            // content of UDP packet will be truncated
                                            // to real message size
    // the same for 057 message
    sink.messages[1].lines_mask     = 0x03;
    sink.messages[1].message_id     = 057;
    sink.messages[1].message_offset = 0;
    sink.messages[1].message_size   = 200;

    // other message filters have zeroed lines_mask, so no AR708 packets
    // will be captured regardless of other settings
    // that is lines_mask == 0 is effectively disables message filter


    // set last modification timestamp
    auto& ts = config.content.timestamp;
    system_to_timestamp(ts);

    // write config back to the device
    // calculate crc of config and issue SET_CONFIG to device
    status = ctrl.set_config(da, config);
    if (status != 0)
      break;

    fmt::printf("config dump:\n");
    dump_config(config);
    fmt::printf("\n");
#endif

#if 0
    uint8_t frame[200];
    size_t frame_size = 200;
    frame[0] = 055;
    for (size_t i = 1; i < frame_size; ++ i)
      frame[i] = static_cast<uint8_t>(i);

    status = ctrl.send_ar708_frame(da, 0, frame, frame_size);
    if (status != 0 && status != 0x0000E006)
    {
      std::cout << errs << '\r';
      ++ errs;
      //break;
    }
#endif

    rw_page_t pg;

#if 0
    // firmware programming check
    std::cerr << std::endl;
    std::ofstream ofs;
    ofs.open("flash.dump", std::ios::binary | std::ios::trunc);
    for (size_t i = 0x08000; i < 0xC0000; i += 1024)
    {
      status = ctrl.read_page_chunk(da, i, pg);
      if (status != 0)
        break;
      //hexdump(&pg, sizeof(pg), i);
      ofs.write(pg.data->data(), 1024);
    }
    ofs.close();
    if (status != 0)
      break;
#endif

#if 1
    std::ifstream ifs;
    ifs.open("flash.dump", std::ios::binary | std::ios::ate);
    std::size_t dump_size = ifs.tellg();
    ifs.seekg(0, std::ios::beg);
    std::cout << fmt::format("size: {:d}\n", dump_size);
    std::cout.flush();

    auto dump_data = std::make_unique<char[]>(dump_size);
    ifs.read(dump_data.get(), dump_size);
    ifs.close();

    const char *p = dump_data.get();
    for (size_t i = 0x08000; i < 0xC0000; i += 1024)
    {
      memcpy(pg.data, p, sizeof(pg.data));
      status = ctrl.write_page_chunk(da, i, pg);
      if (status != 0)
      {
        break;
      }
      p += sizeof(pg.data);

      size_t pc = size_t( (i*100) / 0xC0000 );
      std::cerr << fmt::format("Programm image: {:3d}%\r", pc);
    }
    if (status != 0)
      break;

    std::cerr << '\n';

    p = dump_data.get();
    for (size_t i = 0x08000; i < 0xC0000; i += 1024)
    {
      status = ctrl.read_page_chunk(da, i, pg);
      if (status != 0)
        break;

      if (memcmp(pg.data, p, sizeof(pg.data)) != 0)
      {
        std::cerr << "Verify failed!\n";
        break;
      }

      size_t pc = size_t( (i*100) / 0xC0000 );
      std::cerr << fmt::format("Verify image: {:3d}%\r", pc);

      p += sizeof(pg.data);
    }
    std::cerr << '\n';

#if 1
    status = ctrl.reboot(da);
    if (status != 0)
      break;
#endif

#endif

    break;
  }

  return 0;
}
