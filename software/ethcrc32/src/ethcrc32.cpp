#include <iostream>
#include <memory>
#include <cstdint>
#include <algorithm>
#include <iterator>
#include <cctype>

#include <boost/crc.hpp>

#define FMT_HEADER_ONLY
#include "fmt/fmt/printf.h"

void hexdump(const void *p, std::size_t size, std::ptrdiff_t offset = 0)
{
  const unsigned char *pu = static_cast<const unsigned char *>(p);
  const std::size_t cols = 16;

  while (size != 0)
  {
    //std::cout << fmt::sprintf("%08X: ", offset);
    fmt::printf("%08X: ", offset);

    std::size_t data_size = std::min(size, cols);
    std::size_t padding_size = cols - data_size;

    std::for_each(pu, pu + data_size, [](unsigned char data){
      //std::cout << fmt::sprintf("%02X ", data);
      fmt::printf("%02X ", data);
    });

    std::fill_n(std::ostream_iterator<char>(std::cout), padding_size * 3, ' ');

    std::for_each(pu, pu + data_size, [](unsigned char data){
      //std::cout << (std::isprint(data) ? static_cast<char>(data) : '.');
      std::putchar(std::isprint(data) ? static_cast<char>(data) : '.');
    });

    std::cout << "\n";

    size -= data_size;
    pu += data_size;
    offset += data_size;
  }
}

int main()
{
  typedef boost::crc_32_type crc_computer_t;
  typedef crc_computer_t::value_type crc_t;
  crc_computer_t  crc_computer;

  const std::size_t crc_size  = sizeof(crc_t);

  const std::size_t packet_size = 60;
  auto packet_data_storage = std::make_unique<unsigned char[]>(packet_size + crc_size);
  auto packet_data = packet_data_storage.get();

  unsigned char n = 0;
  std::generate_n(packet_data, packet_size, [&n]{ return n ++; });

  crc_computer.reset();
  crc_computer.process_bytes(packet_data, packet_size);
  auto packet_crc = crc_computer.checksum();

  std::copy_n(reinterpret_cast<const unsigned char *>(&packet_crc), crc_size, packet_data + packet_size);

  crc_computer.reset();
  crc_computer.process_bytes(packet_data, packet_size + crc_size);
  auto check_crc = crc_computer.checksum();

  std::cout << fmt::sprintf("Packet size + CRC size = %u + %u\n", packet_size, crc_size);
  hexdump(packet_data, packet_size + crc_size);

  std::cout << fmt::sprintf("Packet CRC = 0x%08X, residue CRC = 0x%08X\n", packet_crc, check_crc);

  return 0;
}
