TEMPLATE = app
CONFIG += console c++14 static
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/ethcrc32.cpp

INCLUDEPATH += ../
