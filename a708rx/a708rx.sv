module a708rx
#(
  parameter NAR = 2
)
(
  input  wire             clk,

  input  wire [NAR-1: 0]  HI157X_RX_P,
  input  wire [NAR-1: 0]  HI157X_RX_N,
  output wire [NAR-1: 0]  HI157X_RX_IE,
  
  output wire [NAR-1: 0]  HI157X_TX_P,
  output wire [NAR-1: 0]  HI157X_TX_N,
  output wire [NAR-1: 0]  HI157X_TX_OD
);

  assign HI157X_RX_IE = '1;
  
  assign HI157X_TX_P[1]   = '0;
  assign HI157X_TX_N[1]   = '0;
  assign HI157X_TX_OD[1]  = '1;

  wire areset = '0;
  wire ena    = '1;
/*
  logic cc_rx_n;
  logic cc_rx_p;

  (* altera_attribute = "-name SYNCHRONIZER_TOGGLE_RATE 2000000" *)
  ccreg#(.W(2), .S(3), .R(0))
    ccreg_rx(
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .d        ( { HI157X_RX_N[0], HI157X_RX_P[0] } ),
      
      .q        ( { cc_rx_n, cc_rx_p } )
    );
    
  typedef logic [ 1: 0] rx_state_t;
  rx_state_t st0;
  rx_state_t st1;
  rx_state_t stZ;

  always_comb begin
    st0 = 2'b00;
    st1 = 2'b01;
    stZ = 2'b10;
  end

  rx_state_t c_ls;
  always_comb
    case ( { cc_rx_n, cc_rx_p } )
      2'b01   : c_ls = st1;
      2'b10   : c_ls = st0;
      default : c_ls = stZ;
    endcase

  rx_state_t p_ls;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      p_ls <= stZ;
    else
      p_ls <= c_ls;
  
  logic ev_edge;
  always_comb
    ev_edge = (c_ls != stZ) && (c_ls != p_ls);
  
  localparam DIV_CLKS = 25;
  localparam SMP_CLKS = 3;
  localparam CW = $clog2(DIV_CLKS);
  
  logic ev_sample;
  (* noprune *)
  ecdes_samplepoint#( .CW (CW) )
    smp
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .divide_clks  (CW'(DIV_CLKS - 1)),
      .sample_clks  (CW'(SMP_CLKS - 1)),
      
      .ev_edge      (ev_edge),
      .ev_wrap      (),
      .ev_sample    (ev_sample)
    );
    
  rx_state_t [ 5: 0] rp;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rp <= { 6 { stZ } };
    else if (ena) begin
      if (ev_sample)
        rp <= { rp[ 4: 0], c_ls };
    end

  logic is_sow_sync;
  logic is_eow_sync;

  always_comb begin
    is_sow_sync = (rp == { st1, st1, st1, st0, st0, st0 });
    is_eow_sync = (rp == { st0, st0, st0, st1, st1, st1 });
  end

  enum int unsigned
  {
    stIDLE,
    stSOW,
    stDATA,
    stEOW,
    stERROR
  } state, next_state;
*/
/////
  logic         rx_sof;
  logic [ 7: 0] rx_d;
  logic         rx_dv;
  logic         rx_eof;

  (* noprune *)
  arinc708_rx_frame_u8
    arinc708_rx_frame_i
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .HI157X_RX_P  (HI157X_RX_P[0]),
      .HI157X_RX_N  (HI157X_RX_N[0]),
      
      .sof          (rx_sof),
      .d            (rx_d),
      .dv           (rx_dv),
      .eof          (rx_eof)
    );

////
  logic         tx_send;
  logic         tx_busy;
  logic [ 7: 0] tx_d;
  logic         tx_ack;

  logic [15: 0] fcnt_d;
  logic [15: 0] fcnt_q;
  logic         fcnt_set;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      fcnt_q <= '0;
    else if (ena) begin
      if (fcnt_set)
        fcnt_q <= fcnt_d;
    end

  enum int unsigned
  {
    stSTART,
    stSEND,
    stWAIT,
    stPAUSE
  } state, next_state;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stSTART;
    else if (ena) begin
      state <= next_state;
    end

  always_comb begin
    next_state = state;
    
    fcnt_set = '0;
    fcnt_d   = 'x;
    
    tx_send  = '0;
    tx_d     = fcnt_q[0] ? 8'h81 : 8'h55;
    
    case (state)
    stSTART: begin
      fcnt_set    = '1;
      fcnt_d      = 16'd2;

      next_state  =  stSEND;
    end
    
    stSEND: begin
      tx_send = '1;
      if (tx_ack) begin
        fcnt_set  = '1;
        fcnt_d    = fcnt_q - 1'b1;
        
        if (fcnt_d == '0)
          next_state = stWAIT;
      end
    
    end
    
    stWAIT: begin
      if (!tx_busy) begin
        fcnt_set = '1;
        fcnt_d   = 16'd200;
        next_state = stPAUSE;
      end
    end
    
    stPAUSE: begin
      fcnt_set = '1;
      fcnt_d   = fcnt_q - 1'b1;
      if (fcnt_d == '0)
        next_state = stSTART;
    end

    endcase
  end
  
////
  logic ob_send;
  logic ob_busy;
  logic ob_d;
  logic ob_ack;

  arinc708_tx_serialize_u8
    arinc708_tx_serialize_u8_i
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .send         (tx_send),
      .busy         (tx_busy),
      .d            (tx_d),
      .ack          (tx_ack),
      
      .tx_send      (ob_send),
      .tx_busy      (ob_busy),
      .tx_d         (ob_d),
      .tx_ack       (ob_ack)
    );

  arinc708_tx_frame#(
    .CLKFREQ    (50_000_000),
    .BAUDRATE   (1_000_000)
  )
    arinc708_tx_frame_i
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .send         (ob_send),
      .busy         (ob_busy),
      .d            (ob_d),
      .ack          (ob_ack),
      
      .HI157X_TX_P  (HI157X_TX_P[0]),
      .HI157X_TX_N  (HI157X_TX_N[0]),
      .HI157X_TX_OD (HI157X_TX_OD[0])
    );

endmodule
