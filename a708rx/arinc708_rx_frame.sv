
module arinc708_rx_frame_u8
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         HI157X_RX_P,
  input  wire         HI157X_RX_N,
  
  output wire         sof,
  output wire [ 7: 0] d,
  output wire         dv,
  output wire         eof
);
  
  logic rx_sof;
  logic rx_eof;
  logic rx_d;
  logic rx_dv;
  
  arinc708_rx_frame
    arinc708_rx_frame_i
    (
      .areset           (areset),
      .clk              (clk),
      .ena              (ena),
      
      .HI157X_RX_P      (HI157X_RX_P),
      .HI157X_RX_N      (HI157X_RX_N),
      
      .sof              (rx_sof),
      .d                (rx_d),
      .dv               (rx_dv),
      .eof              (rx_eof)
    );
  
  enum int unsigned
  {
    stIDLE,
    stSOF,
    stBIT_P,
    stBIT_N,
    stDV,
    stEOF
  } state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      state <= stIDLE;
    end
    else if (ena) begin
      state <= next_state;
    end
  
  logic bp_q;
  logic bp_d;
  logic bp_set;
  
  logic [ 2: 0] bc_q;
  logic [ 2: 0] bc_d;
  logic         bc_set;
  
  logic [ 7: 0] sh_q;
  logic [ 7: 0] sh_d;
  logic         sh_set;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      bp_q <= '0;
      bc_q <= '0;
      sh_q <= '0;
    end
    else if (ena) begin
      if (bp_set)
        bp_q <= bp_d;
        
      if (bc_set)
        bc_q <= bc_d;
      
      if (sh_set)
        sh_q <= sh_d;
    end

  always_comb begin
    next_state = state;
    
    bp_set  = '0;
    bp_d    = 'x;
    
    bc_set  = '0;
    bc_d    = 'x;
    
    sh_set  = '0;
    sh_d    = 'x;

    case (state)
    
    stIDLE: begin
      if (rx_dv) begin
        if (rx_sof)
          next_state = stSOF;
      end
    end
    
    stSOF: begin
      next_state = stBIT_P;
      
      bc_set  = '1;
      bc_d    = '0;
    end
    
    stBIT_P: begin
      if (rx_dv) begin
        next_state = stBIT_N;
        
        bp_set = '1;
        bp_d   = rx_d;
      end
    end
    
    stBIT_N: begin
      if (rx_dv) begin
      
        if (rx_eof || ( (bp_q ^ rx_d) == '0 ) ) begin
          next_state = stEOF;
        end
        else begin
          bc_set  = '1;
          bc_d    = bc_q + 1'b1;
          
          sh_set  = '1;
          sh_d    = { sh_q[ 6: 0], bp_q };
          
          if (bc_d == '0)
            next_state = stDV;
          else
            next_state = stBIT_P;
        end
      end
    end
    
    stDV: begin
      next_state = stBIT_P;
    end
      
    stEOF: begin
      next_state = stIDLE;
    end

    endcase
  end
  
  assign sof = (state == stSOF);
  assign eof = (state == stEOF);
  assign dv  = (state == stDV);
  assign d   = sh_q;

endmodule

module arinc708_rx_frame
(
  input  wire     areset,
  input  wire     clk,
  input  wire     ena,
  
  input  wire     HI157X_RX_P,
  input  wire     HI157X_RX_N,
  
  output wire     sof,
  output wire     d,
  output wire     dv,
  output wire     eof
);

  logic cc_rx_n;
  logic cc_rx_p;

  (* altera_attribute = "-name SYNCHRONIZER_TOGGLE_RATE 2000000" *)
  ccreg#(.W(2), .S(3), .R(0))
    ccreg_rx(
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .d        ( { HI157X_RX_N, HI157X_RX_P } ),
      
      .q        ( { cc_rx_n, cc_rx_p } )
    );

  // lost information about idle states, but why we need one?
  logic [ 1: 0] rx_d;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rx_d <= '0;
    else if (ena) begin
      if (cc_rx_p ^ cc_rx_n) begin
        rx_d[1] <= rx_d[0];
        rx_d[0] <= cc_rx_p;
      end
    end
  //
  localparam DIV_CLKS = 25;
  localparam SMP_CLKS = 13;
  localparam CW = $clog2(DIV_CLKS);
  
  
  logic ev_edge;
  always_comb
    ev_edge = ^rx_d;

  logic ev_sample;
  ecdes_samplepoint#( .CW (CW) )
    smp
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .divide_clks  (CW'(DIV_CLKS - 1)),
      .sample_clks  (CW'(SMP_CLKS - 1)),
      
      .ev_edge      (ev_edge),
      .ev_wrap      (),
      .ev_sample    (ev_sample)
    );
    
    
  //
  localparam RW = 10;
  logic [RW-1: 0] rp;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rp <= '0;
    else if (ena) begin
      if (ev_sample)
        rp <= { rp[RW-2: 0], rx_d[0] };
    end
    
  assign d = rp[RW-6];
  assign sof  = (rp[RW-1:RW-6] == 8'b111000) && (^rp[RW-7:RW-8]);
  assign eof  = (rp[ 5: 0] == 8'b000111) && (^rp[ 7: 6]);
  assign dv   = ev_sample;

endmodule
