module ecdes_samplepoint
#(
  parameter CW = 10
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  
  input  wire [CW-1: 0] divide_clks,
  input  wire [CW-1: 0] sample_clks,
  
  input  wire           ev_edge,
  output wire           ev_wrap,
  output wire           ev_sample
);

  logic [CW-1: 0] cdiv;
  
  logic wrap;
  logic sample;
  
  always_comb begin
    wrap    = (cdiv == divide_clks);
    sample  = (cdiv == sample_clks);
  end
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cdiv <= '0;
    else if (ena) begin
      if (wrap || ev_edge)
        cdiv <= '0;
      else
        cdiv <= cdiv + 1'b1;
    end

  assign ev_wrap   = wrap;
  assign ev_sample = sample;

endmodule
