create_clock -name {clk} -period "50.0MHz" [get_ports {clk}]

derive_clock_uncertainty
derive_pll_clocks

set_max_skew -from [get_ports {HI157X_RX_*[0]}] 1.0
set_max_skew -to   [get_ports {HI157X_TX_*[0]}] 1.0
