module arinc708_tx_dibit
#(
  parameter CLKFREQ   = 50_000_000,
  parameter BAUDRATE  =  1_000_000
)
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  output wire         ack,
  input  wire [ 1: 0] d,
  input  wire         oe,

  output wire         HI157X_TX_P,
  output wire         HI157X_TX_N,
  output wire         HI157X_TX_OD
);
  localparam DIV = CLKFREQ / BAUDRATE;
  localparam CW  = $clog2(DIV);
  
  logic [CW-1: 0] cd;
  logic sp_half_t;
  logic sp_full_t;
  
  always_comb begin
    sp_half_t = (cd == (DIV/2 - 1));
    sp_full_t = (cd == (DIV   - 1));
  end

  always_ff@(posedge areset or posedge clk)
    if (areset)
      cd <= '0;
    else if (ena) begin
      if (sp_full_t)
        cd <= '0;
      else
        cd <= cd + 1'b1;
    end
  
  logic [ 1: 0] s_d;
  logic         s_oe;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      s_d   <= '0;
      s_oe  <= '0;
    end
    else if (ena) begin
      if      (sp_full_t) begin
        s_d   <= d;
        s_oe  <= oe;
      end
      else if (sp_half_t) begin
        s_d   <= s_d << 1;
      end
    end
  
  logic [ 1: 0] s_dib;
  always_comb
    s_dib = { ~s_d[1], s_d[1] } & { s_oe, s_oe };

  hi157x_tx_buffer
    tx_io
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .oe           (s_oe),
      .d            (s_dib),
      
      .HI157X_TX_P  (HI157X_TX_P),
      .HI157X_TX_N  (HI157X_TX_N),
      .HI157X_TX_OD (HI157X_TX_OD)
    );

  assign ack = sp_full_t;

endmodule
