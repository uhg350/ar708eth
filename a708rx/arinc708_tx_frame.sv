module arinc708_tx_serialize_u8
#(
  parameter W = 8
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  
  input  wire           send,
  output wire           busy,
  input  wire [W-1: 0]  d,
  output wire           ack,
  
  output wire           tx_send,
  input  wire           tx_busy,
  output wire           tx_d,
  input  wire           tx_ack
);
  
  localparam CW = $clog2(W);
  logic [CW-1: 0]   bc;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      bc <= '0;
    else if (ena) begin
      if (!tx_busy) begin
        if (send)
          bc <= '0;
      end
      else begin 
        if (tx_ack)
          bc <= bc + 1'b1;
      end
    end
    
  assign tx_send = send || (tx_ack && (bc != '0));
  assign busy    = tx_busy;
  assign tx_d    = d[W - 1 - bc];
  assign ack     = tx_ack && (bc == (W-1));

endmodule


module arinc708_tx_frame
#(
  parameter CLKFREQ   = 50_000_000,
  parameter BAUDRATE  =  1_000_000
)
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         send,
  output wire         busy,
  input  wire         d,
  output wire         ack,

  output wire         HI157X_TX_P,
  output wire         HI157X_TX_N,
  output wire         HI157X_TX_OD
);

  logic         n_ack;
  logic [ 1: 0] n_d;
  logic         n_oe;

  arinc708_tx_dibit#(
    .CLKFREQ  (CLKFREQ),
    .BAUDRATE (BAUDRATE)
  )
    dibit_io
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),

      .ack          (n_ack),
      .d            (n_d),
      .oe           (n_oe),

      .HI157X_TX_P  (HI157X_TX_P),
      .HI157X_TX_N  (HI157X_TX_N),
      .HI157X_TX_OD (HI157X_TX_OD)
    );

  enum int unsigned 
  {
    stIDLE,
    stSOW0,
    stSOW1,
    stSOW2,
    stFRAME,
    stEOW0,
    stEOW1,
    stEOW2,
    stIFG0,
    stIFG1,
    stIFG2
  } state, next_state;
    
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else if (ena) begin
      state <= next_state;
    end

  always_comb begin
    next_state = state;

    n_oe = '0;
    n_d  = 'x;
  
    case (state)
    stIDLE: begin
      if (send) begin
        if (n_ack) begin
          n_oe = '1;
          n_d  = 2'b11;
          next_state = stSOW1;
        end
        else
          next_state = stSOW0;
      end
    end
    
    stSOW0: begin
      n_oe = '1;
      n_d  = 2'b11;
      if (n_ack)
        next_state = stSOW1;
    end
    
    stSOW1: begin
      n_oe = '1;
      n_d  = 2'b10;
      if (n_ack)
        next_state = stSOW2;
    end
    
    stSOW2: begin
      n_oe = '1;
      n_d  = 2'b00;
      if (n_ack)
        next_state = stFRAME;
    end
    
    stFRAME: begin
      n_oe = '1;
      n_d  = { d, ~d };
      if (n_ack) begin
        if (!send) begin
          n_d = 2'b00;
          next_state = stEOW1;
        end
      end
    end
    
    stEOW0: begin
      n_oe = '1;
      n_d  = 2'b00;
      if (n_ack)
        next_state = stEOW1;
    end
    
    stEOW1: begin
      n_oe = '1;
      n_d  = 2'b01;
      if (n_ack)
        next_state = stEOW2;
    end
    
    stEOW2: begin
      n_oe = '1;
      n_d  = 2'b11;
      if (n_ack)
        next_state = stIDLE;
    end
    
    stIFG0: begin
      if (n_ack)
        next_state = stIFG1;
    end
    
    stIFG1: begin
      if (n_ack)
        next_state = stIFG2;
    end
    
    stIFG2: begin
      if (n_ack)
        next_state = stIDLE;
    end
    
    endcase
  end
  
  assign ack  = (state == stFRAME) && n_ack;
  assign busy = (state != stIDLE);

endmodule
