module hi157x_tx_buffer
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         oe,
  input  wire [ 1: 0] d,
  
  output wire         HI157X_TX_P,
  output wire         HI157X_TX_N,
  output wire         HI157X_TX_OD
);

  logic tx_p;
  logic tx_n;
  logic tx_oe;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      tx_p  <= '0;
      tx_n  <= '0;
      tx_oe <= '0;
    end
    else if (ena) begin
      tx_p  <= d[0];
      tx_n  <= d[1];
      tx_oe <= oe;
    end
    
  assign HI157X_TX_P  =  tx_p;
  assign HI157X_TX_N  =  tx_n;
  assign HI157X_TX_OD = ~tx_oe;

endmodule
