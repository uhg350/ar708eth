module nios_ar708_mac_mmap_impl
#(
  parameter NAR = 2,
  parameter AW  = 10
)
(
  input  wire             areset,
  input  wire             clk,
      
  input  wire             rxm_clken,
  input  wire [ 9: 0]     rxm_address,
  input  wire [ 3: 0]     rxm_byteenable,
  input  wire             rxm_read,
  output wire [31: 0]     rxm_readdata,
  input  wire             rxm_write,
  input  wire [31: 0]     rxm_writedata,

  input  wire             txm_clken,
  input  wire [ 8: 0]     txm_address,
  input  wire [ 3: 0]     txm_byteenable,
  input  wire             txm_read,
  output wire [31: 0]     txm_readdata,
  input  wire             txm_write,
  input  wire [31: 0]     txm_writedata,
  
  input  wire [ 3: 0]     ctl_address,
  input  wire [ 3: 0]     ctl_byteenable,
  input  wire             ctl_write,
  input  wire [31: 0]     ctl_writedata,
  input  wire             ctl_read,
  output wire [31: 0]     ctl_readdata,
  output wire             ctl_readdatavalid,
  output wire             ctl_waitrequest,

  
  input  wire [NAR-1: 0]  HI157X_RX_P,
  input  wire [NAR-1: 0]  HI157X_RX_N,
  output wire [NAR-1: 0]  HI157X_RX_IE,
  output wire [NAR-1: 0]  HI157X_RX_LED,

  output wire [NAR-1: 0]  HI157X_TX_P,
  output wire [NAR-1: 0]  HI157X_TX_N,
  output wire [NAR-1: 0]  HI157X_TX_OD
);

  wire ena = '1;

  logic [ 7: 0] r_config;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_config <= '0;
    else if (ena) begin
      if (ctl_address == 4'h0 && ctl_write && ctl_byteenable == 4'b1111)
        r_config <= ctl_writedata[ 7: 0];
    end


  logic [ 1: 0]         sr_access;
  logic        [ 8: 0]  sr_addr;
  logic [ 1: 0]         sr_write;
  logic [ 1: 0]         sr_read;
  logic [ 1: 0][31: 0]  sr_readdata;
    
  logic           sel_readdata;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      sel_readdata <= '0;
    else if (rxm_clken) begin
      sel_readdata <= rxm_address[9];
    end
  
  always_comb begin
    sr_access[0]  = ~rxm_address[9];
    sr_access[1]  =  rxm_address[9];

    sr_addr       = rxm_address[ 8: 0];
    
    for (int i = 0; i < 2; ++ i) begin
      sr_write[i] = rxm_write & sr_access[i];
      sr_read[i]  = rxm_read  & sr_access[i];
    end
    
    rxm_readdata  = sel_readdata ? sr_readdata[1] : sr_readdata[0];
  end
  
  logic [ 1: 0] rx_active;

  arinc708_rx_frame_mmap
    rx_mmap_0
    (
      .mmap_rx_areset     (areset),
      .mmap_rx_clk        (clk),
      .mmap_rx_clken      (rxm_clken),
      .mmap_rx_address    (sr_addr),
      .mmap_rx_byteenable (rxm_byteenable),
      .mmap_rx_read       (sr_read[0]),
      .mmap_rx_readdata   (sr_readdata[0]),
      .mmap_rx_write      (sr_write[0]),
      .mmap_rx_writedata  (rxm_writedata),
      
      .HI157X_RX_P        (HI157X_RX_P[0]),
      .HI157X_RX_N        (HI157X_RX_N[0]),
      
      .active             (rx_active[0])
    );
    
    rx_activity_led
    activity_led_0
    (
      .areset           (areset),
      .clk              (clk),
      
      .activity         (rx_active[0]),
      
      .led              (HI157X_RX_LED[0])
    );

  arinc708_rx_frame_mmap
    rx_mmap_1
    (
      .mmap_rx_areset     (areset),
      .mmap_rx_clk        (clk),
      .mmap_rx_clken      (rxm_clken),
      .mmap_rx_address    (sr_addr),
      .mmap_rx_byteenable (rxm_byteenable),
      .mmap_rx_read       (sr_read[1]),
      .mmap_rx_readdata   (sr_readdata[1]),
      .mmap_rx_write      (sr_write[1]),
      .mmap_rx_writedata  (rxm_writedata),
      
      .HI157X_RX_P        (HI157X_RX_P[1]),
      .HI157X_RX_N        (HI157X_RX_N[1]),
      
      .active             (rx_active[1])
    );

  rx_activity_led
    activity_led_1
    (
      .areset           (areset),
      .clk              (clk),
      
      .activity         (rx_active[1]),
      
      .led              (HI157X_RX_LED[1])
    );

    ////
  logic         tx_start;
  logic [ 1: 0] tx_line;
  logic [10: 0] tx_size;
  logic [10: 0] tx_offset;
  logic         tx_busy;
  logic [ 1: 0] tx_active;
  
  always_comb begin
    tx_start  = (ctl_address == 4'h8) && ctl_write && (ctl_byteenable == 4'b1111);
    tx_line   = 2'b11;
    tx_size   = ctl_writedata[10: 0];
    tx_offset = '0;
  end
    
  arinc708_tx_frame_mmap
    tx_mmap
    (
      .areset             (areset),
      .clk                (clk),
      .ena                (ena),
      
      .start              (tx_start),
      .line               (tx_line),
      .size               (tx_size),
      .offset             (tx_offset),
      .busy               (tx_busy),
      .active             (tx_active),

      .mmap_tx_areset     (areset),
      .mmap_tx_clk        (clk),
      .mmap_tx_clken      (txm_clken),
      .mmap_tx_address    (txm_address),
      .mmap_tx_byteenable (txm_byteenable),
      .mmap_tx_read       (txm_read),
      .mmap_tx_readdata   (txm_readdata),
      .mmap_tx_write      (txm_write),
      .mmap_tx_writedata  (txm_writedata),
      
      .HI157X_TX_P        (HI157X_TX_P),
      .HI157X_TX_N        (HI157X_TX_N),
      .HI157X_TX_OD       (HI157X_TX_OD)
    );

  assign ctl_waitrequest = '0;
  assign ctl_readdatavalid = ctl_read;
  assign ctl_readdata = tx_busy;

  assign HI157X_RX_IE = '1;
  
  /*
  assign HI157X_TX_P  = 'x;
  assign HI157X_TX_N  = 'x;
  assign HI157X_TX_OD = '1;
  */

endmodule

module rx_activity_led
#(
  parameter BLINK_MS = 40
)
(
  input  wire       areset,
  input  wire       clk,
  
//  input  wire       ena_1ms,
  
  input  wire       activity,
  
  output wire       led
);

  logic ena_1ms;
 
  logic [15: 0] msc;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      msc <= '0;
    else begin
      msc <= msc + 1'b1;
    end

  always_comb
    ena_1ms = (msc == '0);
    
  localparam CW = $clog2(BLINK_MS);

  logic bc_clr;
  logic bc_inc;
  logic [CW-1: 0] bc;
  logic bc_done;

  always_comb
    bc_done = (bc == (BLINK_MS - 1));
    
  always_ff@(posedge areset or posedge clk)
    if (areset)
      bc <= '0;
    else begin
      if (bc_clr)
        bc <= '0;
      else
        bc <= bc + bc_inc;
    end

  logic led_set;
  logic led_q;
  logic led_d;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      led_q <= '0;
    else
      if (led_set)
        led_q <= led_d;
        
  assign led = ~led_q;
  
  enum int unsigned 
  {
    stIDLE,
    stLED_ON,
    stLED_OFF
  } state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else
      state <= next_state;
  
  always_comb begin
    next_state = state;
    
    led_set = '0;
    led_d   = 'x;
    
    bc_clr  = '0;
    bc_inc  = '0;
    
    case (state)
    stIDLE: begin
      if (activity) begin
        led_set = '1;
        led_d   = '1;
        
        bc_clr = '1;
        
        next_state = stLED_ON;
      end
    end
    
    stLED_ON: begin
      bc_inc = ena_1ms;
      
      if (bc_done) begin
        led_set = '1;
        led_d   = '0;
        
        bc_clr  = '1;
        next_state = stLED_OFF;
      end
    end
    
    stLED_OFF: begin
      bc_inc = ena_1ms;
      if (bc_done)
        next_state = stIDLE;
    end
    
    endcase
  end
  
endmodule