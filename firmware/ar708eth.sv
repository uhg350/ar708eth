`default_nettype none

`define ENABLE_PCI_BUS
`define ENABLE_ETHERNET_100M

module ar708eth
#(
  parameter NAR = 2
)
(
//  input  wire             clk50000kHz,

// ARINC-708 after HI-1573 converter
  input  wire [NAR-1: 0]  HI157X_RX_P,
  input  wire [NAR-1: 0]  HI157X_RX_N,
  output wire [NAR-1: 0]  HI157X_RX_IE,

  output wire [NAR-1: 0]  HI157X_TX_P,
  output wire [NAR-1: 0]  HI157X_TX_N,
  output wire [NAR-1: 0]  HI157X_TX_OD

`ifdef ENABLE_ETHERNET_100M
// TI DP83849i Ethernet 100 Mbit RMII interface
  ,
  output wire             E100_RESET_N,
  input  wire             E100_CLK,
    
  input  wire             E100_RX_DV,
//  input  wire             E100_RX_ER,
  input  wire             E100_CRS_DV,
  input  wire [ 1: 0]     E100_RX_D,
  output wire             E100_TX_EN,
//  output wire             E100_TX_ER,
  output wire [ 1: 0]     E100_TX_D,
    
//  inout  wire             E100_INT_N,
  output wire             E100_MDC,
  inout  wire             E100_MDIO
`endif

  ,
  // Reset To Defaults jumper
  input  wire             RTD_JUMPER,

  // activity leds
  output wire [ 1: 0]     HI157X_TRX_LEDS

// PCI local bus pins
`ifdef ENABLE_PCI_BUS
  ,
  input  wire             PCI_RSTN,
  input  wire             PCI_CLK,

  inout  wire [31: 0]     PCI_AD,
  input  wire [ 3: 0]     PCI_CBEN,
  inout  wire             PCI_PAR,

  input  wire             PCI_IDSEL,
  input  wire             PCI_FRAMEN,
  input  wire             PCI_IRDYN,
  
  inout  wire             PCI_TRDYN, // output wire             PCI_TRDYN,
  inout  wire             PCI_DEVSELN, // output wire             PCI_DEVSELN,
  inout  wire             PCI_STOPN, // output wire             PCI_STOPN,
  
  inout  wire             PCI_INTAN,
  
  inout  wire             PCI_PERRN, // output wire             PCI_PERRN,
  inout  wire             PCI_SERRN
`endif
);

  // Force PCI to Z-state
  assign PCI_AD       = 'z;
  assign PCI_PAR      = 'z;
  assign PCI_TRDYN    = 'z;
  assign PCI_DEVSELN  = 'z;
  assign PCI_STOPN    = 'z;
  assign PCI_INTAN    = 'z;
  assign PCI_PERRN    = 'z;
  assign PCI_SERRN    = 'z;
  
  //assign HI157X_RX_IE = '0;
  //assign HI157X_TX_P  = '0;
  //assign HI157X_TX_N  = '0;
  //assign HI157X_TX_OD = '1;
  
  //assign E100_MDC     = '1;
  //assign E100_MDIO    = 'z;
  
  //assign TRX_LEDS     = '1;

////
  wire clk    = E100_CLK;
  wire areset = '0;
  
  logic         eic_valid;
  logic [44: 0] eic_data;
  always_comb begin
    eic_valid = '0;
    eic_data  = '0;
  end

  wire  smi_mdc;
  wire  smi_mdio_d;
  wire  smi_mdio_oe;
  wire  smi_mdio_q;
  
  mcpu
    mcpu_i0
    (
      .clk_clk        (clk),
      .reset_reset_n  (!areset),

      .eic_valid      (eic_valid),
      .eic_data       (eic_data),
      
      .rmii_reset_n   (E100_RESET_N),
      .rmii_clk       (E100_CLK),
      .rmii_rx_dv     (E100_RX_DV),
      .rmii_rx_er     (1'b0),
      .rmii_crs_dv    (E100_CRS_DV),
      .rmii_rx_d      (E100_RX_D),
      .rmii_tx_en     (E100_TX_EN),
      .rmii_tx_er     (),
      .rmii_tx_d      (E100_TX_D),
      
      .smi_mdc        (smi_mdc),
      .smi_mdio_d     (smi_mdio_d),
      .smi_mdio_oe    (smi_mdio_oe),
      .smi_mdio_q     (smi_mdio_q),
      
      .hi157x_rx_p    (HI157X_RX_P),
      .hi157x_rx_n    (HI157X_RX_N),
      .hi157x_rx_ie   (HI157X_RX_IE),
      .hi157x_rx_led  (HI157X_TRX_LEDS),
      
      .hi157x_tx_p    (HI157X_TX_P),
      .hi157x_tx_n    (HI157X_TX_N),
      .hi157x_tx_od   (HI157X_TX_OD),
      
      .rpins_ipins    (RTD_JUMPER)
    );

  assign E100_MDC   = smi_mdc;
  assign E100_MDIO  = smi_mdio_oe ? smi_mdio_q : 1'bz;
  assign smi_mdio_d = E100_MDIO;

/*  
  //assign E100_TX_EN = '0;
  //assign E100_TX_D  = '0;

  logic [20: 0] rrs = 0;
  always_ff@(posedge clk)
    rrs <= rrs + !rrs[20];
    
  assign E100_RESET_N = rrs[20];
*/
    
/*
  wire rmii_areset;
  wire rmii_mode;
  wire rmii_rx_active;
  
  assign rmii_areset = '0;
  assign rmii_mode   = '0;
  assign rmii_rx_active = '1;

  wire          gmii_areset;
  wire          gmii_clk;
  wire          gmii_ena;
  wire          gmii_rx_dv;
  wire          gmii_rx_er;
  wire [ 7: 0]  gmii_rx_d;
  wire          gmii_tx_en;
  wire          gmii_tx_er;
  wire [ 7: 0]  gmii_tx_d;
  
  rmii_to_gmii
    rmii_to_gmii_i0
    (
      .rmii_areset    (rmii_areset),
      .rmii_clk       (E100_CLK),
      .rmii_mode      (rmii_mode),
      .rmii_rx_active (rmii_rx_active),
      .rmii_rx_dv     (E100_RX_DV),
      .rmii_crs_dv    (E100_CRS_DV),
      .rmii_rx_er     (1'b0),
      .rmii_rx_d      (E100_RX_D),
      .rmii_tx_en     (),
      .rmii_tx_er     (),
      .rmii_tx_d      (),

      .gmii_areset    (gmii_areset),
      .gmii_clk       (gmii_clk),
      .gmii_acq       (gmii_ena),
      .gmii_rx_dv     (gmii_rx_dv),
      .gmii_rx_er     (gmii_rx_er),
      .gmii_rx_d      (gmii_rx_d),
      .gmii_tx_en     (gmii_tx_en),
      .gmii_tx_er     (gmii_tx_er),
      .gmii_tx_d      (gmii_tx_d)
    );

  assign E100_RESET_N = !rmii_areset;

  assign E100_MDC     = '1;
  assign E100_MDIO    = 'z;

  assign gmii_tx_en = gmii_rx_dv;
  assign gmii_tx_d  = gmii_rx_d;


  mac_rx_states::type_t  rx_state;
  mac_rx_eoferrs::type_t rx_eoferr;

  (* noprune *)
  gmii_to_mac_rx
    gmii_to_mac_rx_i0
    (
      .gmii_rx_areset (gmii_areset),
      .gmii_rx_clk    (gmii_clk),
      .gmii_rx_ena    (gmii_ena),
      .gmii_rx_dv     (gmii_rx_dv),
      .gmii_rx_er     (gmii_rx_er),
      .gmii_rx_d      (gmii_rx_d),

      .mac_rx_areset  (),
      .mac_rx_clk     (),
      .mac_rx_ena     (),
      .mac_rx_drop    (1'b0),
      .mac_rx_state   (rx_state),
      .mac_rx_eoferr  (rx_eoferr),
      .mac_rx_d       ()
    );

  
  
  logic [ 1: 0] r_d;
  logic         r_dv;
  always_ff@(posedge E100_CLK) begin
    r_dv <= E100_RX_DV;
    r_d  <= E100_RX_D;
  end
  
  assign E100_TX_EN = r_dv;
  assign E100_TX_D  = r_d;
//
  wire areset = '0;
  wire clk    = E100_CLK;

  (* keep, noprune *)
  logic [31: 0] pcount;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      pcount <= '0;
    end
    else if (gmii_ena) begin
      if (rx_state == mac_rx_states::EFD && rx_eoferr == mac_rx_eoferrs::SUCCESS)
        pcount <= pcount + 1'b1;
    end
*/


endmodule
