module nios_cis_bswap_impl
(
  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  assign result = { dataa[23:16], dataa[31:24], dataa[ 7: 0], dataa[15: 8] } |
                  { datab[ 7: 0], datab[15: 8], datab[23:16], datab[31:24] };

endmodule
