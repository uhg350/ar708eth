module nios_cis_bswap
(
  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  nios_cis_bswap_impl
    nios_cis_bswap_i
    (
      .dataa  (dataa),
      .datab  (datab),
      .result (result)
    );

endmodule
