module arinc708_tx_frame_mmap
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         start,
  input  wire [ 1: 0] line,
  input  wire [10: 0] size,
  input  wire [10: 0] offset,
  output wire         busy,
  output wire [ 1: 0] active,

  input  wire         mmap_tx_areset,
  input  wire         mmap_tx_clk,
  input  wire         mmap_tx_clken,
  input  wire [ 8: 0] mmap_tx_address,
  input  wire [ 3: 0] mmap_tx_byteenable,
  input  wire         mmap_tx_read,
  output wire [31: 0] mmap_tx_readdata,
  input  wire         mmap_tx_write,
  input  wire [31: 0] mmap_tx_writedata,
  
  output wire [ 1: 0] HI157X_TX_P,
  output wire [ 1: 0] HI157X_TX_N,
  output wire [ 1: 0] HI157X_TX_OD
);

  localparam AW = 9;

  logic [AW-1: 0]   mem_addr;
  logic             mem_read;
  logic [31: 0]     mem_q;
  
  dcram_rw32_rw32#(
    .SIZE       (2**AW),
    .AW         (AW)
  )
    tx_ram
    (
      .mem_ws_areset  (mmap_tx_areset),
      .mem_ws_clk     (mmap_tx_clk),
      .mem_ws_ena     (mmap_tx_clken),
      .mem_ws_addr    (mmap_tx_address),
      .mem_ws_be      (mmap_tx_byteenable),
      .mem_ws_d       (mmap_tx_writedata),
      .mem_ws_write   (mmap_tx_write),
      .mem_ws_read    (mmap_tx_read),
      .mem_ws_q       (mmap_tx_readdata),

      .mem_rs_areset  (areset),
      .mem_rs_clk     (clk),
      .mem_rs_ena     (1'b1),
      .mem_rs_addr    (mem_addr),
      .mem_rs_be      (4'b1111),
      .mem_rs_d       (32'bx),
      .mem_rs_write   (1'b0),
      .mem_rs_read    (mem_read),
      .mem_rs_q       (mem_q)
    );
  
  //
  logic [ 1: 0]         tx_oe;
  logic [ 1: 0][ 1: 0]  tx_d;
  hi157x_tx_buffer
    line_0
    (
      .areset         (areset),
      .clk            (clk),
      .ena            (ena),
      
      .oe             (tx_oe[0]),
      .d              (tx_d[0]),
      
      .HI157X_TX_P    (HI157X_TX_P[0]),
      .HI157X_TX_N    (HI157X_TX_N[0]),
      .HI157X_TX_OD   (HI157X_TX_OD[0])
    );
    
  hi157x_tx_buffer
    line_1
    (
      .areset         (areset),
      .clk            (clk),
      .ena            (ena),
      
      .oe             (tx_oe[1]),
      .d              (tx_d[1]),
      
      .HI157X_TX_P    (HI157X_TX_P[1]),
      .HI157X_TX_N    (HI157X_TX_N[1]),
      .HI157X_TX_OD   (HI157X_TX_OD[1])
    );
  //
  
  enum int unsigned
  {
    stWAIT_FOR_START,
    stTRANSFER,
    stWAIT_FOR_DONE
  } state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stWAIT_FOR_START;
    else if (ena) begin
      state <= next_state;
    end
  
  typedef logic [10: 0] addr_t;
  typedef logic [10: 0] size_t;
  typedef logic [ 1: 0] line_t;
  typedef logic [ 1: 0] rsel_t;
  
  addr_t  coffset_d;
  addr_t  coffset_q;
  logic   coffset_set;

  size_t  csize_d;
  size_t  csize_q;
  logic   csize_set;
  
  line_t  cline_d;
  line_t  cline_q;
  logic   cline_set;
  
  rsel_t  rsel_d;
  rsel_t  rsel_q;
  logic   rsel_set;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      coffset_q <= '0;
      csize_q   <= '0;
      cline_q   <= '0;
    end
    else if (ena) begin
      if (coffset_set)
        coffset_q <= coffset_d;
      
      if (csize_set)
        csize_q <= csize_d;
        
      if (cline_set)
        cline_q <= cline_d;
        
      if (rsel_set)
        rsel_q <= rsel_d;
    end

  logic         ser_send;
  logic         ser_ack;
  logic [ 7: 0] ser_d;
  logic         ser_busy;
  
  always_comb begin
    unique case (rsel_q)
      2'b00 : ser_d = mem_q[ 7: 0];
      2'b01 : ser_d = mem_q[15: 8];
      2'b10 : ser_d = mem_q[23:16];
      2'b11 : ser_d = mem_q[31:24];
    endcase
  end

  always_comb begin
    next_state = state;
    
    coffset_set = '0;
    coffset_d   = 'x;
    
    csize_set   = '0;
    csize_d     = 'x;
    
    cline_set   = '0;
    cline_d     = 'x;
    
    rsel_set    = '0;
    rsel_d      = '0;
    
    mem_read    = '0;
    mem_addr    = 'x;
    
    ser_send    = '0;
    
    case (state)
    
    stWAIT_FOR_START: begin
      if (start) begin
        coffset_set = '1;
        coffset_d   = offset;
        
        csize_set   = '1;
        csize_d     = size;
        
        cline_set   = '1;
        cline_d     = line;
        
        rsel_set    = '1;
        rsel_d      = coffset_d[ 1: 0];
        
        mem_read    = '1;
        mem_addr    = coffset_d[10: 2];

        next_state = stTRANSFER;
      end
    end
    
    stTRANSFER: begin
      ser_send = '1;
      if (ser_ack) begin
        coffset_set = '1;
        coffset_d   = coffset_q + 1'b1;
        
        csize_set   = '1;
        csize_d     = csize_q - 1'b1;
        
        rsel_set    = '1;
        rsel_d      = coffset_d[ 1: 0];
        
        mem_read    = '1; // (rsel_d == 2'b00);
        mem_addr    = coffset_d[10: 2];
        
        if (csize_d == '0)
          next_state = stWAIT_FOR_DONE;
      end
    end

    stWAIT_FOR_DONE: begin
      if (!ser_busy)
        next_state = stWAIT_FOR_START;
    end

    endcase
  end

  logic stx_send;
  logic stx_busy;
  logic stx_d;
  logic stx_ack;

  arinc708_tx_serialize_u8#(
    .W  (8)
  )
    serialize_u8
    (
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .send     (ser_send),
      .busy     (ser_busy),
      .d        (ser_d),
      .ack      (ser_ack),
      
      .tx_send  (stx_send),
      .tx_busy  (stx_busy),
      .tx_d     (stx_d),
      .tx_ack   (stx_ack)
    );

  logic         otx_oe;
  logic [ 1: 0] otx_db;
  arinc708_tx_frame_serialize#(
    .CLKFREQ  (50_000_000),
    .BAUDRATE (1_000_000)
  )
    serialize_u1
    (
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .send     (stx_send),
      .busy     (stx_busy),
      .d        (stx_d),
      .ack      (stx_ack),
      
      .tx_oe    (otx_oe),
      .tx_q     (otx_db)
    );

  always_comb begin
    tx_oe     = cline_q & { otx_oe, otx_oe };
    tx_d[0]   = { 2 { cline_q[0] } } & otx_db;
    tx_d[1]   = { 2 { cline_q[1] } } & otx_db;
  end
  
  assign busy   = (state != stWAIT_FOR_START);
  assign active = tx_oe;

endmodule

module arinc708_tx_serialize_u8
#(
  parameter W = 8
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  
  input  wire           send,
  output wire           busy,
  input  wire [W-1: 0]  d,
  output wire           ack,
  
  output wire           tx_send,
  input  wire           tx_busy,
  output wire           tx_d,
  input  wire           tx_ack
);
  
  localparam CW = $clog2(W);
  logic [CW-1: 0]   bc;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      bc <= '0;
    else if (ena) begin
      if (!tx_busy) begin
        if (send)
          bc <= '0;
      end
      else begin 
        if (tx_ack)
          bc <= bc + 1'b1;
      end
    end
    
  assign tx_send = send || (tx_ack && (bc != '0));
  assign busy    = tx_busy;
  assign tx_d    = d[W - 1 - bc];
  assign ack     = tx_ack && (bc == (W-1));

endmodule



module arinc708_tx_frame_serialize
#(
  parameter CLKFREQ   = 50_000_000,
  parameter BAUDRATE  =  1_000_000
)
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         send,
  output wire         busy,
  input  wire         d,
  output wire         ack,

  output wire         tx_oe,
  output wire [ 1: 0] tx_q
/*
  output wire         HI157X_TX_P,
  output wire         HI157X_TX_N,
  output wire         HI157X_TX_OD
*/
);

  logic         n_ack;
  logic [ 1: 0] n_d;
  logic         n_oe;

  arinc708_tx_dibit#(
    .CLKFREQ  (CLKFREQ),
    .BAUDRATE (BAUDRATE)
  )
    dibit_io
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),

      .ack          (n_ack),
      .d            (n_d),
      .oe           (n_oe),

      /*
      .HI157X_TX_P  (HI157X_TX_P),
      .HI157X_TX_N  (HI157X_TX_N),
      .HI157X_TX_OD (HI157X_TX_OD)
      */
      
      .tx_oe        (tx_oe),
      .tx_q         (tx_q)
    );

  enum int unsigned 
  {
    stIDLE,
    stSOW0,
    stSOW1,
    stSOW2,
    stFRAME,
    stEOW0,
    stEOW1,
    stEOW2,
    stIFG0,
    stIFG1,
    stIFG2
  } state, next_state;
    
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else if (ena) begin
      state <= next_state;
    end

  always_comb begin
    next_state = state;

    n_oe = '0;
    n_d  = 'x;
  
    case (state)
    stIDLE: begin
      if (send) begin
        if (n_ack) begin
          n_oe = '1;
          n_d  = 2'b11;
          next_state = stSOW1;
        end
        else
          next_state = stSOW0;
      end
    end
    
    stSOW0: begin
      n_oe = '1;
      n_d  = 2'b11;
      if (n_ack)
        next_state = stSOW1;
    end
    
    stSOW1: begin
      n_oe = '1;
      n_d  = 2'b10;
      if (n_ack)
        next_state = stSOW2;
    end
    
    stSOW2: begin
      n_oe = '1;
      n_d  = 2'b00;
      if (n_ack)
        next_state = stFRAME;
    end
    
    stFRAME: begin
      n_oe = '1;
      n_d  = { d, ~d };
      if (n_ack) begin
        if (!send) begin
          n_d = 2'b00;
          next_state = stEOW1;
        end
      end
    end
    
    stEOW0: begin
      n_oe = '1;
      n_d  = 2'b00;
      if (n_ack)
        next_state = stEOW1;
    end
    
    stEOW1: begin
      n_oe = '1;
      n_d  = 2'b01;
      if (n_ack)
        next_state = stEOW2;
    end
    
    stEOW2: begin
      n_oe = '1;
      n_d  = 2'b11;
      if (n_ack)
        next_state = stIDLE;
    end
    
    stIFG0: begin
      if (n_ack)
        next_state = stIFG1;
    end
    
    stIFG1: begin
      if (n_ack)
        next_state = stIFG2;
    end
    
    stIFG2: begin
      if (n_ack)
        next_state = stIDLE;
    end
    
    endcase
  end
  
  assign ack  = (state == stFRAME) && n_ack;
  assign busy = (state != stIDLE);

endmodule

module arinc708_tx_dibit
#(
  parameter CLKFREQ   = 50_000_000,
  parameter BAUDRATE  =  1_000_000
)
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  output wire         ack,
  input  wire [ 1: 0] d,
  input  wire         oe,
/*
  output wire         HI157X_TX_P,
  output wire         HI157X_TX_N,
  output wire         HI157X_TX_OD
*/
  output wire         tx_oe,
  output wire [ 1: 0] tx_q
);
  localparam DIV = CLKFREQ / BAUDRATE;
  localparam CW  = $clog2(DIV);
  
  logic [CW-1: 0] cd;
  logic sp_half_t;
  logic sp_full_t;
  
  always_comb begin
    sp_half_t = (cd == (DIV/2 - 1));
    sp_full_t = (cd == (DIV   - 1));
  end

  always_ff@(posedge areset or posedge clk)
    if (areset)
      cd <= '0;
    else if (ena) begin
      if (sp_full_t)
        cd <= '0;
      else
        cd <= cd + 1'b1;
    end
  
  logic [ 1: 0] s_d;
  logic         s_oe;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      s_d   <= '0;
      s_oe  <= '0;
    end
    else if (ena) begin
      if      (sp_full_t) begin
        s_d   <= d;
        s_oe  <= oe;
      end
      else if (sp_half_t) begin
        s_d   <= s_d << 1;
      end
    end
  
  logic [ 1: 0] s_dib;
  always_comb
    s_dib = { ~s_d[1], s_d[1] } & { s_oe, s_oe };
/*
  hi157x_tx_buffer
    tx_io
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .oe           (s_oe),
      .d            (s_dib),
      
      .HI157X_TX_P  (HI157X_TX_P),
      .HI157X_TX_N  (HI157X_TX_N),
      .HI157X_TX_OD (HI157X_TX_OD)
    );
*/

  assign tx_oe  = s_oe;
  assign tx_q   = s_dib;

  assign ack = sp_full_t;

endmodule

module hi157x_tx_buffer
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         oe,
  input  wire [ 1: 0] d,
  
  output wire         HI157X_TX_P,
  output wire         HI157X_TX_N,
  output wire         HI157X_TX_OD
);

  logic tx_p;
  logic tx_n;
  logic tx_oe;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      tx_p  <= '0;
      tx_n  <= '0;
      tx_oe <= '0;
    end
    else if (ena) begin
      tx_p  <= d[0];
      tx_n  <= d[1];
      tx_oe <= oe;
    end
    
  assign HI157X_TX_P  =  tx_p;
  assign HI157X_TX_N  =  tx_n;
  assign HI157X_TX_OD = ~tx_oe;

endmodule
