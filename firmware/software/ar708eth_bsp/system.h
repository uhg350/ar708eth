/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'nios2_cpu' in SOPC Builder design 'mcpu'
 * SOPC Builder design path: ../../mcpu.sopcinfo
 *
 * Generated: Tue Dec 12 01:35:00 MSK 2017
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x0000f820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 50000000u
#define ALT_CPU_CPU_ID_SIZE 30
#define ALT_CPU_CPU_ID_VALUE 0x34872343
#define ALT_CPU_CPU_IMPLEMENTATION "fast"
#define ALT_CPU_DATA_ADDR_WIDTH 0x15
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EIC_PRESENT
#define ALT_CPU_EXCEPTION_ADDR 0x00000020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 50000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 1
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 1
#define ALT_CPU_HARDWARE_MULX_PRESENT 1
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_DIVISION_ERROR_EXCEPTION
#define ALT_CPU_HAS_EXTRA_EXCEPTION_INFO
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x10
#define ALT_CPU_NAME "nios2_cpu"
#define ALT_CPU_NUM_OF_SHADOW_REG_SETS 7
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00000000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x0000f820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 50000000u
#define NIOS2_CPU_ID_SIZE 30
#define NIOS2_CPU_ID_VALUE 0x34872343
#define NIOS2_CPU_IMPLEMENTATION "fast"
#define NIOS2_DATA_ADDR_WIDTH 0x15
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EIC_PRESENT
#define NIOS2_EXCEPTION_ADDR 0x00000020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 1
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 1
#define NIOS2_HARDWARE_MULX_PRESENT 1
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_DIVISION_ERROR_EXCEPTION
#define NIOS2_HAS_EXTRA_EXCEPTION_INFO
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x10
#define NIOS2_NUM_OF_SHADOW_REG_SETS 7
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00000000


/*
 * Custom instruction macros
 *
 */

#define ALT_CI_ADD1C(A,B) __builtin_custom_inii(ALT_CI_ADD1C_N,(A),(B))
#define ALT_CI_ADD1C_N 0x5
#define ALT_CI_BSWAP(A,B) __builtin_custom_inii(ALT_CI_BSWAP_N,(A),(B))
#define ALT_CI_BSWAP_N 0x4
#define ALT_CI_CRC32_IEEE_802_3(n,A,B) __builtin_custom_inii(ALT_CI_CRC32_IEEE_802_3_N+(n&ALT_CI_CRC32_IEEE_802_3_N_MASK),(A),(B))
#define ALT_CI_CRC32_IEEE_802_3_N 0x0
#define ALT_CI_CRC32_IEEE_802_3_N_MASK ((1<<2)-1)
#define ALT_CI_MTICKS(A,B) __builtin_custom_inii(ALT_CI_MTICKS_N,(A),(B))
#define ALT_CI_MTICKS_N 0x6


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_DUAL_BOOT
#define __ALTERA_NIOS2_GEN2
#define __ALTERA_ONCHIP_FLASH
#define __CRC32_IEEE_802_3_CIS
#define __NIOS_AR708_MAC_MMAP
#define __NIOS_CIS_ADD1C
#define __NIOS_CIS_BSWAP
#define __NIOS_CIS_MTICKS
#define __NIOS_RMII_MAC
#define __NIOS_RPI_MMAP
#define __NIOS_SMI_CTRL
#define __NIOS_UNIQUE_ID


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "MAX 10"
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERRUPT_CONTROLLERS 0
#define ALT_STDERR "/dev/jtag_uart"
#define ALT_STDERR_BASE 0xf7e0
#define ALT_STDERR_DEV jtag_uart
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart"
#define ALT_STDIN_BASE 0xf7e0
#define ALT_STDIN_DEV jtag_uart
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart"
#define ALT_STDOUT_BASE 0xf7e0
#define ALT_STDOUT_DEV jtag_uart
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "mcpu"


/*
 * ar708_mac_ctl configuration
 *
 */

#define ALT_MODULE_CLASS_ar708_mac_ctl nios_ar708_mac_mmap
#define AR708_MAC_CTL_BASE 0xc040
#define AR708_MAC_CTL_IRQ -1
#define AR708_MAC_CTL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define AR708_MAC_CTL_NAME "/dev/ar708_mac_ctl"
#define AR708_MAC_CTL_SPAN 64
#define AR708_MAC_CTL_TYPE "nios_ar708_mac_mmap"


/*
 * ar708_mac_rxm configuration
 *
 */

#define ALT_MODULE_CLASS_ar708_mac_rxm nios_ar708_mac_mmap
#define AR708_MAC_RXM_BASE 0xb000
#define AR708_MAC_RXM_IRQ -1
#define AR708_MAC_RXM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define AR708_MAC_RXM_NAME "/dev/ar708_mac_rxm"
#define AR708_MAC_RXM_SPAN 4096
#define AR708_MAC_RXM_TYPE "nios_ar708_mac_mmap"


/*
 * ar708_mac_txm configuration
 *
 */

#define ALT_MODULE_CLASS_ar708_mac_txm nios_ar708_mac_mmap
#define AR708_MAC_TXM_BASE 0xe000
#define AR708_MAC_TXM_IRQ -1
#define AR708_MAC_TXM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define AR708_MAC_TXM_NAME "/dev/ar708_mac_txm"
#define AR708_MAC_TXM_SPAN 2048
#define AR708_MAC_TXM_TYPE "nios_ar708_mac_mmap"


/*
 * boot_mgr configuration
 *
 */

#define ALT_MODULE_CLASS_boot_mgr altera_dual_boot
#define BOOT_MGR_BASE 0xf780
#define BOOT_MGR_IRQ -1
#define BOOT_MGR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BOOT_MGR_NAME "/dev/boot_mgr"
#define BOOT_MGR_SPAN 32
#define BOOT_MGR_TYPE "altera_dual_boot"


/*
 * chipid configuration
 *
 */

#define ALT_MODULE_CLASS_chipid nios_unique_id
#define CHIPID_BASE 0xf7b0
#define CHIPID_IRQ -1
#define CHIPID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CHIPID_NAME "/dev/chipid"
#define CHIPID_SPAN 8
#define CHIPID_TYPE "nios_unique_id"


/*
 * emac_ctl configuration
 *
 */

#define ALT_MODULE_CLASS_emac_ctl nios_rmii_mac
#define EMAC_CTL_BASE 0xa000
#define EMAC_CTL_IRQ -1
#define EMAC_CTL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define EMAC_CTL_NAME "/dev/emac_ctl"
#define EMAC_CTL_SPAN 32
#define EMAC_CTL_TYPE "nios_rmii_mac"


/*
 * emac_rxmmap configuration
 *
 */

#define ALT_MODULE_CLASS_emac_rxmmap nios_rmii_mac
#define EMAC_RXMMAP_BASE 0x8000
#define EMAC_RXMMAP_IRQ -1
#define EMAC_RXMMAP_IRQ_INTERRUPT_CONTROLLER_ID -1
#define EMAC_RXMMAP_NAME "/dev/emac_rxmmap"
#define EMAC_RXMMAP_SPAN 4096
#define EMAC_RXMMAP_TYPE "nios_rmii_mac"


/*
 * emac_txmmap configuration
 *
 */

#define ALT_MODULE_CLASS_emac_txmmap nios_rmii_mac
#define EMAC_TXMMAP_BASE 0x9000
#define EMAC_TXMMAP_IRQ -1
#define EMAC_TXMMAP_IRQ_INTERRUPT_CONTROLLER_ID -1
#define EMAC_TXMMAP_NAME "/dev/emac_txmmap"
#define EMAC_TXMMAP_SPAN 4096
#define EMAC_TXMMAP_TYPE "nios_rmii_mac"


/*
 * flash_csr configuration
 *
 */

#define ALT_MODULE_CLASS_flash_csr altera_onchip_flash
#define FLASH_CSR_BASE 0x4000
#define FLASH_CSR_BYTES_PER_PAGE 4096
#define FLASH_CSR_IRQ -1
#define FLASH_CSR_IRQ_INTERRUPT_CONTROLLER_ID -1
#define FLASH_CSR_NAME "/dev/flash_csr"
#define FLASH_CSR_READ_ONLY_MODE 0
#define FLASH_CSR_SECTOR1_ENABLED 1
#define FLASH_CSR_SECTOR1_END_ADDR 0x3fff
#define FLASH_CSR_SECTOR1_START_ADDR 0
#define FLASH_CSR_SECTOR2_ENABLED 1
#define FLASH_CSR_SECTOR2_END_ADDR 0x7fff
#define FLASH_CSR_SECTOR2_START_ADDR 0x4000
#define FLASH_CSR_SECTOR3_ENABLED 1
#define FLASH_CSR_SECTOR3_END_ADDR 0x3bfff
#define FLASH_CSR_SECTOR3_START_ADDR 0x8000
#define FLASH_CSR_SECTOR4_ENABLED 1
#define FLASH_CSR_SECTOR4_END_ADDR 0x63fff
#define FLASH_CSR_SECTOR4_START_ADDR 0x3c000
#define FLASH_CSR_SECTOR5_ENABLED 1
#define FLASH_CSR_SECTOR5_END_ADDR 0xbffff
#define FLASH_CSR_SECTOR5_START_ADDR 0x64000
#define FLASH_CSR_SPAN 8
#define FLASH_CSR_TYPE "altera_onchip_flash"


/*
 * flash_data configuration
 *
 */

#define ALT_MODULE_CLASS_flash_data altera_onchip_flash
#define FLASH_DATA_BASE 0x100000
#define FLASH_DATA_BYTES_PER_PAGE 4096
#define FLASH_DATA_IRQ -1
#define FLASH_DATA_IRQ_INTERRUPT_CONTROLLER_ID -1
#define FLASH_DATA_NAME "/dev/flash_data"
#define FLASH_DATA_READ_ONLY_MODE 0
#define FLASH_DATA_SECTOR1_ENABLED 1
#define FLASH_DATA_SECTOR1_END_ADDR 0x3fff
#define FLASH_DATA_SECTOR1_START_ADDR 0
#define FLASH_DATA_SECTOR2_ENABLED 1
#define FLASH_DATA_SECTOR2_END_ADDR 0x7fff
#define FLASH_DATA_SECTOR2_START_ADDR 0x4000
#define FLASH_DATA_SECTOR3_ENABLED 1
#define FLASH_DATA_SECTOR3_END_ADDR 0x3bfff
#define FLASH_DATA_SECTOR3_START_ADDR 0x8000
#define FLASH_DATA_SECTOR4_ENABLED 1
#define FLASH_DATA_SECTOR4_END_ADDR 0x63fff
#define FLASH_DATA_SECTOR4_START_ADDR 0x3c000
#define FLASH_DATA_SECTOR5_ENABLED 1
#define FLASH_DATA_SECTOR5_END_ADDR 0xbffff
#define FLASH_DATA_SECTOR5_START_ADDR 0x64000
#define FLASH_DATA_SPAN 786432
#define FLASH_DATA_TYPE "altera_onchip_flash"


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 32
#define ALT_SYS_CLK none
#define ALT_TIMESTAMP_CLK none


/*
 * jtag_uart configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart altera_avalon_jtag_uart
#define JTAG_UART_BASE 0xf7e0
#define JTAG_UART_IRQ -1
#define JTAG_UART_IRQ_INTERRUPT_CONTROLLER_ID -1
#define JTAG_UART_NAME "/dev/jtag_uart"
#define JTAG_UART_READ_DEPTH 8
#define JTAG_UART_READ_THRESHOLD 4
#define JTAG_UART_SPAN 8
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_WRITE_DEPTH 1024
#define JTAG_UART_WRITE_THRESHOLD 8


/*
 * nios2_sysid configuration
 *
 */

#define ALT_MODULE_CLASS_nios2_sysid altera_avalon_sysid_qsys
#define NIOS2_SYSID_BASE 0xf7f0
#define NIOS2_SYSID_ID 842282240
#define NIOS2_SYSID_IRQ -1
#define NIOS2_SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define NIOS2_SYSID_NAME "/dev/nios2_sysid"
#define NIOS2_SYSID_SPAN 8
#define NIOS2_SYSID_TIMESTAMP 1513028020
#define NIOS2_SYSID_TYPE "altera_avalon_sysid_qsys"


/*
 * ram configuration
 *
 */

#define ALT_MODULE_CLASS_ram altera_avalon_onchip_memory2
#define RAM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define RAM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define RAM_BASE 0x0
#define RAM_CONTENTS_INFO ""
#define RAM_DUAL_PORT 1
#define RAM_GUI_RAM_BLOCK_TYPE "AUTO"
#define RAM_INIT_CONTENTS_FILE "mcpu_ram"
#define RAM_INIT_MEM_CONTENT 1
#define RAM_INSTANCE_ID "NONE"
#define RAM_IRQ -1
#define RAM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RAM_NAME "/dev/ram"
#define RAM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define RAM_RAM_BLOCK_TYPE "AUTO"
#define RAM_READ_DURING_WRITE_MODE "DONT_CARE"
#define RAM_SINGLE_CLOCK_OP 1
#define RAM_SIZE_MULTIPLE 1
#define RAM_SIZE_VALUE 16384
#define RAM_SPAN 16384
#define RAM_TYPE "altera_avalon_onchip_memory2"
#define RAM_WRITABLE 1


/*
 * rpins configuration
 *
 */

#define ALT_MODULE_CLASS_rpins nios_rpi_mmap
#define RPINS_BASE 0xc080
#define RPINS_IRQ -1
#define RPINS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RPINS_NAME "/dev/rpins"
#define RPINS_SPAN 4
#define RPINS_TYPE "nios_rpi_mmap"


/*
 * smi configuration
 *
 */

#define ALT_MODULE_CLASS_smi nios_smi_ctrl
#define SMI_BASE 0xc000
#define SMI_IRQ -1
#define SMI_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SMI_NAME "/dev/smi"
#define SMI_SPAN 64
#define SMI_TYPE "nios_smi_ctrl"

#endif /* __SYSTEM_H_ */
