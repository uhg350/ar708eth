/*
 * global_vars.h
 *
 *  Created on: Jul 19, 2017
 *      Author: ilynxy
 */

#ifndef GLOBAL_VARS_H_
#define GLOBAL_VARS_H_

#include "typedefs.h"
#include "structs.h"

extern ident_t   g_ident  _ALIGNED_(4);
extern config_t  g_config _ALIGNED_(4);


#endif /* GLOBAL_VARS_H_ */
