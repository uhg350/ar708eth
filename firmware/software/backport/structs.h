/*
 * structs.h
 *
 *  Created on: Jul 19, 2017
 *      Author: ilynxy
 */

#ifndef STRUCTS_H_
#define STRUCTS_H_

#include "config.h"
#include "typedefs.h"
#include "eth2.h"
#include "ipv4.h"

typedef struct
{
  le_uint8_t    major;
  le_uint8_t    minor;
  le_uint16_t   patch;
} _PACKED_ version_info_t;

typedef le_uint32_t     ident_size_t;
typedef le_uint32_t     ident_crc_t;

typedef struct
{
  le_uint32_t   number;
} _PACKED_ serial_info_t;

typedef struct
{
  le_uint8_t    u8[8];
} _PACKED_ timestamp_t;

typedef struct
{
  ident_size_t    size;
  version_info_t  version;
} _PACKED_ ident_header_t;

typedef struct
{
  timestamp_t     timestamp;
  version_info_t  version;
  serial_info_t   serial;
  le_uint8_t      user[256];
} _PACKED_ ident_data_t;

typedef struct
{
  ident_header_t  header;
  ident_data_t    data;
} _PACKED_ ident_block_t;

typedef struct
{
  ident_block_t   block;
  ident_crc_t     crc;
} _PACKED_ ident_t;

typedef struct
{
  eth2_mac_t        mac;
  ipv4_port_t       port;
  ipv4_address_t    ip;
} _PACKED_ ethernet_address_t;

typedef struct
{
  le_uint8_t       lines_mask;
  le_uint8_t       message_id;
  le_uint8_t       message_offset;
  le_uint8_t       message_size;
} /* _PACKED_ */ message_filter_t;

typedef struct
{
  ethernet_address_t  src;
  ethernet_address_t  dst;
  le_uint32_t         flags;
  message_filter_t    messages[CONFIG_MAXIMUM_FILTERS];
} _PACKED_ sink_config_t;

typedef struct
{
  timestamp_t         timestamp;
  ethernet_address_t  device_address;
  sink_config_t       sinks[CONFIG_MAXIMUM_SINKS];

} _PACKED_ config_data_t;

typedef version_info_t  config_version_t;
typedef le_uint32_t     config_size_t;
typedef le_uint32_t     config_crc_t;

typedef struct
{
  config_size_t     size;
  config_version_t  version;
} _PACKED_ config_header_t;

typedef struct
{
  config_header_t   header;
  config_data_t     data;
} _PACKED_ config_block_t;

typedef struct
{
  config_block_t    block;
  config_crc_t      crc;
} _PACKED_ config_t;

ASSERT_SIZE_ALIGNED(ident_t, 4)
ASSERT_SIZE_ALIGNED(config_t, 4)

#endif /* STRUCTS_H_ */
