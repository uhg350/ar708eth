/*
 * ipv4.h
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef IPV4_H_
#define IPV4_H_

#include "config.h"
#include "typedefs.h"

typedef struct
{
  union
  {
    be_uint32_t u32;
    be_uint16_t u16[2];
    be_uint8_t  u8[4];  // network byte order (big endian)
  };
} _PACKED_ ipv4_address_t;

typedef struct
{
  union
  {
    be_uint16_t u16;
    be_uint8_t  u8[2]; // network byte order (big endian)
  };
} _PACKED_ ipv4_port_t;

typedef struct
{
  be_uint8_t      version_length;
  be_uint8_t      type_of_service;
  be_uint16_t     total_length;
  be_uint16_t     id;
  be_uint16_t     offset_flags;
  be_uint8_t      ttl;
  be_uint8_t      protocol;
  be_uint16_t     checksum;
  ipv4_address_t  srcIP;
  ipv4_address_t  dstIP;

} _PACKED_ ipv4_header_t;

typedef struct
{
  ipv4_port_t     srcPort;
  ipv4_port_t     dstPort;
  be_uint16_t     length;
  be_uint16_t     checksum;

} _PACKED_ udpv4_header_t;

/*
typedef struct
{
  eth_frame_header_t  eth;
  ipv4_header_t       ip;
  udpv4_header_t      udp;
} _PACKED_ udpv4_packet_header_t;
*/

#endif /* IPV4_H_ */
