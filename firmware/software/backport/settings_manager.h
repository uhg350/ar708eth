/*
 * settings_manager.h
 *
 *  Created on: Oct 15, 2017
 *      Author: ilynxy
 */

#ifndef SETTINGS_MANAGER_H_
#define SETTINGS_MANAGER_H_

void load_ident_from_nvram(void);
void load_config_from_nvram(void);


#endif /* SETTINGS_MANAGER_H_ */
