/*
 * arpv4_handler.c
 *
 *  Created on: Aug 2, 2017
 *      Author: ilynxy
 */

#include "i_eth_rx.h"

#include "eth2.h"
#include "ipv4.h"
#include "arpv4.h"

#include "i_bswap_add1c.h"
#include "global_vars.h"

static
int is_arpv4_frame(const eth2_arpv4_frame_t *pARP)
{

  int result = 0;

  if (pARP->eth.ethertype     != S_BSWAP_U16(0x0806)) // ARP EtherType
    goto _exit_label;

  if (pARP->arp.hardware_type != S_BSWAP_U16(0x0001)) // Ethernet is 1
    goto _exit_label;

  if (pARP->arp.protocol_type != S_BSWAP_U16(0x0800)) // IPv4 is 0x0800
    goto _exit_label;

  if (pARP->arp.hardware_size != 6) // 6 octets for Ethernet address
    goto _exit_label;

  if (pARP->arp.protocol_size != 4) // 4 octets for IPv4 address
    goto _exit_label;
/*
  if (pARP->arp.opcode != S_BSWAP_U16(0x0001) &&
      pARP->arp.opcode != S_BSWAP_U16(0x0002)) // 1 for ARP request, 2 for ARP reply
    goto _exit_label;

  uint32_t target_ip = pARP->arp.target.ip.u32;
  if (target_ip != 0xFFFFFFFF) // broadcast
  {
    uint32_t my_ip     = g_config.block.data.device_address.ip.u32;
    if (target_ip != my_ip)
      goto _exit_label;
  }
*/
  result = 1;

_exit_label:

  return result;
}

void arpv4_handler(void)
{
  const void *pEF = eth_rx_GetCurrentFramePtr();
  const eth2_arpv4_frame_t *pARP = __assume_aligned(pEF, 4);

  if (!is_arpv4_frame(pARP))
    return;

  uint32_t target_ip = pARP->arp.target.ip.u32;
  uint32_t my_ip     = g_config.block.data.device_address.ip.u32;

  if (pARP->arp.opcode == S_BSWAP_U16(0x0001) && (target_ip == my_ip))
    ; // ARP REQUESTing my MAC

  if (pARP->arp.opcode == S_BSWAP_U16(0x0002) && (target_ip == my_ip))
    ; // ARP REPLY to my request (if any?)

  //if (pARP->arp.opcode == S_BSWAP_U16(0x0002) && (target_ip == sender_ip))
  //  ; // ARP GRATUITOUS (update cache)


  asm ("nop");

}
