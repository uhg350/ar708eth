/*
 * eth_mmap_txm.h
 *
 *      Author: ilynxy
 */

#ifndef ETH_MMAP_TXM_P_H_
#define ETH_MMAP_TXM_P_H_

#include "typedefs.h"
#include "eth2.h"
#include "ipv4.h"
#include "arpv4.h"

typedef struct
{
  le_uint8_t    bm8message_source;
  le_uint8_t    u8message_label;
  le_uint8_t    u8message_offset;
  le_uint8_t    u8message_size;
  le_uint32_t   u32message_timestamp;
} data_block_header_t;

typedef struct
{
  uint16_t              u16align;
  eth2_frame_header_t   eth;
  ipv4_header_t         ip;
  udpv4_header_t        udp;
  data_block_header_t   data_block_header;

} eth2_udpv4_data_frame_header_t;

typedef struct
{
  le_uint32_t           tag;
  le_uint32_t           lun;
  le_uint32_t           command;
  le_uint32_t           reserved0;
} ctrl_block_header_t;

typedef struct
{
  uint16_t              u16align;
  eth2_frame_header_t   eth;
  ipv4_header_t         ip;
  udpv4_header_t        udp;
  ctrl_block_header_t   ctrl;

} eth2_udpv4_ctrl_frame_header_t;

typedef struct
{
  eth2_arpv4_frame_t              eth2_arpv4_request      _ALIGNED_(4);
  eth2_arpv4_frame_t              eth2_arpv4_reply        _ALIGNED_(4);
  eth2_udpv4_ctrl_frame_header_t  eth2_udpv4_ctrl_header  _ALIGNED_(4);
  le_uint8_t                      eth2_udpv4_ctrl[1024]   _ALIGNED_(4);

  eth2_udpv4_data_frame_header_t  eth2_udpv4_data_header[CONFIG_MAXIMUM_SINKS] _ALIGNED_(4);
  le_uint8_t                      eth2_udpv4_data[2 * 200]                     _ALIGNED_(4);

} eth_tx_mmap_t;

#endif /* ETH_MMAP_TXM_P_H_ */
