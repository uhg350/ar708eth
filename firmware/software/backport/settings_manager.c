/*
 * read_settings.c
 *
 *  Created on: 26 ���� 2017 �.
 *      Author: ilynxy
 */

#include <assert.h>
#include <stddef.h>

#include "system.h"
#include "gpio.h"

#include "i_crc32_iee802_3.h"

#include "global_vars.h"
#include "settings_mmap.h"

static
uint32_t copy_and_compute_crc_u32(const void *src, void *dst, size_t size)
{
  const uint32_t  *ps = __assume_aligned(src, 4);
  uint32_t        *pd = __assume_aligned(dst, 4);
  uint32_t        *pe = (uint32_t *)(((char *)pd) + size);

  uint32_t crc;

  crc = CRC32_IEE802_3_GetInitial();

  do
  {
    uint32_t u32;

    u32 = *ps ++;
    *pd ++ = u32;

    crc = CRC32_IEE802_3_Compute32(crc, u32);

  } while (pd < pe);

  return crc;
}

static
void load_block_from_nvram(const void *current_block, const void *default_block, void *dst, size_t size)
{
  uint32_t crc;
  crc = copy_and_compute_crc_u32(current_block, dst, size);
  if (crc != CRC32_IEE802_3_GetCheck())
  {
    crc = copy_and_compute_crc_u32(default_block, dst, size - 4);

    uint32_t *pd = (uint32_t *)(((char *)dst) + size - 4);
    *pd = crc;
  }
}

void load_ident_from_nvram(void)
{
  load_block_from_nvram(&flash_content.current_ident, &default_settings.default_ident, &g_ident, sizeof(g_ident));
}

void store_ident_to_nvram(void)
{

}

//
void load_config_from_nvram(void)
{
  load_block_from_nvram(&flash_content.current_config, &default_settings.default_config, &g_config, sizeof(g_config));
}

void store_config_to_nvram(void)
{

}

