/*
 * eth_mmap_txm_manager.c
 *
 *  Created on: Jul 29, 2017
 *      Author: ilynxy
 */

#include "typedefs.h"
#include "eth_mmap_txm.h"
#include "global_vars.h"
#include "i_bswap_add1c.h"
#include "gpio.h"

inline
void etx_update_ip_header_checksum_a4(ipv4_header_t *ip)
{
  ip = __assume_aligned(ip, 4);

  ip->checksum = 0;
  const uint32_t *pIPH = (const uint32_t *)ip;
  uint32_t c0, c1;
  c0 = FOLDU16_ADD1C_U32_U32(pIPH[0], pIPH[1]);
  c1 = FOLDU16_ADD1C_U32_U32(pIPH[2], pIPH[3]);
  //c0 |= c1 << 16;
  c0 = FOLDU16_ADD1C_U32_U32(c0, pIPH[4]);
  c0 = FOLDU16_ADD1C_U32_U32(c0, c1);

  ip->checksum = ~c0;
}

void etx_update_preallocated_structs(void)
{
  config_t *pCFG = &g_config;
  eth2_udpv4_ctrl_frame_header_t  *pUDP = &eth_tx_mmap.eth2_udpv4_ctrl_header;

  pUDP->eth.srcMAC  = pCFG->block.data.device_address.mac;
  pUDP->ip.srcIP    = pCFG->block.data.device_address.ip;
  pUDP->udp.srcPort = pCFG->block.data.device_address.port;

  etx_update_ip_header_checksum_a4(&pUDP->ip);
}
