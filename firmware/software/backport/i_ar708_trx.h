/*
 * i_ar708_trx.h
 *
 *  Created on: Jul 23, 2017
 *      Author: ilynxy
 */

#ifndef I_AR708_TRX_H_
#define I_AR708_TRX_H_

#include "system.h"
#include "gpio.h"

IO_CREATE_SYMBOL(AR708_MMAP_BASE);

typedef struct
{
  le_uint8_t  label;
  le_uint8_t  something_else[7];
} _PACKED_ arinc708_header_t;

typedef struct
{
  arinc708_header_t   header;
  uint8_t             data[192];
} _PACKED_ arinc708_word_t;


#define AR708_RX_MMAP_OFFSET  0x0000
#define AR708_TX_MMAP_OFFSET  0x0400
#define AR708_CT_MMAP_OFFSET  0x0800
#define AR708_RS_MMAP_OFFSET  0x0C00

// offset [0..1023] + AR708_TX_MMAP_BASE
inline
void AR708_SendPacket(const void *offset, size_t size)
{
}

inline
size_t AR708_GetCurrentPacketSize(void)
{
  return RDIO32(IO_PTR_CVU8(AR708_MMAP_BASE) + AR708_CT_MMAP_OFFSET);
}

inline
const void *AR708_GetCurrentPacketPtr(void)
{
  return (const void *)(IO_PTR_CVU8(AR708_MMAP_BASE) + AR708_RX_MMAP_OFFSET);
}

inline
void AR708_DropCurrentPacket(void)
{
  WRIO32(IO_PTR_VU8(AR708_MMAP_BASE) + AR708_CT_MMAP_OFFSET, 0);
}

inline
void AR708_DropAllPackets(void)
{
  WRIO32(IO_PTR_VU8(AR708_MMAP_BASE) + AR708_CT_MMAP_OFFSET + 0x0004, 0);
}

inline static
void *AR708_GetTXBuffer(size_t size)
{
  return (void *)(IO_PTR_CVU8(AR708_MMAP_BASE) + AR708_TX_MMAP_OFFSET);
}

#endif /* I_AR708_TRX_H_ */
