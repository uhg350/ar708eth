/*
 * e100_tx_mmap.c
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#include "system.h"
#include "gpio.h"

#include "eth_mmap_txm_p.h"
#include "global_vars.h"

#define _TXM_MAP_SECTION_INTERNAL_ __attribute__ ((section (".eth_mmap_txm"), aligned (0x1000)))

eth_tx_mmap_t eth_tx_mmap _TXM_MAP_SECTION_INTERNAL_ =
{

};

inline void __assertion_checking_eth_mmap_txm(void)
{
  ASSERT_OFFSET_ALIGNED(eth2_udpv4_data_frame_header_t, ip.srcIP, 4);
  ASSERT_OFFSET_ALIGNED(eth2_udpv4_data_frame_header_t, ip.dstIP, 4);
  ASSERT_OFFSET_ALIGNED(eth2_udpv4_data_frame_header_t, data_block_header, 4);

  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[0], 4);
  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[0].ip.srcIP, 4);
  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[0].ip.dstIP, 4);

#if CONFIG_MAXIMUM_SINKS > 1
  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[1], 4);
#endif

}
