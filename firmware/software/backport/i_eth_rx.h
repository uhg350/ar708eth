/*
 * i_eth_rx.h
 *
 *  Created on: Jul 25, 2017
 *      Author: ilynxy
 */

#ifndef I_ETH_RX_H_
#define I_ETH_RX_H_

#include "system.h"
#include "gpio.h"

IO_CREATE_SYMBOL(ETH_MMAP_RXM_BASE);

#define E1G_RX_RD16_PACKET_LENGTH     (0x0000)
#define E1G_RX_WR16_DROP_PACKET       (0x0000)
#define E1G_RX_WR32_DROP_ALL_PACKETS  (0x0000)

#define eth_rx_GetCurrentFrameLength() RDIO16(IO_PTR_VU16(ETH_MMAP_RXM_BASE) + E1G_RX_RD16_PACKET_LENGTH)
#define eth_rx_DropCurrentFrame()      WRIO16(IO_PTR_VU16(ETH_MMAP_RXM_BASE) + E1G_RX_WR16_DROP_PACKET, 0)
#define eth_rx_DropAllFrames()         WRIO32(IO_PTR_VU32(ETH_MMAP_RXM_BASE) + E1G_RX_WR32_DROP_ALL_PACKETS, 0)

#define eth_rx_GetCurrentFramePtr()    IO_SYMBOL_PTR(ETH_MMAP_RXM_BASE, const void *, 4)

#endif /* I_ETH_RX_H_ */
