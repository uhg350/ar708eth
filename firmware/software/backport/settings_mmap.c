/*
 * settings_mmap.c
 *
 *  Created on: 27 ���� 2017 �.
 *      Author: ilynxy
 */

#include "settings_mmap.h"

#define CONFIG_DEFAULT_SRC_ADDR \
{\
  .mac.u8   = { 0x00, 0x01, 0x02, 0x03, 0x22, DEVICE_SERIAL_NUMBER }, \
  .port.u16 = 4660, \
  .ip.u8    = { 192, 168, 1, DEVICE_SERIAL_NUMBER + 100 } \
}

#define CONFIG_DEFAULT_SINK(x) \
{\
  .src = CONFIG_DEFAULT_SRC_ADDR,\
  .dst =\
    {\
      .mac.u8   = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },\
      .port.u16 = 1234,\
      .ip.u8    = { 192, 168, 1, x }\
    },\
  .flags = 0,\
  .messages =\
    {\
      { 0x03, 055, 0, 200 },\
      { 0x03, 057, 0, 200 }\
    }\
}


flash_content_t flash_content _FLASH_MMAP_SECTION_;

default_settings_t default_settings _DEFAULT_SETTINGS_MMAP_SECTION_ =
{
  // Default IDENT initialization values
  .default_ident.block.header =
    {
      .size = sizeof(ident_t),
      .version = { IDENT_VERSION_MAJOR, IDENT_VERSION_MINOR, IDENT_VERSION_PATCH }
    },

  .default_ident.block.data =
    {
      .timestamp = { { 0 } }, // timestamp is invalid
      .version   = { DEVICE_VERSION_MAJOR, DEVICE_VERSION_MINOR, DEVICE_VERSION_PATCH },
      .serial    = { DEVICE_SERIAL_NUMBER },
      .user      = { "Default identification. Timestamp: " __TIMESTAMP__ }
    },

  .default_ident.crc = 0,

  // Default CONFIG initialization values
  .default_config.block.header =
    {
      .size = sizeof(config_t),
      .version = { CONFIG_MAJOR, CONFIG_MINOR, CONFIG_PATCH }
    },

  .default_config.block.data =
    {
      .timestamp = { { 0 } }, // timestamp is invalid
      .device_address = CONFIG_DEFAULT_SRC_ADDR,
      .sinks[0] = CONFIG_DEFAULT_SINK(132),
      .sinks[1] = CONFIG_DEFAULT_SINK(134),
      .sinks[2] = CONFIG_DEFAULT_SINK(136),
      .sinks[3] = CONFIG_DEFAULT_SINK(138)
    },

  .default_config.crc = 0
};

