/*
 * eth_rx_handler.c
 *
 *  Created on: Aug 3, 2017
 *      Author: ilynxy
 */

#include "i_eth_rx.h"

#include "eth2.h"
#include "ipv4.h"
#include "arpv4.h"

#include "i_bswap_add1c.h"

#include "global_vars.h"

/*
inline
void memcopy32(const void *ps, void *pd, size_t size)
{
  const uint32_t *ps32 = (const uint32_t *)ps;
  uint32_t *pd32 = (uint32_t *)pd;
  uint32_t *pe32 = (uint32_t *)(((char *)ps) + size);
  do
  {
    *pd32 ++ = *ps32 ++;

  } while (ps32 < pe32);
}

inline
void memcopy8(const void *ps, void *pd, size_t size)
{
  const uint8_t *ps8 = (const uint8_t *)ps;
  uint8_t *pd8 = (uint8_t *)pd;
  uint8_t *pe8 = (uint8_t *)(((char *)pd) + size);
  do
  {
    *pd8 ++ = *ps8 ++;

  } while (pd8 < pe8);
}

inline
void copy_mac(const uint16_t * restrict s, uint16_t * restrict d)
{
  *d ++ = *s ++;
  *d ++ = *s ++;
  *d ++ = *s ++;

}
*/

typedef struct
{
  uint16_t            u16align;
  eth2_frame_header_t eth;
  ipv4_header_t       ip;
  udpv4_header_t      udp;
} _PACKED_ udpv4_frame_t;

typedef struct
{
  uint16_t            u16align;
  eth2_frame_header_t eth;
  arpv4_packet_t      arp;
} _PACKED_ arpv4_frame_t;

#define is_aligned(n, a) ((n & (a - 1)) == 0)

#define copy_ipv4_addr_t(sp, sm, dp, dm) \
  if ( is_aligned(offsetof(typeof(*sp), sm), 4) && is_aligned(offsetof(typeof(*sp), sm), 4) ) { dp->dm.u32 = sp->sm.u32; } else { dp->dm.u16[0] = sp->sm.u16[0]; dp->dm.u16[1] = sp->sm.u16[1]; }

#define copy_eth2_mac_t(sp, sm, dp, dm) \
    { dp->dm.u16[0] = sp->sm.u16[0]; dp->dm.u16[1] = sp->sm.u16[1]; dp->dm.u16[2] = sp->sm.u16[2]; }

void eth_rx_handler(void)
{
  size_t eth_frame_length;
  eth_frame_length = eth_rx_GetCurrentFrameLength();
  if (eth_frame_length == 0)
    return;

  const void *pEF = eth_rx_GetCurrentFramePtr();

  const eth2_frame_header_t *pE2H = __builtin_assume_aligned((char *)pEF + 2, 4, 2);
  int g_rx_broadcast_packet = 0;

  uint32_t mac0 = pE2H->dstMAC.u16[0];
  uint32_t mac1 = pE2H->dstMAC.u16[1];
  uint32_t mac2 = pE2H->dstMAC.u16[2];

  const eth2_mac_t *pMY = &g_config.block.data.device_address.mac;

  uint32_t my0  = pMY->u16[0];
  uint32_t my1  = pMY->u16[1];
  uint32_t my2  = pMY->u16[2];

  if (!(my0 == mac0 && my1 == mac1 && my2 == mac2))
  {
    if (mac0 == 0xFFFF && mac1 == 0xFFFF && mac2 == 0xFFFF)
    {
      g_rx_broadcast_packet = mac0;

      uint32_t eth2_type = pE2H->ethertype;

      if (eth2_type == S_BSWAP_U16(0x0806))
        arpv4_handler();


    }
  }

__drop_packet_and_exit:
  eth_rx_DropCurrentFrame();
}
