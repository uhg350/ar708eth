/*
 * eth_mmap_txm.h
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef ETH_MMAP_TXM_H_
#define ETH_MMAP_TXM_H_

#include "eth_mmap_txm_p.h"

#define _TXM_MAP_SECTION_EXTERNAL_ __attribute__ ((aligned (0x1000)))
extern eth_tx_mmap_t eth_tx_mmap _TXM_MAP_SECTION_EXTERNAL_;

#endif /* ETH_MMAP_TXM_H_ */
