#ifndef GPIO_H_
#define GPIO_H_

#include <stddef.h>
#include <stdint.h>

#define __assume_aligned(p, a)  __builtin_assume_aligned(p, a)

// It is guaranteed ldb and ldh return zero at upper part of data bus
#define GPIO_CONFIG_NO_UPPER_TRASH

/*
#define XSTRINGIFY(a) STRINGIFY(a)
#define STRINGIFY(a) #a

#define IO_CREATE_SYMBOL(SYM) \
  extern char __gp__##SYM; \
  asm("\t" STRINGIFY(__gp__##SYM) " = " XSTRINGIFY(SYM) " + __page_offset_0x0000\n")

#define IO_SYMBOL_PTR(SYM, TYPE, ALIGN) ({ TYPE __ptr = __builtin_assume_aligned( (&(__gp__##SYM)), ALIGN); __ptr; })

#define IO_PTR_VU32(SYM)  ({ volatile uint32_t *__ptr = __builtin_assume_aligned( (&(__gp__##SYM)), 4); __ptr; })
#define IO_PTR_CVU32(SYM) ({ const volatile uint32_t *__ptr = __builtin_assume_aligned( (&(__gp__##SYM)), 4); __ptr; })
#define IO_PTR_VU16(SYM)  ({ volatile uint16_t *__ptr = __builtin_assume_aligned( (&(__gp__##SYM)), 2); __ptr; })
#define IO_PTR_CVU16(SYM) ({ const volatile uint16_t *__ptr = __builtin_assume_aligned( (&(__gp__##SYM)), 2); __ptr; })
#define IO_PTR_VU8(SYM)   ({ volatile uint8_t *__ptr = __builtin_assume_aligned( (&(__gp__##SYM)), 1); __ptr; })
#define IO_PTR_CVU8(SYM)  ({ const volatile uint8_t *__ptr = __builtin_assume_aligned( (&(__gp__##SYM)), 1); __ptr; })
*/

extern char __page_offset_0x0000;

#define IO_CREATE_SYMBOL(SYM)

#define IO_SYMBOL_PTR(SYM, TYPE, ALIGN) ({ TYPE __ptr = __builtin_assume_aligned(&__page_offset_0x0000 + (SYM), ALIGN); __ptr; })

#define IO_PTR_VU32(SYM)  ({ volatile uint32_t *__ptr = __builtin_assume_aligned(&__page_offset_0x0000 + (SYM), 4); __ptr; })
#define IO_PTR_CVU32(SYM)  ({ const volatile uint32_t *__ptr = __builtin_assume_aligned(&__page_offset_0x0000 + (SYM), 4); __ptr; })

#define IO_PTR_VU16(SYM)  ({ volatile uint16_t *__ptr = __builtin_assume_aligned(&__page_offset_0x0000 + (SYM), 2); __ptr; })
#define IO_PTR_CVU16(SYM)  ({ const volatile uint16_t *__ptr = __builtin_assume_aligned(&__page_offset_0x0000 + (SYM), 2); __ptr; })

#define IO_PTR_VU8(SYM)  ({ volatile uint8_t *__ptr = __builtin_assume_aligned(&__page_offset_0x0000 + (SYM), 1); __ptr; })
#define IO_PTR_CVU8(SYM)  ({ const volatile uint8_t *__ptr = __builtin_assume_aligned(&__page_offset_0x0000 + (SYM), 1); __ptr; })

#ifdef GPIO_CONFIG_NO_UPPER_TRASH
  #define RDIO8(PTR) \
    ({  unsigned int __value; \
        const volatile uint8_t *__pointer = (const volatile uint8_t *)(PTR); \
        asm volatile (" ldbuio %0, %1 \n"  : "=r"(__value) : "w"(*__pointer) : "memory"); __value; })
#else
  #define RDIO8(PTR)      __builtin_ldbuio(PTR)
#endif

#define RDIOS8(PTR)     __builtin_ldbio(PTR)
#define WRIO8(PTR, VAL) __builtin_stbio((PTR), (VAL))

#ifdef GPIO_CONFIG_NO_UPPER_TRASH
  #define RDIO16(PTR) \
    ({  unsigned int __value; \
        const volatile uint16_t *__pointer = (const volatile uint16_t *)(PTR); \
        asm volatile (" ldhuio %0, %1 \n"  : "=r"(__value) : "w"(*__pointer) : "memory"); __value; })

#else
  #define RDIO16(PTR)      __builtin_ldhuio((PTR))
#endif

#define RDIOS16(PTR)     __builtin_ldhio((PTR))
#define WRIO16(PTR, VAL) __builtin_sthio((PTR), (VAL))

#define RDIO32(PTR)      __builtin_ldwio((PTR))
#define WRIO32(PTR, VAL) __builtin_stwio((PTR), (VAL))

#endif /* GPIO_H_ */
