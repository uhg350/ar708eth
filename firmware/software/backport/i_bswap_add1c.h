/*
 * i_bswap_add1c.h
 *
 *  Created on: 28 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef I_BSWAP_ADD1C_H_
#define I_BSWAP_ADD1C_H_

#include <system.h>

#define BSWAP_U16(A)                    ALT_CI_BSWAP_U16(A,0)
#define BSWAP_U32(A)                    ALT_CI_BSWAP_U16(0,B)

#define FOLDU16_ADD1C_U32_U32(A, B)     ALT_CI_BSWAP_ADD1C_CIS(A,B)

#define S_BSWAP_U16(A) \
  __builtin_choose_expr(__builtin_constant_p(A), ( ( (A & 0xFF00) >> 8 ) | ( (A & 0x00FF) << 8 ) ),  (void)0 )

#endif /* I_BSWAP_ADD1C_H_ */
