/*
 * emac_rx_handler.c
 *
 *  Created on: Nov 16, 2017
 *      Author: ilynxy
 */

#include "debug.h"

#include "i_emac.h"

#include "eth2.h"
#include "global_vars.h"

#include "i_bswap_add1c.h"

#include "arpv4_handler.h"

#include "emac_rx_handler.h"

#include "arpv4_handler.h"
#include "icmpv4_handler.h"
#include "udpv4_handler.h"

void EMAC_RX_Handler(void)
{
  arpv4_refresh_table();

  size_t eth_frame_length;
  eth_frame_length = EMAC_RX_GetCurrentPacketLength();
  if (eth_frame_length == 0)
    return;

  const void *pEF = EMAC_RX_GetCurrentPacketPtr();

  const eth2_frame_header_t *pE2H = __builtin_assume_aligned((char *)pEF + 2, 4, 2);
  // const eth2_frame_header_t *pE2H = __builtin_assume_aligned(&__page_offset_0x0000 + EMAC_RXMMAP_BASE + 2, 4, 2);
  int g_rx_broadcast_packet = 0;

  uint32_t mac0 = RDIOU16(pE2H->dstMAC.u16 + 0); // pE2H->dstMAC.u16[0];
  uint32_t mac1 = RDIOU16(pE2H->dstMAC.u16 + 1); // pE2H->dstMAC.u16[1];
  uint32_t mac2 = RDIOU16(pE2H->dstMAC.u16 + 2); // pE2H->dstMAC.u16[2];

  const eth2_mac_t *pMY = &g_config.block.data.device_address.mac;

  uint32_t my0  = RDIOU16(pMY->u16 + 0); // pMY->u16[0];
  uint32_t my1  = RDIOU16(pMY->u16 + 1); // pMY->u16[1];
  uint32_t my2  = RDIOU16(pMY->u16 + 2); // pMY->u16[2];

  //unsigned is_my_mac = (my0 == mac0 && my1 == mac1 && my2 == mac2);
  //unsigned is_broadcast_mac = (mac0 == 0xFFFF && mac1 == 0xFFFF && mac2 == 0xFFFF);

  //if (!is_my_mac && !is_broadcast_mac)
  //  goto __drop_packet_and_exit;
  if (   (my0 == mac0 && my1 == mac1 && my2 == mac2)
      || (mac0 == 0xFFFF && mac1 == 0xFFFF && mac2 == 0xFFFF))
  {
    g_rx_broadcast_packet = mac0;

    uint32_t eth2_type = pE2H->ethertype;

    if      (eth2_type == S_BSWAP_U16(0x0806))
    {
      arpv4_handler();
    }
    else if (eth2_type == S_BSWAP_U16(0x0800))
    {
      icmpv4_handler();
      udpv4_handler();
    }
  }
//__drop_packet_and_exit:
  EMAC_RX_DropCurrentPacket();
}

