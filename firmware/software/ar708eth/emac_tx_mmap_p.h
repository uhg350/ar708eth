/*
 * eth_mmap_txm.h
 *
 *      Author: ilynxy
 */

#ifndef ETH_MMAP_TXM_P_H_
#define ETH_MMAP_TXM_P_H_

#include "typedefs.h"
#include "eth2.h"
#include "ipv4.h"
#include "arpv4.h"
#include "icmpv4.h"

#define CTRL_CB_TAG  0x30314243
#define CTRL_SB_TAG  0x30314253

typedef struct
{
  le_uint8_t    bm8message_source;
  le_uint8_t    u8message_label;
  le_uint8_t    u8message_offset;
  le_uint8_t    u8message_size;
  le_uint32_t   u32message_timestamp;
} _PACKED_ data_block_header_t;

typedef struct
{
  uint16_t              u16align;
  eth2_frame_header_t   eth;
  ipv4_header_t         ip;
  udpv4_header_t        udp;
  //data_block_header_t   data_block_header;
  uint32_t              arp_flags;
  uint32_t              arp_stamp;
} _PACKED_ eth2_udpv4_data_frame_header_t;

#define ARP_FLAGS_USE_CUSTOM  (0x00000001)
#define ARP_FLAGS_KNOWN       (0x00000002)

typedef struct
{
  le_uint32_t           tag;
  le_uint32_t           lun;
  le_uint16_t           command;
  le_uint16_t           id;
  le_uint32_t           status;
  le_uint32_t           args[4];
} _PACKED_ ctrl_block_header_t;

typedef struct
{
  uint16_t              u16align;
  eth2_frame_header_t   eth;
  ipv4_header_t         ip;
  udpv4_header_t        udp;
  ctrl_block_header_t   ctrl;

} _PACKED_ eth2_udpv4_ctrl_frame_header_t;

typedef struct
{
  uint32_t  segments[4];
} emac_tx_segments_t;

typedef struct
{
  emac_tx_segments_t              emac_tx_segments        _ALIGNED_(4);
  eth2_arpv4_frame_t              eth2_arpv4_request      _ALIGNED_(4);
  eth2_arpv4_frame_t              eth2_arpv4_reply        _ALIGNED_(4);
  eth2_icmpv4_frame_t             eth2_icmpv4_reply       _ALIGNED_(4);
  eth2_udpv4_ctrl_frame_header_t  eth2_udpv4_ctrl_header  _ALIGNED_(4);
  le_uint8_t                      eth2_udpv4_ctrl[1024]   _ALIGNED_(4);

  eth2_udpv4_data_frame_header_t  eth2_udpv4_data_header[CONFIG_MAXIMUM_SINKS] _ALIGNED_(4);
  data_block_header_t             data_block_header                            _ALIGNED_(4);
  le_uint8_t                      eth2_udpv4_data[2 * 200]                     _ALIGNED_(4);

} eth_tx_mmap_t;

#endif /* ETH_MMAP_TXM_P_H_ */
