/*
 * i_chipid.h
 *
 *  Created on: 11 ���. 2017 �.
 *      Author: ilynxy
 */

#ifndef I_CHIPID_H_
#define I_CHIPID_H_

#include "system.h"
#include "gpio.h"

#define CID_GetLow()      RDIOU32(IO_PTR_CVU32(CHIPID_BASE) + 0)
#define CID_GetHigh()     RDIOU32(IO_PTR_CVU32(CHIPID_BASE) + 1)

#define CID_GetID()       (((uint64_t)(CID_GetHigh()) << 32) | CID_GetLow())


#endif /* I_CHIPID_H_ */
