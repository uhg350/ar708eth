/*
 * i_lfs_button.h
 *
 *  Created on: Dec 5, 2017
 *      Author: ilynxy
 */

#ifndef I_LFS_BUTTON_H_
#define I_LFS_BUTTON_H_

#include "system.h"
#include "gpio.h"

#define LFS_BUTTON_GetState() RDIOU32(IO_PTR_CVU32(RPINS_BASE))


#endif /* I_LFS_BUTTON_H_ */
