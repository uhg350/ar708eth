/*
 * i_msticks.h
 *
 *  Created on: Nov 19, 2017
 *      Author: ilynxy
 */

#ifndef I_MSTICKS_H_
#define I_MSTICKS_H_

#include "system.h"

#define MS_GetTicks()       ALT_CI_MTICKS(0, 0)
#define MS_GetDiffTicks(x)  ALT_CI_MTICKS(x, 0)

#endif /* I_MSTICKS_H_ */
