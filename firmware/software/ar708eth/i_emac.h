/*
 * i_emac.h
 *
 *  Created on: Oct 23, 2017
 *      Author: ilynxy
 */

#ifndef I_EMAC_H_
#define I_EMAC_H_

#include "system.h"
#include "typedefs.h"

#include "gpio.h"

#define EMAC_RX_GetCurrentPacketPtr()     ((const void *)(EMAC_RXMMAP_BASE))
#define EMAC_RX_GetCurrentPacketLength()  RDIOU16(IO_PTR_CVU16(EMAC_RXMMAP_BASE))

#define EMAC_RX_DropCurrentPacket()       do { unsigned register r = EMAC_RX_GetCurrentPacketLength(); r = (r + 5) >> 2; WRIO16(IO_PTR_VU16(EMAC_RXMMAP_BASE), r); } while (0)
#define EMAC_RX_DropAllPackets()          WRIO32(IO_PTR_VU32(EMAC_RXMMAP_BASE), 0)


#define EMAC_TX_GetSGDMAState()                 RDIOU32(IO_PTR_CVU32(EMAC_CTL_BASE) + 0)
#define EMAC_TX_SetDMADescriptor(offset, size)  \
  WRIO32(IO_PTR_VU32(EMAC_CTL_BASE) + 0, (((offset) << 16) | (size)) )

#define EMAC_MODE_100M                    0
#define EMAC_MODE_10M                     2
#define EMAC_MODE_FULL_DUPLEX             4
#define EMAC_MODE_HALF_DUPLEX             0

#define EMAC_SetMode(x)                   WRIO32(IO_PTR_VU32(EMAC_CTL_BASE) + 1, x)
#define EMAC_GetMode()                    RDIOU32(IO_PTR_CVU32(EMAC_CTL_BASE) + 1)

#define EMAC_SetResetPin(x)               WRIO32(IO_PTR_VU32(EMAC_CTL_BASE) + 7, x)

#endif /* I_EMAC_H_ */
