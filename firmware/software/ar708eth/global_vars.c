/*
 * global_vars.c
 *
 *  Created on: Jul 19, 2017
 *      Author: ilynxy
 */

#include "global_vars.h"

ident_t   g_ident  _ALIGNED_(4);
config_t  g_config _ALIGNED_(4);

uint32_t  g_need_reconfig __attribute__(( section (".rwdata") )) = 0;
uint32_t  g_need_reboot   __attribute__(( section (".rwdata") )) = 0;
