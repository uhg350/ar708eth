/*
 * udpv4_handler.c
 *
 *  Created on: Nov 18, 2017
 *      Author: ilynxy
 */

#define _DEBUG
#include "debug.h"

#include "i_emac.h"
#include "udpv4.h"

#include "i_bswap_add1c.h"
#include "global_vars.h"
#include "emac_tx_mmap.h"

#include "i_crc32_ieee_802_3.h"
#include "settings_manager.h"

#include "i_arinc708_mac.h"
#include "aocf_support.h"

static
size_t is_udpv4_packet(const eth2_udpv4_frame_t *pUDP)
{
  size_t result = 0;

  if (pUDP->ip.version_length != 0x45) // TODO: handle variable length IPv4 header
    goto _exit_label;

  if (pUDP->ip.protocol != 0x11) // 0x11 -- UDP proto
    goto _exit_label;


  uint32_t my_ip     = g_config.block.data.device_address.ip.u32;
  if (
          pUDP->ip.dstIP.u32 != my_ip
       && pUDP->ip.dstIP.u32 != 0xFFFFFFFF // broadcast ping
     )
    goto _exit_label;

  uint32_t my_port   = g_config.block.data.device_address.port.u16;
  if (pUDP->udp.dstPort.u16 != my_port)
    goto _exit_label;

  size_t packet_length = EMAC_RX_GetCurrentPacketLength();
  packet_length -= (sizeof(pUDP->eth) + sizeof(uint32_t)); // eth2 header + crc32 is frame attributes

  size_t total_length = BSWAP_U16(pUDP->ip.total_length);
  if (packet_length < total_length)
    goto _exit_label;

  size_t udp_length = BSWAP_U16(pUDP->udp.length);
  packet_length -= sizeof(pUDP->ip);
  if (udp_length > packet_length)
    goto _exit_label;

  udp_length -= sizeof(pUDP->udp);

  result = udp_length;

_exit_label:

  return result;
}

//void arpv4_request(uint32_t addr);
//arpv4_request(pUDP->ip.srcIP.u32);


uint32_t g_data_size;

size_t cmd_get_ident(void)
{
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;
  p->ctrl.status = 0;

  uint32_t *ps  = (uint32_t *)(&g_ident);
  uint32_t *pse = (uint32_t *)((uint8_t *)ps + sizeof(g_ident));
  uint32_t *pd  = (uint32_t *)(eth_tx_mmap.eth2_udpv4_ctrl);

  do
  {
    *pd ++ = *ps ++;
  } while (ps < pse);

  return sizeof(g_ident);
}

size_t cmd_get_config()
{
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;
  p->ctrl.status = 0;

  uint32_t *ps  = (uint32_t *)(&g_config);
  uint32_t *pse = (uint32_t *)((uint8_t *)ps + sizeof(g_config));
  uint32_t *pd  = (uint32_t *)(eth_tx_mmap.eth2_udpv4_ctrl);

  do
  {
    *pd ++ = *ps ++;
  } while (ps < pse);

  return sizeof(g_config);
}

size_t cmd_set_ident(void)
{
  uint32_t status;
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;
  //p->ctrl.status = 0x0001; // unknown command

  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_udpv4_ctrl_frame_header_t *pCTL = __assume_aligned(pEF, 4);

  do
  {
    size_t size = g_data_size;
    status = 4; // size of ident mismatch
    if (size != sizeof(ident_t))
      break;

    const uint32_t *ps  = (uint32_t *)(pCTL + 1);
    const uint32_t *pse = (uint32_t *)((uint8_t *)ps + size);

    uint32_t crc;
    crc = CRC32_IEEE_802_3_GetInitial();

    do
    {
      uint32_t u32;
      u32 = *ps;
      crc = CRC32_IEEE_802_3_Compute32(crc, u32);
      ++ ps;
    } while (ps < pse);

    status = 5; // crc mismatch
    if (crc != CRC32_IEEE_802_3_GetCheck())
      break;

    ps  = (uint32_t *)(pCTL + 1);
    uint32_t *pd  = (uint32_t *)&g_ident;
    do
    {
      *pd ++ = *ps ++;
    } while (ps < pse);

    //void store_ident_to_nvram(void);
    store_ident_to_nvram();
    status = 0;

  } while (0);

  p->ctrl.status = status;
  return 0;
}

size_t cmd_set_config(void)
{
  uint32_t status;
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;
  //p->ctrl.status = 0x0001; // unknown command

  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_udpv4_ctrl_frame_header_t *pCTL = __assume_aligned(pEF, 4);

  do
  {
    size_t size = g_data_size;
    status = 4; // size of config mismatch
    if (size != sizeof(config_t))
      break;

    const uint32_t *ps  = (uint32_t *)(pCTL + 1);
    const uint32_t *pse = (uint32_t *)((uint8_t *)ps + size);

    uint32_t crc;
    crc = CRC32_IEEE_802_3_GetInitial();

    do
    {
      uint32_t u32;
      u32 = *ps;
      crc = CRC32_IEEE_802_3_Compute32(crc, u32);
      ++ ps;
    } while (ps < pse);

    status = 5; // crc mismatch
    if (crc != CRC32_IEEE_802_3_GetCheck())
      break;

    ps  = (uint32_t *)(pCTL + 1);
    uint32_t *pd  = (uint32_t *)&g_config;
    do
    {
      *pd ++ = *ps ++;
    } while (ps < pse);

    store_config_to_nvram();

    g_need_reconfig = 50000; // TODO: handle it properly!

    status = 0;

  } while (0);

  p->ctrl.status = status;
  return 0;
}

size_t cmd_read_page_chunk(void)
{

  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_udpv4_ctrl_frame_header_t *pCTL = __assume_aligned(pEF, 4);

  uint32_t offset = pCTL->ctrl.args[0];

  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;

  if ( (offset & 0x3FF) != 0 )
  {
    p->ctrl.status = 0x0002; // offset alignment error
    return 0;
  }

  if (offset > (FLASH_DATA_SPAN - 1024))
  {
    p->ctrl.status = 0x0003; // offset too big error
    return 0;
  }

  uint32_t *ps  = (uint32_t *)(FLASH_DATA_BASE + offset);
  uint32_t *pse = ps + 1024/sizeof(uint32_t);
  uint32_t *pd  = (uint32_t *)(eth_tx_mmap.eth2_udpv4_ctrl);

  do
  {
    *pd ++ = *ps ++;
  } while (ps < pse);

  p->ctrl.status = 0;

  return 1024;
}

size_t cmd_write_page_chunk(void)
{
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;
  //p->ctrl.status = 0x0001; // unknown command

  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_udpv4_ctrl_frame_header_t *pCTL = __assume_aligned(pEF, 4);

  uint32_t offset = pCTL->ctrl.args[0];

  if ( (offset & 0x3FF) != 0 )
  {
    p->ctrl.status = 0x0002; // offset alignment error
    return 0;
  }

  if (offset > (FLASH_DATA_SPAN - 1024))
  {
    p->ctrl.status = 0x0003; // offset too big error
    return 0;
  }

  size_t size = g_data_size;
  if (size != 1024)
  {
    p->ctrl.status = 0x0004; // size of page mismatch
    return 0;
  }

  if ( (offset & (FLASH_DATA_BYTES_PER_PAGE - 1)) == 0)
  {
    AOCF_WriteEnable();

    uint32_t paddr = offset;
    paddr &= AOCF_CONTROL_PAGE_ERASE_MASK;
    paddr /= 4;

    AOCF_ErasePage(paddr);
    aocf_wait_for_idle_and_get_status();

    AOCF_WriteDisable();
    //aocf_wait_for_idle_and_get_status();
  }

  AOCF_WriteEnable();


  const uint32_t *ps  = (uint32_t *)(pCTL + 1);
  const uint32_t *pse = ps + 1024/sizeof(uint32_t);
  uint32_t *pd        = (uint32_t *)(FLASH_DATA_BASE + offset);

  do
  {
    *pd ++ = *ps ++;

    aocf_wait_for_idle_and_get_status();

  } while (ps < pse);

  AOCF_WriteDisable();

  p->ctrl.status = 0;

  return 0;
}

size_t cmd_reboot(void)
{
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;
  p->ctrl.status = 0x0000;

  g_need_reboot = 50000;

  return 0;
}

size_t cmd_send_ar708_frame(void)
{
  uint32_t status;

  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;

  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_udpv4_ctrl_frame_header_t *pCTL = __assume_aligned(pEF, 4);

  do
  {
    status = 7; // no such line
    uint32_t line = pCTL->ctrl.args[0];
    if (line > 2)
      break;

    status = 6;
    if (AR708_TX_IsBusy())
      break;

    size_t size = g_data_size;
    status = 4; // size of fame
    if (size == 0 || size > 1024)
      break;

    const uint32_t *ps  = (uint32_t *)(pCTL + 1);
    const uint32_t *pse = (uint32_t *)((uint8_t *)ps + size);
    uint32_t *pd  = (uint32_t *)(AR708_TX_GetBuffer());

    do
    {
      *pd ++ = *ps ++;
    } while (ps < pse);

    AR708_TX_SendFrame(line, size);

    status = 0;

  } while (0);

  p->ctrl.status = status;
  return 0;
}

size_t cmd_unknown(void)
{
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;

  p->ctrl.status = 0x0001; // unknown command

  return 0;
}

size_t cmd_get_hwinfo(void)
{
  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;

  p->ctrl.status = 0x0001; // unknown command

  return 0;
}

void udpv4_handler(void)
{
  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_udpv4_frame_t *pUDP = __assume_aligned(pEF, 4);

  size_t packet_length = is_udpv4_packet(pUDP);
  if (packet_length == 0)
    return;

  if (packet_length < sizeof(ctrl_block_header_t))
    return;

  packet_length -= sizeof(ctrl_block_header_t);

  g_data_size = packet_length;

  const eth2_udpv4_ctrl_frame_header_t *pCTL = __assume_aligned(pEF, 4);

  //if (pCTL->ctrl.tag != CTRL_CB_TAG)
  //  return;

  eth2_udpv4_ctrl_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_ctrl_header;

  p->eth.dstMAC.u16[0]  = pCTL->eth.srcMAC.u16[0];
  p->eth.dstMAC.u16[1]  = pCTL->eth.srcMAC.u16[1];
  p->eth.dstMAC.u16[2]  = pCTL->eth.srcMAC.u16[2];

  p->ip.dstIP.u32       = pCTL->ip.srcIP.u32;
  p->udp.dstPort.u16    = pCTL->udp.srcPort.u16;

  p->ctrl.command       = pCTL->ctrl.command;
  p->ctrl.id            = pCTL->ctrl.id;

//
  size_t dsize;
  switch (pCTL->ctrl.command)
  {
  case 0x0000 :
    dsize = cmd_get_ident();
    break;
  case 0x0001 :
    dsize = cmd_get_config();
    break;
  case 0x0002 :
    dsize = cmd_set_ident();
    break;
  case 0x0003 :
    dsize = cmd_set_config();
    break;
  case 0x0004 :
    dsize = cmd_read_page_chunk();
    break;
  case 0x0005 :
    dsize = cmd_write_page_chunk();
    break;
  case 0x0006 :
    dsize = cmd_reboot();
    break;
  case 0x0007 :
    dsize = cmd_send_ar708_frame();
    break;
  case 0x0008 :
    dsize = cmd_get_hwinfo();
    break;

  default:
    dsize = cmd_unknown();
    break;
  }
//
  dsize += sizeof(p->ctrl); // ctrl header
  dsize += sizeof(p->udp); // udp header
  p->udp.length = BSWAP_U16(dsize);
  dsize += sizeof(p->ip);
  p->ip.total_length = BSWAP_U16(dsize);

  ++ p->ip.id;

  { // Update reply ip header checksum
    ipv4_header_t *ip = &p->ip;
    ip = __assume_aligned(ip, 4);
    ip->checksum = 0;

    asm volatile ("" ::: "memory");
    const uint32_t *pIPH = (const uint32_t *)ip;
    uint32_t c0, c1;
    c0 = FOLDU16_ADD1C_U32_U32(pIPH[0], pIPH[1]);
    c1 = FOLDU16_ADD1C_U32_U32(pIPH[2], pIPH[3]);
    //c0 |= c1 << 16;
    c0 = FOLDU16_ADD1C_U32_U32(c0, pIPH[4]);
    c0 = FOLDU16_ADD1C_U32_U32(c0, c1);

    ip->checksum = ~c0;
  }

  dsize += sizeof(p->eth);

  while (EMAC_TX_GetSGDMAState() != 0)
    ;
  eth_tx_mmap.emac_tx_segments.segments[0] = (((uint32_t)(&eth_tx_mmap.eth2_udpv4_ctrl_header.eth)) << 16) | dsize;
  //eth_tx_mmap.emac_tx_segments.segments[1] = (((uint32_t)(&eth_tx_mmap.eth2_udpv4_ctrl)) << 16) | length;
  EMAC_TX_SetDMADescriptor(0, 1);


  //DBG_MSG("UDP packet\n");
}
