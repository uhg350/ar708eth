/*
 * icmpv4_handler.c
 *
 *  Created on: Nov 18, 2017
 *      Author: ilynxy
 */

//#define _DEBUG
#include "debug.h"

#include "i_emac.h"
#include "icmpv4.h"

#include "i_bswap_add1c.h"
#include "global_vars.h"
#include "emac_tx_mmap.h"

static
size_t is_icmpv4_packet(const eth2_icmpv4_frame_t *pICMP)
{
  size_t result = 0;

  if (pICMP->ip.version_length != 0x45) // TODO: handle variable length IPv4 header
    goto _exit_label;

  uint32_t my_ip     = g_config.block.data.device_address.ip.u32;

  if (
          pICMP->ip.dstIP.u32 != my_ip
       && pICMP->ip.dstIP.u32 != 0xFFFFFFFF // broadcast ping
     )
    goto _exit_label;

  size_t packet_length = EMAC_RX_GetCurrentPacketLength();
  packet_length -= (sizeof(pICMP->eth) + sizeof(uint32_t)); // eth2 header + crc32 is frame attributes

  size_t total_length = BSWAP_U16(pICMP->ip.total_length);
  if (packet_length < total_length)
    goto _exit_label;

  result = packet_length;

_exit_label:
  return result;
}

void icmpv4_handler(void)
{
  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_icmpv4_frame_t *pICMP = __assume_aligned(pEF, 4);

  size_t packet_length = is_icmpv4_packet(pICMP);
  if (packet_length == 0)
    return;

  if (pICMP->ip.protocol != 0x01) // ICMP proto
    return;

  if (pICMP->icmp.code != 0x00) // must be 0x00
    return;

  if (pICMP->icmp.type != 0x08) // ICMP PING REQUEST
    return;

  if (packet_length > 1024)
    return;

  //__debug_break();

  // TODO: Checksum
  eth2_icmpv4_frame_t *pIH = &eth_tx_mmap.eth2_icmpv4_reply;
  pIH->eth.dstMAC.u16[0]  = pICMP->eth.srcMAC.u16[0];
  pIH->eth.dstMAC.u16[1]  = pICMP->eth.srcMAC.u16[1];
  pIH->eth.dstMAC.u16[2]  = pICMP->eth.srcMAC.u16[2];

  pIH->ip.total_length    = pICMP->ip.total_length;
  pIH->ip.dstIP           = pICMP->ip.srcIP;

  ++ pIH->ip.id;

  { // Update reply ip header checksum
    ipv4_header_t *ip = &pIH->ip;
    ip = __assume_aligned(ip, 4);

    ip->checksum = 0;
    const uint32_t *pIPH = (const uint32_t *)ip;
    uint32_t c0, c1;
    c0 = FOLDU16_ADD1C_U32_U32(pIPH[0], pIPH[1]);
    c1 = FOLDU16_ADD1C_U32_U32(pIPH[2], pIPH[3]);
    //c0 |= c1 << 16;
    c0 = FOLDU16_ADD1C_U32_U32(c0, pIPH[4]);
    c0 = FOLDU16_ADD1C_U32_U32(c0, c1);

    ip->checksum = ~c0;
  }

  size_t length = packet_length - (sizeof(pICMP->ip) + sizeof(pICMP->icmp));

  { // copy data and calculate checksum
    uint8_t *p8 = (uint8_t *)(&pICMP->icmp + 1);

    uint32_t *ps = (uint32_t *)(p8);
    uint32_t *pe = (uint32_t *)(p8 + length);

    uint32_t *pd = (uint32_t *)(eth_tx_mmap.eth2_udpv4_ctrl);

    pIH->icmp.data.u32 = pICMP->icmp.data.u32;
    uint32_t c0 = pICMP->icmp.checksum;
    c0 = FOLDU16_ADD1C_U32_U32(c0, 0x0008); // first field of icmp
    //uint32_t c0 = 0;
    pIH->icmp.checksum = c0;
    do
    {
      //uint32_t u16 = *ps ++;
      *pd ++ = *ps ++;
      //c0 = FOLDU16_ADD1C_U32_U32(c0, u16);
    } while (ps < pe);

    //pIH->icmp.checksum = ~c0;
  }

  while (EMAC_TX_GetSGDMAState() != 0)
    ;
  eth_tx_mmap.emac_tx_segments.segments[0] = (((uint32_t)(&eth_tx_mmap.eth2_icmpv4_reply.eth)) << 16) | (sizeof(eth_tx_mmap.eth2_icmpv4_reply) - 2);
  eth_tx_mmap.emac_tx_segments.segments[1] = (((uint32_t)(&eth_tx_mmap.eth2_udpv4_ctrl)) << 16) | length;
  EMAC_TX_SetDMADescriptor(0, 2);


  DBG_MSG("ICMP PING_REQUEST packet\n");
}
