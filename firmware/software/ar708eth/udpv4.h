/*
 * udpv4.h
 *
 *  Created on: 30 ����. 2017 �.
 *      Author: ilynxy
 */

#ifndef UDPV4_H_
#define UDPV4_H_

#include "eth2.h"
#include "ipv4.h"

typedef struct
{
  uint16_t            u16align;
  eth2_frame_header_t eth;
  ipv4_header_t       ip;
  udpv4_header_t      udp;
} eth2_udpv4_frame_t;

#endif /* UDPV4_H_ */
