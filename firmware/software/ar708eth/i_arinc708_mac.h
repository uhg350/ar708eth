/*
 * i_arinc708_mac.h
 *
 *  Created on: Dec 3, 2017
 *      Author: ilynxy
 */

#ifndef I_ARINC708_MAC_H_
#define I_ARINC708_MAC_H_

#include "system.h"
#include "gpio.h"

#define AR708_MAC_Configure(cb)             WRIO32(IO_PTR_VU32(AR708_MAC_CTL_BASE) + 0, cb)

#define AR708_TX_GetBuffer()                ((void *)(AR708_MAC_TXM_BASE + 0x0000))
#define AR708_TX_IsBusy()                   RDIOU32(IO_PTR_CVU32(AR708_MAC_CTL_BASE) + 8)
#define AR708_TX_SendFrame(line, size)      WRIO32(IO_PTR_VU32(AR708_MAC_CTL_BASE) + 8, size)


#define AR708_RX0_GetCurrentPacketPtr()      ((const void *)(AR708_MAC_RXM_BASE))
#define AR708_RX0_GetCurrentPacketLength()   RDIOU16(IO_PTR_CVU16(AR708_MAC_RXM_BASE))
#define AR708_RX0_DropCurrentPacket() \
  do { unsigned register r = AR708_RX0_GetCurrentPacketLength(); r = (r + 5) >> 2; WRIO16(IO_PTR_VU16(AR708_MAC_RXM_BASE), r); } while (0)
#define AR708_RX0_DropAllPackets()           WRIO32(IO_PTR_VU32(AR708_MAC_RXM_BASE), 0)

#define AR708_RX1_GetCurrentPacketPtr()      ((const void *)(AR708_MAC_RXM_BASE + 0x0800))
#define AR708_RX1_GetCurrentPacketLength()   RDIOU16(IO_PTR_CVU16(AR708_MAC_RXM_BASE + 0x0800))
#define AR708_RX1_DropCurrentPacket() \
  do { unsigned register r = AR708_RX1_GetCurrentPacketLength(); r = (r + 5) >> 2; WRIO16(IO_PTR_VU16(AR708_MAC_RXM_BASE + 0x0800), r); } while (0)
#define AR708_RX1_DropAllPackets()           WRIO32(IO_PTR_VU32(AR708_MAC_RXM_BASE + 0x0800), 0)



#endif /* I_ARINC708_MAC_H_ */
