/*
 * icmpv4_handler.h
 *
 *  Created on: Nov 18, 2017
 *      Author: ilynxy
 */

#ifndef ICMPV4_HANDLER_H_
#define ICMPV4_HANDLER_H_

void icmpv4_handler(void);

#endif /* ICMPV4_HANDLER_H_ */
