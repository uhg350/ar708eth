/*
 * debug.h
 *
 *  Created on: Nov 14, 2017
 *      Author: ilynxy
 */

#ifndef DEBUG_H_
#define DEBUG_H_

//#define _DEBUG

#define __debug_break() asm volatile (" break " ::: "memory")

#ifdef _DEBUG

#include "system.h"
#include "gpio.h"

#define DBG_PUTCHAR(c)        WRIO32(IO_PTR_VU32(JTAG_UART_BASE), c)

#define DBG_PUTSTR(s)         _DBG_PUTSTR(s)
#define DBG_ENTRY()           _DBG_ENTRY(__PRETTY_FUNCTION__)
#define DBG_LEAVE()           _DBG_LEAVE(__PRETTY_FUNCTION__)

#define DBG_SCTEXT(s)         ({ static const char p[] = s; p; })

#define DBG_MSG(s)            _DBG_PUTSTR(DBG_SCTEXT(s))


// gcc optimizer is ... f**g asshole...
//#define DBG_CMSG(c, s1, s2)   ({ const char *__s; __s = DBG_SCTEXT(s2); if (c) __s = DBG_SCTEXT(s1); __s; })
#define DBG_SEL(c, s1, s2)    ({ const char *__s = s1; asm (" bne %1,r0, . + 8\n movui %0, %2" : "+r"(__s) : "r"(c), "i"(s2) ); __s; })
//#define DBG_SEL(c, s1, s2)    ({ const char *__s = s2; if (c) __s = s1; __s; })
#define DBG_CMSG(c, s1, s2)   DBG_PUTSTR(DBG_SEL((c), DBG_SCTEXT(s1), DBG_SCTEXT(s2)))

void _DBG_PUTSTR(const char *s);
void _DBG_ENTRY(const char *s);
void _DBG_LEAVE(const char *s);

#else

#define DBG_PUTCHAR(c)
#define DBG_PUTSTR(x)
#define DBG_ENTRY()
#define DBG_LEAVE()
#define DBG_MSG(s)
#define DBG_CMSG(c, s1, s2)

#endif


#endif /* DEBUG_H_ */
