/*
 * config.h
 *
 *  Created on: Jul 19, 2017
 *      Author: ilynxy
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define CONFIG_MAJOR  1
#define CONFIG_MINOR  0
#define CONFIG_PATCH  0

#ifndef CONFIG_MAJOR
#define CONFIG_MAJOR  1
#endif

#ifndef CONFIG_MINOR
#define CONFIG_MINOR  0
#endif

#ifndef CONFIG_PATH
#define CONFIG_PATCH  0
#endif

#if (CONFIG_MAJOR == 1) && (CONFIG_MINOR == 0) && (CONFIG_PATCH == 0)

  #define CONFIG_MAXIMUM_SINKS    10
  #define CONFIG_MAXIMUM_FILTERS  4

#endif

#define IDENT_VERSION_MAJOR   CONFIG_MAJOR
#define IDENT_VERSION_MINOR   CONFIG_MINOR
#define IDENT_VERSION_PATCH   CONFIG_PATCH

#define DEVICE_VERSION_MAJOR  1
#define DEVICE_VERSION_MINOR  0
#define DEVICE_VERSION_PATCH  1

#define DEVICE_SERIAL_NUMBER  1


#endif /* CONFIG_H_ */
