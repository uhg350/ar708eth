/*
 * ar708eth.c
 *
 *  Created on: Jul 2, 2017
 *      Author: ilynxy
 */

#if 1
#include "debug.h"

#include "settings_manager.h"
#include "emac_phy.h"
#include "emac_rx_handler.h"

#include "emac_tx_mmap_manager.h"

#include "gpio.h"

#include "i_emac.h"

#include "global_vars.h"

#include "ar708_handler.h"

int main(void)
{
  DBG_ENTRY();

  EMAC_SetResetPin(1);
  for (size_t i = 0; i < 100; ++ i)
    asm volatile ( " nop ");

  EMAC_SetResetPin(0);
  for (size_t i = 0; i < 10000000; ++ i)
    asm volatile ( " nop ");

  load_ident_from_nvram();
  load_config_from_nvram();

  etx_update_preallocated_structs();
  EMAC_PHY_Configure_CD();


  EMAC_RX_DropAllPackets();


  //EMAC_PHY_Configure(EMAC_PHY_CONFIG_AN_10_100_FD);
  //EMAC_PHY_Configure(EMAC_PHY_CONFIG_100_FD);

  for (;;)
  {
    uint32_t rcfg = g_need_reconfig;
    if (rcfg != 0)
    {
      -- rcfg;
      g_need_reconfig = rcfg;
      if (rcfg == 0)
      {
        etx_update_preallocated_structs();
        EMAC_PHY_Configure_CD();
      }
    }

    uint32_t rst = g_need_reboot;
    if (rst != 0)
    {
      -- rst;
      g_need_reboot = rst;
      if (rst == 0)
      {
        // Trigger reconfig
        WRIO32(IO_PTR_VU32(BOOT_MGR_BASE) + 0, 0x00000001);
      }
    }

    EMAC_PHY_HandleLinkStates();
    EMAC_RX_Handler();

    ar708_handler();
  }

  __builtin_unreachable();
}
#endif

#if 0

#include "i_emac.h"
#include "i_smi.h"

#define _DEBUG
#include "debug.h"

#include "emac_phy.h"

struct
{
  uint32_t  segments[16];

  uint8_t   test_area[256];

} txmmap __attribute__((section(".emac_txmmap"))) ;

void Configure_DP83848K(void)
{

  DBG_MSG("Configure PHY begin...\n");

  SMI_SetPHYAddress(0);

  SMI_ReadRegister(0x04);
  DBG_MSG("Write to reg0x04...");
  SMI_WriteRegister(0x04, 0x01e1);

  //SMI_WriteRegister(0x04, 0x0181);
  //SMI_WriteRegister(0x04, 0x0061);
  DBG_MSG("done\n");

  /*
  __debug_break();

  SMI_ReadRegister(0x01);
  __debug_break();

  SMI_ReadRegister(0x19);
  __debug_break();
*/
  DBG_MSG("Write to reg0x19...");
  SMI_WriteRegister(0x19, 0x8000);
  DBG_MSG("done\n");

  DBG_MSG("Write to reg0x00...");
  SMI_WriteRegister(0x00, 0x1200);
  DBG_MSG("done\n");

  DBG_MSG("Configure PHY done...\n");
}

uint32_t g_EMAC_LINK_STATE = 0;

uint32_t g_SMI_TO = 0;

void handle_EMAC_events(void)
{
/*
  ++ g_SMI_TO;
  if (g_SMI_TO < 10000)
    return;

  g_SMI_TO = 0;
*/
  uint32_t reg0x01;
  reg0x01 = SMI_ReadRegister(0x01); // Basic mode status register

  //uint32_t reg0x10 = SMI_ReadRegister(0x10);

  uint32_t link_status       = (reg0x01 & 0x0024);
  uint32_t prev_link_status  = g_EMAC_LINK_STATE;

  if (link_status != prev_link_status)
  {
    //dbg_putstr("\n");
    g_EMAC_LINK_STATE = link_status;

    DBG_MSG("Link state changed: ");


    uint32_t link_up = (reg0x01 & 0x0004);
    uint32_t autoneg = (reg0x01 & 0x0020);

    if (autoneg)
      DBG_MSG("(autoneg) ");

    if (link_up)
    {
//      __debug_break();

      if (autoneg)
      {
      uint32_t reg0x10 = SMI_ReadRegister(0x10);
      uint32_t mode_status   = (reg0x10 & 0x0006);


      if (reg0x10 & 0x0002)
        DBG_MSG("10M ");
      else
        DBG_MSG("100M ");

      if (reg0x10 & 0x0004)
        DBG_MSG("FullDuplex ");
      else
        DBG_MSG("HalfDuplex ");

      if (reg0x10 & 0x4000)
        DBG_MSG("MDI-X ");


      EMAC_SetMode(mode_status);
      EMAC_GetMode();

      SMI_WriteRegister(0x18, 0x0000); // LEDCR, Normal operation
      // SMI_WriteRegister(0x18, 0x0024); // LEDCR, Disable SPEED LED (too bright for debug =)

      EMAC_RX_DropAllPackets();

      DBG_MSG("up\n");
      }
    }
    else
    {
      //dbg_putstr("disbale LEDs\n");
      SMI_WriteRegister(0x18, 0x0036); // LEDCR, drive SPEED and LINK to 0

      DBG_MSG("down\n");
    }
  }
}

int main(void)
{
  DBG_MSG("Entry to main(void)\n");

  //for (;;)

  {
    SMI_ReadRegister(0x01);
    SMI_ReadRegister(0x10);
  }

  g_EMAC_LINK_STATE = 0;

  for (size_t i = 0; i < 256; ++ i)
    txmmap.test_area[i] = (uint8_t)i;

  uint32_t reg0x00;
  do
  {
    reg0x00 = SMI_ReadRegister(0x00);
  } while ( (reg0x00 & 0x8000) != 0 );


  Configure_DP83848K();

  EMAC_RX_DropAllPackets();

  size_t k = 0;

  for (;;)
  {
    //dbg_putstr("Handle events\n");
    //handle_EMAC_events();
    EMAC_PHY_HandleLinkStates();
/*
    txmmap.segments[0] = (((uint32_t)(txmmap.test_area + (k & 0x3F))) << 16) | (( (k>>2) & 0x3F) + 64);
    //txmmap.segments[0] = (((uint32_t)(txmmap.test_area)) << 16) | (7);
    EMAC_TX_SetDMADescriptor(0, 1);
    while (EMAC_TX_GetSGDMAState() != 0)
      ;
    ++ k;
*/

    //__debug_break();
/*
    txmmap.segments[0] = (((uint32_t)(txmmap.test_area)) << 16) | 64;
    EMAC_TX_SetDMADescriptor(0, 1);
    while (EMAC_TX_GetSGDMAState() != 0)
      ;
    __debug_break();

    txmmap.segments[0] = (((uint32_t)(txmmap.test_area)) << 16) | 65;
    EMAC_TX_SetDMADescriptor(0, 1);
    while (EMAC_TX_GetSGDMAState() != 0)
      ;
    __debug_break();

    txmmap.segments[0] = (((uint32_t)(txmmap.test_area + 1)) << 16) | 63;
    EMAC_TX_SetDMADescriptor(0, 1);
    while (EMAC_TX_GetSGDMAState() != 0)
      ;
    __debug_break();

    txmmap.segments[0] = (((uint32_t)(txmmap.test_area + 0)) << 16) | 32;
    txmmap.segments[1] = (((uint32_t)(txmmap.test_area + 16)) << 16) | 32;
    EMAC_TX_SetDMADescriptor(0, 2);
    while (EMAC_TX_GetSGDMAState() != 0)
      ;
    __debug_break();
*/


    unsigned int frame_length = EMAC_RX_GetCurrentPacketLength();
    if (frame_length != 0)
    {
      //__debug_break();
      DBG_MSG("Packet received\n");
      EMAC_RX_DropCurrentPacket();
    }

  }

  __builtin_unreachable();
}
#endif
