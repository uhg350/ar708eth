/*
 * i_smi.h
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef I_SMI_H_
#define I_SMI_H_

#include "system.h"
#include "gpio.h"

#define ddd()
//for(int ___u = 0; ___u < 1000; ++ ___u) asm volatile (" nop ");

#define SMI_SetPHYAddress(x)      WRIO32(IO_PTR_VU32(SMI_BASE), x)
#define SMI_GetPHYAddress()       RDIOU32(IO_PTR_CVU32(SMI_BASE))

#define SMI_ReadRegister(ra)      ({ ddd(); RDIOU16(IO_PTR_CVU16(SMI_BASE) + ra); })
#define SMI_WriteRegister(ra, x)  ({ ddd(); WRIO16(IO_PTR_VU16(SMI_BASE) + ra, x); })

#endif /* I_SMI_H_ */
