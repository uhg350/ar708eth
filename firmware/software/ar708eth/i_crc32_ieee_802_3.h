/*
 * i_crc32_iee802_3.h
 *
 *  Created on: 26 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef I_CRC32_IEE802_3_H_
#define I_CRC32_IEE802_3_H_

#include "system.h"

#define CRC32_IEEE_802_3_GetInitial()         (0x00000000)
#define CRC32_IEEE_802_3_GetCheck()           (0x2144DF1C)

#define CRC32_IEEE_802_3_Compute8(crc, u8)    ALT_CI_CRC32_IEEE_802_3(0, crc, u8)
#define CRC32_IEEE_802_3_Compute16(crc, u16)  ALT_CI_CRC32_IEEE_802_3(1, ALT_CI_CRC32_IEEE_802_3(0, crc, u16), u16)
#define CRC32_IEEE_802_3_Compute32(crc, u32)  ALT_CI_CRC32_IEEE_802_3(3, ALT_CI_CRC32_IEEE_802_3(2, ALT_CI_CRC32_IEEE_802_3(1, ALT_CI_CRC32_IEEE_802_3(0, crc, u32), u32), u32), u32)

#endif /* I_CRC32_IEE802_3_H_ */
