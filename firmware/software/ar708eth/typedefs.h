/*
 * typedefs.h
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#include <stddef.h>
#include <stdint.h>
#include <assert.h>

typedef uint8_t   le_uint8_t;
typedef uint16_t  le_uint16_t;
typedef uint32_t  le_uint32_t;

typedef uint8_t   be_uint8_t;
typedef uint16_t  be_uint16_t;
typedef uint32_t  be_uint32_t;

#define _PACKED_      __attribute__((packed))
#define _ALIGNED_(x)  __attribute__((aligned(x)))

#define ASSERT_SIZE_ALIGNED(s, a)       static_assert( (sizeof(s) % (a)) == 0, "sizeof(" #s ") is not multiple of " #a);
#define ASSERT_OFFSET_ALIGNED(s, m, a)  static_assert( (offsetof(s,m) % (a)) == 0, "offsetof(" #s "," #m ") is not multiple of " #a);

#define SIZE_OF_ARRAY(x) (sizeof (x) / sizeof (x)[0])

#endif /* TYPEDEFS_H_ */
