/*
 * icmpv4.h
 *
 *  Created on: Nov 18, 2017
 *      Author: ilynxy
 */

#ifndef ICMPV4_H_
#define ICMPV4_H_

#include "eth2.h"
#include "ipv4.h"

typedef struct
{
  uint16_t            u16align;
  eth2_frame_header_t eth;
  ipv4_header_t       ip;
  icmpv4_header_t     icmp;
} eth2_icmpv4_frame_t;

#endif /* ICMPV4_H_ */
