/*
 * eth_mmap_txm_manager.c
 *
 *  Created on: Jul 29, 2017
 *      Author: ilynxy
 */

#include "emac_tx_mmap.h"
#include "global_vars.h"
#include "i_bswap_add1c.h"
#include "gpio.h"

#include "arpv4.h"

#include "i_msticks.h"

inline
void etx_update_ip_header_checksum_a4(ipv4_header_t *ip)
{
  ip = __assume_aligned(ip, 4);

  ip->checksum = 0;
  const uint32_t *pIPH = (const uint32_t *)ip;
  uint32_t c0, c1;
  c0 = FOLDU16_ADD1C_U32_U32(pIPH[0], pIPH[1]);
  c1 = FOLDU16_ADD1C_U32_U32(pIPH[2], pIPH[3]);
  //c0 |= c1 << 16;
  c0 = FOLDU16_ADD1C_U32_U32(c0, pIPH[4]);
  c0 = FOLDU16_ADD1C_U32_U32(c0, c1);

  ip->checksum = ~c0;
}

void etx_update_preallocated_structs(void)
{
  const config_t *pCFG = &g_config;

  uint32_t mac0 = pCFG->block.data.device_address.mac.u16[0];
  uint32_t mac1 = pCFG->block.data.device_address.mac.u16[1];
  uint32_t mac2 = pCFG->block.data.device_address.mac.u16[2];
  uint32_t ip   = pCFG->block.data.device_address.ip.u32;
  uint32_t port = pCFG->block.data.device_address.port.u16;

  // ARP reply and request
  {
    eth2_arpv4_frame_t *const pRPL = &eth_tx_mmap.eth2_arpv4_reply;
    eth2_arpv4_frame_t *const pREQ = &eth_tx_mmap.eth2_arpv4_request;

    pRPL->eth.srcMAC.u16[0]     = mac0;
    pRPL->eth.srcMAC.u16[1]     = mac1;
    pRPL->eth.srcMAC.u16[2]     = mac2;
    pRPL->arp.sender.mac.u16[0] = mac0;
    pRPL->arp.sender.mac.u16[1] = mac1;
    pRPL->arp.sender.mac.u16[2] = mac2;
    pRPL->arp.sender.ip.u32     = ip;

    pREQ->eth.srcMAC.u16[0]     = mac0;
    pREQ->eth.srcMAC.u16[1]     = mac1;
    pREQ->eth.srcMAC.u16[2]     = mac2;
    pREQ->arp.sender.mac.u16[0] = mac0;
    pREQ->arp.sender.mac.u16[1] = mac1;
    pREQ->arp.sender.mac.u16[2] = mac2;
    pREQ->arp.sender.ip.u32     = ip;
  }

  // ICMP reply
  {
    eth2_icmpv4_frame_t *const pICMP = &eth_tx_mmap.eth2_icmpv4_reply;
    pICMP->eth.srcMAC.u16[0]    = mac0;
    pICMP->eth.srcMAC.u16[1]    = mac1;
    pICMP->eth.srcMAC.u16[2]    = mac2;
    pICMP->ip.srcIP.u32         = ip;
  }

  {
    eth2_udpv4_ctrl_frame_header_t  *pUDP = &eth_tx_mmap.eth2_udpv4_ctrl_header;

    pUDP->eth.srcMAC.u16[0]  = mac0;
    pUDP->eth.srcMAC.u16[1]  = mac1;
    pUDP->eth.srcMAC.u16[2]  = mac2;
    pUDP->ip.srcIP.u32       = ip;
    pUDP->udp.srcPort.u16    = port;

    pUDP->ctrl.lun = g_ident.block.data.serial.number;

    for (size_t i = 0; i < CONFIG_MAXIMUM_SINKS; ++ i)
    {
      eth2_udpv4_data_frame_header_t *p = &eth_tx_mmap.eth2_udpv4_data_header[i];
      sink_config_t *ps = &g_config.block.data.sinks[i];

      if (ps->flags & SINK_CONFIG_FLAG_USE_CUSTOM_SRC)
      {
        p->eth.srcMAC.u16[0]    = ps->src.mac.u16[0];
        p->eth.srcMAC.u16[1]    = ps->src.mac.u16[1];
        p->eth.srcMAC.u16[2]    = ps->src.mac.u16[2];
        p->ip.srcIP.u32         = ps->src.ip.u32;
        p->udp.srcPort.u16      = ps->src.port.u16;
      }
      else
      {
        p->eth.srcMAC.u16[0]    = mac0;
        p->eth.srcMAC.u16[1]    = mac1;
        p->eth.srcMAC.u16[2]    = mac2;
        p->ip.srcIP.u32         = ip;
        p->udp.srcPort.u16      = port;
      }

      p->eth.dstMAC.u16[0]    = ps->dst.mac.u16[0];
      p->eth.dstMAC.u16[1]    = ps->dst.mac.u16[1];
      p->eth.dstMAC.u16[2]    = ps->dst.mac.u16[2];
      if (ps->flags & SINK_CONFIG_FLAG_USE_CUSTOM_DST_MAC)
      {
        p->arp_flags = ARP_FLAGS_KNOWN | ARP_FLAGS_USE_CUSTOM;
      }
      p->arp_stamp = MS_GetTicks();

      p->ip.dstIP.u32         = ps->dst.ip.u32;
      p->udp.dstPort.u16      = ps->dst.port.u16;

      p->eth.ethertype        = S_BSWAP_U16(0x0800);
      p->ip.version_length    = 0x45;   // version 4, length 5 * 4 octets = 20
      p->ip.type_of_service   = 0x00;
      p->ip.id                = 0x0000;
      p->ip.offset_flags      = S_BSWAP_U16(0x4000); // don't fragment, offset = 0
      p->ip.ttl               = 64; // default TTL is 64
      p->ip.protocol          = 0x11; // UDP protocol
   }

    //etx_update_ip_header_checksum_a4(&pUDP->ip);
  }
}
