/*
 * settings_mmap.h
 *
 *  Created on: 27 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef SETTINGS_MMAP_H_
#define SETTINGS_MMAP_H_

#include "system.h"
#include "structs.h"

//#define _FLASH_MMAP_SECTION_ __attribute__ ((section (".flash_data"), aligned(0x1000)))
//#define _DEFAULT_SETTINGS_MMAP_SECTION_ __attribute__ ((section (".rodata"), aligned(4)))
#define _DEFAULT_SETTINGS_MMAP_SECTION_
//__attribute__ ((section (".rodata")))
typedef struct
{
  uint8_t u8[FLASH_DATA_BYTES_PER_PAGE];
} flash_page_t;

typedef struct
{
  union
  {
    ident_t       current_ident;
    flash_page_t  page0;
  };

  union
  {
    config_t      current_config;
    flash_page_t  page1;
  };
} flash_content_t;

typedef struct
{
  ident_t       default_ident;
  config_t      default_config;
} default_settings_t;

//extern flash_content_t flash_content _FLASH_MMAP_SECTION_;
//#define flash_content ((flash_content_t *)(FLASH_DATA_BASE))
static flash_content_t * const flash_content = (flash_content_t *)(FLASH_DATA_BASE);
extern default_settings_t default_settings _ALIGNED_(4) _DEFAULT_SETTINGS_MMAP_SECTION_;

#endif /* SETTINGS_MMAP_H_ */
