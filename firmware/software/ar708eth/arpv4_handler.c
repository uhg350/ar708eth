/*
 * arpv4_handler.c
 *
 *  Created on: Aug 2, 2017
 *      Author: ilynxy
 */

#define _DEBUG
#include "debug.h"

#include "i_emac.h"
#include "i_bswap_add1c.h"
#include "global_vars.h"

#include "arpv4.h"

#include "emac_tx_mmap.h"

#include "i_msticks.h"

static
int is_arpv4_frame(const eth2_arpv4_frame_t *pARP)
{

  int result = 0;

//  if (pARP->eth.ethertype     != S_BSWAP_U16(0x0806)) // ARP EtherType
//    goto _exit_label;

  if (pARP->arp.hardware_type != S_BSWAP_U16(0x0001)) // Ethernet is 1
    goto _exit_label;

  if (pARP->arp.protocol_type != S_BSWAP_U16(0x0800)) // IPv4 is 0x0800
    goto _exit_label;

  if (pARP->arp.hardware_size != 6) // 6 octets for Ethernet address
    goto _exit_label;

  if (pARP->arp.protocol_size != 4) // 4 octets for IPv4 address
    goto _exit_label;
/*
  if (pARP->arp.opcode != S_BSWAP_U16(0x0001) &&
      pARP->arp.opcode != S_BSWAP_U16(0x0002)) // 1 for ARP request, 2 for ARP reply
    goto _exit_label;

  uint32_t target_ip = pARP->arp.target.ip.u32;
  if (target_ip != 0xFFFFFFFF) // broadcast
  {
    uint32_t my_ip     = g_config.block.data.device_address.ip.u32;
    if (target_ip != my_ip)
      goto _exit_label;
  }
*/
  result = 1;

_exit_label:

  return result;
}

void arpv4_request(uint32_t addr)
{
  eth2_arpv4_frame_t *pREQ = __assume_aligned(&eth_tx_mmap.eth2_arpv4_request, 4);
  pREQ->arp.target.ip.u32 = addr;

  while (EMAC_TX_GetSGDMAState() != 0)
    ;

  eth_tx_mmap.emac_tx_segments.segments[0] = (((uint32_t)(&eth_tx_mmap.eth2_arpv4_request.eth)) << 16) | (sizeof(eth_tx_mmap.eth2_arpv4_request) - 2);
  EMAC_TX_SetDMADescriptor(0, 1);

  while (EMAC_TX_GetSGDMAState() != 0)
    ;
}

// 60 seconds poll period
#define ARP_POLL_PERIOD   (1000 * 60)

uint32_t g_arp_stamp = -1;

void arpv4_refresh_table(void)
{
  uint32_t stamp = MS_GetTicks();
  if ( ( g_arp_stamp != -1 ) && ( (stamp - g_arp_stamp) < ARP_POLL_PERIOD ) )
    return;

  g_arp_stamp = stamp;

  for (size_t k = 0; k < CONFIG_MAXIMUM_SINKS; ++ k)
  {
    if (    (g_config.block.data.sinks[k].flags & SINK_CONFIG_FLAG_ENABLED)
        &&  ( (g_config.block.data.sinks[k].flags & SINK_CONFIG_FLAG_USE_CUSTOM_DST_MAC) == 0) )
    {
      uint32_t addr = g_config.block.data.sinks[k].dst.ip.u32;
      arpv4_request(addr);
    }
  }
}

void arpv4_handler(void)
{
  const void *pEF = EMAC_RX_GetCurrentPacketPtr();
  const eth2_arpv4_frame_t *pARP = __assume_aligned(pEF, 4);

  if (!is_arpv4_frame(pARP))
    return;

  uint32_t target_ip = pARP->arp.target.ip.u32;
  uint32_t sender_ip = pARP->arp.sender.ip.u32;
  uint32_t my_ip     = g_config.block.data.device_address.ip.u32;

  if      (pARP->arp.opcode == S_BSWAP_U16(0x0001) && (target_ip == my_ip))
  {
    eth2_arpv4_frame_t *pRPL = __assume_aligned(&eth_tx_mmap.eth2_arpv4_reply, 4);

    pRPL->eth.dstMAC.u16[0]     = pARP->eth.srcMAC.u16[0];
    pRPL->arp.target.mac.u16[0] = pARP->eth.srcMAC.u16[0];
    pRPL->eth.dstMAC.u16[1]     = pARP->eth.srcMAC.u16[1];
    pRPL->arp.target.mac.u16[1] = pARP->eth.srcMAC.u16[1];
    pRPL->eth.dstMAC.u16[2]     = pARP->eth.srcMAC.u16[2];
    pRPL->arp.target.mac.u16[2] = pARP->eth.srcMAC.u16[2];

    pRPL->arp.target.ip         = pARP->arp.sender.ip;

    while (EMAC_TX_GetSGDMAState() != 0)
      ;
    eth_tx_mmap.emac_tx_segments.segments[0] = (((uint32_t)(&eth_tx_mmap.eth2_arpv4_reply.eth)) << 16) | (sizeof(eth_tx_mmap.eth2_arpv4_reply) - 2);
    EMAC_TX_SetDMADescriptor(0, 1);


    DBG_MSG("ARP REQUESTing my MAC\n"); // ARP REQUESTing my MAC
  }
  else if (pARP->arp.opcode == S_BSWAP_U16(0x0002) && (target_ip == my_ip))
  {
    eth2_udpv4_data_frame_header_t *pDFH  = __assume_aligned(&eth_tx_mmap.eth2_udpv4_data_header[0], 4);
    eth2_udpv4_data_frame_header_t *pDFHe = pDFH + CONFIG_MAXIMUM_SINKS;

    do
    {
      if (pDFH->ip.dstIP.u32 == pARP->arp.sender.ip.u32)
      {
        if ( (pDFH->arp_flags & ARP_FLAGS_USE_CUSTOM) == 0)
        {
          uint16_t *p16 = pDFH->eth.dstMAC.u16;
          p16[0] = pARP->arp.sender.mac.u16[0];
          p16[1] = pARP->arp.sender.mac.u16[1];
          p16[2] = pARP->arp.sender.mac.u16[2];

          pDFH->arp_flags |= ARP_FLAGS_KNOWN;
        }
        //pDFH->eth.dstMAC.u16[0] = pARP->arp.sender.mac.u16[0];
        //pDFH->eth.dstMAC.u16[1] = pARP->arp.sender.mac.u16[1];
        //pDFH->eth.dstMAC.u16[2] = pARP->arp.sender.mac.u16[2];
        break;
      }

      ++ pDFH;
    } while (pDFH < pDFHe);

    DBG_MSG("ARP REPLY to my request\n"); // ARP REPLY to my request (if any?)
  }
  else if (pARP->arp.opcode == S_BSWAP_U16(0x0002) && (target_ip == sender_ip))
  {
    DBG_MSG("ARP GRATUITOUS\n"); // ARP GRATUITOUS (update cache)
  }
}
