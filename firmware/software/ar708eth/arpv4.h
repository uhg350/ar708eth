/*
 * arpv4.h
 */

#ifndef ARPV4_H_
#define ARPV4_H_

#include "config.h"
#include "typedefs.h"

#include "eth2.h"
#include "ipv4.h"

typedef struct
{
  eth2_mac_t      mac;
  ipv4_address_t  ip;
} _PACKED_ arpv4_node_t;

typedef struct
{
  be_uint16_t     hardware_type;
  be_uint16_t     protocol_type;
  be_uint8_t      hardware_size;
  be_uint8_t      protocol_size;
  be_uint16_t     opcode;

  arpv4_node_t    sender;
  arpv4_node_t    target;

} _PACKED_ arpv4_packet_t;

typedef struct
{
  uint16_t            u16align;
  eth2_frame_header_t eth;
  arpv4_packet_t      arp;
} eth2_arpv4_frame_t;

#endif /* ARPV4_H_ */
