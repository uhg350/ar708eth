/*
 * emac_phy.c
 *
 *  Created on: Nov 14, 2017
 *      Author: ilynxy
 */

#include "i_emac.h"
#include "i_smi.h"
#include "i_msticks.h"
#include "emac_phy.h"

#include "global_vars.h"

#define _DEBUG
#include "debug.h"

uint32_t g_EMAC_PHY_reg0x00   = 0;
uint32_t g_EMAC_PHY_reg0x01   = -1;
uint32_t g_EMAC_PHY_pollstamp = 0;

// milliseconds
#define EMAC_PHY_POLL_PERIOD  200

// WORKAROUND: gcc optimizer for nios2 currently is... uhg... sucks.
//#define SET_BITS(r, c) r |= c
#define SET_BITS(r, c) asm (" ori %0, %0, %1 " : "+r" (r) : "i"(c) )

void EMAC_PHY_Configure_CD(void)
{
  EMAC_PHY_Configure(g_config.block.data.device_e100mode);
}

void EMAC_PHY_Configure(uint32_t config)
{
  DBG_ENTRY();

  uint32_t reg0x00 = 0x0000;
  uint32_t reg0x04 = 0x0001; // Protocol selection bits: IEEE 802.3
  uint32_t reg0x19 = 0x0000;

  unsigned auto100M   = config & EMAC_PHY_CONFIG_AN100M;
  unsigned auto10M    = config & EMAC_PHY_CONFIG_AN10M;
  unsigned fullduplex = config & EMAC_PHY_CONFIG_FULLDUPLEX;
  unsigned force100M  = config & EMAC_PHY_CONFIG_FORCE100M;
  unsigned mdix       = config & EMAC_PHY_CONFIG_MDI_X;

  if (auto100M)
  {
    SET_BITS(reg0x04, 0x0080); // Enable AN100M-HD
    if (fullduplex)
      SET_BITS(reg0x04, 0x0100); // Enable AN100M-FD

    SET_BITS(reg0x00, 0x1200); // Enable Auto-negotiation and Restart Auto-negotiation
    SET_BITS(reg0x19, 0x8000); // Enable Auto-MDIX
  }

  if (auto10M)
  {
    SET_BITS(reg0x04, 0x0020); // Enable AN10M-HD
    if (fullduplex)
      SET_BITS(reg0x04, 0x0040); // Enable AN10M-FD

    SET_BITS(reg0x00, 0x1200); // Enable Auto-negotiation and Restart Auto-negotiation
    SET_BITS(reg0x19, 0x8000); // Enable Auto-MDIX
  }

  if (fullduplex)
  {
    SET_BITS(reg0x00, 0x0100); // Duplex Mode
  }

  if (force100M)
  {
    SET_BITS(reg0x00, 0x2000); // Speed Select
  }

  if (mdix)
  {
    SET_BITS(reg0x19, 0x4000); // Force MDIX
  }

  SMI_WriteRegister(0x19, reg0x19);
  SMI_WriteRegister(0x04, reg0x04);
  g_EMAC_PHY_reg0x00 = reg0x00;
  SMI_WriteRegister(0x00, reg0x00);

  DBG_LEAVE();
}

static void DUMP_PHY_regs(uint32_t reg0x01, uint32_t reg0x10)
{
#ifdef _DEBUG
  DBG_MSG("reg0x01 | reg0x10: ");

  if (reg0x01 & 0x0020)
    DBG_MSG("negotiated ");

  if (reg0x01 & 0x0010)
    DBG_MSG("remote ");

  if (reg0x01 & 0x0002)
    DBG_MSG("jabber ");

  DBG_CMSG((reg0x01 & 0x0004), "up ", "down ");
  if (reg0x01 & 0x0004)
  {
    DBG_SEL((reg0x10 & 0x0002), "| 10M", "| 100M");
    DBG_CMSG((reg0x10 & 0x0002), "| 10M", "| 100M");
    DBG_CMSG((reg0x10 & 0x0004), "-FD", "-HD");
    if (reg0x10 & 0x4000)
      DBG_MSG("-X");
  }

  DBG_PUTCHAR('\n');

#endif
}

/*
static void EMAC_PHY_LinkUp(void)
{

}

static void EMAC_PHY_LinkDown(void)
{

}
*/

extern uint32_t g_arp_stamp;

void EMAC_PHY_HandleLinkStates(void)
{
  uint32_t stamp = MS_GetTicks();
  if ((stamp - g_EMAC_PHY_pollstamp) < EMAC_PHY_POLL_PERIOD)
    return;
  g_EMAC_PHY_pollstamp = stamp;

  uint32_t reg0x01; // Basic mode status register
  reg0x01 = SMI_ReadRegister(0x01);

  if (reg0x01 != g_EMAC_PHY_reg0x01)
  {
    g_EMAC_PHY_reg0x01 = reg0x01;

    uint32_t reg0x00 = g_EMAC_PHY_reg0x00;
    uint32_t reg0x10 = SMI_ReadRegister(0x10);

    unsigned link_valid   = reg0x01 & 0x0004;
    unsigned aneg_done    = reg0x01 & 0x0020;
    unsigned aneg_enabled = reg0x00 & 0x1000;

    if (link_valid && (aneg_done || !aneg_enabled))
    {
      // EMAC_PHY_LinkUp();
      SMI_WriteRegister(0x18, 0x0000); // LEDCR, Normal operation
      //DBG_MSG("Link up, ");
      g_arp_stamp = -1;
    }
    else
    {
      // EMAC_PHY_LinkDown();
      SMI_WriteRegister(0x18, 0x0036); // LEDCR, turn off SPEED and LINK leds
      //DBG_MSG("Link down, ");
    }

    DUMP_PHY_regs(reg0x01, reg0x10);
  }
}
