/*
 * build_stamp.h
 *      Author: ilynxy
 */

#ifndef BUILD_STAMP_H_
#define BUILD_STAMP_H_


#define BUILD_YEAR \
    ( \
        (__DATE__[ 7] - '0') * 1000 + \
        (__DATE__[ 8] - '0') *  100 + \
        (__DATE__[ 9] - '0') *   10 + \
        (__DATE__[10] - '0') \
    )

#define BUILD_MONTH \
  ( \
    (__DATE__[0] == 'D') ? 12 : \
    (__DATE__[0] == 'N') ? 11 : \
    (__DATE__[0] == 'O') ? 10 : \
    (__DATE__[0] == 'S') ?  9 : \
    (__DATE__[0] == 'A') ? \
      ( (__DATE__[1] == 'u') ? 8 : 4 ) : \
    (__DATE__[0] == 'J') ? \
      ( (__DATE__[1] == 'a') ? 1 : \
        ( (__DATE__[2] == 'n') ? 6 : 7 ) ) : \
    (__DATE__[0] == 'M') ? \
      ( (__DATE__[2] == 'y') ? 5 : 3 ) : \
    (__DATE__[0] == 'F') ? 2 : \
    0 \
  )

#define BUILD_DAY \
    ( \
        ((__DATE__[4] >= '0') ? (__DATE__[4] - '0') * 10 : 0) + \
        (__DATE__[5] - '0') \
    )

#define BUILD_HOUR ((__TIME__[0] - '0') * 10 + __TIME__[1] - '0')
#define BUILD_MIN  ((__TIME__[3] - '0') * 10 + __TIME__[4] - '0')
#define BUILD_SEC  ((__TIME__[6] - '0') * 10 + __TIME__[7] - '0')

#endif /* BUILD_STAMP_H_ */
