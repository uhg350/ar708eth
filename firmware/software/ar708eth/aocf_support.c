/*
 * aocf_support.c
 *
 *  Created on: 17 ���. 2017 �.
 *      Author: ilynxy
 */


#include "aocf_support.h"

uint32_t aocf_erase_page(uint32_t page_offset)
{
  uint32_t status;
  status = aocf_wait_for_idle_and_get_status();
  AOCF_WriteEnable();

  page_offset /= 4;
  page_offset &= AOCF_CONTROL_PAGE_ERASE_MASK;

  AOCF_ErasePage(page_offset);
  status = aocf_wait_for_idle_and_get_status();

  AOCF_WriteDisable();
  status = aocf_wait_for_idle_and_get_status();

  return status;
}


/*
uint32_t store_calibrate_table_to_nvram(void)
{
  const uint32_t page_erase_address = 0;

  uint32_t status;
  status = aocf_wait_for_idle_and_get_status();

  static uint32_t pticks __attribute__((used));

  AOCF_ErasePage(page_erase_address);

  GAR_PERFCOUNTER();
  status = aocf_wait_for_idle_and_get_status();
  pticks = GAR_PERFCOUNTER();

  const uint32_t *ps = (const uint32_t *)(&g_CalibrateTable);
  const uint32_t *pe = ps + (sizeof(g_CalibrateTable) / sizeof(uint32_t));
  volatile uint32_t *pd = (volatile uint32_t *)FLASH_DATA_BASE;

  do
  {

    uint32_t v;
    v = *ps;
    *pd = v;

    ++ ps;
    ++ pd;

    GAR_PERFCOUNTER();
    status = aocf_wait_for_idle_and_get_status();
    pticks = GAR_PERFCOUNTER();

  } while (ps != pe);

  AOCF_WriteDisable();

  return status;
}
*/
