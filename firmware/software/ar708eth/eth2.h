/*
 * ethernet.h
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef ETHERNET_H_
#define ETHERNET_H_

#include "typedefs.h"

typedef struct
{
  union
  {
    be_uint8_t  u8[6];  // network byte order (big endian)
    be_uint16_t u16[3];
  };
} _PACKED_ eth2_mac_t;

typedef struct
{
  eth2_mac_t    dstMAC;
  eth2_mac_t    srcMAC;
  be_uint16_t   ethertype;

} _PACKED_ eth2_frame_header_t;

#endif /* ETHERNET_H_ */
