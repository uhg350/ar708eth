/*
 * ar708_handler.c
 *
 *  Created on: 04 ���. 2017 �.
 *      Author: ilynxy
 */

#include "i_arinc708_mac.h"

#include "emac_tx_mmap.h"
#include "global_vars.h"

#include "i_bswap_add1c.h"
#include "i_emac.h"
#include "i_msticks.h"

void ar708_handler_process(void);

void ar708_handler(void)
{
  // channel 1
  {
    uint32_t psize = AR708_RX0_GetCurrentPacketLength();
    if (psize != 0)
    {
      data_block_header_t *pDBH = __assume_aligned(&eth_tx_mmap.data_block_header, 4);
      pDBH->u32message_timestamp  = MS_GetTicks();

      const uint8_t *ps = AR708_RX0_GetCurrentPacketPtr();

      uint32_t  length = psize;
      uint32_t  line   = 0x01;
      uint32_t  msgid  = ps[2];

      pDBH->bm8message_source = line;
      pDBH->u8message_label   = msgid;
      pDBH->u8message_size    = length;

      const uint8_t *pse = ps + length + 2;
      uint8_t *pd = __assume_aligned(eth_tx_mmap.eth2_udpv4_data, 4);

      {
        const uint32_t *p32s  = (const uint32_t *)ps;
        const uint32_t *p32se = (const uint32_t *)pse;
        uint32_t *p32d        = (uint32_t *)pd;
        do
        {
          *p32d ++ = *p32s ++;
        } while (p32s < p32se);
      }

      AR708_RX0_DropCurrentPacket();
      ar708_handler_process();
    }
  }

  // channel 2
  {
    uint32_t psize = AR708_RX1_GetCurrentPacketLength();
    if (psize != 0)
    {
      data_block_header_t *pDBH = __assume_aligned(&eth_tx_mmap.data_block_header, 4);
      pDBH->u32message_timestamp  = MS_GetTicks();

      const uint8_t *ps = AR708_RX1_GetCurrentPacketPtr();

      uint32_t  length = psize;
      uint32_t  line   = 0x02;
      uint32_t  msgid  = ps[2];

      pDBH->bm8message_source = line;
      pDBH->u8message_label   = msgid;
      pDBH->u8message_size    = length;

      const uint8_t *pse = ps + length + 2;
      uint8_t *pd = __assume_aligned(eth_tx_mmap.eth2_udpv4_data, 4);

      {
        const uint32_t *p32s  = (const uint32_t *)ps;
        const uint32_t *p32se = (const uint32_t *)pse;
        uint32_t *p32d        = (uint32_t *)pd;
        do
        {
          *p32d ++ = *p32s ++;
        } while (p32s < p32se);
      }

      AR708_RX1_DropCurrentPacket();
      ar708_handler_process();
    }
  }
}

void ar708_handler_process(void)
{
  data_block_header_t *pDBH = __assume_aligned(&eth_tx_mmap.data_block_header, 4);

  size_t    length = pDBH->u8message_size;
  uint32_t  line   = pDBH->bm8message_source;
  uint32_t  msgid  = pDBH->u8message_label;

  uint8_t *pd  = __assume_aligned(eth_tx_mmap.eth2_udpv4_data, 4);

  // prepare output packet
  eth2_udpv4_data_frame_header_t *pDFH = __assume_aligned(eth_tx_mmap.eth2_udpv4_data_header, 4);

  //
#if 1
  for (size_t k = 0; k < CONFIG_MAXIMUM_SINKS; ++ k)
  {
    if ( (g_config.block.data.sinks[k].flags & SINK_CONFIG_FLAG_ENABLED) )
    {
      for (size_t i = 0; i < CONFIG_MAXIMUM_FILTERS; ++ i)
      {
        uint32_t lines_mask = g_config.block.data.sinks[k].messages[i].lines_mask;
        uint32_t message_id = g_config.block.data.sinks[k].messages[i].message_id;

        if ( (lines_mask & line) == 0 )
          continue;

        if ( message_id != msgid )
          continue;

        uint32_t message_offset = g_config.block.data.sinks[k].messages[i].message_offset;
        uint32_t message_size   = g_config.block.data.sinks[k].messages[i].message_size;

        if (message_offset >= length)
          continue;

        if (message_offset + message_size > length)
        {
          message_size = length - message_offset;
        }

        if (message_size == 0)
          continue;

        //asm volatile (" " : : "r"(lines_mask), "r"(message_id), "r"(message_offset), "r"(message_size) );

        // send to sink
#if 1
        pDBH->u8message_offset = message_offset;
        pDBH->u8message_size   = message_size;

        uint32_t dsize = message_size;
        dsize += sizeof(*pDBH) + sizeof(pDFH->udp);
        pDFH->udp.length = BSWAP_U16(dsize);
        dsize += sizeof(pDFH->ip);
        pDFH->ip.total_length = BSWAP_U16(dsize);
        ++ pDFH->ip.id;

        { // Update reply ip header checksum
          ipv4_header_t *ip = &pDFH->ip;
          ip = __assume_aligned(ip, 4);
          ip->checksum = 0;

          asm volatile ("" ::: "memory");
          const uint32_t *pIPH = (const uint32_t *)ip;
          uint32_t c0, c1;
          c0 = FOLDU16_ADD1C_U32_U32(pIPH[0], pIPH[1]);
          c1 = FOLDU16_ADD1C_U32_U32(pIPH[2], pIPH[3]);
          //c0 |= c1 << 16;
          c0 = FOLDU16_ADD1C_U32_U32(c0, pIPH[4]);
          c0 = FOLDU16_ADD1C_U32_U32(c0, c1);

          ip->checksum = ~c0;
        }

        uint32_t seg0_ptr  = (uint32_t)(&pDFH->eth);
        uint32_t seg0_size = sizeof(pDFH->eth) + sizeof(pDFH->ip) + sizeof(pDFH->udp);
        uint32_t seg1_ptr  = (uint32_t)(pDBH);
        uint32_t seg1_size = sizeof(*pDBH);
        uint32_t seg2_ptr  = (uint32_t)(pd + 2 + message_offset);
        uint32_t seg2_size = message_size;

        while (EMAC_TX_GetSGDMAState() != 0)
          ;

        eth_tx_mmap.emac_tx_segments.segments[0] = (seg0_ptr << 16) | seg0_size;
        eth_tx_mmap.emac_tx_segments.segments[1] = (seg1_ptr << 16) | seg1_size;
        eth_tx_mmap.emac_tx_segments.segments[2] = (seg2_ptr << 16) | seg2_size;

        EMAC_TX_SetDMADescriptor(0, 3);

        while (EMAC_TX_GetSGDMAState() != 0)
          ;
#endif
      }
    }

    ++ pDFH;
  }
#endif

#if 0
  const sink_config_t *pSINK  = __assume_aligned(g_config.block.data.sinks, 4);
  const sink_config_t *pSINKe = pSINK + CONFIG_MAXIMUM_SINKS;

  do
  {
    if (pSINK->flags & SINK_CONFIG_FLAG_ENABLED)
    {

      const message_filter_t *pMF   = __assume_aligned(&pSINK->messages[0], 4);
      const message_filter_t *pMFe  = pMF + CONFIG_MAXIMUM_FILTERS;
      do
      {
        pMF = ({ asm volatile("": "+r"(pMF)); pMF; });
        uint32_t lines_mask = pMF->lines_mask;
        uint32_t message_id = pMF->message_id;

        if ( (lines_mask & line) == 0 )
          continue;

        if ( message_id != msgid )
          continue;

        uint32_t message_offset = pMF->message_offset;
        uint32_t message_size   = pMF->message_size;

        if (message_offset >= length)
          continue;

        if (message_offset + message_size > length)
        {
          message_size = length - message_offset;
        }
        if (message_size == 0)
          continue;

        pDBH->u8message_offset = message_offset;
        pDBH->u8message_size   = message_size;

        asm volatile (" " : : "r"(lines_mask), "r"(message_id), "r"(message_offset), "r"(message_size) );
#if 0
        uint32_t dsize = message_size;
        dsize += sizeof(*pDBH) + sizeof(pDFH->udp);
        pDFH->udp.length = BSWAP_U16(dsize);
        dsize += sizeof(pDFH->ip);
        pDFH->ip.total_length = BSWAP_U16(dsize);
        ++ pDFH->ip.id;

        { // Update reply ip header checksum
          ipv4_header_t *ip = &pDFH->ip;
          ip = __assume_aligned(ip, 4);

          ip->checksum = 0;
          const uint32_t *pIPH = (const uint32_t *)ip;
          uint32_t c0, c1;
          c0 = FOLDU16_ADD1C_U32_U32(pIPH[0], pIPH[1]);
          c1 = FOLDU16_ADD1C_U32_U32(pIPH[2], pIPH[3]);
          //c0 |= c1 << 16;
          c0 = FOLDU16_ADD1C_U32_U32(c0, pIPH[4]);
          c0 = FOLDU16_ADD1C_U32_U32(c0, c1);

          ip->checksum = ~c0;
        }

        uint32_t seg0_ptr  = (uint32_t)(&pDFH->eth);
        uint32_t seg0_size = sizeof(pDFH->eth) + sizeof(pDFH->ip) + sizeof(pDFH->udp);
        uint32_t seg1_ptr  = (uint32_t)(pDBH);
        uint32_t seg1_size = sizeof(*pDBH);
        uint32_t seg2_ptr  = (uint32_t)(pd + message_offset);
        uint32_t seg2_size = message_size;

        while (EMAC_TX_GetSGDMAState() != 0)
          ;

        eth_tx_mmap.emac_tx_segments.segments[0] = (seg0_ptr << 16) | seg0_size;
        eth_tx_mmap.emac_tx_segments.segments[1] = (seg1_ptr << 16) | seg1_size;
        eth_tx_mmap.emac_tx_segments.segments[2] = (seg2_ptr << 16) | seg2_size;

        EMAC_TX_SetDMADescriptor(0, 3);
#endif
        ++ pMF;
      } while (pMF < pMFe);

    }

    ++ pDFH;
    ++ pSINK;
  } while (pSINK < pSINKe);
#endif
}
