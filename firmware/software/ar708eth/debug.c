/*
 * debug.c
 *
 *  Created on: Nov 14, 2017
 *      Author: ilynxy
 */

#define _DEBUG
#include "debug.h"

#ifdef _DEBUG

#include "system.h"
#include "gpio.h"

void _DBG_PUTSTR(const char *s)
{
  unsigned c;
  // c = RDIOU08(s);
  c = *s;
  ++ s;

  if (c != 0)
  {
    do
    {
      DBG_PUTCHAR(c);
      // c = RDIOU08(s);
      c = *s;
      ++ s;

    } while (c != '\0');
  }
}

void _DBG_ENTRY(const char *s)
{
  DBG_PUTSTR(s);
  static const char __entry_pfx[] = ": entry\n";
  DBG_PUTSTR(__entry_pfx);
}

void _DBG_LEAVE(const char *s)
{
  DBG_PUTSTR(s);
  static const char __leave_pfx[] = ": leave\n";
  DBG_PUTSTR(__leave_pfx);
}

#endif

