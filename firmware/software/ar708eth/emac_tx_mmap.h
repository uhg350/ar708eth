/*
 * eth_mmap_txm.h
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef ETH_MMAP_TXM_H_
#define ETH_MMAP_TXM_H_

#include "emac_tx_mmap_p.h"

#define _TXM_MAP_SECTION_EXTERNAL_ __attribute__ ((aligned (0x1000)))
extern eth_tx_mmap_t eth_tx_mmap _TXM_MAP_SECTION_EXTERNAL_;
//#define eth_tx_mmap (* ( (eth_tx_mmap_t *) EMAC_TXMMAP_BASE ) )

#endif /* ETH_MMAP_TXM_H_ */
