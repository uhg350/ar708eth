/*
 * read_settings.c
 *
 *  Created on: 26 ���� 2017 �.
 *      Author: ilynxy
 */

#include <assert.h>
#include <stddef.h>

#include "system.h"
#include "gpio.h"

#include "i_crc32_ieee_802_3.h"

#include "global_vars.h"
#include "settings_mmap.h"

#include "aocf_support.h"
#include "i_lfs_button.h"

static
uint32_t copy_and_compute_crc_u32(const void *src, void *dst, size_t size)
{
  const uint32_t  *ps = __assume_aligned(src, 4);
  uint32_t        *pd = __assume_aligned(dst, 4);
  uint32_t        *pe = (uint32_t *)(((char *)pd) + size);

  uint32_t crc;

  crc = CRC32_IEEE_802_3_GetInitial();

  do
  {
    uint32_t u32;

    u32 = *ps ++;
    *pd ++ = u32;

    crc = CRC32_IEEE_802_3_Compute32(crc, u32);

  } while (pd < pe);

  return crc;
}

static
void load_block_from_nvram(const void *current_block, const void *default_block, void *dst, size_t size)
{
  uint32_t crc;
  crc = copy_and_compute_crc_u32(current_block, dst, size);
  uint32_t force_load_default = (LFS_BUTTON_GetState() == 0);
  if ( (crc != CRC32_IEEE_802_3_GetCheck()) || force_load_default )
  {
    crc = copy_and_compute_crc_u32(default_block, dst, size - 4);

    uint32_t *pd = (uint32_t *)(((char *)dst) + size - 4);
    *pd = crc;
  }
}

static
void store_block_to_nvram(const void *src, void *dst, size_t size)
{
  uint32_t status;

  const uint32_t  *ps = __assume_aligned(src, 4);
  uint32_t        *pd = __assume_aligned(dst, 4);
  uint32_t        *pe = (uint32_t *)(((char *)pd) + size);

  status = aocf_wait_for_idle_and_get_status();
  AOCF_WriteEnable();

  uint32_t paddr = (uintptr_t)pd;
  paddr &= AOCF_CONTROL_PAGE_ERASE_MASK;
  paddr /= 4;

  AOCF_ErasePage(paddr);
  status = aocf_wait_for_idle_and_get_status();

  AOCF_WriteDisable();
  status = aocf_wait_for_idle_and_get_status();

  AOCF_WriteEnable();

  do
  {

    uint32_t v;
    v = *ps;
    *pd = v;

    ++ ps;
    ++ pd;

    status = aocf_wait_for_idle_and_get_status();

  } while (pd != pe);

  AOCF_WriteDisable();
}

void load_ident_from_nvram(void)
{
  load_block_from_nvram(&flash_content->current_ident, &default_settings.default_ident, &g_ident, sizeof(g_ident));
}

void store_ident_to_nvram(void)
{
  store_block_to_nvram(&g_ident, &flash_content->current_ident, sizeof(g_ident));
}

//
void load_config_from_nvram(void)
{
  load_block_from_nvram(&flash_content->current_config, &default_settings.default_config, &g_config, sizeof(g_config));
}

void store_config_to_nvram(void)
{
  store_block_to_nvram(&g_config, &flash_content->current_config, sizeof(g_config));
}

