/*
 * aocf_support.h
 *
 *  Created on: 30 ����. 2017 �.
 *      Author: ilynxy
 */

#ifndef AOCF_SUPPORT_H_
#define AOCF_SUPPORT_H_

#include "i_aocf.h"

inline
uint32_t aocf_wait_for_idle_and_get_status()
{
  uint32_t status;

  for(;;)
  {
    status = AOCF_GetStatus();

    if ( (status & AOCF_STATUS_BUSY_MASK) == AOCF_STATUS_BUSY_IDLE)
      break;
  }

  return status;
}

uint32_t aocf_erase_page(uint32_t page_offset);

#endif /* AOCF_SUPPORT_H_ */
