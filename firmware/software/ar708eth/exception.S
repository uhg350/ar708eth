#include "system.h"
#include "nios2.h"

.altmacro
.macro VIC_IRQ_ENTRY IDX
  .weak vic_handler_irq\IDX
  rdprs   sp, sp, 0
  call    vic_handler_irq\IDX
vic_handler_irq\IDX:
  addi    ea, ea, -4
  eret
.endm

  .section .exceptions, "xa"
  .align 4
  .globl  vic_irq_table
vic_irq_table:

// Vector 0 is General Exception

  .weak   general_exception_handler
  jmpi    general_exception_handler
general_exception_handler:
  break   0
  break   0
  break   0

#if (NIOS2_NUM_OF_SHADOW_REG_SETS > 0)
  .set  N, 1
  .rept NIOS2_NUM_OF_SHADOW_REG_SETS
  VIC_IRQ_ENTRY %N
  .set  N, N + 1
  .endr
#endif
