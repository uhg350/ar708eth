/*
 * e100_tx_mmap.c
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#include "system.h"
#include "gpio.h"

#include "emac_tx_mmap_p.h"
#include "global_vars.h"

#include "i_bswap_add1c.h"

#define _TXM_MAP_SECTION_INTERNAL_ __attribute__ ((section (".emac_txmmap"), aligned (0x1000)))

eth_tx_mmap_t eth_tx_mmap _TXM_MAP_SECTION_INTERNAL_ =
{
  .eth2_arpv4_request =
  {
    .eth =
    {
      .dstMAC.u8 = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF },
      .ethertype = S_BSWAP_U16(0x0806)
    },
    .arp =
    {
      .hardware_type = S_BSWAP_U16(0x0001),
      .protocol_type = S_BSWAP_U16(0x0800),
      .hardware_size = 6,
      .protocol_size = 4,
      .opcode        = S_BSWAP_U16(0x0001) // request
    }
  },

  .eth2_arpv4_reply =
  {
    .eth =
    {
      .ethertype = S_BSWAP_U16(0x0806)
    },
    .arp =
    {
      .hardware_type = S_BSWAP_U16(0x0001),
      .protocol_type = S_BSWAP_U16(0x0800),
      .hardware_size = 6,
      .protocol_size = 4,
      .opcode        = S_BSWAP_U16(0x0002) // reply
    }
  },

  .eth2_icmpv4_reply =
  {
    .eth =
    {
      .ethertype = S_BSWAP_U16(0x0800)
    },
    .ip =
    {
      .version_length   = 0x45,   // version 4, length 5 * 4 octets = 20
      .type_of_service  = 0x00,
      .id               = 0x0000,
      .offset_flags     = S_BSWAP_U16(0x4000), // don't fragment, offset = 0
      .ttl              = 64, // default TTL is 64
      .protocol         = 0x01, // ICMP protocol
    },
    .icmp =
    {
    }
  },

  .eth2_udpv4_ctrl_header =
  {
    .eth =
    {
      .ethertype = S_BSWAP_U16(0x0800)
    },
    .ip =
    {
      .version_length   = 0x45,   // version 4, length 5 * 4 octets = 20
      .type_of_service  = 0x00,
      .id               = 0x0000,
      .offset_flags     = S_BSWAP_U16(0x4000), // don't fragment, offset = 0
      .ttl              = 64, // default TTL is 64
      .protocol         = 0x11, // UDP protocol
    },
    .udp =
    {
      .checksum = 0
    },
    .ctrl =
    {
      .tag = CTRL_SB_TAG,
    }
  }
};

inline void __assertion_checking_eth_mmap_txm(void)
{
  ASSERT_OFFSET_ALIGNED(eth2_udpv4_data_frame_header_t, ip.srcIP, 4);
  ASSERT_OFFSET_ALIGNED(eth2_udpv4_data_frame_header_t, ip.dstIP, 4);
  //ASSERT_OFFSET_ALIGNED(eth2_udpv4_data_frame_header_t, data_block_header, 4);

  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[0], 4);
  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[0].ip.srcIP, 4);
  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[0].ip.dstIP, 4);

#if CONFIG_MAXIMUM_SINKS > 1
  ASSERT_OFFSET_ALIGNED(eth_tx_mmap_t, eth2_udpv4_data_header[1], 4);
#endif
}
