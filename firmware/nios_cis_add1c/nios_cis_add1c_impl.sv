module nios_cis_add1c_impl
(
  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  logic [31: 0] r32;
  logic         c32;
  
  always_comb begin
    { c32, r32 } = { 1'b0, dataa } + { 1'b0, datab };
    r32 = r32 + c32;
  end
  
  logic [15: 0] r16;
  logic         c16;
  always_comb begin
    { c16, r16 } = { 1'b0, r32[15: 0] } + { 1'b0, r32[31:16] };
    r16 = r16 + c16;
  end
  
  assign result = { 16'b0, r16 };


/*
  logic [17: 0] la, ha, lb, hb;
  logic [17: 0] r;
  
  always_comb begin
  
    la = { 2'b00, dataa[15: 0] };
    ha = { 2'b00, dataa[31:16] };

    lb = { 2'b00, datab[15: 0] };
    hb = { 2'b00, datab[31:16] };
  end
  
  always_comb begin
    r  = (la + ha) + (lb + hb);
    
    r  = { 2'b00, r[15: 0] } + { 16'b0, r[17: 16] };
    r  = { 2'b00, r[15: 0] } + { 17'b0, r[16] };
    //r  = { 2'b00, r[15: 0] } + { 17'b0, r[16] };
  end

  assign result = { 16'b0, r[15: 0] };
*/
endmodule
