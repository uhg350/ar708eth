module nios_cis_add1c
(
  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  nios_cis_add1c_impl
    nios_cis_add1c_i
    (
      .dataa  (dataa),
      .datab  (datab),
      .result (result)
    );

endmodule
