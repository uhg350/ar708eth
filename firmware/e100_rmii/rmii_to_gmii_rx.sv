module rmii_to_gmii_rx
(
  input  wire         rmii_areset,
  input  wire         rmii_clk,
  input  wire         rmii_ena,
  input  wire         rmii_rx_active,
  input  wire         rmii_rx_dv,
  input  wire         rmii_rx_er,
  input  wire [ 1: 0] rmii_rx_d,
  
  input  wire         gmii_areset,
  input  wire         gmii_clk,
  input  wire         gmii_acq,
  output wire         gmii_rx_dv,
  output wire         gmii_rx_er,
  output wire [ 7: 0] gmii_rx_d
);

  // bufferize it
  logic         r_rmii_dv;
  logic         r_rmii_er;
  logic [ 1: 0] r_rmii_d;
  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset) begin
      r_rmii_dv <= '0;
      r_rmii_er <= '0;
      r_rmii_d  <= '0;
    end
    else if (rmii_ena) begin
      r_rmii_dv <= rmii_rx_dv;
      r_rmii_er <= rmii_rx_er;
      r_rmii_d  <= rmii_rx_d;
    end

  // recover octet stream
  logic [ 1: 0] bcnt;
  logic [ 7: 0] octet;
  logic [ 3: 0] error;
  logic         valid;

  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset) begin
      bcnt  <= '0;
      octet <= '0;
      error <= '0;
      valid <= '0;
    end
    else if (rmii_ena) begin
      if (r_rmii_dv) begin
        bcnt  <= bcnt + 1'b1;
        octet <= { r_rmii_d,  octet[7:2] };
        error <= { r_rmii_er, error[3:1] };
      end
      else
        bcnt <= '0;
      
      valid <= (bcnt == 2'b11) && r_rmii_dv;
    end

  logic [ 7: 0] rx_d;
  logic         rx_dv;
  logic         rx_er;
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset) begin
      rx_d  <= '0;
      rx_dv <= '0;
      rx_er <= '0;
    end
    else if (rmii_ena) begin
      if (valid) begin
        rx_d  <= octet;
        rx_er <= |error;
      end

      if (valid || gmii_acq)
        rx_dv <= valid && rmii_rx_active;
    end

  assign gmii_rx_dv  = rx_dv;
  assign gmii_rx_er  = rx_er;
  assign gmii_rx_d   = rx_d;

endmodule
