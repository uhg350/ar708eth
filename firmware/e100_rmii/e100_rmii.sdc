create_clock -period "50.000MHz" -name {rmii_clk} [get_ports {E100_CLK}]
set_false_path -to [get_ports {E100_RESET_N}]

create_clock -period "50.000MHz" -name {rmii_vclk}
# rx path
set_input_delay  -clock rmii_vclk -max  14.0 [get_ports {E100_RX_DV E100_RX_D[*]}]
set_input_delay  -clock rmii_vclk -min   2.0 [get_ports {E100_RX_DV E100_RX_D[*]}]

# tx path
set_output_delay -clock rmii_vclk -max   4.0 [get_ports {E100_TX_EN E100_TX_D[*]}]
set_output_delay -clock rmii_vclk -min  -2.0 [get_ports {E100_TX_EN E100_TX_D[*]}]

