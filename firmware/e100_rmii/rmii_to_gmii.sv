module rmii_to_gmii
(
  input  wire         rmii_areset,
  input  wire         rmii_clk,
  input  wire         rmii_mode,
  input  wire         rmii_rx_active,
  input  wire         rmii_rx_dv,
  input  wire         rmii_crs_dv,
  input  wire         rmii_rx_er,
  input  wire [ 1: 0] rmii_rx_d,
  output wire         rmii_tx_en,
  output wire         rmii_tx_er,
  output wire [ 1: 0] rmii_tx_d,
  
  output wire         gmii_areset,
  output wire         gmii_clk,
  output wire         gmii_acq,
  output wire         gmii_rx_dv,
  output wire         gmii_rx_er,
  output wire [ 7: 0] gmii_rx_d,
  input  wire         gmii_tx_en,
  input  wire         gmii_tx_er,
  input  wire [ 7: 0] gmii_tx_d
);

  assign gmii_areset = rmii_areset;
  assign gmii_clk    = rmii_clk;

  wire   rmii_ena;
  
  logic [ 4: 0] rcnt;
  logic         rcnt_wrap;

  always_comb
    rcnt_wrap = (rcnt == 5'd9);

  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset)
      rcnt <= '0;
    else begin
      if (rcnt_wrap)
        rcnt <= '0;
      else
        rcnt <= rcnt + 1'b1;
    end
  
  logic rmii_sp;
  always_ff
    rmii_sp = rcnt_wrap || !rmii_mode;

  logic [ 1: 0] bcnt;
  logic         bcnt_wrap;
  always_comb
    bcnt_wrap = (bcnt == 2'd3);
  
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset)
      bcnt <= '0;
    else if (rmii_sp) begin
      if (bcnt_wrap)
        bcnt <= '0;
      else
        bcnt <= bcnt + 1'b1;
    end

  logic r_rmii_ena;
  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset)
      r_rmii_ena <= '0;
    else
      r_rmii_ena <= rmii_sp;
  
  logic r_gmii_ack;
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset)
      r_gmii_ack <= '0;
    else
      r_gmii_ack <= rmii_sp && bcnt_wrap;

  assign rmii_ena = r_rmii_ena;
  assign gmii_acq = r_gmii_ack;
  
  rmii_to_gmii_rx
    rmii_to_gmii_rx_i0
    (
      .*
    );
    
  rmii_to_gmii_tx
    rmii_to_gmii_tx_i0
    (
      .*
    );

endmodule
