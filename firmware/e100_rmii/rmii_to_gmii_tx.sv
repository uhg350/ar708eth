module rmii_to_gmii_tx
(
  input  wire         gmii_areset,
  input  wire         gmii_clk,
  input  wire         gmii_acq,
  input  wire         gmii_tx_en,
  input  wire         gmii_tx_er,
  input  wire [ 7: 0] gmii_tx_d,

  input  wire         rmii_areset,
  input  wire         rmii_clk,
  input  wire         rmii_ena,
  output wire         rmii_tx_en,
  output wire         rmii_tx_er,
  output wire [ 1: 0] rmii_tx_d
);

  logic         tx_en;
  logic         tx_er;
  logic [ 7: 0] tx_d;
  logic [ 1: 0] tx_idx;
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset) begin
      tx_en   <= '0;
      tx_er   <= '0;
      tx_d    <= '0;
      tx_idx  <= '0;
    end
    else if (rmii_ena) begin
      if (gmii_acq) begin
        tx_en  <= gmii_tx_en;
        tx_er  <= gmii_tx_er;
        tx_d   <= gmii_tx_d;
        tx_idx <= '0;
      end
      else
        tx_idx <= tx_idx + 1'b1;
    end

  logic       rmii_en;
  logic       rmii_er;
  logic [1:0] rmii_d;

  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset) begin
      rmii_en <= '0;
      rmii_er <= '0;
      rmii_d  <= '0;
    end
    else if (rmii_ena) begin
      rmii_en <= tx_en;
      rmii_er <= tx_er;
      
      case (tx_idx)
        2'b00 : rmii_d  <= tx_d[1:0];
        2'b01 : rmii_d  <= tx_d[3:2];
        2'b10 : rmii_d  <= tx_d[5:4];
        2'b11 : rmii_d  <= tx_d[7:6];
      endcase
    end

  assign rmii_tx_en = rmii_en;
  assign rmii_tx_er = rmii_er;
  assign rmii_tx_d  = rmii_d;
endmodule
