module nios_ar708_mac_mmap
#(
  parameter NAR = 2,
  parameter AW  = 10
)
(
  input  wire             areset,
  input  wire             clk,

  input  wire             rxm_clken,
  input  wire [ 9: 0]     rxm_address,
  input  wire [ 3: 0]     rxm_byteenable,
  input  wire             rxm_read,
  output wire [31: 0]     rxm_readdata,
  input  wire             rxm_write,
  input  wire [31: 0]     rxm_writedata,

  input  wire             txm_clken,
  input  wire [ 8: 0]     txm_address,
  input  wire [ 3: 0]     txm_byteenable,
  input  wire             txm_read,
  output wire [31: 0]     txm_readdata,
  input  wire             txm_write,
  input  wire [31: 0]     txm_writedata,
  
  input  wire [ 3: 0]     ctl_address,
  input  wire [ 3: 0]     ctl_byteenable,
  input  wire             ctl_write,
  input  wire [31: 0]     ctl_writedata,
  input  wire             ctl_read,
  output wire [31: 0]     ctl_readdata,
  output wire             ctl_readdatavalid,
  output wire             ctl_waitrequest,
  
  input  wire [NAR-1: 0]  HI157X_RX_P,
  input  wire [NAR-1: 0]  HI157X_RX_N,
  output wire [NAR-1: 0]  HI157X_RX_IE,
  output wire [NAR-1: 0]  HI157X_RX_LED,

  output wire [NAR-1: 0]  HI157X_TX_P,
  output wire [NAR-1: 0]  HI157X_TX_N,
  output wire [NAR-1: 0]  HI157X_TX_OD
);

  nios_ar708_mac_mmap_impl#(
    .NAR  (NAR),
    .AW   (AW)
  )
    nios_ar708_mac_mmap_impl_i
    (
      .areset             (areset),
      .clk                (clk),

      .rxm_clken          (rxm_clken),
      .rxm_address        (rxm_address),
      .rxm_byteenable     (rxm_byteenable),
      .rxm_read           (rxm_read),
      .rxm_readdata       (rxm_readdata),
      .rxm_write          (rxm_write),
      .rxm_writedata      (rxm_writedata),
      
      .txm_clken          (txm_clken),
      .txm_address        (txm_address),
      .txm_byteenable     (txm_byteenable),
      .txm_read           (txm_read),
      .txm_readdata       (txm_readdata),
      .txm_write          (txm_write),
      .txm_writedata      (txm_writedata),
      
      .ctl_address        (ctl_address),
      .ctl_byteenable     (ctl_byteenable),
      .ctl_write          (ctl_write),
      .ctl_writedata      (ctl_writedata),
      .ctl_read           (ctl_read),
      .ctl_readdata       (ctl_readdata),
      .ctl_readdatavalid  (ctl_readdatavalid),
      .ctl_waitrequest    (ctl_waitrequest),

      .HI157X_RX_P        (HI157X_RX_P),
      .HI157X_RX_N        (HI157X_RX_N),
      .HI157X_RX_IE       (HI157X_RX_IE),
      .HI157X_RX_LED      (HI157X_RX_LED),
    
      .HI157X_TX_P        (HI157X_TX_P),
      .HI157X_TX_N        (HI157X_TX_N),
      .HI157X_TX_OD       (HI157X_TX_OD)
    );

endmodule
