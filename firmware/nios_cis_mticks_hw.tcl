# TCL File Generated by Component Editor 17.0
# Sun Nov 19 22:23:46 MSK 2017
# DO NOT MODIFY


# 
# nios_cis_mticks "nios_cis_mticks" v1.0
#  2017.11.19.22:23:46
# 
# 

# 
# request TCL package from ACDS 16.1
# 
package require -exact qsys 16.1


# 
# module nios_cis_mticks
# 
set_module_property DESCRIPTION ""
set_module_property NAME nios_cis_mticks
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property AUTHOR ""
set_module_property DISPLAY_NAME nios_cis_mticks
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL nios_cis_mticks
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property QUARTUS_SYNTH ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file nios_cis_mticks.sv SYSTEM_VERILOG PATH nios_cis_mticks.sv TOP_LEVEL_FILE


# 
# parameters
# 
add_parameter CLKMS INTEGER 50000
set_parameter_property CLKMS DEFAULT_VALUE 50000
set_parameter_property CLKMS DISPLAY_NAME CLKMS
set_parameter_property CLKMS TYPE INTEGER
set_parameter_property CLKMS UNITS None
set_parameter_property CLKMS ALLOWED_RANGES -2147483648:2147483647
set_parameter_property CLKMS HDL_PARAMETER true


# 
# display items
# 


# 
# connection point reset
# 
add_interface reset reset end
set_interface_property reset associatedClock clk
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true
set_interface_property reset EXPORT_OF ""
set_interface_property reset PORT_NAME_MAP ""
set_interface_property reset CMSIS_SVD_VARIABLES ""
set_interface_property reset SVD_ADDRESS_GROUP ""

add_interface_port reset areset reset Input 1

# 
# connection point clk
# 
add_interface clk clock end
set_interface_property clk clockRate 0
set_interface_property clk ENABLED true
set_interface_property clk EXPORT_OF ""
set_interface_property clk PORT_NAME_MAP ""
set_interface_property clk CMSIS_SVD_VARIABLES ""
set_interface_property clk SVD_ADDRESS_GROUP ""

add_interface_port clk clk clk Input 1


# 
# connection point cis
# 
add_interface cis nios_custom_instruction end
set_interface_property cis clockCycle 0
set_interface_property cis operands 2
set_interface_property cis ENABLED true
set_interface_property cis EXPORT_OF ""
set_interface_property cis PORT_NAME_MAP ""
set_interface_property cis CMSIS_SVD_VARIABLES ""
set_interface_property cis SVD_ADDRESS_GROUP ""

add_interface_port cis dataa dataa Input 32
add_interface_port cis datab datab Input 32
add_interface_port cis result result Output 32


