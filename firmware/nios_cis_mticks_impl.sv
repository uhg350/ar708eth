module nios_cis_mticks_impl
#(
  parameter CLKMS = 50000
)
(
  input  wire         clk,
  input  wire         areset,

  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  localparam MW = $clog2(CLKMS);
  
  logic [MW-1: 0] mdiv;
  logic mdiv_wrap;

  always_comb
    mdiv_wrap = (mdiv == (CLKMS - 1));
  always_ff@(posedge areset or posedge clk)
    if (areset)
      mdiv <= '0;
    else begin
      if (mdiv_wrap)
        mdiv <= '0;
      else
        mdiv <= mdiv + 1'b1;
    end
  
  logic [31: 0] mscounter;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      mscounter <= '0;
    else
      if (mdiv_wrap)
        mscounter <= mscounter + 1'b1;
  
  assign result = mscounter - dataa;

endmodule
