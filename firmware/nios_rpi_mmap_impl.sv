module nios_rpi_mmap_impl
#(
  parameter W = 1,
  parameter S = 3
)
(
  input  wire           areset,
  input  wire           clk,
  
  input  wire [ 3: 0]   byteenable,
  input  wire           write,
  input  wire [31: 0]   writedata,
  input  wire           read,
  output wire [31: 0]   readdata,
  output wire           readdatavalid,
  output wire           waitrequest,
    
  input  wire [W-1: 0]  ipins
);
  
  logic [W-1: 0] cc_q;

  ccreg#(
    .W  (W), 
    .S  (S), 
    .R  (0)
  )
    ccreg_i
    (
      .areset (areset),
      .clk    (clk),
      .ena    (1'b1),
      
      .d      (ipins),
      .q      (cc_q)
    );
  
  assign readdata       = cc_q;
  assign readdatavalid  = read;
  assign waitrequest    = '0;

endmodule
