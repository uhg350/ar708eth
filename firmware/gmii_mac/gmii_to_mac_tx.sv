`default_nettype none

module gmii_to_mac_tx
(
  input  wire         gmii_tx_areset,
  input  wire         gmii_tx_clk,
  input  wire         gmii_tx_ena,
  output wire         gmii_tx_en,
  output wire         gmii_tx_er,
  output wire [ 7: 0] gmii_tx_d,

  output wire         mac_tx_areset,
  output wire         mac_tx_clk,
  output wire         mac_tx_ena,
  output wire         mac_tx_busy,
  input  wire         mac_tx_send,
  input  wire [ 7: 0] mac_tx_d,
  output wire         mac_tx_ack
);

  localparam PREAMBLE_LENGTH  = 7;
  localparam PADDING_LENGTH   = 60;
  localparam IFG_LENGTH       = 12;
  localparam FCS_LENGTH       = 4;

  localparam PREAMBLE_OCTET   = 8'h55;
  localparam SFD_OCTET        = 8'hD5;

  wire areset = gmii_tx_areset;
  wire clk    = gmii_tx_clk;
  wire ena    = gmii_tx_ena;

  // Quartus doesn't support 'let' keyword
  //  let max(a,b) = (a > b) ? (a) : (b);
  
  function int max(input int a, input int b);
    max = (a > b) ? (a) : (b);
  endfunction

  localparam OCMAX = max(max(PREAMBLE_LENGTH, PADDING_LENGTH), IFG_LENGTH);
  localparam OCW   = $clog2(OCMAX);
  
  typedef logic [OCW-1: 0] counter_t;
  counter_t counter_d, counter_q;
  logic counter_set, counter_dec;

  /*
  ud_counter#(.W(OCW), .DIR("DOWN"))
    counter
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      
      .step   (counter_dec),
      .set    (counter_set),
      .d      (counter_d),
      .q      (counter_q)
    );
*/

  lpm_counter#(.LPM_WIDTH(OCW), .LPM_DIRECTION("DOWN"))
    counter
    (
      .aclr   (areset),
      .clock  (clk),
      .clk_en (ena),
      
      .sload  (counter_set),
      .data   (counter_d),
      .cnt_en (counter_dec),
      
      .q      (counter_q)
    );

  logic counter_done;
  always_comb
    counter_done = (counter_q == '0);

  enum int unsigned
  { 
    stIDLE,
    stPREAMBLE,
    stSFD,
    stDATA,
    stPADDING,
    stFCS,
    stIFG
  } state, next_state;
  
  always_ff@(posedge clk or posedge areset)
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  logic tx_er, tx_en;
  logic [ 7: 0] tx_d;
  
  logic         fcs_init;
  logic         fcs_next;
  logic [31: 0] fcs_q;
  
  logic [ 7: 0] fcs_qi;
  
  always_comb begin
    case (counter_q[ 1: 0])
      2'b11 : fcs_qi = fcs_q[ 7: 0];
      2'b10 : fcs_qi = fcs_q[15: 8];
      2'b01 : fcs_qi = fcs_q[23:16];
      2'b00 : fcs_qi = fcs_q[31:24];
    endcase
  end

  always_comb begin
    next_state = state;
    
    counter_set = '0;
    counter_d   = 'x;
    counter_dec = !counter_done;
    
    tx_en       = '0;
    tx_er       = '0;
    tx_d        = '0;
    
    fcs_init    = '0;
    fcs_next    = '0;

    case (state)

    stIDLE : begin
      if (mac_tx_send) begin
        counter_set = '1;
        counter_d   = counter_t'(PREAMBLE_LENGTH - 1);
        
        next_state = stPREAMBLE;
      end
    end

    stPREAMBLE : begin
      tx_en       = '1;
      tx_d        = PREAMBLE_OCTET;

      if (counter_done)
        next_state = stSFD;
    end
    
    stSFD : begin
      fcs_init = '1;
      
      tx_en    = '1;
      tx_d     = SFD_OCTET;

      counter_set = '1;
      counter_d   = counter_t'(PADDING_LENGTH - 1);
      
      if (!mac_tx_send)
        next_state = stPADDING;
      else
        next_state = stDATA;
    end

    stDATA : begin
      tx_en       = '1;
      tx_d        = late_frame_d;

      fcs_next    = '1;

      if (!mac_tx_send) begin

        if (counter_done) begin
          counter_set = '1;
          counter_d   = counter_t'(FCS_LENGTH - 1);
          next_state = stFCS;
        end
        else
          next_state = stPADDING;

      end
    end
    
    stPADDING: begin

      tx_en     = '1;
      tx_d      = 8'h00;

      fcs_next  = '1;
      
      if (counter_done) begin
        counter_set = '1;
        counter_d   = counter_t'(FCS_LENGTH - 1);
        next_state = stFCS;
      end

    end

    stFCS : begin
      tx_en       = '1;
      tx_d        = fcs_qi;

      if (counter_done) begin
        counter_set = '1;
        counter_d   = counter_t'(IFG_LENGTH - 2);

        next_state = stIFG;
      end

    end
    
    stIFG : begin
    
      if (counter_done)
        next_state = stIDLE;
    end

    endcase
  end

  crc32_ieee802_3
    frame_check_sum
    (
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .init     (fcs_init),
      .next     (fcs_next),
      
      .d        (tx_d),
      .crc      (fcs_q)
    );
    
  logic [ 7: 0] late_frame_d;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      late_frame_d <= '0;
    else if (ena) begin
      if (state == stSFD || state == stDATA)
        late_frame_d <= mac_tx_d;
    end

  assign gmii_tx_en = tx_en;
  assign gmii_tx_er = tx_er;
  assign gmii_tx_d  = tx_d;

  assign mac_tx_busy = (state != stIDLE);
  assign mac_tx_ack  = (state == stSFD || state == stDATA) && mac_tx_send;

  assign mac_tx_areset  = gmii_tx_areset;
  assign mac_tx_clk     = gmii_tx_clk;
  assign mac_tx_ena     = gmii_tx_ena;
  
endmodule
