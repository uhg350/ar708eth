package mac_rx_types;

  typedef logic unsigned [ 7: 0] uint8_t;
  typedef logic unsigned [15: 0] uint16_t;
  typedef logic unsigned [32: 0] uint32_t;
  
  typedef struct packed
  {
    logic [6 * 8 - 1: 0]  mac;
    logic [15: 0]         port;
    logic [31: 0]         ip;
  } ethernet_address_t;
  
  typedef struct packed
  {
    ethernet_address_t  ethernet_address;
    logic [15: 0]       flags;
  } mac_filter_t;

endpackage
