package mac_rx_eoferrs;

typedef enum logic [ 1: 0]
{
  SUCCESS,
  CODING,
  FORCE,
  FCS
} type_t;

endpackage
