`default_nettype none

module gmii_to_mac_rx
(
  input  wire                   gmii_rx_areset,
  input  wire                   gmii_rx_clk,
  input  wire                   gmii_rx_ena,
  input  wire                   gmii_rx_dv,
  input  wire                   gmii_rx_er,
  input  wire [ 7: 0]           gmii_rx_d,
  
  output wire                   mac_rx_areset,
  output wire                   mac_rx_clk,
  output wire                   mac_rx_ena,
  input  wire                   mac_rx_drop,
  output mac_rx_states::type_t  mac_rx_state,
  output mac_rx_eoferrs::type_t mac_rx_eoferr,
  output wire [ 7: 0]           mac_rx_d
);

  localparam PREAMBLE_OCTET   = 8'h55;
  localparam SFD_OCTET        = 8'hD5;

  wire areset = gmii_rx_areset;
  wire clk    = gmii_rx_clk;
  wire ena    = gmii_rx_ena;

  enum int unsigned
  {
    stIDLE,
    stPREAMBLE,
    stSFD,
    stDATA,
    stEOF,
    stERROR,
    stDROP
  } state, next_state;

  always_ff@(posedge areset or posedge clk) 
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  wire          rx_dv = gmii_rx_dv;
  wire          rx_er = gmii_rx_er;
  wire [ 7: 0]  rx_d  = gmii_rx_d;

  always_comb begin
    next_state = state;

    case (state)
      stIDLE: begin
        if (rx_dv) begin
          if (!rx_er && rx_d == PREAMBLE_OCTET)
            next_state = stPREAMBLE;
          else
            next_state = stERROR;
        end
      end
      
      stPREAMBLE: begin
        if      (!rx_dv)
          next_state = stERROR;
        else if (rx_er)
          next_state = stERROR;
        else if (rx_d == SFD_OCTET)
          next_state = stSFD;
        else if (rx_d != PREAMBLE_OCTET)
          next_state = stERROR;
      end 
      
      stSFD: begin
        if      (!rx_dv)
          next_state = stERROR;
        else if (rx_er)
          next_state = stERROR;
        else if (mac_rx_drop)
          next_state = stERROR;
        else
          next_state = stDATA;
      end
      
      stDATA: begin
        if      (!rx_dv)
          next_state = stEOF;
        else if (rx_er)
          next_state = stERROR;
        else if (mac_rx_drop)
          next_state = stERROR;
      end
      
      stEOF : begin
        next_state = stIDLE;
      end
      
      stERROR : begin
        if (rx_dv)
          next_state = stDROP;
        else
          next_state = stIDLE;
      end
      
      stDROP : begin
        if (!rx_dv)
          next_state = stIDLE;
      end

    endcase
  end
//

  wire is_sfd       = (state == stSFD);
  wire is_data      = (state == stDATA);
  //wire is_eof       = (state == stEOF);
  //wire is_err       = (state == stERROR);

  logic [ 7: 0] rx_d_l1;
  logic         rx_er_l1;
  logic         rx_drop_l1;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      rx_d_l1     <= '0;
      rx_er_l1    <= '0;
      rx_drop_l1  <= '0;
    end
    else if (ena) begin
      rx_d_l1     <= rx_d;
      rx_er_l1    <= rx_er;
      rx_drop_l1  <= mac_rx_drop;
    end

  logic [31: 0] fcs;
  crc32_ieee802_3
    frame_check_sum
    (
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .init     (is_sfd),
      .next     (is_data),
      
      .d        (rx_d_l1),
      .crc      (fcs)
    );
  
  wire is_crcerr    = (fcs != 32'h2144df1c);
  
  mac_rx_states::type_t  rx_state;
  mac_rx_eoferrs::type_t rx_eoferr;
  
  always_comb begin
    rx_eoferr = mac_rx_eoferrs::SUCCESS;

    case (state)
      stSFD:    
        rx_state = mac_rx_states::SFD;

      stDATA:   
        rx_state = mac_rx_states::DATA;

      stEOF: begin
        rx_state = mac_rx_states::EFD;
        
        if (is_crcerr)
          rx_eoferr = mac_rx_eoferrs::FCS;
      end
      
      stERROR: begin
        rx_state = mac_rx_states::EFD;

        if      (rx_er_l1)
          rx_eoferr = mac_rx_eoferrs::CODING;
        else if (rx_drop_l1)
          rx_eoferr = mac_rx_eoferrs::FORCE;
        else
          rx_eoferr = mac_rx_eoferrs::CODING;
      end
      
      default:  rx_state = mac_rx_states::IDLE;
    endcase

  end

  assign mac_rx_areset  = gmii_rx_areset;
  assign mac_rx_clk     = gmii_rx_clk;
  assign mac_rx_ena     = gmii_rx_ena;
  assign mac_rx_state   = rx_state;
  assign mac_rx_eoferr  = rx_eoferr;
  assign mac_rx_d       = rx_d_l1;

endmodule
