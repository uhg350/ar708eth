package mac_rx_states;

typedef enum logic [ 1: 0]
{
  IDLE,
  SFD,
  DATA,
  EFD
} type_t;

endpackage

