`default_nettype none

module mac_tx_to_mmap
(
  input  wire           ctl_tx_sg_set,
  input  wire [15: 0]   ctl_tx_sg_offset,
  input  wire [15: 0]   ctl_tx_sg_length,
  output wire           ctl_tx_sg_busy,

  input  wire           mmap_tx_areset,
  input  wire           mmap_tx_clk,
  input  wire           mmap_tx_clken,
  input  wire [ 9: 0]   mmap_tx_address,
  input  wire [ 3: 0]   mmap_tx_byteenable,
  input  wire           mmap_tx_read,
  output wire [31: 0]   mmap_tx_readdata,
  input  wire           mmap_tx_write,
  input  wire [31: 0]   mmap_tx_writedata,
  
  input  wire           mac_tx_areset,
  input  wire           mac_tx_clk,
  input  wire           mac_tx_ena,
  input  wire           mac_tx_busy,
  output wire           mac_tx_send,
  output wire [ 7: 0]   mac_tx_d,
  input  wire           mac_tx_ack
);

  localparam AW = 10;

  logic [AW-1: 0]   mem_addr;
  logic             mem_read;
  logic [31: 0]     mem_q;
  
  dcram_rw32_rw32#(
    .SIZE       (2**AW),
    .AW         (AW),
    .INIT_FILE  ("txm_mmap_init.hex")
  )
    tx_ram
    (
      .mem_ws_areset  (mmap_tx_areset),
      .mem_ws_clk     (mmap_tx_clk),
      .mem_ws_ena     (mmap_tx_clken),
      .mem_ws_addr    (mmap_tx_address),
      .mem_ws_be      (mmap_tx_byteenable),
      .mem_ws_d       (mmap_tx_writedata),
      .mem_ws_write   (mmap_tx_write),
      .mem_ws_read    (mmap_tx_read),
      .mem_ws_q       (mmap_tx_readdata),

      .mem_rs_areset  (mmap_tx_areset),
      .mem_rs_clk     (mmap_tx_clk),
      .mem_rs_ena     (1'b1),
      .mem_rs_addr    (mem_addr),
      .mem_rs_be      (4'b1111),
      .mem_rs_d       (32'bx),
      .mem_rs_write   (1'b0),
      .mem_rs_read    (mem_read),
      .mem_rs_q       (mem_q)
    );

  logic         queue_full;
  logic         queue_push;
  logic [ 2: 0] queue_cnt;
  logic [31: 0] queue_d;

  mac_tx_sgdma
    mac_tx_sgdma_i0
    (
      .areset     (mmap_tx_areset),
      .clk        (mmap_tx_clk),
      .ena        (1'b1),
      
      .start      (ctl_tx_sg_set),
      .offset     (ctl_tx_sg_offset[ 9: 0]),
      .size       (ctl_tx_sg_length[ 3: 0]),
      .busy       (ctl_tx_sg_busy),

      .mem_addr   (mem_addr),
      .mem_read   (mem_read),
      .mem_q      (mem_q),

      .queue_full (queue_full),
      .queue_push (queue_push),
      .queue_cnt  (queue_cnt),
      .queue_d    (queue_d)
    );

  mac_tx_queue_sc
    mac_tx_queue_i0
    (
      .queue_areset   (mmap_tx_areset),
      .queue_clk      (mmap_tx_clk),
      .queue_ena      (1'b1),
      .queue_full     (queue_full),
      .queue_push     (queue_push),
      .queue_cnt      (queue_cnt),
      .queue_d        (queue_d),
      
      .mac_tx_areset  (mac_tx_areset),
      .mac_tx_clk     (mac_tx_clk),
      .mac_tx_ena     (mac_tx_ena),
      .mac_tx_busy    (mac_tx_busy),
      .mac_tx_send    (mac_tx_send),
      .mac_tx_d       (mac_tx_d),
      .mac_tx_ack     (mac_tx_ack)
    );

endmodule

module dbg_reg
#(
  parameter W = 10
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  input  wire           set,
  input  wire [W-1: 0]  d,
  output wire [W-1: 0]  q
);
  logic [W-1: 0] r;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      r <= '0;
    else if (ena) begin
      if (set)
        r <= d;
    end
  
  assign q = r;

endmodule

module mac_tx_sgdma
#(
  parameter SW = 4,
  parameter AW = 10
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  
  input  wire           start,
  input  wire [AW-1: 0] offset,
  input  wire [SW-1: 0] size,
  output wire           busy,

  output wire [AW-1: 0] mem_addr,
  output wire           mem_read,
  input  wire [31: 0]   mem_q,

  input  wire           queue_full,
  output wire           queue_push,
  output wire [ 2: 0]   queue_cnt,
  output wire [31: 0]   queue_d
);

  localparam SAW = AW + 2;
  localparam SSW = AW + 2;

  typedef logic [AW-1: 0]  mem_addr_t;
  
  typedef logic [AW-1: 0]  des_addr_t;  // descriptor address, word aligned
  typedef logic [SW-1: 0]  des_size_t;  // decriptor size, word size
  
  typedef logic [SAW-1: 0]  seg_addr_t;  // segment address, byte aligned
  typedef logic [SSW-1: 0]  seg_size_t;  // segment size, byte size

  enum int unsigned
  {
    stIDLE,
    stREQUEST_SEGMENT_PARAMS,
    stLATCH_SEGMENT_PARAMS,
    stTRANSFER_SEGMENT,
    stWRITE_EOF
  } state, next_state;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  des_addr_t des_addr_d, des_addr_q;
  logic des_addr_set, des_addr_inc;
  lpm_counter#(
    .LPM_WIDTH      (AW),
    .LPM_DIRECTION  ("UP")
  )
    des_addr
    (
      .aclr   (areset), 
      .clock  (clk), 
      .clk_en (ena),    
      .cnt_en (des_addr_inc), 
      .sload  (des_addr_set), 
      .data   (des_addr_d),
      .q      (des_addr_q)
    );
  
  des_size_t des_size_d, des_size_q;
  logic des_size_set, des_size_dec;
  lpm_counter#(
    .LPM_WIDTH      (SW),
    .LPM_DIRECTION  ("DOWN")
  )
    des_size
    (
      .aclr   (areset), 
      .clock  (clk), 
      .clk_en (ena),    
      .cnt_en (des_size_dec), 
      .sload  (des_size_set), 
      .data   (des_size_d),
      .q      (des_size_q)
    );

  seg_addr_t seg_addr_d, seg_addr_q;
  logic seg_addr_set;

  seg_size_t seg_size_d, seg_size_q;
  logic seg_size_set;
/*
  dbg_reg#( .W (SAW) )
    seg_addr
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      .set    (seg_addr_set),
      .d      (seg_addr_d),
      .q      (seg_addr_q)
    );

  dbg_reg#( .W (SSW) )
    seg_size
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      .set    (seg_size_set),
      .d      (seg_size_d),
      .q      (seg_size_q)
    );
*/

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      seg_addr_q <= '0;
      seg_size_q <= '0;
    end
    else if (ena) begin
    
      if (seg_addr_set)
        seg_addr_q <= seg_addr_d;
        
      if (seg_size_set)
        seg_size_q <= seg_size_d;
    end


  logic [ 2: 0] seg_size_sub;
  always_comb begin
    unique case (seg_addr_q[ 1: 0])
      2'b00 : seg_size_sub = 3'd4;
      2'b01 : seg_size_sub = 3'd3;
      2'b10 : seg_size_sub = 3'd2;
      2'b11 : seg_size_sub = 3'd1;
    endcase
    
    if (seg_size_sub > seg_size_q)
      seg_size_sub = seg_size_q[ 2: 0];
  end
  
  logic         pipe_full;
  logic         pipe_push;
  logic [31: 0] pipe_d;
  logic [ 2: 0] pipe_cnt;
  logic [ 1: 0] pipe_sel;
    
  always_comb begin
    next_state = state;

    des_size_set  = '0;
    des_size_dec  = '0;
    des_size_d    = 'x;
    
    des_addr_set  = '0;
    des_addr_inc  = '0;
    des_addr_d    = 'x;

    seg_addr_set  = '0;
    seg_addr_d    = 'x;
    
    seg_size_set  = '0;
    seg_size_d    = 'x;
    
    mem_read      = '0;
    mem_addr      = 'x;
    
    pipe_d        = mem_q;

    pipe_push     = '0;
    pipe_cnt      = 'x;
    pipe_sel      = 'x;
    
    case (state)
    stIDLE : begin
      if (start) begin        
        des_addr_set = '1;
        des_addr_d   = offset;

        des_size_set = '1;
        des_size_d   = size;

        next_state = stREQUEST_SEGMENT_PARAMS;
      end
    end
    
    stREQUEST_SEGMENT_PARAMS : begin
      if (!pipe_full) begin
        mem_read        = '1;
        mem_addr        = des_addr_q;
        
        des_addr_inc    = '1;
        des_size_dec    = '1;
        
        next_state      = stLATCH_SEGMENT_PARAMS;
      end
    end
    
    stLATCH_SEGMENT_PARAMS : begin
      seg_addr_set = '1;
      seg_addr_d   = seg_addr_t'(mem_q[31:16]); // !
      
      seg_size_set = '1;
      seg_size_d   = seg_size_t'(mem_q[15: 0]); // !
      
      next_state   = stTRANSFER_SEGMENT;
    end

    stTRANSFER_SEGMENT : begin
      if (!pipe_full) begin
        mem_read      = '1;
        mem_addr      = seg_addr_q[SAW-1: 2];

        seg_addr_set  = '1;
        seg_addr_d    = seg_addr_q + seg_size_sub;
        
        seg_size_set  = '1;
        seg_size_d    = seg_size_q - seg_size_sub;
        
        pipe_push     = '1;
        pipe_cnt      = seg_size_sub;
        pipe_sel      = seg_addr_q[ 1: 0];
        
        if (seg_size_d != '0)
          next_state = stTRANSFER_SEGMENT;
        else
          next_state = (des_size_q != 0) ? stREQUEST_SEGMENT_PARAMS : stWRITE_EOF;
      end
    end
    
    stWRITE_EOF : begin
      if (!pipe_full) begin
        pipe_push = '1;
        pipe_cnt  = '0;
        pipe_sel  = 'x;
        
        next_state = stIDLE;
      end
    end
    
    endcase
  end // always_comb begin

  mac_tx_sgdma_mem_latency_compensate
    latency_compensator
    (
      .areset     (areset),
      .clk        (clk),
      .ena        (ena),
      
      .pipe_full  (pipe_full),
      .pipe_push  (pipe_push),
      .pipe_d     (pipe_d),
      .pipe_cnt   (pipe_cnt),
      .pipe_sel   (pipe_sel),
      
      .queue_full (queue_full),
      .queue_push (queue_push),
      .queue_cnt  (queue_cnt),
      .queue_d    (queue_d)
    );

  assign busy = (state != stIDLE);
  
endmodule

module mac_tx_sgdma_mem_latency_compensate
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  output wire         pipe_full,
  input  wire         pipe_push,
  input  wire [31: 0] pipe_d,
  input  wire [ 2: 0] pipe_cnt,
  input  wire [ 1: 0] pipe_sel,

  input  wire         queue_full,
  output wire         queue_push,
  output wire [ 2: 0] queue_cnt,
  output wire [31: 0] queue_d
);

  logic         push_dl;
  logic [ 2: 0] cnt_dl;
  logic [ 1: 0] sel_dl;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      push_dl <= '0;
      cnt_dl  <= '0;
      sel_dl  <= '0;
    end
    else if (ena) begin
      if (!queue_full) begin
        push_dl <= pipe_push;
        cnt_dl <= pipe_cnt;
        sel_dl <= pipe_sel;
      end
    end
  
  assign pipe_full = queue_full;
  
  assign queue_push = push_dl && !queue_full;
  assign queue_cnt  = cnt_dl;
  
  logic [31: 0] d;
  always_comb begin
    unique case (sel_dl)
      2'b00 : d = pipe_d;
      2'b01 : d = {  8'bx, pipe_d[31: 8] };
      2'b10 : d = { 16'bx, pipe_d[31:16] };
      2'b11 : d = { 24'bx, pipe_d[31:24] };
    endcase
  end
  
  assign queue_d    = d;

endmodule

module mac_tx_queue_sc
(
  input  wire         queue_areset,
  input  wire         queue_clk,
  input  wire         queue_ena,
  output wire         queue_full,
  input  wire         queue_push,
  input  wire [ 2: 0] queue_cnt,
  input  wire [31: 0] queue_d,
  
  input  wire         mac_tx_areset,
  input  wire         mac_tx_clk,
  input  wire         mac_tx_ena,
  input  wire         mac_tx_busy,
  output wire         mac_tx_send,
  output wire [ 7: 0] mac_tx_d,
  input  wire         mac_tx_ack
);
  logic         ws_write;
  logic [ 2: 0] ws_size;
  logic         ws_full;
  
  always_comb begin
    ws_write = queue_ena && queue_push && (queue_cnt != '0);
    ws_size  = queue_cnt;
    queue_full = ws_full || wait_for_mac_done;
  end

  logic         rs_read;
  logic [ 7: 0] rs_q;
  logic         rs_empty;

  mac_tx_realigner_v4f1
    realigner
    (
      .areset   (queue_areset),
      .clk      (queue_clk),
      
      .ws_write (ws_write),
      .ws_size  (ws_size),
      .ws_d     (queue_d),
      .ws_full  (ws_full),
      
      .rs_read  (rs_read),
      .rs_q     (rs_q),
      .rs_empty (rs_empty)
    );
  
  wire areset = mac_tx_areset;
  wire clk    = mac_tx_clk;
  wire ena    = mac_tx_ena;
  
  logic wait_for_mac_done;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      wait_for_mac_done <= '0;
    else begin
      if (!wait_for_mac_done)
        wait_for_mac_done <= queue_ena && queue_push && (queue_cnt == '0);
      else
        wait_for_mac_done <= !rs_empty;
    end
    
  assign mac_tx_send = !rs_empty;
  assign mac_tx_d    = rs_q;
  assign rs_read     = !rs_empty && mac_tx_ack && mac_tx_ena;

endmodule

module mac_tx_realigner_v4f1
(
  input  wire         areset,
  input  wire         clk,
  
  input  wire         ws_write,
  input  wire [ 2: 0] ws_size,
  input  wire [31: 0] ws_d,
  output wire         ws_full,
  
  input  wire         rs_read,
  output wire [ 7: 0] rs_q,
  output wire         rs_empty
);
  logic [ 3: 0][ 7: 0] d;
  always_comb
    d = ws_d;
  
  logic [ 3: 0][ 7: 0]  rp;
  logic [ 3: 0][ 7: 0]  nrp;
  
  logic [ 2: 0] cnt;
  logic [ 2: 0] ncnt;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      rp  <= '0;
      cnt <= '0;
    end
    else begin
      rp  <= nrp;
      cnt <= ncnt;
    end
  
  logic rs_can_read;
  logic ws_can_write;
  
  always_comb begin
    nrp  = rp;
    ncnt = cnt;

    rs_can_read = (cnt != 0);
  
    if (rs_read) begin
      ncnt = ncnt - 1'b1;
      nrp[ 2: 0]  = rp[ 3: 1];
    end
    
    ws_can_write = (ws_size <= (3'd4 - ncnt));
    
    if (ws_write) begin
      unique case (ncnt[ 1: 0])
        2'd0 : nrp[ 3: 0] = d[ 3: 0];
        2'd1 : nrp[ 3: 1] = d[ 2: 0];
        2'd2 : nrp[ 3: 2] = d[ 1: 0];
        2'd3 : nrp[ 3: 3] = d[ 0: 0];
      endcase
      ncnt = ncnt + ws_size;
    end
  end
  
  assign rs_empty = !rs_can_read;
  assign ws_full  = !ws_can_write;
  
  assign rs_q = rp[0];
  
endmodule
