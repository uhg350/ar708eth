`default_nettype none

module mac_rx_to_mmap
#(
  parameter MIN_FRAME_LENGTH = 64,
  parameter MAX_FRAME_LENGTH = 1522
)
(
  input  wire                   mac_rx_areset,
  input  wire                   mac_rx_clk,
  input  wire                   mac_rx_ena,
  output wire                   mac_rx_drop,
  input  mac_rx_states::type_t  mac_rx_state,
  input  mac_rx_eoferrs::type_t mac_rx_eoferr,
  input  wire [ 7: 0]           mac_rx_d,

  input  wire                   mmap_rx_areset,
  input  wire                   mmap_rx_clk,
  input  wire                   mmap_rx_clken,
  input  wire [ 9: 0]           mmap_rx_address,
  input  wire [ 3: 0]           mmap_rx_byteenable,
  input  wire                   mmap_rx_read,
  output wire [31: 0]           mmap_rx_readdata,
  input  wire                   mmap_rx_write,
  input  wire [31: 0]           mmap_rx_writedata
);

  localparam AW = 10;
  localparam DW = 32;
  localparam BW = 4;

  wire areset = mac_rx_areset;
  wire clk    = mac_rx_clk;
  wire ena    = mac_rx_ena;
  
  assign mac_rx_drop = '0; // not used

  logic           mem_ws_areset;
  logic           mem_ws_clk;
  logic           mem_ws_ena;
  logic           mem_ws_write;
  logic [AW-1: 0] mem_ws_addr;
  logic [BW-1: 0] mem_ws_be;
  logic [DW-1: 0] mem_ws_d;
  
  logic           mem_rs_areset;
  logic           mem_rs_clk;
  logic           mem_rs_ena;
  logic           mem_rs_read;
  logic [AW-1: 0] mem_rs_addr;
  logic [DW-1: 0] mem_rs_q;
  
  dcram_w32_r32#(
    .SIZE (2**AW),
    .AW   (AW)
    )
    mapped_fifo
    (
      .*
    );

  //
  logic [AW: 0]   ws_w_ptr;
  logic [AW: 0]   ws_w_ptr_sp;
  logic [AW: 0]   ws_r_ptr;
  logic [AW: 0]   ws_r_ptr_sp;
  logic [AW: 0]   rs_r_ptr;
  logic [AW: 0]   rs_r_ptr_sp;
  logic [AW: 0]   rs_w_ptr;
  logic [AW: 0]   rs_w_ptr_sp;
  
  // Same clocks, no need to CDC
  always_comb begin
    //ws_r_ptr    = rs_r_ptr;
    ws_r_ptr_sp = rs_r_ptr_sp;
    
    //rs_w_ptr    = ws_w_ptr;
    rs_w_ptr_sp = ws_w_ptr_sp;
  end
  
  logic is_sfd;
  logic is_data;
  logic is_efd;
  
  always_comb begin
    is_sfd  = (mac_rx_state == mac_rx_states::SFD);
    //is_data = (mac_rx_state == mac_rx_states::DATA);
    is_efd  = (mac_rx_state == mac_rx_states::EFD);
  end
  
  logic ws_fifo_full;
  logic rs_fifo_empty;
  
  always_comb begin
    ws_fifo_full  = { ~ws_r_ptr_sp[AW], ws_r_ptr_sp[AW-1: 0] } == ws_w_ptr;
    //rs_fifo_empty = (rs_r_ptr_sp == rs_w_ptr_sp);
  end
  
  localparam LW = $clog2(MAX_FRAME_LENGTH + 1);
  logic [LW-1: 0] frame_length;
  logic           frame_length_clr;
  logic           frame_length_inc;

  logic           frame_recv;
  logic           frame_recv_set;
  logic           frame_recv_d;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      frame_length  <= '0;
      frame_recv    <= '0;
    end
    else if (ena) begin
      if (frame_length_clr)
        frame_length <= '0;
      else
        frame_length <= frame_length + frame_length_inc;
        
      if (frame_recv_set)
        frame_recv <= frame_recv_d;
    end
  
  logic           ws_w_ptr_set;
  logic [AW: 0]   ws_w_ptr_d;
  
  logic           ws_w_ptr_sp_set;
  logic [AW: 0]   ws_w_ptr_sp_d;
    
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      ws_w_ptr    <= '0;
      ws_w_ptr_sp <= '0;
    end
    else if (ena) begin
      if (ws_w_ptr_set)
        ws_w_ptr <= ws_w_ptr_d;
        
      if (ws_w_ptr_sp_set)
        ws_w_ptr_sp <= ws_w_ptr_sp_d;
    end

  always_comb begin
    logic [ 3: 0] ws_w_be;
    logic frame_too_short;
    logic frame_too_long;
    logic is_no_error_on_efd;
    logic is_drop;

    mem_ws_areset     = areset;
    mem_ws_clk        = clk;
    mem_ws_ena        = ena;
    
    mem_ws_write      = '0;
    mem_ws_addr       = 'x;
    mem_ws_be         = 'x;
    mem_ws_d          = 'x;
    
    frame_length_clr  = '0;
    frame_length_inc  = '0;
    
    frame_recv_set    = '0;
    frame_recv_d      = 'x;
    
    ws_w_ptr_set      = '0;
    ws_w_ptr_d        = 'x;
    
    ws_w_ptr_sp_set   = '0;
    ws_w_ptr_sp_d     = '0;
    
    unique case (frame_length[ 1: 0])
      2'b00 : ws_w_be = 4'b0100;
      2'b01 : ws_w_be = 4'b1000;
      2'b10 : ws_w_be = 4'b0001;
      2'b11 : ws_w_be = 4'b0010;
    endcase
    
    if (is_sfd && !ws_fifo_full) begin
      frame_recv_set = '1;
      frame_recv_d   = '1;
      
      frame_length_clr  = '1;
    end
    
    frame_too_short = (frame_length  < MIN_FRAME_LENGTH);
    frame_too_long  = (frame_length == (MAX_FRAME_LENGTH + 1));
    
    is_no_error_on_efd = !frame_too_short && (mac_rx_eoferr == mac_rx_eoferrs::SUCCESS);
    
    is_drop = frame_too_long || ws_fifo_full;
    
    if (frame_recv) begin
      if (is_efd) begin
      
        if (is_no_error_on_efd) begin
          ws_w_ptr_set    = '1;
          ws_w_ptr_d      = ws_w_ptr + !ws_w_be[0];
          
          ws_w_ptr_sp_set = '1;
          ws_w_ptr_sp_d   = ws_w_ptr + !ws_w_be[0];

          mem_ws_write   = '1;
          mem_ws_addr    = ws_w_ptr_sp[AW-1: 0];
          mem_ws_be      = 4'b0011;
          mem_ws_d       = frame_length;
        end
        else begin
          ws_w_ptr_set    = '1;
          ws_w_ptr_d      = ws_w_ptr_sp;
        end
        
        frame_recv_set = '1;
        frame_recv_d   = '0;
      end
      else if (is_drop) begin
        ws_w_ptr_set    = '1;
        ws_w_ptr_d      = ws_w_ptr_sp;

        frame_recv_set = '1;
        frame_recv_d   = '0;
      end
      else begin        
        mem_ws_write   = '1;
        mem_ws_addr    = ws_w_ptr[AW-1: 0];
        mem_ws_be      = ws_w_be;
        mem_ws_d       = { 4 { mac_rx_d } };
        
        frame_length_inc  = '1;
        
        ws_w_ptr_set      = '1;
        ws_w_ptr_d        = ws_w_ptr + ws_w_be[3];
      end
    end
  end
  
  //// mmap
  logic read_packet_length;
  logic drop_current_packet;
  logic drop_all_packets;
  
  always_comb begin
    //read_packet_length  = (mmap_rx_read && mmap_rx_address == '0);
    
    drop_current_packet = (mmap_rx_write && mmap_rx_byteenable == 4'b0011);
    drop_all_packets    = (mmap_rx_write && mmap_rx_byteenable == 4'b1111);
  end
  
  logic packet_available;
  always_comb
    packet_available = (rs_r_ptr_sp != rs_w_ptr_sp);

  logic [AW: 0] drop_offset;
  always_comb
    drop_offset = mmap_rx_writedata[AW: 0];
  
  logic         pavail;
  always_ff@(posedge mmap_rx_areset or posedge mmap_rx_clk)
    if (mmap_rx_areset) begin
      rs_r_ptr_sp <= '0;
      pavail      <= '0;
    end
    else begin
      if (mmap_rx_clken) begin
        
        if (drop_all_packets) begin
          rs_r_ptr_sp <= rs_w_ptr_sp;
        end
        
        if (drop_current_packet) begin
          rs_r_ptr_sp <= rs_r_ptr_sp + drop_offset;
        end

      end

      pavail <= packet_available; // need one cycle delay
    end
  
  
  always_comb begin
    mem_rs_areset     = mmap_rx_areset;
    mem_rs_clk        = mmap_rx_clk;
    mem_rs_ena        = mmap_rx_clken;
    mem_rs_read       = mmap_rx_read;
    mem_rs_addr       = rs_r_ptr_sp[AW-1: 0] + mmap_rx_address[AW-1: 0];
    mmap_rx_readdata  = mem_rs_q & { 16'hFFFF, { 16 { pavail } } };
  end

endmodule
