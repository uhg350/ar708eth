module crc32_ieee_802_3_cis_impl
(
  input  wire [ 1: 0] n,
  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  localparam CRC_WIDTH     = 32;
  localparam CRC_POLYNOME  = 32'h04C11DB7;
  localparam CRC_INITIAL   = 32'hFFFFFFFF;
  localparam CRC_XOR       = 32'hFFFFFFFF;

  localparam DATA_WIDTH    = 8;

  localparam DATA_REFLECT  = 1;
  localparam CRC_REFLECT   = 1;

  typedef logic [DATA_WIDTH-1:0] data_t;
  typedef logic [CRC_WIDTH-1:0]  crc_t;

  crc_t remainder;
  crc_t new_remainder;
  crc_t inital_remainder;
  crc_t residue_remainder;
  data_t d;

  i_generic_crc#(
    .CRC_WIDTH    (CRC_WIDTH),
    .CRC_POLYNOME (CRC_POLYNOME),

    .CRC_INITIAL  (CRC_INITIAL),
    .CRC_XOR      (CRC_XOR),

    .DATA_WIDTH   (DATA_WIDTH),
    .DATA_REFLECT (DATA_REFLECT),
    .CRC_REFLECT  (CRC_REFLECT)
  )
    crc_computer(
      .data       (d), 
      .remainder  (remainder), 
      .q          (new_remainder), 
      .init       (inital_remainder), 
      .residue    (residue_remainder)
    );
  
  always_comb begin
    remainder  = dataa;
    
    case (n)
      2'b00 : d = datab[ 7: 0];
      2'b01 : d = datab[15: 8];
      2'b10 : d = datab[23:16];
      2'b11 : d = datab[31:24];
    endcase
  end
  
  assign result     = new_remainder;
  
endmodule
