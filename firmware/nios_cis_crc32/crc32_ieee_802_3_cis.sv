module crc32_ieee_802_3_cis
(
  input  wire [ 1: 0] n,
  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  crc32_ieee_802_3_cis_impl
    crc32_ieee_802_3_cis_i
    (
      .n      (n),
      .dataa  (dataa),
      .datab  (datab),
      .result (result)
    );

endmodule
