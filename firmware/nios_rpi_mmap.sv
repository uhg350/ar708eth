module nios_rpi_mmap
#(
  parameter W = 1,
  parameter S = 3
)
(
  input  wire           areset,
  input  wire           clk,
  
  input  wire [ 3: 0]   byteenable,
  input  wire           write,
  input  wire [31: 0]   writedata,
  input  wire           read,
  output wire [31: 0]   readdata,
  output wire           readdatavalid,
  output wire           waitrequest,
    
  input  wire [W-1: 0]  ipins
);

  nios_rpi_mmap_impl#(
    .W  (W),
    .S  (S)
  )
    impl
    (
      .areset         (areset),
      .clk            (clk),
      
      .byteenable     (byteenable),
      .write          (write),
      .writedata      (writedata),
      .read           (read),
      .readdata       (readdata),
      .readdatavalid  (readdatavalid),
      .waitrequest    (waitrequest),
      
      .ipins          (ipins)
    );

endmodule
