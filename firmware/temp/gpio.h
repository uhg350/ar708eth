#ifndef GPIO_H_
#define GPIO_H_

#include <stddef.h>
#include <stdint.h>

#define GPIO_CONFIG_NO_UPPER_TRASH

#ifndef _gp_value
#define _gp_value (0x8000)
#endif

extern char __page_offset_0x0000 __attribute__((aligned(65536)));

#define __assume_aligned(p, a)  __builtin_assume_aligned(p, a)
#define __is_constexpr(e)       __builtin_constant_p(e)
#define __is_constzero(e)       __builtin_choose_expr(__is_constexpr(e), (e == 0), 0)
#define __zeroreg()             ({ register int zero asm("r0"); zero; })
#define __is_gp_addressable(p)  __builtin_choose_expr(__is_constexpr(p), 1 , 0)

#ifdef _gp_value
#define __gp_offset(p)          ((intptr_t)(p) - _gp_value)
#else
#define __gp_offset(p)          (&__page_offset_0x0000 + (intptr_t)(p))
#endif

#if __GNUC__ > 4
#define __optimized_expr(e)     __builtin_choose_expr(__is_constzero(e), __zeroreg(), e)
#else
#define __optimized_expr(e)     (e)
#endif

#define IO_PTR_VU32(SYM)        ((volatile uint32_t * restrict const)(SYM))
#define IO_PTR_CVU32(SYM)       ((const volatile uint32_t * restrict const)(SYM))

#define IO_PTR_VU16(SYM)        ((volatile uint16_t * restrict const)(SYM))
#define IO_PTR_CVU16(SYM)       ((const volatile uint16_t * restrict const)(SYM))

#define IO_PTR_VU8(SYM)         ((volatile uint8_t * restrict const)(SYM))
#define IO_PTR_CVU8(SYM)        ((const volatile uint8_t * restrict const)(SYM))

#ifdef GPIO_CONFIG_NO_UPPER_TRASH
  #define RDIOU08N(p) \
    ({  unsigned int __value; \
        const volatile uint8_t *__pointer = (const volatile uint8_t *)(p); \
        asm volatile (" ldbuio %0, %1 \n"  : "=r"(__value) : "w"(*__pointer) : "memory"); __value; })

  #define RDIOS08N(p) \
    ({  signed int __value; \
        const volatile int8_t *__pointer = (const volatile int8_t *)(p); \
        asm volatile (" ldbio  %0, %1 \n"  : "=r"(__value) : "w"(*__pointer) : "memory"); __value; })

  #define RDIOU16N(p) \
    ({  unsigned int __value; \
        const volatile uint16_t *__pointer = (const volatile uint16_t *)(p); \
        asm volatile (" ldhuio %0, %1 \n"  : "=r"(__value) : "w"(*__pointer) : "memory"); __value; })

  #define RDIOS16N(p) \
    ({  signed int __value; \
        const volatile int16_t *__pointer = (const volatile int16_t *)(p); \
        asm volatile (" ldhio  %0, %1 \n"  : "=r"(__value) : "w"(*__pointer) : "memory"); __value; })

#else
  #define RDIOU08N(p)     __builtin_ldbuio(p)
  #define RDIOS08N(p)     __builtin_ldbio(p)
  #define RDIOU16N(p)     __builtin_ldhuio(p)
  #define RDIOS16N(p)     __builtin_ldhio(p)
#endif

#define RDIOU32N(p)       __builtin_ldwio((PTR))
#define RDIOS32N(p)       __builtin_ldwio((PTR))

#define RDGPU08N(p) ({ unsigned int v; asm volatile (" ldbuio %0, %%gprel(%1)(gp) "  : "=r"(v) : "i"(__gp_offset(p)) : "memory"); v; })
#define RDGPS08N(p) ({   signed int v; asm volatile (" ldbio  %0, %%gprel(%1)(gp) "  : "=r"(v) : "i"(__gp_offset(p)) : "memory"); v; })
#define RDGPU16N(p) ({ unsigned int v; asm volatile (" ldhuio %0, %%gprel(%1)(gp) "  : "=r"(v) : "i"(__gp_offset(p)) : "memory"); v; })
#define RDGPS16N(p) ({   signed int v; asm volatile (" ldhio  %0, %%gprel(%1)(gp) "  : "=r"(v) : "i"(__gp_offset(p)) : "memory"); v; })
#define RDGPU32N(p) ({ unsigned int v; asm volatile (" ldwio  %0, %%gprel(%1)(gp) "  : "=r"(v) : "i"(__gp_offset(p)) : "memory"); v; })
#define RDGPS32N(p) ({   signed int v; asm volatile (" ldwio  %0, %%gprel(%1)(gp) "  : "=r"(v) : "i"(__gp_offset(p)) : "memory"); v; })

#define RDIOU08(p) __builtin_choose_expr(__is_gp_addressable(p), RDGPU08N(p), RDIOU08N(p))
#define RDIOS08(p) __builtin_choose_expr(__is_gp_addressable(p), RDGPS08N(p), RDIOS08N(p))
#define RDIOU16(p) __builtin_choose_expr(__is_gp_addressable(p), RDGPU16N(p), RDIOU16N(p))
#define RDIOS16(p) __builtin_choose_expr(__is_gp_addressable(p), RDGPS16N(p), RDIOS16N(p))
#define RDIOU32(p) __builtin_choose_expr(__is_gp_addressable(p), RDGPU32N(p), RDIOU32N(p))
#define RDIOS32(p) __builtin_choose_expr(__is_gp_addressable(p), RDGPS32N(p), RDIOS32N(p))


#define WRGP32N(p, v) ({ asm volatile (" stwio %1, %%gprel(%0)(gp) " : : "i"(__gp_offset(p)), "r"(v) : "memory"); })
#define WRGP16N(p, v) ({ asm volatile (" sthio %1, %%gprel(%0)(gp) " : : "i"(__gp_offset(p)), "r"(v) : "memory"); })
#define WRGP08N(p, v) ({ asm volatile (" stbio %1, %%gprel(%0)(gp) " : : "i"(__gp_offset(p)), "r"(v) : "memory"); })

#define WRIO32(p, v) __builtin_choose_expr(__is_gp_addressable(p), WRGP32N(p, __optimized_expr(v)), __builtin_stwio(p, v))
#define WRIO16(p, v) __builtin_choose_expr(__is_gp_addressable(p), WRGP16N(p, __optimized_expr(v)), __builtin_sthio(p, v))
#define WRIO08(p, v) __builtin_choose_expr(__is_gp_addressable(p), WRGP08N(p, __optimized_expr(v)), __builtin_stbio(p, v))

#define GETPTR(x) x

#endif /* GPIO_H_ */
