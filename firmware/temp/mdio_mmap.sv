`default_nettype none

module mdio_mmap
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire [ 3: 0] mmap_addr,
  input  wire [ 3: 0] mmap_be,
  input  wire         mmap_write,
  input  wire [31: 0] mmap_d,
  input  wire         mmap_read,
  output wire [31: 0] mmap_q,
  output wire         mmap_wrq,
  
  output wire         smi_mdc,
  input  wire         smi_mdio_d,
  output wire         smi_mdio_oe,
  output wire         smi_mdio_q
);

  logic           u32access;
  assign          u32access = (mmap_be == 4'b1111);

  logic [ 4: 0] phy_addr;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      phy_addr <= '0;
    else if (ena) begin
      if (u32access && mmap_write)
        phy_addr <= mmap_d[4:0];
    end

  logic           u16low;
  logic           u16high;
  logic           u16access;
  logic           start;
  logic           done;
  logic           ready;
  logic [31: 0]   d;
  logic [15: 0]   q;
  
  logic [ 4: 0]   reg_addr;
  logic [15: 0]   reg_value;
  
  assign u16low     = (mmap_be == 4'b0011);
  assign u16high    = (mmap_be == 4'b1100);
  assign u16access  = (u16low || u16high);
  
  assign start = u16access && (mmap_write || mmap_read);
  assign mmap_wrq = start && !done;

  assign reg_addr   = { mmap_addr, u16high };
  assign reg_value  = u16low ? mmap_d[15:0] : mmap_d[31:16];
  
  assign mmap_q     = u32access ? {16'b0, 11'b0, phy_addr } : { q, q };
  
  assign d = { 2'b01, {mmap_read, mmap_write}, phy_addr, reg_addr, {mmap_write, 1'b0}, reg_value };

  mdio_trx
    mdio_trx_i0
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),

      .start  (start),
      .done   (done),
      .ready  (ready),

      .d      (d),
      .q      (q),

      .mdc    (smi_mdc),
      .mdio_oe(smi_mdio_oe),
      .mdio_d (smi_mdio_d),
      .mdio_q (smi_mdio_q)
    );
  
endmodule

module mdio_trx
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,

  input  wire         start,
  output wire         done,
  output wire         ready,

  input  wire [31: 0] d,
  output wire [15: 0] q,

  output wire         mdc,
  output wire         mdio_oe,
  input  wire         mdio_d,
  output wire         mdio_q
);

  localparam CW = 1;
  logic [CW:0] cdiv;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cdiv <= '0;
    else
      cdiv <= cdiv + 1'b1;
      
  logic cposedge;
  logic cnegedge;
  logic cdiv_lowpart_match;
  always_comb begin
    cdiv_lowpart_match = (&cdiv[CW-1:0]);
    cposedge = !cdiv[CW] && cdiv_lowpart_match;
    cnegedge =  cdiv[CW] && cdiv_lowpart_match;
  end
  
  logic [ 6: 0] bc;
  logic         idle;
  assign        idle = bc[6];
  logic [31: 0] shd;
  logic         oe;
  logic         inr;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      bc  <= '0;
      shd <= '0;
      oe  <= '0;
      inr <= '0;
    end
    else begin
      if (!idle) begin
        if (cposedge) begin
          bc  <= bc + 1'b1;
          inr <= mdio_d;
        end
        
        if (cnegedge && bc != 0) begin
          shd <= { shd[30:0], inr };
          if (bc == 14 || bc == 32) // TA bit
            oe <= shd[30] && (bc != 32);
        end
      end
      else begin
        bc[6] <= !start;
        shd   <= d;
        oe    <= start;
      end
    end

  logic done_pulse;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      done_pulse <= '0;
    else
      done_pulse <= cnegedge && bc == 32;

  logic mdc_clk_reg;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      mdc_clk_reg <= '0;
    else begin
      if      (cposedge)
        mdc_clk_reg <= '1;
      else if (cnegedge)
        mdc_clk_reg <= '0;
    end
  assign mdc = mdc_clk_reg;

  assign mdio_oe = oe;
  assign done = done_pulse;
  assign q = shd[15:0];
  assign mdio_q = shd[31];
  assign ready = idle;

endmodule
