/*
 * i_smi.h
 *
 *  Created on: 25 ���� 2017 �.
 *      Author: ilynxy
 */

#ifndef I_SMI_H_
#define I_SMI_H_

#include "system.h"
#include "gpio.h"

#define SMI_SetPHYAddress(x)      WRIO32(IO_PTR_VU32(SMI_CTRL_BASE), x)
#define SMI_GetPHYAddress()       RDIOU32(IO_PTR_CVU32(SMI_CTRL_BASE))

#define SMI_ReadRegister(ra)      RDIOU16(IO_PTR_CVU16(SMI_CTRL_BASE) + ra)
#define SMI_WriteRegister(ra, x)  WRIO16(IO_PTR_VU16(SMI_CTRL_BASE) + ra, x)

#endif /* I_SMI_H_ */
