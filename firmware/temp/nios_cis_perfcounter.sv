module nios_cis_perfcounter
(
  input  wire         reset,
  input  wire         clk,
  input  wire         clk_en,

  input  wire         start,
  output wire         done,

  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  logic [31: 0] tick_counter;
  
  always_ff@(posedge reset or posedge clk)
    if (reset)
      tick_counter <= '0;
    else begin
      if (start && clk_en)
        tick_counter <= '0;
      else
        tick_counter <= tick_counter + 1'b1;
    end
      
  assign result = tick_counter;

  assign done = start;

endmodule
