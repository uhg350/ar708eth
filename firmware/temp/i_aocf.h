/*
 * i_flash.h
 *
 *  Created on: 17 ���. 2017 �.
 *      Author: ilynxy
 */

#ifndef I_FLASH_H_
#define I_FLASH_H_

#include <stddef.h>
#include <stdint.h>

#include "system.h"
#include "gpio.h"

#define AOCF_STATUS_BUSY_MASK                     (0x00000003)
#define AOCF_STATUS_BUSY_IDLE                     (0x00000000)
#define AOCF_STATUS_BUSY_ERASE                    (0x00000001)
#define AOCF_STATUS_BUSY_WRITE                    (0x00000002)
#define AOCF_STATUS_BUSY_READ                     (0x00000003)

#define AOCF_CONTROL_ALLSECTOR_WRITE_PROTECT_MASK (0x0F800000)
#define AOCF_CONTROL_ALLSECTOR_WRITE_ENABLE       (0x00000000)
#define AOCF_CONTROL_ALLSECTOR_WRITE_DISABLE      (0x0F800000)

#define AOCF_CONTROL_PAGE_ERASE_MASK              (0x000FFFFF)
#define AOCF_CONTROL_PAGE_ERASE_NOT_SET           (0x000FFFFF)

#define AOCF_CONTROL_SECTOR_ERASE_MASK            (0x00700000)
#define AOCF_CONTROL_SECTOR_ERASE_NOT_SET         (0x00700000)

#define AOCF_GetStatus()          RDGP32(FLASH_CSR_BASE, 0x00)
#define AOCF_SetControl(x)        WRGP32(FLASH_CSR_BASE, 0x04, x)

#define AOCF_WriteEnable() \
  AOCF_SetControl(AOCF_CONTROL_ALLSECTOR_WRITE_ENABLE | AOCF_CONTROL_SECTOR_ERASE_NOT_SET | AOCF_CONTROL_PAGE_ERASE_NOT_SET)

#define AOCF_WriteDisable() \
  AOCF_SetControl(AOCF_CONTROL_ALLSECTOR_WRITE_DISABLE | AOCF_CONTROL_SECTOR_ERASE_NOT_SET | AOCF_CONTROL_PAGE_ERASE_NOT_SET)

#define AOCF_ErasePage(x) \
  AOCF_SetControl(AOCF_CONTROL_ALLSECTOR_WRITE_ENABLE | AOCF_CONTROL_SECTOR_ERASE_NOT_SET | x)

#endif /* I_FLASH_H_ */
