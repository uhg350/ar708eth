#
set smi_vclk_div 4
create_generated_clock -source [get_pins -hierarchical {*mdc_clk_reg|clk}] -divide_by $smi_vclk_div -name {smi_vclk} [get_pins -hierarchical {*mdc_clk_reg|q}]
create_generated_clock -source [get_pins -hierarchical {*mdc_clk_reg|q}] -name {mdc_clk} [get_ports {E100M_MDC}]

set_multicycle_path -to [get_clocks mdc_clk] -setup -start [expr {$smi_vclk_div / 2}]
set_multicycle_path -to [get_clocks mdc_clk] -hold  -start [expr {$smi_vclk_div - 1}]

set_output_delay -clock mdc_clk -max  10.0 [get_ports {E100M_MDIO}]
set_output_delay -clock mdc_clk -min -10.0 [get_ports {E100M_MDIO}]

set_multicycle_path -from [get_clocks mdc_clk] -setup -end  [expr {$smi_vclk_div - 0}]
set_multicycle_path -from [get_clocks mdc_clk] -hold  -end  [expr {$smi_vclk_div - 1}]

set_input_delay  -clock mdc_clk -max  30.0 [get_ports {E100M_MDIO}]
set_input_delay  -clock mdc_clk -min   0.0 [get_ports {E100M_MDIO}]
