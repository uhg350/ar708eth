/*
 * ar708eth.c
 *
 *  Created on: Jul 2, 2017
 *      Author: ilynxy
 */

#include "stdint.h"
#include "i_smi.h"
#include "global_vars.h"

#define BREAK() asm volatile (" break " ::: "memory");


void* memcpy(void *restrict dest, const void *restrict src, size_t count)
{
  if (count != 0)
  {
    char *restrict pd         = (char *restrict)dest;
    const char *restrict ps   = (const char *restrict)src;
    const char *restrict pse  = ps + count;
    do
    {
      *pd ++ = *ps ++;
    } while (ps < pse);
  }
  return dest;
}

int main(void)
{
  SMI_SetPHYAddress(0);
  SMI_SetPHYAddress(1); // Port B only is connected

  // SMI_WriteRegister(0x00, 0x8000); // Reset
  SMI_WriteRegister(0x00, 0x2100); // Only 100 MBit Full duplex

  uint32_t link_state = 0x0;

  for (;;)
  {
    uint32_t status = SMI_ReadRegister(0x10);
    if ( (status & 0x01) != link_state )
    {
      // Link state changed
      link_state = (status & 0x01);
      BREAK();

      g_struct0_copy = g_struct0;
      g_struct0.b = link_state;
    }
  }

  __builtin_unreachable();
}
