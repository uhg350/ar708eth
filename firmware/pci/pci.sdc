set pci_ins   [get_ports { PCI_AD[*] PCI_CBEN[*] PCI_PAR PCI_IDSEL PCI_FRAMEN PCI_IRDYN }]
set pci_outs  [get_ports { PCI_AD[*] PCI_PAR PCI_TRDYN PCI_DEVSELN PCI_STOPN PCI_PERRN PCI_SERRN }]

#set pci_oes   [get_ports { PCI_AD[*] PCI_PAR }]

# PCI 33MHz
set pci_clk_period  30.000

# tSU and tH (Time when Input Valid, MINimum before active clock edge, and MAXimum after active clock edge)
set pci_ti_min  7.000
set pci_ti_max  0.000

# tCO min and max (Time to Output become valid, MINimum and MAXimum after active clock edge)
set pci_to_min  2.000
set pci_to_max 11.000

# tOD min and max (Valid Time Output Disable MINimum and MAXimum after active clock edge)
set pci_td_min  2.000
set pci_td_max 28.000

set pci_in_delay_max  [expr { $pci_clk_period - $pci_ti_min }]
set pci_in_delay_min  [expr {                   $pci_ti_max }]

set pci_out_delay_max [expr { $pci_clk_period - $pci_to_max }]
set pci_out_delay_min [expr {                  -$pci_to_min }]

set pci_oe_delay_max  [expr { $pci_out_delay_max + $pci_td_max }]
set pci_oe_delay_min  [expr { $pci_out_delay_min + $pci_td_min }]

create_clock -period $pci_clk_period -name {pci_clk}  [get_ports {PCI_CLK}]
create_clock -period $pci_clk_period -name {pci_vclk}

set_false_path -from [get_ports {PCI_RSTN}]
set_false_path -to   [get_ports {PCI_INTAN}]

set_input_delay  -clock pci_vclk -max $pci_in_delay_max $pci_ins
set_input_delay  -clock pci_vclk -min $pci_in_delay_min $pci_ins

set_output_delay -clock pci_vclk -max $pci_out_delay_max $pci_outs
set_output_delay -clock pci_vclk -min $pci_out_delay_min $pci_outs

#set_max_delay -from [get_registers {*r_ad_oe* *r_par_oe*}] $pci_oe_delay_max
#set_min_delay -from [get_registers {*r_ad_oe* *r_par_oe*}] $pci_oe_delay_min
