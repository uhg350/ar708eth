module pci_stub
(
  input  wire             PCI_RSTN,
  input  wire             PCI_CLK,

  inout  wire [31: 0]     PCI_AD,
  input  wire [ 3: 0]     PCI_CBEN,
  inout  wire             PCI_PAR,

  input  wire             PCI_IDSEL,
  input  wire             PCI_FRAMEN,
  input  wire             PCI_IRDYN,
  output wire             PCI_TRDYN,
  output wire             PCI_DEVSELN,
  output wire             PCI_STOPN,
  
  inout  wire             PCI_INTAN,
  
  output wire             PCI_PERRN,
  inout  wire             PCI_SERRN
);

  assign PCI_AD       = 'z;
  assign PCI_PAR      = 'z;
  assign PCI_TRDYN    = '1;
  assign PCI_DEVSELN  = '1;
  assign PCI_STOPN    = '1;
  assign PCI_INTAN    = 'z;
  assign PCI_PERRN    = '1;
  assign PCI_SERRN    = 'z;


endmodule
