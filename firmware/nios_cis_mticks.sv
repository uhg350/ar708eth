module nios_cis_mticks
#(
  parameter CLKMS = 50000
)
(
  input  wire         clk,
  input  wire         areset,

  input  wire [31: 0] dataa,
  input  wire [31: 0] datab,
  output wire [31: 0] result
);

  nios_cis_mticks_impl#(
    .CLKMS  (50000)
  )
    nios_cis_mticks_i
    (
      .clk    (clk),
      .areset (areset),
      
      .dataa  (dataa),
      .datab  (datab),
      .result (result)
    );

endmodule
