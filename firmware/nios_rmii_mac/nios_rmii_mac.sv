module nios_rmii_mac
(
  input  wire           areset,
  input  wire           clk,
      
  input  wire           tcm_rx_clken,
  input  wire [ 9: 0]   tcm_rx_address,
  input  wire [ 3: 0]   tcm_rx_byteenable,
  input  wire           tcm_rx_read,
  output wire [31: 0]   tcm_rx_readdata,
  input  wire           tcm_rx_write,
  input  wire [31: 0]   tcm_rx_writedata,
      
  input  wire           tcm_tx_clken,
  input  wire [ 9: 0]   tcm_tx_address,
  input  wire [ 3: 0]   tcm_tx_byteenable,
  input  wire           tcm_tx_read,
  output wire [31: 0]   tcm_tx_readdata,
  input  wire           tcm_tx_write,
  input  wire [31: 0]   tcm_tx_writedata,
      
  input  wire [ 2: 0]   avs_ctl_address,
  input  wire [ 3: 0]   avs_ctl_byteenable,
  input  wire           avs_ctl_read,
  output wire [31: 0]   avs_ctl_readdata,
  input  wire           avs_ctl_write,
  input  wire [31: 0]   avs_ctl_writedata,
  
  output wire           RMII_RESET_N,
  input  wire           RMII_CLK,
  input  wire           RMII_RX_DV,
  input  wire           RMII_RX_ER,
  input  wire           RMII_CRS_DV,
  input  wire [ 1: 0]   RMII_RX_D,
  output wire           RMII_TX_EN,
  output wire           RMII_TX_ER,
  output wire [ 1: 0]   RMII_TX_D
);

  nios_rmii_mac_impl
    i0
    (
      .*
    );

endmodule

/*
extern module nios_rmii_mac_impl
(
  input  wire           areset,
  input  wire           clk,
      
  input  wire           tcm_rx_clken,
  input  wire [ 9: 0]   tcm_rx_address,
  input  wire [ 3: 0]   tcm_rx_byteenable,
  input  wire           tcm_rx_read,
  output wire [31: 0]   tcm_rx_readdata,
  input  wire           tcm_rx_write,
  input  wire [31: 0]   tcm_rx_writedata,
      
  input  wire           tcm_tx_clken,
  input  wire [ 9: 0]   tcm_tx_address,
  input  wire [ 3: 0]   tcm_tx_byteenable,
  input  wire           tcm_tx_read,
  output wire [31: 0]   tcm_tx_readdata,
  input  wire           tcm_tx_write,
  input  wire [31: 0]   tcm_tx_writedata,
      
  input  wire [ 2: 0]   avs_ctl_address,
  input  wire [ 3: 0]   avs_ctl_byteenable,
  input  wire           avs_ctl_read,
  output wire [31: 0]   avs_ctl_readdata,
  input  wire           avs_ctl_write,
  input  wire [31: 0]   avs_ctl_writedata,
  
  output wire           RMII_RESET_N,
  input  wire           RMII_CLK,
  input  wire           RMII_RX_DV,
  input  wire           RMII_RX_ER,
  input  wire           RMII_CRS_DV,
  input  wire [ 1: 0]   RMII_RX_D,
  output wire           RMII_TX_EN,
  output wire           RMII_TX_ER,
  output wire [ 1: 0]   RMII_TX_D
);
*/