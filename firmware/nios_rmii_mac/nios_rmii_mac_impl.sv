module nios_rmii_mac_impl
(
  input  wire           areset,
  input  wire           clk,
      
  input  wire           tcm_rx_clken,
  input  wire [ 9: 0]   tcm_rx_address,
  input  wire [ 3: 0]   tcm_rx_byteenable,
  input  wire           tcm_rx_read,
  output wire [31: 0]   tcm_rx_readdata,
  input  wire           tcm_rx_write,
  input  wire [31: 0]   tcm_rx_writedata,
      
  input  wire           tcm_tx_clken,
  input  wire [ 9: 0]   tcm_tx_address,
  input  wire [ 3: 0]   tcm_tx_byteenable,
  input  wire           tcm_tx_read,
  output wire [31: 0]   tcm_tx_readdata,
  input  wire           tcm_tx_write,
  input  wire [31: 0]   tcm_tx_writedata,
      
  input  wire [ 2: 0]   avs_ctl_address,
  input  wire [ 3: 0]   avs_ctl_byteenable,
  input  wire           avs_ctl_read,
  output wire [31: 0]   avs_ctl_readdata,
  input  wire           avs_ctl_write,
  input  wire [31: 0]   avs_ctl_writedata,
  
  output wire           RMII_RESET_N,
  input  wire           RMII_CLK,
  input  wire           RMII_RX_DV,
  input  wire           RMII_RX_ER,
  input  wire           RMII_CRS_DV,
  input  wire [ 1: 0]   RMII_RX_D,
  output wire           RMII_TX_EN,
  output wire           RMII_TX_ER,
  output wire [ 1: 0]   RMII_TX_D
);

  wire rmii_areset;
  wire rmii_mode;
  wire rmii_rx_active;
  
  assign rmii_areset = '0;
  //assign rmii_mode   = '0;
  assign rmii_rx_active = '1;

  wire          gmii_areset;
  wire          gmii_clk;
  wire          gmii_ena;
  wire          gmii_rx_dv;
  wire          gmii_rx_er;
  wire [ 7: 0]  gmii_rx_d;
  wire          gmii_tx_en;
  wire          gmii_tx_er;
  wire [ 7: 0]  gmii_tx_d;

  rmii_to_gmii
    rmii_to_gmii_i0
    (
      .rmii_areset    (rmii_areset),
      .rmii_clk       (RMII_CLK),
      .rmii_mode      (rmii_mode),
      .rmii_rx_active (rmii_rx_active),
      .rmii_rx_dv     (RMII_RX_DV),
      .rmii_crs_dv    (RMII_CRS_DV),
      .rmii_rx_er     (1'b0),
      .rmii_rx_d      (RMII_RX_D),
      .rmii_tx_en     (RMII_TX_EN),
      .rmii_tx_er     (RMII_TX_ER),
      .rmii_tx_d      (RMII_TX_D),

      .gmii_areset    (gmii_areset),
      .gmii_clk       (gmii_clk),
      .gmii_acq       (gmii_ena),
      .gmii_rx_dv     (gmii_rx_dv),
      .gmii_rx_er     (gmii_rx_er),
      .gmii_rx_d      (gmii_rx_d),
      .gmii_tx_en     (gmii_tx_en),
      .gmii_tx_er     (gmii_tx_er),
      .gmii_tx_d      (gmii_tx_d)
    );

  wire                    mac_rx_areset;
  wire                    mac_rx_clk;
  wire                    mac_rx_ena;
  mac_rx_states::type_t   mac_rx_state;
  mac_rx_eoferrs::type_t  mac_rx_eoferr;
  wire [ 7: 0]            mac_rx_d;
  
  gmii_to_mac_rx
    gmii_to_mac_rx_i0
    (
      .gmii_rx_areset (gmii_areset),
      .gmii_rx_clk    (gmii_clk),
      .gmii_rx_ena    (gmii_ena),
      .gmii_rx_dv     (gmii_rx_dv),
      .gmii_rx_er     (gmii_rx_er),
      .gmii_rx_d      (gmii_rx_d),

      .mac_rx_areset  (mac_rx_areset),
      .mac_rx_clk     (mac_rx_clk),
      .mac_rx_ena     (mac_rx_ena),
      .mac_rx_drop    (1'b0),
      .mac_rx_state   (mac_rx_state),
      .mac_rx_eoferr  (mac_rx_eoferr),
      .mac_rx_d       (mac_rx_d)
    );

  mac_rx_to_mmap
    mac_rx_to_mmap_i0
    (
      .mac_rx_areset      (mac_rx_areset),
      .mac_rx_clk         (mac_rx_clk),
      .mac_rx_ena         (mac_rx_ena),
      .mac_rx_drop        (),
      .mac_rx_state       (mac_rx_state),
      .mac_rx_eoferr      (mac_rx_eoferr),
      .mac_rx_d           (mac_rx_d),
      
      .mmap_rx_areset     (areset),
      .mmap_rx_clk        (clk),
      .mmap_rx_clken      (tcm_rx_clken),
      .mmap_rx_address    (tcm_rx_address),
      .mmap_rx_byteenable (tcm_rx_byteenable),
      .mmap_rx_read       (tcm_rx_read),
      .mmap_rx_readdata   (tcm_rx_readdata),
      .mmap_rx_write      (tcm_rx_write),
      .mmap_rx_writedata  (tcm_rx_writedata)
    );
//
  wire          mac_tx_areset;
  wire          mac_tx_clk;
  wire          mac_tx_ena;
  wire          mac_tx_busy;
  wire          mac_tx_send;
  wire [ 7: 0]  mac_tx_d;
  wire          mac_tx_ack;

  gmii_to_mac_tx
    gmii_to_mac_tx_i
    (
      .gmii_tx_areset     (gmii_areset),
      .gmii_tx_clk        (gmii_clk),
      .gmii_tx_ena        (gmii_ena),
      .gmii_tx_en         (gmii_tx_en),
      .gmii_tx_er         (gmii_tx_er),
      .gmii_tx_d          (gmii_tx_d),
      
      .mac_tx_areset      (mac_tx_areset),
      .mac_tx_clk         (mac_tx_clk),
      .mac_tx_ena         (mac_tx_ena),
      .mac_tx_busy        (mac_tx_busy),
      .mac_tx_send        (mac_tx_send),
      .mac_tx_d           (mac_tx_d),
      .mac_tx_ack         (mac_tx_ack)
    );
    
  wire          ctl_tx_sg_set;
  wire [15: 0]  ctl_tx_sg_offset;
  wire [15: 0]  ctl_tx_sg_length;
  wire          ctl_tx_sg_busy;

  mac_tx_to_mmap
    mac_tx_to_mmap_i
    (  
      .ctl_tx_sg_set      (ctl_tx_sg_set),
      .ctl_tx_sg_offset   (ctl_tx_sg_offset),
      .ctl_tx_sg_length   (ctl_tx_sg_length),
      .ctl_tx_sg_busy     (ctl_tx_sg_busy),

      .mmap_tx_areset     (areset),
      .mmap_tx_clk        (clk),
      .mmap_tx_clken      (tcm_tx_clken),
      .mmap_tx_address    (tcm_tx_address),
      .mmap_tx_byteenable (tcm_tx_byteenable),
      .mmap_tx_read       (tcm_tx_read),
      .mmap_tx_readdata   (tcm_tx_readdata),
      .mmap_tx_write      (tcm_tx_write),
      .mmap_tx_writedata  (tcm_tx_writedata),

      .mac_tx_areset      (mac_tx_areset),
      .mac_tx_clk         (mac_tx_clk),
      .mac_tx_ena         (mac_tx_ena),
      .mac_tx_busy        (mac_tx_busy),
      .mac_tx_send        (mac_tx_send),
      .mac_tx_d           (mac_tx_d),
      .mac_tx_ack         (mac_tx_ack)
    );

  logic r_rmii_mode;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_rmii_mode <= '0;
    else begin
      if (avs_ctl_write && (avs_ctl_address == 3'd1))
        r_rmii_mode <= avs_ctl_writedata[1];
    end
    
  logic r_rmii_reset;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_rmii_reset <= '0;
    else
      if (avs_ctl_write && (avs_ctl_address == 3'd7))
        r_rmii_reset <= avs_ctl_writedata[0];

  assign RMII_RESET_N = ~r_rmii_reset;

  assign ctl_tx_sg_set    = (avs_ctl_write && (avs_ctl_address == '0));
  assign ctl_tx_sg_length = avs_ctl_writedata[15: 0];
  assign ctl_tx_sg_offset = avs_ctl_writedata[31:16];
  assign avs_ctl_readdata = (avs_ctl_address == 3'd1) ? { 30'b0, r_rmii_mode, 1'b0 } : { 31'b0, ctl_tx_sg_busy };
  
  assign rmii_mode   = r_rmii_mode;

endmodule
