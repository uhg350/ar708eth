`default_nettype none
module cpu_i
(
  input  wire     areset,
  input  wire     clk,
  
  output wire     smi_mdc,
  input  wire     smi_mdio_d,
  output wire     smi_mdio_oe,
  output wire     smi_mdio_q
);
  logic [31: 0] crc32_iee802_3_cis_dataa;
  logic [31: 0] crc32_iee802_3_cis_datab;
  logic [31: 0] crc32_iee802_3_cis_result;
  logic [ 1: 0] crc32_iee802_3_cis_n;

  logic [31: 0] bswap_add1c_cis_dataa;
  logic [31: 0] bswap_add1c_cis_datab;
  logic [31: 0] bswap_add1c_cis_result;

  logic [31: 0] bswap_u16_cis_dataa;
  logic [31: 0] bswap_u16_cis_datab;
  logic [31: 0] bswap_u16_cis_result;

  logic         eth_mmap_arc_reset;
  logic         eth_mmap_rxm_clken;
  logic [ 9: 0] eth_mmap_rxm_address;
  logic [ 3: 0] eth_mmap_rxm_byteenable;
  logic         eth_mmap_rxm_read;
  logic [31: 0] eth_mmap_rxm_readdata;
  logic         eth_mmap_rxm_write;
  logic [31: 0] eth_mmap_rxm_writedata;
  
  logic         eth_mmap_txm_clken;
  logic [ 9: 0] eth_mmap_txm_address;
  logic [ 3: 0] eth_mmap_txm_byteenable;
  logic         eth_mmap_txm_read;
  logic [31: 0] eth_mmap_txm_readdata;
  logic         eth_mmap_txm_write;
  logic [31: 0] eth_mmap_txm_writedata;

  logic         ar708_mmap_arc_reset;
  logic         ar708_mmap_tcm_clken;
  logic [ 9: 0] ar708_mmap_tcm_address;
  logic [ 3: 0] ar708_mmap_tcm_byteenable;
  logic         ar708_mmap_tcm_read;
  logic [31: 0] ar708_mmap_tcm_readdata;
  logic         ar708_mmap_tcm_write;
  logic [31: 0] ar708_mmap_tcm_writedata;
  
  logic         smi_ctrl_arc_reset;
  logic [ 3: 0] smi_ctrl_avs_address;
  logic [ 3: 0] smi_ctrl_avs_byteenable;
  logic         smi_ctrl_avs_read;
  logic [31: 0] smi_ctrl_avs_readdata;
  logic         smi_ctrl_avs_write;
  logic [31: 0] smi_ctrl_avs_writedata;
  logic         smi_ctrl_avs_waitrequest;

  logic         eic_valid;
  logic [44: 0] eic_data;
  always_comb begin
    eic_valid = '0;
    eic_data  = '0;
  end

  mcpu
    mcpu_i0
    (
      .clk_clk                    (clk),
      .reset_reset_n              (!areset),

      .eic_valid                  (eic_valid),
      .eic_data                   (eic_data),
      
      .crc32_iee802_3_cis_dataa   (crc32_iee802_3_cis_dataa),  // crc32_iee802_3_cis.dataa
      .crc32_iee802_3_cis_datab   (crc32_iee802_3_cis_datab),  //                   .datab
      .crc32_iee802_3_cis_result  (crc32_iee802_3_cis_result), //                   .result
      .crc32_iee802_3_cis_n       (crc32_iee802_3_cis_n),      //                   .n

      .bswap_add1c_cis_dataa      (bswap_add1c_cis_dataa),     //    bswap_add1c_cis.dataa
      .bswap_add1c_cis_datab      (bswap_add1c_cis_datab),     //                   .datab
      .bswap_add1c_cis_result     (bswap_add1c_cis_result),    //                   .result

      .bswap_u16_cis_dataa        (bswap_u16_cis_dataa),       //    bswap_add1c_cis.dataa
      .bswap_u16_cis_datab        (bswap_u16_cis_datab),       //                   .datab
      .bswap_u16_cis_result       (bswap_u16_cis_result),      //                   .result
      
      .eth_mmap_arc_reset         (eth_mmap_arc_reset),        //   eth_mmap_arc.reset
      .eth_mmap_rxm_clken         (eth_mmap_rxm_clken),        //   eth_mmap_rxm.clken
      .eth_mmap_rxm_address       (eth_mmap_rxm_address),      //               .address
      .eth_mmap_rxm_byteenable    (eth_mmap_rxm_byteenable),   //               .byteenable
      .eth_mmap_rxm_read          (eth_mmap_rxm_read),         //               .read
      .eth_mmap_rxm_readdata      (eth_mmap_rxm_readdata),     //               .readdata
      .eth_mmap_rxm_write         (eth_mmap_rxm_write),        //               .write
      .eth_mmap_rxm_writedata     (eth_mmap_rxm_writedata),    //               .writedata

      .eth_mmap_txm_clken         (eth_mmap_txm_clken),        //   eth_mmap_txm.clken
      .eth_mmap_txm_address       (eth_mmap_txm_address),      //               .address
      .eth_mmap_txm_byteenable    (eth_mmap_txm_byteenable),   //               .byteenable
      .eth_mmap_txm_read          (eth_mmap_txm_read),         //               .read
      .eth_mmap_txm_readdata      (eth_mmap_txm_readdata),     //               .readdata
      .eth_mmap_txm_write         (eth_mmap_txm_write),        //               .write
      .eth_mmap_txm_writedata     (eth_mmap_txm_writedata),    //               .writedata
      
      .ar708_mmap_arc_reset       (ar708_mmap_arc_reset),      // ar708_mmap_arc.reset
      .ar708_mmap_tcm_clken       (ar708_mmap_tcm_clken),      // ar708_mmap_tcm.clken
      .ar708_mmap_tcm_address     (ar708_mmap_tcm_address),    //               .address
      .ar708_mmap_tcm_byteenable  (ar708_mmap_tcm_byteenable), //               .byteenable
      .ar708_mmap_tcm_read        (ar708_mmap_tcm_read),       //               .read
      .ar708_mmap_tcm_readdata    (ar708_mmap_tcm_readdata),   //               .readdata
      .ar708_mmap_tcm_write       (ar708_mmap_tcm_write),      //               .write
      .ar708_mmap_tcm_writedata   (ar708_mmap_tcm_writedata),  //               .writedata

      .smi_ctrl_arc_reset         (smi_ctrl_arc_reset),        //   smi_ctrl_arc.reset
      .smi_ctrl_avs_address       (smi_ctrl_avs_address),      //   smi_ctrl_avs.address
      .smi_ctrl_avs_read          (smi_ctrl_avs_read),         //               .read
      .smi_ctrl_avs_readdata      (smi_ctrl_avs_readdata),     //               .readdata
      .smi_ctrl_avs_write         (smi_ctrl_avs_write),        //               .write
      .smi_ctrl_avs_writedata     (smi_ctrl_avs_writedata),    //               .writedata
      .smi_ctrl_avs_waitrequest   (smi_ctrl_avs_waitrequest),  //               .waitrequest
      .smi_ctrl_avs_byteenable    (smi_ctrl_avs_byteenable)    //               .byteenable
    );

  crc32_iee802_3_cis_impl
    crc32_iee802_3_cis_impl_i0
    (
      .n      (crc32_iee802_3_cis_n),
      .dataa  (crc32_iee802_3_cis_dataa),
      .datab  (crc32_iee802_3_cis_datab),
      .result (crc32_iee802_3_cis_result)
    );

  bswap_add1c_cis_impl
    bswap_add1c_cis_impl_i0
    (
      .dataa  (bswap_add1c_cis_dataa),
      .datab  (bswap_add1c_cis_datab),
      .result (bswap_add1c_cis_result)
    );

  bswap_u16_cis_impl
    bswap_u16_cis_impl_i0
    (
      .dataa  (bswap_u16_cis_dataa),
      .datab  (bswap_u16_cis_datab),
      .result (bswap_u16_cis_result)
    );

  always_comb begin
    eth_mmap_rxm_readdata = '0;
    eth_mmap_txm_readdata = '0;
  end

  
  ar708_mmap
    ar708_mmap_i0
    (
      .areset     (ar708_mmap_arc_reset),
      .clk        (clk),
      .ena        (ar708_mmap_tcm_clken),
      
      .address    (ar708_mmap_tcm_address),
      .byteenable (ar708_mmap_tcm_byteenable),
      .read       (ar708_mmap_tcm_read),
      .readdata   (ar708_mmap_tcm_readdata),
      .write      (ar708_mmap_tcm_write),
      .writedata  (ar708_mmap_tcm_writedata)
    );

  mdio_mmap
    smi_mdio_mmap_i0
    (
      .areset       (smi_ctrl_arc_reset),
      .clk          (clk),
      .ena          (1'b1),
        
      .mmap_addr    (smi_ctrl_avs_address),
      .mmap_be      (smi_ctrl_avs_byteenable),
      .mmap_write   (smi_ctrl_avs_write),
      .mmap_d       (smi_ctrl_avs_writedata),
      .mmap_read    (smi_ctrl_avs_read),
      .mmap_q       (smi_ctrl_avs_readdata),
      .mmap_wrq     (smi_ctrl_avs_waitrequest),
      
      .smi_mdc      (smi_mdc),
      .smi_mdio_d   (smi_mdio_d),
      .smi_mdio_oe  (smi_mdio_oe),
      .smi_mdio_q   (smi_mdio_q)
    );


endmodule

