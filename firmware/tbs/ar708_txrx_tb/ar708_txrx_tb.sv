`timescale 1 ns / 1 ps

module ar708_txrx_tb
(
);

  initial begin
    #10000000;
    $stop;
  end

  localparam SYSCLK_FREQ = 50_000_000;

  logic areset;
  logic clk;

  clockgen_tb#(
    .FREQ        (SYSCLK_FREQ),
    .RESET_DELAY (40.0),
    .JITTER      (0.000)
  )
    sysclk_gen
    (
      .areset (areset),
      .clk    (clk)
    );

  logic tx_start;
  logic tx_busy;
  logic tx_acq;
  logic tx_d;
  logic tx_oe;
  logic tx_p;
  logic tx_n;
  
  initial begin
    tx_start = '0;
    #500 
    @(posedge clk)
      tx_start = '1;

    @(posedge clk)
      tx_start = '0;
  end
  
  logic [1599: 0] shr;
  always_ff @(posedge areset or posedge clk)
    if (areset)
      shr <= { 1592'b0, 8'b10110100 };
    else
      if (tx_acq)
        shr <= { shr[0], shr[1599: 1] };
  
  assign tx_d = shr[0];
  
  ar708_frame_transmitter#(
    .CLK_FREQ   (SYSCLK_FREQ),
    .AR708_FREQ (1_000_000)
  ) 
    ar708_tx
    (
      .areset (areset),
      .clk    (clk),
      
      .start  (tx_start),
      .busy   (tx_busy),
      
      .acq    (tx_acq),
      .d      (tx_d),
      
      .tx_oe  (tx_oe),
      .tx_p   (tx_p),
      .tx_n   (tx_n)
    );

  logic tx_bus;
  always_comb begin
    if ( !tx_oe || (tx_p == tx_n) )
      tx_bus = 'z;
    else
      tx_bus = tx_p;
  end

endmodule
