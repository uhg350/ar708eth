onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /ar708_txrx_tb/areset
add wave -noupdate /ar708_txrx_tb/clk
add wave -noupdate /ar708_txrx_tb/tx_start
add wave -noupdate /ar708_txrx_tb/tx_busy
add wave -noupdate /ar708_txrx_tb/tx_acq
add wave -noupdate /ar708_txrx_tb/tx_d
add wave -noupdate /ar708_txrx_tb/tx_oe
add wave -noupdate /ar708_txrx_tb/tx_p
add wave -noupdate /ar708_txrx_tb/tx_n
add wave -noupdate -radix unsigned /ar708_txrx_tb/ar708_tx/step_q
add wave -noupdate -height 40 /ar708_txrx_tb/tx_bus
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4520000 ps} 0} {{Cursor 2} {1605040000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 219
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 1
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {20784576 ps}
