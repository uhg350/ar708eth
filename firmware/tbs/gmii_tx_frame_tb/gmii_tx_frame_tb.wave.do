onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /gmii_tx_frame_tb/gmii_tx_areset
add wave -noupdate /gmii_tx_frame_tb/gmii_tx_clk
add wave -noupdate /gmii_tx_frame_tb/gmii_tx_acq
add wave -noupdate /gmii_tx_frame_tb/gmii_tx_en
add wave -noupdate /gmii_tx_frame_tb/gmii_tx_er
add wave -noupdate -radix hexadecimal -radixshowbase 0 /gmii_tx_frame_tb/gmii_tx_d
add wave -noupdate /gmii_tx_frame_tb/frame_busy
add wave -noupdate /gmii_tx_frame_tb/frame_ena
add wave -noupdate /gmii_tx_frame_tb/frame_d
add wave -noupdate /gmii_tx_frame_tb/frame_ack
add wave -noupdate /gmii_tx_frame_tb/gmii_rx_areset
add wave -noupdate /gmii_tx_frame_tb/gmii_rx_clk
add wave -noupdate /gmii_tx_frame_tb/gmii_rx_acq
add wave -noupdate /gmii_tx_frame_tb/gmii_rx_dv
add wave -noupdate /gmii_tx_frame_tb/gmii_rx_er
add wave -noupdate /gmii_tx_frame_tb/gmii_rx_d
add wave -noupdate /gmii_tx_frame_tb/gmii_rx_frame_i0/state
add wave -noupdate /gmii_tx_frame_tb/drop
add wave -noupdate /gmii_tx_frame_tb/q
add wave -noupdate /gmii_tx_frame_tb/is_idle
add wave -noupdate /gmii_tx_frame_tb/is_preamble
add wave -noupdate /gmii_tx_frame_tb/is_sfd
add wave -noupdate /gmii_tx_frame_tb/is_data
add wave -noupdate /gmii_tx_frame_tb/is_eof
add wave -noupdate /gmii_tx_frame_tb/is_eoferr
add wave -noupdate /gmii_tx_frame_tb/is_crcerr
add wave -noupdate /gmii_tx_frame_tb/is_drop
add wave -noupdate -radix hexadecimal -radixshowbase 0 /gmii_tx_frame_tb/gmii_rx_frame_i0/frame_check_sum/remainder
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2900000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 240
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2221022 ps} {3260366 ps}
