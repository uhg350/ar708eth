`timescale 1 ns / 1 ps

module gmii_tx_frame_tb
(
);

  initial begin
    #1000000;
    $stop;
  end

  localparam SYSCLK_FREQ = 50_000_000;

  logic areset;
  logic clk;

  clockgen_tb#(
    .FREQ        (SYSCLK_FREQ),
    .RESET_DELAY (40.0),
    .JITTER      (0.000)
  )
    sysclk_gen
    (
      .areset (areset),
      .clk    (clk)
    );

  logic         gmii_tx_areset;
  logic         gmii_tx_clk;
  logic         gmii_tx_acq;
  logic         gmii_tx_en;
  logic         gmii_tx_er;
  logic [ 7: 0] gmii_tx_d;
  
  logic         frame_busy;
  logic         frame_ena;
  logic [ 7: 0] frame_d;
  logic         frame_ack;

  assign gmii_tx_areset = areset;
  assign gmii_tx_clk    = clk;
  
  initial begin
    gmii_tx_acq = '0;
    forever @(posedge clk)
      gmii_tx_acq <= ~gmii_tx_acq;
  end
  
  assign frame_d = '0;

  always_comb
    frame_ena = !frame_busy;

  gmii_tx_frame
    gmii_tx_frame_i0
    (
      .*
    );

  logic         gmii_rx_areset;
  logic         gmii_rx_clk;
  logic         gmii_rx_acq;
  logic         gmii_rx_dv;
  logic         gmii_rx_er;
  logic [ 7: 0] gmii_rx_d;
  logic         drop;
  logic [ 7: 0] q;
  logic [10: 0] csize;
  logic         is_idle;
  logic         is_preamble;
  logic         is_sfd;
  logic         is_data;
  logic         is_eof;
  logic         is_eoferr;
  logic         is_crcerr;
  logic         is_drop;

  assign gmii_rx_areset = gmii_tx_areset;
  assign gmii_rx_clk    = gmii_tx_clk;
  assign gmii_rx_acq    = gmii_tx_acq;
  assign gmii_rx_dv     = gmii_tx_en;
  assign gmii_rx_er     = gmii_tx_er;
  assign gmii_rx_d      = gmii_tx_d;
  
  assign drop = '0;
  
  gmii_rx_frame
    gmii_rx_frame_i0
    (
      .*
    );

endmodule
