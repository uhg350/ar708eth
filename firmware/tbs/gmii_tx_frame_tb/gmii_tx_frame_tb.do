set library_name   {gmii_tx_frame_tb}
set top_level      ${library_name}

proc tbcompile {} {
  global library_name
  uplevel #0 source "$library_name.files"
  uplevel #0 source {../compile_tb.tcl}
}

proc tbupdate {} {
  global last_compile_time
  set last_compile_time 0
  tbcompile
}

proc tbrun {} {
  restart -force
  run -all
}

tbupdate

vmap altera_mf_ver    "../verilog_libs/altera_mf_ver"
vmap altera_ver       "../verilog_libs/altera_ver"
vmap altera_lnsim_ver "../verilog_libs/altera_lnsim_ver"
vmap lpm_ver          "../verilog_libs/lpm_ver"
vmap sgate_ver        "../verilog_libs/sgate_ver"
vmap fiftyfivenm_ver  "../verilog_libs/fiftyfivenm_ver"
eval vsim -L altera_mf_ver -L altera_ver -L altera_lnsim_ver -L lpm_ver -L sgate_ver -L fiftyfivenm_ver -voptargs="+acc" $top_level

#eval vsim -voptargs="+acc" $top_level
