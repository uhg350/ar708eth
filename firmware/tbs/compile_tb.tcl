# compilation script
set time_now [clock seconds]
if [catch {set last_compile_time}] {
  set last_compile_time 0
}

vlib $library_name
vmap work $library_name

foreach {file_name} $file_list {
#  puts $file_name
  if { $last_compile_time < [file mtime $file_name] } {
    if [regexp {.vhdl?$} $file_name] {
      vcom -93 $file_name
    } else {
      vlog -sv $file_name
    }
    set last_compile_time 0
  }
}
set last_compile_time $time_now

