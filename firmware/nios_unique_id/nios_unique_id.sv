module nios_unique_id
(
  input  wire         areset,
  input  wire         clk,

  input  wire [ 0: 0] addr,
  input  wire         read,
  output wire         readdatavalid,
  output wire [31: 0] readdata,
  output wire         waitrequest
);

  nios_unique_id_impl
    i0
    (
      .areset         (areset),
      .clk            (clk),

      .addr           (addr),
      .read           (read),
      .readdatavalid  (readdatavalid),
      .readdata       (readdata),
      .waitrequest    (waitrequest)
    );

endmodule
