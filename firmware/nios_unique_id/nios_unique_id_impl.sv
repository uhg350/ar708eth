module nios_unique_id_impl
(
  input  wire         areset,
  input  wire         clk,

  input  wire [ 0: 0] addr,
  input  wire         read,
  output wire         readdatavalid,
  output wire [31: 0] readdata,
  output wire         waitrequest
);

  logic [63: 0] cid;
  unique_id
   unique_id_i
  (
    .clkin      (clk),
    .reset      (areset),
    .data_valid (),
    .chip_id    (cid)
  );

 
  logic rdvalid;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rdvalid <= '0;
    else
      rdvalid <= read;

  assign readdatavalid  = rdvalid;
  assign readdata       = addr ? cid[63:32] : cid[31: 0];
  
  assign waitrequest    = '0;

endmodule
