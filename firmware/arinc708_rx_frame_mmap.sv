module arinc708_rx_frame_mmap
(
  input  wire         mmap_rx_areset,
  input  wire         mmap_rx_clk,
  input  wire         mmap_rx_clken,
  input  wire [ 8: 0] mmap_rx_address,
  input  wire [ 3: 0] mmap_rx_byteenable,
  input  wire         mmap_rx_read,
  output wire [31: 0] mmap_rx_readdata,
  input  wire         mmap_rx_write,
  input  wire [31: 0] mmap_rx_writedata,
  
  input  wire         HI157X_RX_P,
  input  wire         HI157X_RX_N,
  
  output wire         active
);
  wire areset = mmap_rx_areset;
  wire clk    = mmap_rx_clk;
  wire ena    = '1;
//

  logic         mac_rx_sof;
  logic [ 7: 0] mac_rx_d;
  logic         mac_rx_dv;
  logic         mac_rx_eof;

  arinc708_rx_frame_u8
    arinc708_rx_frame_u8_i
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .HI157X_RX_P  (HI157X_RX_P),
      .HI157X_RX_N  (HI157X_RX_N),
      
      .sof          (mac_rx_sof),
      .d            (mac_rx_d),
      .dv           (mac_rx_dv),
      .eof          (mac_rx_eof)
    );
    
  logic [ 7: 0] mac_rx_d_bitrev;
  always_comb begin
    for (int i = 0; i < 8; ++ i)
      mac_rx_d_bitrev[i] = mac_rx_d[7 - i];
  end
    
  assign active = mac_rx_dv;
//

  localparam AW = 9;
  localparam DW = 32;
  localparam BW = 4;

  logic           mem_ws_areset;
  logic           mem_ws_clk;
  logic           mem_ws_ena;
  logic           mem_ws_write;
  logic [AW-1: 0] mem_ws_addr;
  logic [BW-1: 0] mem_ws_be;
  logic [DW-1: 0] mem_ws_d;
  
  logic           mem_rs_areset;
  logic           mem_rs_clk;
  logic           mem_rs_ena;
  logic           mem_rs_read;
  logic [AW-1: 0] mem_rs_addr;
  logic [DW-1: 0] mem_rs_q;
  
  dcram_w32_r32#(
    .SIZE (2**AW),
    .AW   (AW)
    )
    mapped_fifo
    (
      .*
    );
    
  //
  
  localparam MIN_FRAME_LENGTH = 4;
  localparam MAX_FRAME_LENGTH = 200;

  logic [AW: 0]   ws_w_ptr;
  logic [AW: 0]   ws_w_ptr_sp;
  logic [AW: 0]   ws_r_ptr;
  logic [AW: 0]   ws_r_ptr_sp;
  logic [AW: 0]   rs_r_ptr;
  logic [AW: 0]   rs_r_ptr_sp;
  logic [AW: 0]   rs_w_ptr;
  logic [AW: 0]   rs_w_ptr_sp;
  
  // Same clocks, no need to CDC
  always_comb begin
    //ws_r_ptr    = rs_r_ptr;
    ws_r_ptr_sp = rs_r_ptr_sp;
    
    //rs_w_ptr    = ws_w_ptr;
    rs_w_ptr_sp = ws_w_ptr_sp;
  end
  
  logic is_sfd;
  logic is_data;
  logic is_efd;
  
  always_comb begin
    is_sfd  = mac_rx_sof;
    is_data = mac_rx_dv;
    is_efd  = mac_rx_eof;
  end
  
  logic ws_fifo_full;
  logic rs_fifo_empty;
  
  always_comb begin
    ws_fifo_full  = { ~ws_r_ptr_sp[AW], ws_r_ptr_sp[AW-1: 0] } == ws_w_ptr;
    //rs_fifo_empty = (rs_r_ptr_sp == rs_w_ptr_sp);
  end
  
  localparam LW = $clog2(MAX_FRAME_LENGTH + 1);
  logic [LW-1: 0] frame_length;
  logic           frame_length_clr;
  logic           frame_length_inc;

  logic           frame_recv;
  logic           frame_recv_set;
  logic           frame_recv_d;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      frame_length  <= '0;
      frame_recv    <= '0;
    end
    else if (ena) begin
      if (frame_length_clr)
        frame_length <= '0;
      else
        frame_length <= frame_length + frame_length_inc;
        
      if (frame_recv_set)
        frame_recv <= frame_recv_d;
    end
  
  logic           ws_w_ptr_set;
  logic [AW: 0]   ws_w_ptr_d;
  
  logic           ws_w_ptr_sp_set;
  logic [AW: 0]   ws_w_ptr_sp_d;
    
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      ws_w_ptr    <= '0;
      ws_w_ptr_sp <= '0;
    end
    else if (ena) begin
      if (ws_w_ptr_set)
        ws_w_ptr <= ws_w_ptr_d;
        
      if (ws_w_ptr_sp_set)
        ws_w_ptr_sp <= ws_w_ptr_sp_d;
    end

  always_comb begin
    logic [ 3: 0] ws_w_be;
    logic frame_too_short;
    logic frame_too_long;
    logic is_no_error_on_efd;
    logic is_drop;

    mem_ws_areset     = areset;
    mem_ws_clk        = clk;
    mem_ws_ena        = ena;
    
    mem_ws_write      = '0;
    mem_ws_addr       = 'x;
    mem_ws_be         = 'x;
    mem_ws_d          = 'x;
    
    frame_length_clr  = '0;
    frame_length_inc  = '0;
    
    frame_recv_set    = '0;
    frame_recv_d      = 'x;
    
    ws_w_ptr_set      = '0;
    ws_w_ptr_d        = 'x;
    
    ws_w_ptr_sp_set   = '0;
    ws_w_ptr_sp_d     = '0;
    
    unique case (frame_length[ 1: 0])
      2'b00 : ws_w_be = 4'b0100;
      2'b01 : ws_w_be = 4'b1000;
      2'b10 : ws_w_be = 4'b0001;
      2'b11 : ws_w_be = 4'b0010;
    endcase
    
    if (is_sfd && !ws_fifo_full) begin
      frame_recv_set = '1;
      frame_recv_d   = '1;
      
      frame_length_clr  = '1;
    end
    
    frame_too_short = (frame_length  < MIN_FRAME_LENGTH);
    frame_too_long  = (frame_length == (MAX_FRAME_LENGTH + 1));
    
    is_no_error_on_efd = !frame_too_short; //&& (mac_rx_eoferr == mac_rx_eoferrs::SUCCESS);
    
    is_drop = frame_too_long || ws_fifo_full;
    
    if (frame_recv) begin
      if (is_efd) begin
      
        if (is_no_error_on_efd) begin
          ws_w_ptr_set    = '1;
          ws_w_ptr_d      = ws_w_ptr + !ws_w_be[0];
          
          ws_w_ptr_sp_set = '1;
          ws_w_ptr_sp_d   = ws_w_ptr + !ws_w_be[0];

          mem_ws_write   = '1;
          mem_ws_addr    = ws_w_ptr_sp[AW-1: 0];
          mem_ws_be      = 4'b0011;
          mem_ws_d       = frame_length;
        end
        else begin
          ws_w_ptr_set    = '1;
          ws_w_ptr_d      = ws_w_ptr_sp;
        end
        
        frame_recv_set = '1;
        frame_recv_d   = '0;
      end
      else if (is_drop) begin
        ws_w_ptr_set    = '1;
        ws_w_ptr_d      = ws_w_ptr_sp;

        frame_recv_set = '1;
        frame_recv_d   = '0;
      end
      else begin
        if (is_data) begin
          mem_ws_write   = '1;
          mem_ws_addr    = ws_w_ptr[AW-1: 0];
          mem_ws_be      = ws_w_be;
          mem_ws_d       = (frame_length != '0) ? { 4 { mac_rx_d_bitrev } } : { 4 { mac_rx_d } };
          
          frame_length_inc  = '1;
          
          ws_w_ptr_set      = '1;
          ws_w_ptr_d        = ws_w_ptr + ws_w_be[3];
        end
      end
    end
  end

  //// mmap
  logic read_packet_length;
  logic drop_current_packet;
  logic drop_all_packets;
  
  always_comb begin
    //read_packet_length  = (mmap_rx_read && mmap_rx_address == '0);
    
    drop_current_packet = (mmap_rx_write && mmap_rx_byteenable == 4'b0011);
    drop_all_packets    = (mmap_rx_write && mmap_rx_byteenable == 4'b1111);
  end
  
  logic packet_available;
  always_comb
    packet_available = (rs_r_ptr_sp != rs_w_ptr_sp);

  logic [AW: 0] drop_offset;
  always_comb
    drop_offset = mmap_rx_writedata[AW: 0];
  
  logic         pavail;
  always_ff@(posedge mmap_rx_areset or posedge mmap_rx_clk)
    if (mmap_rx_areset) begin
      rs_r_ptr_sp <= '0;
      pavail      <= '0;
    end
    else begin
      if (mmap_rx_clken) begin
        
        if (drop_all_packets) begin
          rs_r_ptr_sp <= rs_w_ptr_sp;
        end
        
        if (drop_current_packet) begin
          rs_r_ptr_sp <= rs_r_ptr_sp + drop_offset;
        end

      end

      pavail <= packet_available; // need one cycle delay
    end
  
  
  always_comb begin
    mem_rs_areset     = mmap_rx_areset;
    mem_rs_clk        = mmap_rx_clk;
    mem_rs_ena        = mmap_rx_clken;
    mem_rs_read       = mmap_rx_read;
    mem_rs_addr       = rs_r_ptr_sp[AW-1: 0] + mmap_rx_address[AW-1: 0];
    mmap_rx_readdata  = mem_rs_q & { 16'hFFFF, { 16 { pavail } } };
  end

endmodule


module arinc708_rx_frame_u8
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         HI157X_RX_P,
  input  wire         HI157X_RX_N,
  
  output wire         sof,
  output wire [ 7: 0] d,
  output wire         dv,
  output wire         eof
);
  
  logic rx_sof;
  logic rx_eof;
  logic rx_d;
  logic rx_dv;
  
  arinc708_rx_frame
    arinc708_rx_frame_i
    (
      .areset           (areset),
      .clk              (clk),
      .ena              (ena),
      
      .HI157X_RX_P      (HI157X_RX_P),
      .HI157X_RX_N      (HI157X_RX_N),
      
      .sof              (rx_sof),
      .d                (rx_d),
      .dv               (rx_dv),
      .eof              (rx_eof)
    );
  
  enum int unsigned
  {
    stIDLE,
    stSOF,
    stBIT_P,
    stBIT_N,
    stDV,
    stEOF
  } state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      state <= stIDLE;
    end
    else if (ena) begin
      state <= next_state;
    end
  
  logic bp_q;
  logic bp_d;
  logic bp_set;
  
  logic [ 2: 0] bc_q;
  logic [ 2: 0] bc_d;
  logic         bc_set;
  
  logic [ 7: 0] sh_q;
  logic [ 7: 0] sh_d;
  logic         sh_set;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      bp_q <= '0;
      bc_q <= '0;
      sh_q <= '0;
    end
    else if (ena) begin
      if (bp_set)
        bp_q <= bp_d;
        
      if (bc_set)
        bc_q <= bc_d;
      
      if (sh_set)
        sh_q <= sh_d;
    end

  always_comb begin
    next_state = state;
    
    bp_set  = '0;
    bp_d    = 'x;
    
    bc_set  = '0;
    bc_d    = 'x;
    
    sh_set  = '0;
    sh_d    = 'x;

    case (state)
    
    stIDLE: begin
      if (rx_dv) begin
        if (rx_sof)
          next_state = stSOF;
      end
    end
    
    stSOF: begin
      next_state = stBIT_P;
      
      bc_set  = '1;
      bc_d    = '0;
    end
    
    stBIT_P: begin
      if (rx_dv) begin
        next_state = stBIT_N;
        
        bp_set = '1;
        bp_d   = rx_d;
      end
    end
    
    stBIT_N: begin
      if (rx_dv) begin
      
        if (rx_eof || ( (bp_q ^ rx_d) == '0 ) ) begin
          next_state = stEOF;
        end
        else begin
          bc_set  = '1;
          bc_d    = bc_q + 1'b1;
          
          sh_set  = '1;
          sh_d    = { sh_q[ 6: 0], bp_q };
          
          if (bc_d == '0)
            next_state = stDV;
          else
            next_state = stBIT_P;
        end
      end
    end
    
    stDV: begin
      next_state = stBIT_P;
    end
      
    stEOF: begin
      next_state = stIDLE;
    end

    endcase
  end
  
  assign sof = (state == stSOF);
  assign eof = (state == stEOF);
  assign dv  = (state == stDV);
  assign d   = sh_q;

endmodule

module arinc708_rx_frame
(
  input  wire     areset,
  input  wire     clk,
  input  wire     ena,
  
  input  wire     HI157X_RX_P,
  input  wire     HI157X_RX_N,
  
  output wire     sof,
  output wire     d,
  output wire     dv,
  output wire     eof
);

  logic cc_rx_n;
  logic cc_rx_p;

  (* altera_attribute = "-name SYNCHRONIZER_TOGGLE_RATE 2000000" *)
  ccreg#(.W(2), .S(3), .R(0))
    ccreg_rx(
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .d        ( { HI157X_RX_N, HI157X_RX_P } ),
      
      .q        ( { cc_rx_n, cc_rx_p } )
    );

  // lost information about idle states, but why we need one?
  logic [ 1: 0] rx_d;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rx_d <= '0;
    else if (ena) begin
      if (cc_rx_p ^ cc_rx_n) begin
        rx_d[1] <= rx_d[0];
        rx_d[0] <= cc_rx_p;
      end
    end
  //
  localparam DIV_CLKS = 25;
  localparam SMP_CLKS = 13;
  localparam CW = $clog2(DIV_CLKS);
  
  
  logic ev_edge;
  always_comb
    ev_edge = ^rx_d;

  logic ev_sample;
  ecdes_samplepoint#( .CW (CW) )
    smp
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (ena),
      
      .divide_clks  (CW'(DIV_CLKS - 1)),
      .sample_clks  (CW'(SMP_CLKS - 1)),
      
      .ev_edge      (ev_edge),
      .ev_wrap      (),
      .ev_sample    (ev_sample)
    );
    
    
  //
  localparam RW = 10;
  logic [RW-1: 0] rp;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rp <= '0;
    else if (ena) begin
      if (ev_sample)
        rp <= { rp[RW-2: 0], rx_d[0] };
    end
    
  assign d = rp[RW-6];
  assign sof  = (rp[RW-1:RW-6] == 8'b111000) && (^rp[RW-7:RW-8]);
  assign eof  = (rp[ 5: 0] == 8'b000111) && (^rp[ 7: 6]);
  assign dv   = ev_sample;

endmodule

module ecdes_samplepoint
#(
  parameter CW = 10
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  
  input  wire [CW-1: 0] divide_clks,
  input  wire [CW-1: 0] sample_clks,
  
  input  wire           ev_edge,
  output wire           ev_wrap,
  output wire           ev_sample
);

  logic [CW-1: 0] cdiv;
  
  logic wrap;
  logic sample;
  
  always_comb begin
    wrap    = (cdiv == divide_clks);
    sample  = (cdiv == sample_clks);
  end
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cdiv <= '0;
    else if (ena) begin
      if (wrap || ev_edge)
        cdiv <= '0;
      else
        cdiv <= cdiv + 1'b1;
    end

  assign ev_wrap   = wrap;
  assign ev_sample = sample;

endmodule
