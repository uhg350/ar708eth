module nios_smi_ctrl
(
  input  wire         areset,
  input  wire         clk,
  
  input  wire [ 3: 0] mmap_addr,
  input  wire [ 3: 0] mmap_be,
  input  wire         mmap_write,
  input  wire [31: 0] mmap_d,
  input  wire         mmap_read,
  output wire         mmap_valid,
  output wire [31: 0] mmap_q,
  output wire         mmap_wrq,
  
  output wire         smi_mdc,
  input  wire         smi_mdio_d,
  output wire         smi_mdio_oe,
  output wire         smi_mdio_q
);

  nios_smi_ctrl_impl
    nios_smi_ctrl_i
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (1'b1),
      
      .mmap_addr    (mmap_addr),
      .mmap_be      (mmap_be),
      .mmap_write   (mmap_write),
      .mmap_d       (mmap_d),
      .mmap_read    (mmap_read),
      .mmap_valid   (mmap_valid),
      .mmap_q       (mmap_q),
      .mmap_wrq     (mmap_wrq),
      
      .smi_mdc      (smi_mdc),
      .smi_mdio_d   (smi_mdio_d),
      .smi_mdio_oe  (smi_mdio_oe),
      .smi_mdio_q   (smi_mdio_q)
    );

endmodule

