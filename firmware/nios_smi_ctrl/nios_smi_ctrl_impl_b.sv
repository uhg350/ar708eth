`default_nettype none

module nios_smi_ctrl_impl
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire [ 3: 0] mmap_addr,
  input  wire [ 3: 0] mmap_be,
  input  wire         mmap_write,
  input  wire [31: 0] mmap_d,
  input  wire         mmap_read,
  output wire         mmap_valid,
  output wire [31: 0] mmap_q,
  output wire         mmap_wrq,
  
  output wire         smi_mdc,
  input  wire         smi_mdio_d,
  output wire         smi_mdio_oe,
  output wire         smi_mdio_q
);

  //assign mmap_valid = mmap_read;
  //assign mmap_wrq = '0;
  //assign mmap_q   = 'x;

  logic [ 4: 0] phy_addr;
  
  always_comb
    phy_addr = '0;

  logic           u16low;
  logic           u16high;
  logic           u16access;
  logic           start;

  logic [ 4: 0]   reg_addr;
  logic [15: 0]   reg_value;
  
  logic [31: 0]   sr_data;

  always_comb begin
  
    u16low    = (mmap_be == 4'b0011);
    u16high   = (mmap_be == 4'b1100);
    u16access = (u16low || u16high);

    reg_addr   = { mmap_addr, u16high };
    reg_value  = u16low ? mmap_d[15:0] : mmap_d[31:16];

    start     = u16access && (mmap_write || mmap_read);
    
    sr_data   = { 2'b01, {mmap_read, mmap_write}, phy_addr, reg_addr, {1'b1, 1'b0}, reg_value };

  end
  
  assign mmap_wrq   = (state != stIDLE);
  assign mmap_valid = valid_q;
  assign mmap_q     = { sr_q[15: 0], sr_q[15: 0] };

  //
  logic         ne;
  
  logic         oe_sload;
  logic         oe_d;
  
  logic         q_sload;
  logic         q_d;
  
  logic         sr_shift;
  logic         sr_sload;
  logic [31: 0] sr_d;
  logic [31: 0] sr_q;
  
  
  smi_ifregs#( .D (2) )
    smi_ifregs_i
    (
      .areset       (areset),
      .clk          (clk),

      .pedge        (),
      .nedge        (ne),

      .oe_sload     (oe_sload),
      .oe_d         (oe_d),
      
      .q_sload      (q_sload),
      .q_d          (q_d),
      
      .sr_shift     (sr_shift),
      .sr_sload     (sr_sload),
      .sr_d         (sr_d),
      .sr_q         (sr_q),

      .smi_mdc      (smi_mdc),
      .smi_mdio_d   (smi_mdio_d),
      .smi_mdio_oe  (smi_mdio_oe),
      .smi_mdio_q   (smi_mdio_q)
    );

  logic is_read_q;
  logic is_read_d;
  logic is_read_set;
  logic valid_d;
  logic valid_q;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      is_read_q <= '0;
      valid_q   <= '0;
    end
    else begin
      if (is_read_set)
        is_read_q <= is_read_d;
        
      valid_q <= valid_d;
    end

  logic [ 5: 0] bc_q;
  logic         bc_clr;
  logic         bc_inc;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      bc_q <= '0;
    else begin
      if (bc_clr)
        bc_q <= '0;
      else begin
        if (bc_inc)
          bc_q <= bc_q + 1'b1;
      end
    end
  
    
  enum int unsigned
  {
    stIDLE,
    stALIGN,
    stTRANSACT
  } state, next_state;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else
      state <= next_state;

  always_comb begin
    next_state = state;
    
    oe_sload  = '0;
    oe_d      = 'x;
    
    q_sload   = '0;
    q_d       = 'x;
    
    sr_shift  = '0;
    sr_sload  = '0;
    sr_d      = 'x;
    
    bc_clr    = '0;
    bc_inc    = '0;
    
    is_read_set = '0;
    is_read_d   = 'x;
    
    valid_d     = '0;
        
    case (state)
    
      stIDLE: begin
        oe_sload = '1;
        oe_d     = '1;
        
        q_sload  = '1;
        q_d      = '1;

        if (start) begin
          sr_sload  = '1;
          sr_d      = sr_data;
          
          bc_clr    = '1;
          
          is_read_set = '1;
          is_read_d   = mmap_read;
          
          if (ne) begin
            next_state  = stTRANSACT;
            oe_sload    = '1;
            oe_d        = '1;
            
            q_sload     = '1;
            q_d         = sr_data[31];
          end
          else
            next_state = stALIGN;
        end
      end
      
      stALIGN: begin
        if (ne) begin
          next_state = stTRANSACT;
          oe_sload    = '1;
          oe_d        = '1;

          q_sload     = '1;
          q_d         = sr_q[31];
        end
      end
      
      stTRANSACT : begin
        if (ne) begin
                    
          if (bc_q == 13 && is_read_q) begin
            oe_sload = '1;
            oe_d     = '0;
          end
          
          q_sload = '1;
          q_d     = sr_q[30];

          sr_shift = '1;
          bc_inc = '1;
          
          if (bc_q == 31) begin
            oe_sload  = '1;
            oe_d      = '1;
            
            q_sload = '1;
            q_d     = '1;
            
            valid_d   = is_read_q;
          
            next_state = stIDLE;
          end
        end
      end

    endcase
  
  end

endmodule

module smi_ifregs
#(
  parameter D = 2
)
(
  input  wire         areset,
  input  wire         clk,
  
  output wire         pedge,
  output wire         nedge,

  input  wire         oe_sload,
  input  wire         oe_d,
  
  input  wire         q_sload,
  input  wire         q_d,

  input  wire         sr_shift,
  input  wire         sr_sload,
  input  wire [31: 0] sr_d,
  output wire [31: 0] sr_q,
  
  output wire         smi_mdc,
  input  wire         smi_mdio_d,
  output wire         smi_mdio_oe,
  output wire         smi_mdio_q
);
  localparam W = $clog2(D);

  logic [W-1: 0] cdiv;

  logic pe;
  logic ne;
  
  always_comb begin
    pe = (cdiv == (D / 1 - 1));
    ne = (cdiv == (D / 2 - 1));
  end
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      cdiv <= '0;
    else begin
      if (cdiv == (D - 1))
        cdiv <= '0;
      else
        cdiv <= cdiv + 1'b1;
    end

  logic mdc_clk_reg;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      mdc_clk_reg <= '0;
    else begin
      if      (pe)
        mdc_clk_reg <= '1;
      else if (ne)
        mdc_clk_reg <= '0;
    end

  logic r_d;
  logic [31: 0] r_sr;
  logic r_oe;
  logic r_q;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_d <= '0;
      r_sr <= '0;
      r_oe <= '0;
      r_q  <= '0;
    end
    else begin
      if (pe)
        r_d <= smi_mdio_d;

      if (oe_sload)
        r_oe <= oe_d;
        
      if (q_sload)
        r_q <= q_d;
    
      if      (sr_sload)
        r_sr <= sr_d;
      else if (sr_shift)
        r_sr <= { r_sr[30: 0], r_d };
        
    end

  //assign d = r_d;
  
  assign sr_q        = r_sr;
  
  assign smi_mdc     = mdc_clk_reg;
  assign smi_mdio_oe = r_oe;
  assign smi_mdio_q  = r_q;

  assign pedge = pe;
  assign nedge = ne;
  
endmodule
