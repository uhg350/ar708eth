`default_nettype none

module ccreg
#(
  parameter W = 1,
  parameter S = 3,
  parameter R = 0
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  
  input  wire [W-1: 0]  d,
  output wire [W-1: 0]  q
);

  generate if (S != 0) begin
  
    (* altera_attribute = "-name SYNCHRONIZER_IDENTIFICATION \"FORCED IF ASYNCHRONOUS\"" *)
    logic [S-1: 0][W-1: 0] cc;
    
    always_ff@(posedge areset or posedge clk)
      if (areset)
        cc <= W'(R);
      else if (ena) begin
        cc[0] <= d;
        for (int i = 1; i < S; ++ i)
          cc[i] <= cc[i-1];
      end
    assign q = cc[S-1];
    
  end // if (STAGES != 0)
  else begin
  
    assign q = d;
    
  end // else if (STATES != 0)
  endgenerate

endmodule
