`default_nettype none

module dcram_w32_r32
#(
  parameter SIZE  = 8192,
  parameter AW    = $clog2(SIZE),
  parameter DEPTH = "AUTO"
)
(
  input  wire             mem_ws_areset,
  input  wire             mem_ws_clk,
  input  wire             mem_ws_ena,
  input  wire  [AW-1: 0]  mem_ws_addr,
  input  wire  [ 3: 0]    mem_ws_be,
  input  wire  [31: 0]    mem_ws_d,
  input  wire             mem_ws_write,

  input  wire             mem_rs_areset,
  input  wire             mem_rs_clk,
  input  wire             mem_rs_ena,
  input  wire  [AW-1: 0]  mem_rs_addr,
  input  wire             mem_rs_read,
  output wire  [31: 0]    mem_rs_q
);

  localparam  RAM_BW    = 4;
  localparam  RAM_SIZE  = SIZE;
  localparam  RAM_AW    = AW;
  localparam  RAM_DW    = 32;

  altsyncram#(
    .operation_mode("DUAL_PORT"),
    .power_up_uninitialized("TRUE"),
    .read_during_write_mode_mixed_ports("DONT_CARE"),
    
    .MAXIMUM_DEPTH    (DEPTH),

    .width_byteena_a  (RAM_BW),
    .byte_size        (8),

    .width_a          (RAM_DW),
    .widthad_a        (RAM_AW),
    .numwords_a       (RAM_SIZE),

    .address_reg_b    ("CLOCK1"),
    .outdata_reg_b    ("UNREGISTERED"),
    // .outdata_reg_b    ("CLOCK1"),

    .width_b          (RAM_DW),
    .widthad_b        (RAM_AW),
    .numwords_b       (RAM_SIZE)
    )
    ram_buffer(
      .clock0     (mem_ws_clk),
      .clocken0   (mem_ws_ena),
      .wren_a     (mem_ws_write),
      .address_a  (mem_ws_addr),
      .data_a     (mem_ws_d),
      .byteena_a  (mem_ws_be),
      
      .clock1     (mem_rs_clk),
      .clocken1   (mem_rs_ena),
      .rden_b     (mem_rs_read),
      .address_b  (mem_rs_addr),
      .q_b        (mem_rs_q),
      
      // elminate simulation warnings
      .wren_b         (),
      .rden_a         (),
      .data_b         (),
      .clocken2       (),
      .clocken3       (),
      .aclr0          (),
      .aclr1          (),
      .byteena_b      (),
      .addressstall_a (),
      .addressstall_b (),
      .q_a            (),
      .eccstatus      ()
    );

endmodule
