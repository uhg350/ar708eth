`default_nettype none

module crc32_ieee802_3
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,
  
  input  wire         init,
  input  wire         next,
  
  input  wire [ 7: 0] d,
  output wire [31: 0] crc
);

  localparam CRC_WIDTH     = 32;
  localparam CRC_POLYNOME  = 32'h04C11DB7;
  localparam CRC_INITIAL   = 32'hFFFFFFFF;
  localparam CRC_XOR       = 32'hFFFFFFFF;

  localparam DATA_WIDTH    = 8;

  localparam DATA_REFLECT  = 1;
  localparam CRC_REFLECT   = 1;

  typedef logic [DATA_WIDTH-1:0] data_t;
  typedef logic [CRC_WIDTH-1:0]  crc_t;

  crc_t remainder;
  crc_t new_remainder;
  crc_t inital_remainder;
  crc_t residue_remainder;

  i_generic_crc#(
    .CRC_WIDTH    (CRC_WIDTH),
    .CRC_POLYNOME (CRC_POLYNOME),

    .CRC_INITIAL  (CRC_INITIAL),
    .CRC_XOR      (CRC_XOR),

    .DATA_WIDTH   (DATA_WIDTH),
    .DATA_REFLECT (DATA_REFLECT),
    .CRC_REFLECT  (CRC_REFLECT)
  )
    crc_computer(
      .data       (d), 
      .remainder  (remainder), 
      .q          (new_remainder), 
      .init       (inital_remainder), 
      .residue    (residue_remainder)
    );

  always_ff@(posedge areset or posedge clk)
    if (areset)
      remainder <= '0; //inital_remainder;
    else
      if (ena) begin
        if (init)
          remainder <= inital_remainder;
        else 
          if (next)
            remainder <= new_remainder;
      end

  assign crc = remainder;
endmodule
