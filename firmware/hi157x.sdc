#set_false_path -to    [get_ports {HI157X*}]
#set_false_path -from  [get_ports {HI157X*}]
#
set_max_skew -from [get_ports {HI157X_RX_*[0]}] 10.0
set_max_skew -to   [get_ports {HI157X_TX_*[0]}] 10.0

set_false_path -to [get_ports {HI157X_TRX_LEDS[*]}]
