set tck_port [get_ports -nowarn {altera_reserved_tck}]
if {[get_collection_size $tck_port] != 0} {
  set tck_clock [get_clocks -nowarn {altera_reserved_tck}]

  if {[get_collection_size $tck_clock] == 0} {
    create_clock -name altera_reserved_tck -period "10MHz" [get_ports {altera_reserved_tck}]
    set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}]
  }
  
  set_input_delay  -clock altera_reserved_tck 20 [get_ports altera_reserved_tdi]
  set_input_delay  -clock altera_reserved_tck 20 [get_ports altera_reserved_tms]
  set_output_delay -clock altera_reserved_tck 20 [get_ports altera_reserved_tdo]
}
