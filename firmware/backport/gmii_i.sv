interface gmii_i
(
  input wire no_warning
);

  logic         tx_areset;
  logic         tx_clk;
  logic         tx_acq;
  logic         tx_en;
  logic         tx_er;
  logic [ 7: 0] tx_d;

  logic         rx_areset;
  logic         rx_clk;
  logic         rx_acq;
  logic         rx_dv;
  logic         rx_er;
  logic [ 7: 0] rx_d;

endinterface
