/*
  memory map

sg_descriptor:
  0x0000  sg_segment_0    { 16'size, 16'offset }
  0x0001  sg_segment_1    { 16'size, 16'offset }
  0x0002  sg_segment_2    { 16'size, 16'offset }
  ...
  0x0007  sg_segment_end  { 16'0000, 16'0000 }
  
  0x0008
  ...     frame data
  0xN000
*/

`default_nettype none

module byte_serializer_mmap_min_latency
#(
  parameter MAW = 10,
  parameter SAW = MAW + 2,
  parameter SSW = MAW + 2
)
(
  input  wire             areset,
  input  wire             clk,
  input  wire             ena,

  input  wire             start,
  input  wire [SAW-1: 0]  offset,
  input  wire [SSW-1: 0]  size,
  output wire             busy,
  
  input  wire             ser_ready,
  output wire             ser_write,
  output wire [ 7: 0]     ser_q,

  output wire             mem_read,
  output wire [MAW-1: 0]  mem_addr,
  input  wire [31: 0]     mem_q
);

  typedef logic [MAW-1: 0]  mem_addr_t;

  typedef logic [SAW-1: 0]  seg_addr_t;  // segment address
  typedef logic [SSW-1: 0]  seg_size_t;  // segment size

  seg_addr_t seg_addr_d, seg_addr_q;
  logic seg_addr_set, seg_addr_inc;
  lpm_counter#(
    .LPM_WIDTH      (SAW),
    .LPM_DIRECTION  ("UP")
  )
    seg_addr
    (
      .aclr   (areset), 
      .clock  (clk), 
      .clk_en (ena),
      .cnt_en (seg_addr_inc), 
      .sload  (seg_addr_set), 
      .data   (seg_addr_d),
      .q      (seg_addr_q)
    );
  
  seg_size_t seg_size_d, seg_size_q;
  logic seg_size_set, seg_size_dec;
  lpm_counter#(
    .LPM_WIDTH      (SSW),
    .LPM_DIRECTION  ("DOWN")
  )
    seg_size
    (
      .aclr   (areset),
      .clock  (clk),
      .clk_en (ena),
      .cnt_en (seg_size_dec),
      .sload  (seg_size_set),
      .data   (seg_size_d),
      .q      (seg_size_q)
    );

  logic [ 1: 0] seg_bi_q;
  logic [ 7: 0] mem_qi;

  mem_addr_t    seg_addr_to_mem_addr;
  mem_addr_t    offset_to_mem_addr;
  
  logic seg_last_byte;
  logic seg_bi_next;
  
  always_comb begin
    case (seg_addr_q[ 1: 0])
      2'b00 : seg_bi_q = 2'b11;
      2'b01 : seg_bi_q = 2'b00;
      2'b10 : seg_bi_q = 2'b01;
      2'b11 : seg_bi_q = 2'b10;
    endcase
    
    case (seg_bi_q)
      2'b00 : mem_qi = mem_q[ 7: 0];
      2'b01 : mem_qi = mem_q[15: 8];
      2'b10 : mem_qi = mem_q[23:16];
      2'b11 : mem_qi = mem_q[31:24];
    endcase
    
    seg_addr_to_mem_addr = seg_addr_q[SAW-1: 2];
    offset_to_mem_addr   = offset[SAW-1: 2];
    
    seg_bi_next   = (seg_bi_q == 2'b11);
    seg_last_byte = (seg_size_q == 1);

  end

  always_comb begin
    process   = (seg_size_q != 0);
    
    busy = process && !(seg_last_byte && ser_write);
    
    mem_read  = '0;
    mem_addr  = seg_addr_to_mem_addr;;

    ser_d       = mem_qi;

    seg_addr_set  = '0;
    seg_add_inc   = '0;
    seg_addr_d    = offset + 1'b1;
    
    seg_size_set  = '0;
    seg_size_dec  = '0;
    seg_size_d    = size - 1'b1;
    
    if (!busy) begin
      if (start) begin
        seg_addr_set  = '1;
        seg_size_set  = '1;
        
        mem_read      = '1;
        mem_addr      = offset_to_mem_addr[SAW-1: 2];
      end
    end
    
    if (process) begin
      if (ser_ready) begin
        ser_write   = '1;

        seg_addr_inc  = '1;
        seg_size_dec  = '1;
        
        mem_read = !seg_last_byte && seg_bi_next;
      end
    end
    
  end

endmodule

module sg_serializer_mmap
#(
  parameter SGW = 1,
  parameter MAW = 10
)
(
  input  wire             areset,
  input  wire             clk,
  input  wire             ena,
  
  input  wire             start,
  output wire             busy,
  input  wire [MAW-1: 0]  offset,
  input  wire [SGW-1: 0]  count,
  
  input  wire             pipe_empty,
  input  wire             pipe_ready,
  output wire             pipe_push,
  output wire [ 7: 0]     pipe_d,

  output wire             mem_areset,
  output wire             mem_clk,
  output wire             mem_ena,
  output wire             mem_read,
  output wire [MAW-1: 0]  mem_addr,
  input  wire [31: 0]     mem_q
);

  localparam SAW = MAW + 2;
  localparam SSW = MAW + 2;

  typedef logic [MAW-1: 0]  mem_addr_t;
  
  typedef logic [MAW-1: 0]  des_addr_t;  // descriptor address
  typedef logic [SGW-1: 0]  des_size_t;  // decriptor size
  
  typedef logic [SAW-1: 0]  seg_addr_t;  // segment address
  typedef logic [SSW-1: 0]  seg_size_t;  // segment size

  enum int unsigned
  { 
    stIDLE,
    stREQUEST_NEXT_SEGMENT_PARAMS,
    stLOAD_SEGMENT_PARAMS,
    stREQUEST_FIRST_DATA,
    stPUSH_DATA_AND_REQUEST_NEXT
  } state, next_state;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;
        
  des_addr_t des_addr_d, des_addr_q;
  logic des_addr_set, des_addr_inc;
  lpm_counter#(
    .LPM_WIDTH      (MAW),
    .LPM_DIRECTION  ("UP")
  )
    des_addr
    (
      .aclr   (areset), 
      .clock  (clk), 
      .clk_en (ena),    
      .cnt_en (des_addr_inc), 
      .sload  (des_addr_set), 
      .data   (des_addr_d),
      .q      (des_addr_q)
    );
  
  des_size_t des_size_d, des_size_q;
  logic des_size_set, des_size_dec;
  lpm_counter#(
    .LPM_WIDTH      (SGW),
    .LPM_DIRECTION  ("DOWN")
  )
    des_size
    (
      .aclr   (areset), 
      .clock  (clk), 
      .clk_en (ena),    
      .cnt_en (des_size_dec), 
      .sload  (des_size_set), 
      .data   (des_size_d),
      .q      (des_size_q)
    );

  seg_addr_t seg_addr_d, seg_addr_q;
  logic seg_addr_set, seg_addr_inc;
  lpm_counter#(
    .LPM_WIDTH      (SAW),
    .LPM_DIRECTION  ("UP")
  )
    seg_addr
    (
      .aclr   (areset), 
      .clock  (clk), 
      .clk_en (ena),    
      .cnt_en (seg_addr_inc), 
      .sload  (seg_addr_set), 
      .data   (seg_addr_d),
      .q      (seg_addr_q)
    );
  
  seg_size_t seg_size_d, seg_size_q;
  logic seg_size_set, seg_size_dec;
  lpm_counter#(
    .LPM_WIDTH      (SSW),
    .LPM_DIRECTION  ("DOWN")
  )
    seg_size
    (
      .aclr   (areset), 
      .clock  (clk), 
      .clk_en (ena),    
      .cnt_en (seg_size_dec), 
      .sload  (seg_size_set), 
      .data   (seg_size_d),
      .q      (seg_size_q)
    );
  
  logic [ 1: 0] seg_bi_q;
  logic [ 7: 0] mem_qi;

  mem_addr_t    seg_addr_to_mem_addr;
  
  logic seg_bi_done;
  logic seg_size_done;
  logic des_size_done;
  
  always_comb begin
    case (seg_addr_q[ 1: 0])
      2'b00 : seg_bi_q = 2'b11;
      2'b01 : seg_bi_q = 2'b00;
      2'b10 : seg_bi_q = 2'b01;
      2'b11 : seg_bi_q = 2'b10;
    endcase
    
    case (seg_bi_q)
      2'b00 : mem_qi = mem_q[ 7: 0];
      2'b01 : mem_qi = mem_q[15: 8];
      2'b10 : mem_qi = mem_q[23:16];
      2'b11 : mem_qi = mem_q[31:24];
    endcase
    
    seg_addr_to_mem_addr = seg_addr_q[SAW-1: 2];
    
    seg_bi_done   = (seg_bi_q == 2'b11);
    seg_size_done = (seg_size_q == 0);
    des_size_done = (des_size_q == 0);
    
    busy = (state != stIDLE);
  end

  always_comb begin
    next_state = state;

    des_size_set  = '0;
    des_size_dec  = '0;
    des_size_d    = 'x;
    
    des_addr_set  = '0;
    des_addr_inc  = '0;
    des_addr_d    = 'x;

    seg_addr_set  = '0;
    seg_addr_inc  = '0;
    seg_addr_d    = 'x;
    
    seg_size_set  = '0;
    seg_size_inc  = '0;
    seg_addr_d    = 'x;
    
    mem_read      = '0;
    mem_addr      = 'x;
    
    pipe_push     = '0;
    pipe_d        = 'x;
    
    case (state)
    stIDLE : begin
      if (start) begin
        des_size_set = '1;
        des_size_d   = count;
        
        des_addr_set = '1;
        des_addr_d   = offset;
        
        next_state = stREQUEST_SEGMENT_PARAMS;
      end
    end
    
    stREQUEST_NEXT_SEGMENT_PARAMS : begin
      mem_read        = '1;
      mem_addr        = des_address_q;
      
      des_addr_inc    = '1;
      des_size_dec    = '1;
      
      next_state      = stLOAD_NEXT_SEGMENT_PARAMS;
    end
    
    stLOAD_SEGMENT_PARAMS : begin
      seg_addr_set = '1;
      seg_addr_d   = seg_addr_t'(mem_q[15: 0]);
      
      seg_size_set = '1;
      seg_size_d   = seg_size_t'(mem_q[32:16]);
      
      next_state   = stREQUEST_FIRST_DATA;
    end
    
    stREQUEST_FIRST_DATA : begin
      mem_read = '1;
      mem_addr = seg_addr_to_mem_addr;

      seg_addr_inc  = '1;
      seg_size_dec  = '1;

      next_state = stPUSH_DATA_AND_REQUEST_NEXT;
    end
    
    stPUSH_DATA_AND_REQUEST_NEXT : begin
      if (pipe_ready) begin
        pipe_push     = '1;
        pipe_d        = mem_qi;
        
        seg_addr_inc  = '1;
        seg_size_dec  = '1;
        
        if (!seg_size_done) begin

          if (seg_bi_done) begin
            mem_read = '1;
            mem_addr = seg_addr_to_mem_addr;
          end

          next_state = stPUSH_DATA_AND_REQUEST_NEXT;
        end
        else begin
          next_state = (!des_size_done) ? stREQUEST_NEXT_SEGMENT_PARAMS : stIDLE;
        end
      end
    end

    endcase

  end
  
endmodule

/*
module eth_frame_tx_mmap_sg
#(
  parameter MAW = 10,
  parameter MDW = 32,
  
  parameter SAW = MAW + $clog2(MDW / 8) - 1;
  parameter SSW = SAW;
)
(
  input  wire                     gmii_areset,
  input  wire                     gmii_clk,
  input  wire                     gmii_acq,
  output wire                     gmii_tx_en,
  output wire                     gmii_tx_er,
  output wire [ 7: 0]             gmii_tx_d,

  output wire                     mem_areset,
  output wire                     mem_clk,
  output wire                     mem_ena,
  output wire                     mem_read,
  output wire [AW-1: 0]           mem_addr,
  input  wire [DW-1: 0]           mem_q,
  
  input  wire                     ctl_start,
  output wire                     ctl_busy
);

  localparam PREAMBLE_LENGTH  = 8;
  localparam PADDING_LENGTH   = 60;
  localparam IFG_LENGTH       = 12;

  localparam PREAMBLE_OCTET   = 8'h55;
  localparam SOF_OCTET        = 8'hD5;

  typedef logic [ 7: 0] octet_t;

  typedef logic [$clog2(SG)-1: 0] sg_index_t;
  typedef logic [AW-1: 0]         sg_offset_t;
  typedef logic [SW-1: 0]         sg_size_t;
  
  typedef logic [AW-1: 0]         mem_addr_t;
  typedef logic [DW-1: 0]         mem_data_t;

  wire areset = gmii_areset;
  wire clk    = gmii_clk;
  wire ena    = gmii_acq;
  
  assign mem_areset = areset;
  assign mem_clk    = clk;
  assign mem_ena    = ena;
  
  enum int unsigned
  { 
    sgIDLE,
    sgREAD_SEGMENT,
    sgLATCH_SEGMENT
  } sg_state, sg_next_state;

  always_ff@(posedge areset or posedge clk)
    if (areset)
      sg_state <= stIDLE;
    else
      if (ena)
        sg_state <= next_state;

endmodule
*/