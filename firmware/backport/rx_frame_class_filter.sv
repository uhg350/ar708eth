typedef struct packed
{
  logic [ 0: 1][ 7: 0]  port;
  logic [ 0: 5][ 7: 0]  mac;
  logic [ 0: 3][ 7: 0]  ip;

} filter_settings_t;

module dpipe
#(
  parameter W  = 8,
  parameter IL = 1,
  parameter OL = 1
)
(
  input  wire                   areset,
  input  wire                   clk,
  input  wire                   ena,
  
  input  wire [IL-1: 0][W-1: 0] d,
  output wire [OL-1: 0][W-1: 0] q
);

  logic [OL-1: 0][W-1: 0] r;

  generate
    
    if (IL >= OL) begin
    
      always_comb
        r = d[OL-1: 0];
        
    end
    else begin
    
      always_comb
        r[IL-1: 0] = d;
        
      always_ff@(posedge areset or posedge clk)
        if (areset)
          r[OL-1: IL] <= '0;
        else
          if (ena)
            r[OL-1: IL] <= r[OL-2:IL-1];

    end
  endgenerate
  
  assign q = r;

endmodule

module rx_frame_arp_filter
#(
  parameter SW = 11,
  parameter PL = 1
)
(
  input  wire                   areset,
  input  wire                   clk,
  input  wire                   ena,

  input  wire [PL-1: 0][ 7: 0]  d,
  input  wire [SW-1: 0]         offset,
  input  wire                   is_sfd,
  input  wire                   is_data,
  input  wire                   is_eof,
  input  filter_settings_t      fsettings,

  output wire                   not_arp_frame
);

  logic [ 1: 0][ 7: 0] dp;

  dpipe#(.W(8), .IL(PL), .OL(2))
    data_pipe
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      
      .d      (d),
      .q      (dp)
    );

  logic not_arp;
  
  logic not_match;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      not_arp <= '0;
    else if (ena) begin
      if (is_sfd)
        not_arp <= '0;
      else begin
        if (is_data)
          not_arp <= not_arp | not_match;
      end
    end
  
  always_comb begin
    not_match = '0;
  
    case (offset)
      'd13 : not_match = (dp != 16'h0806); // ether_type: ARP
      'd15 : not_match = (dp != 16'h0001); // hw type: Ethernet
      'd17 : not_match = (dp != 16'h0800); // protocol : IPv4
      'd19 : not_match = (dp != 16'h0604); // hw length: 6, proto length: 4
      'd21 : not_match = (dp != 16'h0001) && (dp != 16'h0002); // not request or reply
      'd39 : not_match = (dp != fsettings.ip[ 0: 1]); // high part of target ip
      'd41 : not_match = (dp != fsettings.ip[ 2: 3]); // low part of target ip
    endcase
    
  end

endmodule

module rx_frame_udp_filter
#(
  parameter SW = 11,
  parameter PL = 1
)
(
  input  wire                   areset,
  input  wire                   clk,
  input  wire                   ena,

  input  wire [PL-1: 0][ 7: 0]  d,
  input  wire [SW-1: 0]         offset,
  input  wire                   is_sfd,
  input  wire                   is_data,
  input  wire                   is_eof,
  input  filter_settings_t      fsettings,

  output wire                   not_udp_frame
);

  logic [ 1: 0][ 7: 0] dp;

  dpipe#(.W(8), .IL(PL), .OL(2))
    data_pipe
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      
      .d      (d),
      .q      (dp)
    );

  logic not_udp;
  
  logic not_match;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      not_udp <= '0;
    else if (ena) begin
      if (is_sfd)
        not_udp <= '0;
      else begin
        if (is_data)
          not_udp <= not_udp | not_match;
      end
    end
  
  always_comb begin
    not_match = '0;
  
    case (offset)
      'd13 : not_match = (dp != 16'h0800); // ether_type: IPv4
    endcase
    
  end

endmodule


module rx_frame_mac_filter
#(
  parameter SW = 11
)
(
  input  wire                 areset,
  input  wire                 clk,
  input  wire                 ena,
  
  input  wire [ 7: 0]         d,
  input  wire [SW-1: 0]       offset,
  input  wire                 is_sfd,
  input  wire                 is_data,
  input  wire                 is_eof,
  input  filter_settings_t    fsettings,
  
  output wire                 not_matched_mac,
  output wire                 not_broadcast_mac
);

  logic nmm;
  logic nbm;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      nmm <= '0;
      nbm <= '0;
    end
    else if (ena) begin
      if (is_sfd) begin
        nmm <= '0;
        nbm <= '0;
      end
      else begin 
        if (is_data && (offset < 6)) begin
          nmm <= nmm | (d != fsettings.mac[offset]);
          nbm <= nbm | (d != 8'hFF);
        end
      end
    end

  assign not_matched_mac    = nmm;
  assign not_broadcast_mac  = nbm;
endmodule


module rx_frame_class_filter
#(
  parameter SW = 11
)
(
  input  wire                 areset,
  input  wire                 clk,
  input  wire                 ena,
  
  input  wire [ 7: 0]         d,
  input  wire [SW-1: 0]       size,
  input  wire                 is_sfd,
  input  wire                 is_data,
  input  wire                 is_eof,

  input  filter_settings_t    fsettings,
  output wire [ 5: 0]         fclass,
  output wire                 fdrop
);

  // fclass ARP REQUEST, ARP REPLY (GRATIOTOUS if broadcast)
  //        UDP REQUEST, UDP REPLY
  
 

endmodule
