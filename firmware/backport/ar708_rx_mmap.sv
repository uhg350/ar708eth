module ar708_rx_mmap
(
  input  wire         areset,
  input  wire         clk,
  
  input  wire         mmap_ena,
  input  wire [ 7: 0] mmap_address,
  input  wire [ 3: 0] mmap_byteenable,
  input  wire         mmap_read,
  output wire [31: 0] mmap_readdata,
  input  wire         mmap_write,
  input  wire [31: 0] mmap_writedata
);

  dcram_w32_r32#(
    .SIZE (256)
  )
    mmap_fifo_buffer
    (
      .mem_ws_areset  (areset),
      .mem_ws_clk     (clk),
      .mem_ws_ena     (1'b0),
      .mem_ws_addr    (8'b0),
      .mem_ws_be      (4'b0),
      .mem_ws_d       (32'b0),
      .mem_ws_write   (1'b0),

      .mem_rs_areset  (areset),
      .mem_rs_clk     (clk),
      .mem_rs_ena     (mmap_ena),
      .mem_rs_addr    (mmap_address),
      .mem_rs_read    (mmap_read),
      .mem_rs_q       (mmap_readdata)
    );

endmodule
