module process_steps
#(
  parameter DIV     = 2,
  parameter STEPS   = 32,
  parameter WDIV    = $clog2(DIV),
  parameter WSTEPS  = $clog2(STEPS)
)
(
  input  wire               areset,
  input  wire               clk,
  input  wire               ena,
  
  input  wire               start,
  output wire               busy,

  output wire               process_q,

  output wire               next_div_q,
  output wire [WDIV-1: 0]   div_q,
  
  output wire               next_step_q,
  output wire [WSTEPS-1: 0] step_q
);
  
  typedef logic [WDIV-1: 0]   div_t;
  typedef logic [WSTEPS-1: 0] step_t;

  div_t   cdiv;
  step_t  cstep;
  
  logic last_cdiv;
  logic last_cstep;
  
  always_comb begin
    last_cdiv   = (cdiv  == (DIV - 1));
    last_cstep  = (cstep == (STEPS - 1));
  end
  
  logic process;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      process <= '0;
      cdiv    <= '0;
      cstep   <= '0;
    end
    else if (ena) begin
      if (!busy)
        process <= start;
        
      if (process) begin
        if (last_cdiv) begin
          cdiv <= '0;

          if (last_cstep)
            cstep <= '0;
          else
            cstep <= cstep + 1'b1;

        end
        else
          cdiv <= cdiv + 1'b1;
      end
    end

  assign process_q    = process;

  assign div_q        = cdiv;
  assign next_div_q   = last_cdiv;

  assign step_q       = cstep;
  assign next_step_q  = last_cstep;

  assign busy         = process && !(last_cdiv && last_cstep);

endmodule
