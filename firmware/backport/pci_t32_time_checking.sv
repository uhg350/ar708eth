`default_nettype none
module pci_t32_time_checking
(
  input  wire             rstn,
  input  wire             clk,

  input  wire [31: 0]     ad_d,
  output wire [31: 0]     ad_q,
  output wire [31: 0]     ad_oe,

  input  wire [ 3: 0]     cben,

  input  wire             par_d,
  output wire             par_q,
  output wire             par_oe,

  input  wire             idsel,
  input  wire             framen,
  input  wire             irdyn,
  output wire             trdyn,
  output wire             devseln,
  output wire             stopn,
  
  output wire             intan,
  
  output wire             perrn,
  output wire             serrn
);

  logic [ 2: 0] areset_pipe;
  always_ff@(negedge rstn or posedge clk)
    if (!rstn)
      areset_pipe <= '1;
    else
      areset_pipe <= { areset_pipe[ 1: 0], 1'b0 };
  wire areset = areset_pipe[2];
  
  logic g_oe;
  logic g_d;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      g_oe  <= '0;
      g_d   <= '0;
    end
    else begin
      g_oe  <= ~g_oe;
      g_d   <= ~g_d;
    end
  
  logic [31: 0] r_ad_d;
  logic [31: 0] r_ad_q;
  logic [31: 0] r_ad_oe;
  
  logic [ 3: 0] r_cben;

  logic         r_par_d;
  logic         r_par_q;
  logic         r_par_oe;

  logic         r_idsel;
  logic         r_framen;
  logic         r_irdyn;
  logic         r_trdyn;
  logic         r_devseln;
  logic         r_stopn;
  logic         r_intan;
  logic         r_perrn;
  logic         r_serrn;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_ad_d  <= '0;
      r_ad_q  <= '0;
      r_ad_oe <= '0;
    end
    else begin
      r_ad_d  <= ad_d;
      r_ad_q  <= { 32 { g_d } };
      r_ad_oe <= { 32 { g_oe } };
    end
  
  assign ad_q   = r_ad_q;
  assign ad_oe  = r_ad_oe;

  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_par_d   <= '0;
      r_par_q   <= '0;
      r_par_oe  <= '0;
    end
    else begin
      r_par_d   <= par_d;
      r_par_q   <= (^r_ad_d) ^ (^r_cben);
      r_par_oe  <= g_oe;
    end
  
  assign par_q  = r_par_q;
  assign par_oe = r_par_oe;
  
  wire noprune_src = r_par_d ^ r_idsel ^ r_framen ^ r_irdyn;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_cben    <= '0;
      
      r_idsel   <= '0;
      r_framen  <= '0;
      r_irdyn   <= '0;
      
      r_trdyn   <= '0;
      r_devseln <= '0;
      r_stopn   <= '0;
      r_intan   <= '0;
      r_perrn   <= '0;
      r_serrn   <= '0;
    end
    else begin
      r_cben    <= cben;
      
      r_idsel   <= idsel;
      r_framen  <= framen;
      r_irdyn   <= irdyn;

      r_trdyn   <= noprune_src;
      r_devseln <= g_d;
      r_stopn   <= g_d;
      r_intan   <= g_d;
      r_perrn   <= g_d;
      r_serrn   <= g_d;
    end
  
  
  assign  trdyn   = r_trdyn;
  assign  devseln = r_devseln;
  assign  stopn   = r_stopn;
  assign  intan   = r_intan;
  assign  perrn   = r_perrn;
  assign  serrn   = r_serrn;

endmodule
