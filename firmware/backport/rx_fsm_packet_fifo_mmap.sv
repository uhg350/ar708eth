interface avalon_tcm
#(
  parameter AW = 10
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  input  wire [AW-1: 0] address,
  input  wire [ 3: 0]   be,
  input  wire           write,
  input  wire [31: 0]   writedata,
  input  wire           read,
  output wire [31: 0]   readdata
);

endinterface

module gmii_rx_frame_mmap
#(
  parameter FRAME_FIFO_SIZE  = 8192,
  parameter MMAP_ADDR_WIDTH  = $clog2(FRAME_FIFO_SIZE/4),
  parameter FRAME_LENGTH_MIN = 32,   // minimum frame length (including CRC32)
  parameter FRAME_LENGTH_MAX = 1522  // maximum frame length (including CRC32)
)
(
  // gmii.rx interface
  input  wire                        gmii_rx_areset,
  input  wire                        gmii_rx_clk,
  input  wire                        gmii_rx_acq,
  input  wire                        gmii_rx_dv,
  input  wire                        gmii_rx_er,
  input  wire [ 7: 0]                gmii_rx_d,

  // avalon slave interface (tightly coupled memory compatible)
  input  wire                        avs_areset,
  input  wire                        avs_clk,
  input  wire                        avs_ena,
  input  wire [MMAP_ADDR_WIDTH-1: 0] avs_address,
  input  wire [ 3: 0]                avs_be,
  input  wire                        avs_write,
  input  wire [31: 0]                avs_writedata,
  input  wire                        avs_read,
  output wire [31: 0]                avs_readdata
);

  localparam FSW = $clog2(FRAME_LENGTH_MAX + 1);
  logic [FSW-1: 0] frame_size;

  gmii_rx_frame#(
    .SW (FSW)
  )
    rx_fsm
    (
      .gmii_rx_areset (gmii_rx_areset),
      .gmii_rx_clk    (gmii_rx_clk),
      .gmii_rx_acq    (gmii_rx_acq),
      .gmii_rx_dv     (gmii_rx_dv),
      .gmii_rx_er     (gmii_rx_er),
      .gmii_rx_d      (gmii_rx_d),

      .drop           (),
      .q              (),
      .csize          (frame_size),
      .is_idle        (),
      .is_preamble    (),
      .is_sfd         (),
      .is_data        (),
      .is_eof         (),
      .is_eoferr      (),
      .is_crcerr      (),
      .is_drop        ()
    );


endmodule

module rx_fsm_packet_fifo_mmap
#(
  parameter SIZE = 8192,
  parameter AW   = $clog2(SIZE)
)
(
  // write side
  input  wire         ws_areset,
  input  wire         ws_clk,
  input  wire         ws_ena,

  input  wire         ws_sop,
  input  wire         ws_eof,
  input  wire         ws_drop,

  input  wire [ 7: 0] ws_d,

  input  wire [15: 0] ws_hd,
  output wire [AW: 0] ws_size,
  
  output wire         ws_full,
  
  // read side
  input  wire         rs_areset,
  input  wire         rs_clk,
  input  wire         rs_ena,
  input  wire         rs_mmap_read,
  input  wire         rs_mmap_address,
  output wire [31: 0] rs_mmap_readdata,

  output wire         rs_packets_available,
  input  wire         rs_drop_all_packets,
  input  wire         rs_advance_ptr,
  input  wire [AW: 0] rs_advance_offset
);

  logic           mem_ws_areset;
  logic           mem_ws_clk;
  logic           mem_ws_ena;
  logic [AW-1: 0] mem_ws_addr;
  logic [ 3: 0]   mem_ws_be;
  logic [31: 0]   mem_ws_d;
  logic           mem_ws_write;
  
  logic           mem_rs_areset;
  logic           mem_rs_clk;
  logic           mem_rs_ena;
  logic [AW-1: 0] mem_rs_addr;
  logic           mem_rs_read;
  logic [31: 0]   mem_rs_q;
  
  always_comb begin
    mem_ws_areset = ws_areset;
    mem_ws_clk    = ws_clk;
    mem_ws_ena    = ws_ena;
    
    mem_rs_areset = rs_areset;
    mem_rs_clk    = rs_clk;
    mem_rs_ena    = rs_ena;
  end
  
  dcram_w32_r32
  #(
    .SIZE   (SIZE),
    .AW     (AW)
  )
    mmap_fifo_ram
    (
      .*
    );

  // fifo pointers
  logic [AW: 0] ws_wptr;
  logic [AW: 0] ws_rptr;
  
  logic [AW: 0] rs_rptr; // read side read prt
  logic [AW: 0] rs_wptr; // read side write ptr

  // write side logic
  logic [AW: 0] ws_wptr_c;
  
  // cross side logic
  assign ws_rptr = rs_rptr;
  assign rs_wptr = ws_wptr;
  
  // read side logic
  
  always_ff@(posedge rs_areset or posedge rs_clk)
    if (rs_areset)
      rs_rptr <= '0;
    else 
      if (rs_ena) begin
        if      (rs_drop_all_packets)
          rs_rptr <= rs_wptr;
        else if (rs_advance_ptr)
          rs_rptr <= rs_rptr + rs_advance_offset;
      end
  
  assign rs_packets_available = (rs_rptr != rs_wptr);
  
  always_comb begin
    mem_rs_read = rs_mmap_read;
    mem_rs_addr = rs_rptr[AW-1: 0] + rs_mmap_address;
  end
  
  assign rs_mmap_readdata = mem_rs_q;

endmodule

