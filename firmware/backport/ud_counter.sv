`default_nettype none

module ud_counter
#(
  parameter W   = 10,
  parameter DIR = "UP"
)
(
  input  wire           areset,
  input  wire           clk,
  input  wire           ena,
  
  input  wire           step,
  input  wire           set,
  input  wire [W-1: 0]  d,
  
  output wire [W-1: 0]  q
);
`ifdef MODEL_TECH  
  typedef logic [W-1: 0] counter_t;
  counter_t counter;
  localparam SD = (DIR == "UP") ? 1 : -1;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      counter <= '0;
    else
      if (ena) begin
        if (set) begin
          counter <= d;
        end
        else begin 
          //if (step)
          //counter <= counter + counter_t'(SD);
          if (DIR == "UP")
            counter <= counter + step;
          else
            counter <= counter - step;
        end
      end
  
  assign q = counter;
`else

  lpm_counter#(.LPM_WIDTH(W), .LPM_DIRECTION(DIR))
    counter
    (
      .aclr   (areset),
      .clock  (clk),
      .clk_en (ena),
      
      .sload  (set),
      .data   (d),
      .cnt_en (step),
      
      .q      (q)
    );

`endif

endmodule
