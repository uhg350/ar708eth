`default_nettype none

module i_generic_crc
#(
  parameter CRC_WIDTH     = 32,
  parameter CRC_POLYNOME  = 32'h04C11DB7,
  parameter CRC_INITIAL   = 32'hFFFFFFFF,
  parameter CRC_XOR       = 32'hFFFFFFFF,

  parameter DATA_WIDTH    = 8,

  parameter DATA_REFLECT  = 1,
  parameter CRC_REFLECT   = 1
)
(
  input   wire  [DATA_WIDTH-1: 0] data,
  input   wire  [CRC_WIDTH-1: 0]  remainder,
  output  wire  [CRC_WIDTH-1: 0]  q,
  output  wire  [CRC_WIDTH-1: 0]  init,
  output  wire  [CRC_WIDTH-1: 0]  residue
);
  typedef logic [DATA_WIDTH - 1: 0] data_t;
  typedef logic [CRC_WIDTH - 1: 0]  crc_t;
  
  assign q        = crc_next(data, remainder);
  assign init     = initial_crc();
  assign residue  = residue_crc();

  function crc_t compute_remainder(input data_t d, input crc_t c);
    
    for (int i = 0; i < DATA_WIDTH; i ++) begin
      logic b;

      b = d[DATA_WIDTH - 1 - i];

      b = b ^ c[CRC_WIDTH-1];
      c = (c << 1);

      if (b)
        c = c ^ CRC_POLYNOME;
      //c = c ^ (CRC_POLYNOME & { CRC_WIDTH { b } });
    end

    compute_remainder = c;
  endfunction

  function crc_t crc_next(input data_t d, input crc_t c);
    c = c ^ CRC_XOR;
    if (CRC_REFLECT != 0)
      c = reflect_crc(c);
      
    if (DATA_REFLECT != 0)
      d = reflect_data(d);

    c = compute_remainder(d, c);

    if (CRC_REFLECT != 0) 
      c = reflect_crc(c);
    
    c = c ^ CRC_XOR;
    
    crc_next = c;
  endfunction

  function crc_t reflect_crc(input crc_t c);
    for (int i = 0; i < CRC_WIDTH; i ++)
      reflect_crc[i] = c[CRC_WIDTH-1 - i];
  endfunction

  function data_t reflect_data(input data_t d);
    for (int i = 0; i < DATA_WIDTH; i ++)
      reflect_data[i] = d[DATA_WIDTH-1 - i];
  endfunction

  function crc_t initial_crc();
    initial_crc = CRC_INITIAL;
    
    if (CRC_REFLECT != 0)
      initial_crc = reflect_crc(initial_crc);
      
    initial_crc = initial_crc ^ CRC_XOR;
  endfunction

  function crc_t residue_crc();

    automatic crc_t d = initial_crc();
    automatic crc_t c = CRC_INITIAL;

    for (int i = 0; i < CRC_WIDTH; i ++) begin
      logic b;

      b = d[CRC_WIDTH - 1 - i];

      b = b ^ c[CRC_WIDTH-1];
      c = (c << 1);

      if (b)
        c = c ^ CRC_POLYNOME;
    end

    if (CRC_REFLECT != 0)
      c = reflect_crc(c);
      
    c = c ^ CRC_XOR;
    
    residue_crc = c;

  endfunction
  
endmodule

// Quartus doesn't compile functions inside interface in right way!

/*
interface i_generic_crc
#(
  parameter CRC_WIDTH     = 32,
  parameter CRC_POLYNOME  = 32'h04C11DB7,
  parameter CRC_INITIAL   = 32'hFFFFFFFF,
  parameter CRC_XOR       = 32'hFFFFFFFF,

  parameter DATA_WIDTH    = 8,

  parameter DATA_REFLECT  = 1,
  parameter CRC_REFLECT   = 1
)
(
  input   wire  [DATA_WIDTH-1: 0] d,
  input   wire  [CRC_WIDTH-1: 0]  c,
  output  wire  [CRC_WIDTH-1: 0]  r,
  output  wire  [CRC_WIDTH-1: 0]  i,
  output  wire  [CRC_WIDTH-1: 0]  rr
);
  typedef logic [DATA_WIDTH - 1: 0] data_t;
  typedef logic [CRC_WIDTH - 1: 0]  crc_t;
  
  assign r = crc_next(d, c);
  assign i = initial_crc();
  assign rr = residue_crc();

  function crc_t compute_remainder(input data_t d, input crc_t c);
    
    for (int i = 0; i < DATA_WIDTH; i ++) begin
      logic b;

      b = d[DATA_WIDTH - 1 - i];

      b = b ^ c[CRC_WIDTH-1];
      c = (c << 1);

      if (b)
        c = c ^ CRC_POLYNOME;
    end

    compute_remainder = c;
  endfunction

  function crc_t crc_next(input data_t d, input crc_t c);
    c = c ^ CRC_XOR;
    if (CRC_REFLECT != 0)
      c = reflect_crc(c);
      
    if (DATA_REFLECT != 0)
      d = reflect_data(d);

    c = compute_remainder(d, c);

    if (CRC_REFLECT != 0) 
      c = reflect_crc(c);
    
    c = c ^ CRC_XOR;
    
    crc_next = c;
  endfunction

  function crc_t reflect_crc(input crc_t c);
    for (int i = 0; i < CRC_WIDTH; i ++)
      reflect_crc[i] = c[CRC_WIDTH-1 - i];
  endfunction

  function data_t reflect_data(input data_t d);
    for (int i = 0; i < DATA_WIDTH; i ++)
      reflect_data[i] = d[DATA_WIDTH-1 - i];
  endfunction

  function crc_t initial_crc();
    initial_crc = CRC_INITIAL;
    
    if (CRC_REFLECT != 0)
      initial_crc = reflect_crc(initial_crc);
      
    initial_crc = initial_crc ^ CRC_XOR;
  endfunction

  function crc_t residue_crc();

    automatic crc_t d = initial_crc();
    automatic crc_t c = CRC_INITIAL;

    for (int i = 0; i < CRC_WIDTH; i ++) begin
      logic b;

      b = d[CRC_WIDTH - 1 - i];

      b = b ^ c[CRC_WIDTH-1];
      c = (c << 1);

      if (b)
        c = c ^ CRC_POLYNOME;
    end

    if (CRC_REFLECT != 0)
      c = reflect_crc(c);
      
    c = c ^ CRC_XOR;
    
    residue_crc = c;

  endfunction
  
endinterface
*/
