module gmii_rx_fsm
(
  input wire          gmii_areset,
  input wire          gmii_clk,
  input wire          gmii_acq,
  input wire          gmii_rx_dv,
  input wire          gmii_rx_er,
  input wire [ 7: 0]  gmii_rx_d
);

  typedef logic [ 7: 0] octet_t;

  localparam octet_t  PREAMBLE_OCTET = 8'h55;
  localparam octet_t  SOF_OCTET      = 8'hD5;

endmodule
