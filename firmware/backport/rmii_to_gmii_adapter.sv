/*
module rmii_to_gmii_adapter_i
(
  rmii_i  rmii,
  gmii_i  gmii
);

  logic gmii_areset;
  logic gmii_clk;
  logic gmii_acq;

  rmii_to_gmii_adapter
    adapter
    (
      .rmii_areset    (rmii.areset),
      .rmii_clk       (rmii.clk),
      .rmii_mode      (rmii.mode),
      .rmii_rx_active (rmii.rx_active),
      .rmii_rx_dv     (rmii.rx_dv),
      .rmii_rx_er     (rmii.rx_er),
      .rmii_rx_d      (rmii.rx_d),
      .rmii_tx_en     (rmii.tx_en),
      .rmii_tx_er     (rmii.tx_er),
      .rmii_tx_d      (rmii.tx_d),

      .gmii_areset    (gmii_areset),
      .gmii_clk       (gmii_clk),
      .gmii_acq       (gmii_acq),
      .gmii_rx_dv     (gmii.rx_dv),
      .gmii_rx_er     (gmii.rx_er),
      .gmii_rx_d      (gmii.rx_d),
      .gmii_tx_en     (gmii.tx_en),
      .gmii_tx_er     (gmii.tx_er),
      .gmii_tx_d      (gmii.tx_d)
    );

  assign gmii.tx_areset = gmii_areset;
  assign gmii.tx_clk    = gmii_clk;
  assign gmii.tx_acq    = gmii_acq;
  
  assign gmii.rx_areset = gmii_areset;
  assign gmii.rx_clk    = gmii_clk;
  assign gmii.rx_acq    = gmii_acq;

endmodule
*/

module rmii_to_gmii_adapter
(
  input  wire         rmii_areset,
  input  wire         rmii_clk,
  input  wire         rmii_mode,
  input  wire         rmii_rx_active,
  input  wire         rmii_rx_dv,
  input  wire         rmii_rx_er,
  input  wire [ 1: 0] rmii_rx_d,
  output wire         rmii_tx_en,
  output wire         rmii_tx_er,
  output wire [ 1: 0] rmii_tx_d,
  
  output wire         gmii_areset,
  output wire         gmii_clk,
  output wire         gmii_acq,
  output wire         gmii_rx_dv,
  output wire         gmii_rx_er,
  output wire [ 7: 0] gmii_rx_d,
  input  wire         gmii_tx_en,
  input  wire         gmii_tx_er,
  input  wire [ 7: 0] gmii_tx_d
);

  assign gmii_areset = rmii_areset;
  assign gmii_clk    = rmii_clk;


  logic [ 5: 0] acnt;
  logic [ 5: 0] acnt_max;
  logic         acnt_wrap;
  
  
  always_comb begin
    acnt_max  = rmii_mode ? 6'd39 : 6'd3; // (4*10 - 1) : (4*1 - 1);
    acnt_wrap = (acnt == acnt_max);
  end
  
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset)
      acnt <= '0;
    else begin
      if (acnt_wrap)
        acnt <= '0;
      else
        acnt <= acnt + 1'b1;
    end

  logic acq;
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset)
      acq <= '0;
    else
      acq <= acnt_wrap;

  assign gmii_acq = acq;
  
  rmii2gmii
    rmii2gmii_i0
    (
      .*
    );
    
  gmii2rmii
    gmii2rmii_i0
    (
      .*
    );

endmodule

module rmii2gmii
(
  input  wire         rmii_areset,
  input  wire         rmii_clk,
  input  wire         rmii_rx_active,
  input  wire         rmii_rx_dv,
  input  wire         rmii_rx_er,
  input  wire [ 1: 0] rmii_rx_d,
  
  input  wire         gmii_areset,
  input  wire         gmii_clk,
  input  wire         gmii_acq,
  output wire         gmii_rx_dv,
  output wire         gmii_rx_er,
  output wire [ 7: 0] gmii_rx_d
);

  // bufferize it
  logic         r_rmii_dv;
  logic         r_rmii_er;
  logic [ 1: 0] r_rmii_d;
  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset) begin
      r_rmii_dv <= '0;
      r_rmii_er <= '0;
      r_rmii_d  <= '0;
    end
    else begin
      r_rmii_dv <= rmii_rx_dv;
      r_rmii_er <= rmii_rx_er;
      r_rmii_d  <= rmii_rx_d;
    end

  // recover octet stream
  logic [ 1: 0] bcnt;
  logic [ 7: 0] octet;
  logic [ 3: 0] error;
  logic         valid;

  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset) begin
      bcnt  <= '0;
      octet <= '0;
      error <= '0;
      valid <= '0;
    end
    else begin
      if (r_rmii_dv) begin
        bcnt  <= bcnt + 1'b1;
        octet <= { r_rmii_d,  octet[7:2] };
        error <= { r_rmii_er, error[3:1] };
      end
      else
        bcnt <= '0;
      
      valid <= (bcnt == 2'b11) && r_rmii_dv;
    end

  logic [ 7: 0] rx_d;
  logic         rx_dv;
  logic         rx_er;
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset) begin
      rx_d  <= '0;
      rx_dv <= '0;
      rx_er <= '0;
    end
    else begin
      if (valid) begin
        rx_d  <= octet;
        rx_er <= |error;
      end

      if (valid || gmii_acq)
        rx_dv <= valid && rmii_rx_active;
    end

  assign gmii_rx_dv  = rx_dv;
  assign gmii_rx_er  = rx_er;
  assign gmii_rx_d   = rx_d;

endmodule

module gmii2rmii
(
  input  wire         gmii_areset,
  input  wire         gmii_clk,
  input  wire         gmii_acq,
  input  wire         gmii_tx_en,
  input  wire         gmii_tx_er,
  input  wire [ 7: 0] gmii_tx_d,

  input  wire         rmii_areset,
  input  wire         rmii_clk,
  output wire         rmii_tx_en,
  output wire         rmii_tx_er,
  output wire [ 1: 0] rmii_tx_d
);

  logic         tx_en;
  logic         tx_er;
  logic [ 7: 0] tx_d;
  logic [ 1: 0] tx_idx;
  always_ff@(posedge gmii_areset or posedge gmii_clk)
    if (gmii_areset) begin
      tx_en   <= '0;
      tx_er   <= '0;
      tx_d    <= '0;
      tx_idx  <= '0;
    end
    else begin
      if (gmii_acq) begin
        tx_en  <= gmii_tx_en;
        tx_er  <= gmii_tx_er;
        tx_d   <= gmii_tx_d;
        tx_idx <= '0;
      end
      else
        tx_idx <= tx_idx + 1'b1;
    end

  logic       rmii_en;
  logic       rmii_er;
  logic [1:0] rmii_d;

  always_ff@(posedge rmii_areset or posedge rmii_clk)
    if (rmii_areset) begin
      rmii_en <= '0;
      rmii_er <= '0;
      rmii_d  <= '0;
    end
    else begin
      rmii_en <= tx_en;
      rmii_er <= tx_er;
      
      case (tx_idx)
        2'b00 : rmii_d  <= tx_d[1:0];
        2'b01 : rmii_d  <= tx_d[3:2];
        2'b10 : rmii_d  <= tx_d[5:4];
        2'b11 : rmii_d  <= tx_d[7:6];
      endcase
    end

  assign rmii_tx_en = rmii_en;
  assign rmii_tx_er = rmii_er;
  assign rmii_tx_d  = rmii_d;
endmodule
