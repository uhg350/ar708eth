module hi157x_rx_adapter
(
  input  wire   areset,
  input  wire   clk,
  input  wire   ena,
  
  input  wire   rx_p,
  input  wire   rx_n,
  
  output wire   v,
  output wire   q
);

  localparam MS = 2;
  logic [MS: 0] ms_pipe_p;
  logic [MS: 0] ms_pipe_n;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      ms_pipe_p <= '0;
      ms_pipe_n <= '0;
    end
    else begin
      if (ena) begin
        ms_pipe_p <= { rx_p, ms_pipe_p[MS: 1] };
        ms_pipe_n <= { rx_n, ms_pipe_n[MS: 1] };
      end
    end
    
  logic q_p, q_n;
  always_comb
    { q_p, q_n } = { ms_pipe_p[0], ms_pipe_n[0] };
    

  logic r_v;
  logic r_q;
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      r_v <= '0;
      r_q <= '0;
    end
    else begin
      if (ena) begin
        r_v <= q_p ^ q_n;
        r_q <= q_p;
      end
    end

  assign v = r_v;
  assign q = r_q;

endmodule
