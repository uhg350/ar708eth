module gmii_tx_frame
(
  input  wire         gmii_tx_areset,
  input  wire         gmii_tx_clk,
  input  wire         gmii_tx_acq,
  output wire         gmii_tx_en,
  output wire         gmii_tx_er,
  output wire [ 7: 0] gmii_tx_d,

  output wire         frame_busy,
  input  wire         frame_ena,
  input  wire [ 7: 0] frame_d,
  output wire         frame_ack
);

  localparam PREAMBLE_LENGTH  = 7;
  localparam PADDING_LENGTH   = 60;
  localparam IFG_LENGTH       = 12;
  localparam FCS_LENGTH       = 4;

  localparam PREAMBLE_OCTET   = 8'h55;
  localparam SFD_OCTET        = 8'hD5;

  wire areset = gmii_tx_areset;
  wire clk    = gmii_tx_clk;
  wire ena    = gmii_tx_acq;

  // Quartus doesn't support 'let' keyword
  //  let max(a,b) = (a > b) ? (a) : (b);
  
  function int max(input int a, input int b);
    max = (a > b) ? (a) : (b);
  endfunction

  localparam OCMAX = max(max(PREAMBLE_LENGTH, PADDING_LENGTH), IFG_LENGTH);
  localparam OCW   = $clog2(OCMAX);
  
  typedef logic [OCW-1: 0] counter_t;
  counter_t counter_d, counter_q;
  logic counter_set, counter_dec;

  ud_counter#(.W(OCW), .DIR("DOWN"))
    counter
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      
      .step   (counter_dec),
      .set    (counter_set),
      .d      (counter_d),
      .q      (counter_q)
    );

/*
  lpm_counter#(.LPM_WIDTH(OCW), .LPM_DIRECTION("DOWN"))
    counter
    (
      .aclr   (areset),
      .clock  (clk),
      .clk_en (ena),
      
      .sload  (counter_set),
      .data   (counter_d),
      .cnt_en (counter_dec),
      
      .q      (counter_q)
    );
*/

  logic counter_done;
  always_comb
    counter_done = (counter_q == '0);

  enum int unsigned
  { 
    stIDLE,
    stPREAMBLE,
    stSFD,
    stDATA,
    stFCS,
    stIFG
  } state, next_state;
  
  always_ff@(posedge clk or posedge areset)
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  logic tx_er, tx_en;
  logic [ 7: 0] tx_d;
  
  logic         fcs_init;
  logic         fcs_next;
  logic [31: 0] fcs_q;
  
  logic [ 7: 0] fcs_qi;
  
  always_comb begin
    case (counter_q[ 1: 0])
      2'b11 : fcs_qi = fcs_q[ 7: 0];
      2'b10 : fcs_qi = fcs_q[15: 8];
      2'b01 : fcs_qi = fcs_q[23:16];
      2'b00 : fcs_qi = fcs_q[31:24];
    endcase
  end

  always_comb begin
    next_state = state;
    
    counter_set = '0;
    counter_d   = 'x;
    counter_dec = !counter_done;
    
    tx_en       = '0;
    tx_er       = '0;
    tx_d        = 'x;
    
    fcs_init    = '0;
    fcs_next    = '0;

    case (state)

    stIDLE : begin
      if (frame_ena) begin
        counter_set = '1;
        counter_d   = counter_t'(PREAMBLE_LENGTH - 1);
        
        next_state = stPREAMBLE;
      end
    end

    stPREAMBLE : begin
      tx_en       = '1;
      tx_d        = PREAMBLE_OCTET;

      if (counter_done)
        next_state = stSFD;
    end
    
    stSFD : begin
      fcs_init = '1;
      
      tx_en    = '1;
      tx_d     = SFD_OCTET;

      counter_set = '1;
      counter_d   = counter_t'(PADDING_LENGTH - 1);
      
      next_state = stDATA;
    end

    stDATA : begin
      tx_en       = '1;
      
      tx_d        = frame_ena ? frame_d : 8'h00;
      
      fcs_next    = '1;

      if (!frame_ena && counter_done) begin
        counter_set = '1;
        counter_d   = counter_t'(FCS_LENGTH - 1);
        
        next_state = stFCS;
      end
    end
    
    stFCS : begin
      tx_en       = '1;
      
      tx_d        = fcs_qi;

      if (counter_done) begin
        counter_set = '1;
        counter_d   = counter_t'(IFG_LENGTH - 2);

        next_state = stIFG;
      end

    end
    
    stIFG : begin
    
      if (counter_done)
        next_state = stIDLE;
    end

    endcase
  end

  crc32_ieee_802_3
    frame_check_sum
    (
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .init     (fcs_init),
      .next     (fcs_next),
      
      .d        (tx_d),
      .crc      (fcs_q)
    );

  assign gmii_tx_en = tx_en;
  assign gmii_tx_er = tx_er;
  assign gmii_tx_d  = tx_d;

  assign frame_busy = (state != stIDLE);
  assign frame_ack  = (state == stDATA);

endmodule

/*
/////////////////////////////////////////////////////////////
module gmii_tx_frame
(
  input  wire         gmii_tx_areset,
  input  wire         gmii_tx_clk,
  input  wire         gmii_tx_acq,
  output wire         gmii_tx_en,
  output wire         gmii_tx_er,
  output wire [ 7: 0] gmii_tx_d,

  output wire         frame_busy,
  input  wire         frame_ena,
  input  wire [ 7: 0] frame_d,
  output wire         frame_ack
);

  localparam PREAMBLE_LENGTH  = 7;
  localparam PADDING_LENGTH   = 60;
  localparam IFG_LENGTH       = 12;
  localparam FCS_LENGTH       = 4;

  localparam PREAMBLE_OCTET   = 8'h55;
  localparam SOF_OCTET        = 8'hD5;

  wire areset = gmii_tx_areset;
  wire clk    = gmii_tx_clk;
  wire ena    = gmii_tx_acq;

  //let max(a,b) = (a > b) ? (a) : (b);

  localparam OCMAX = `max(`max(PREAMBLE_LENGTH, PADDING_LENGTH), IFG_LENGTH);
  localparam OCW   = $clog2(OCMAX);
  
  typedef logic [OCW-1: 0] counter_t;
  counter_t counter_d, counter_q;
  logic counter_set, counter_dec;
  ud_counter#(.W(OCW), .DIR("DOWN"))
    counter
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      
      .step   (counter_dec),
      .set    (counter_set),
      .d      (counter_d),
      .q      (counter_q)
    );
    
  logic counter_done;
  always_comb
    counter_done = (counter_q == '0);
  
  enum int unsigned
  { 
    stIDLE,
    stPREAMBLE,
    stDATA,
    stFCS
  } state, next_state;
  
  always_ff@(posedge clk or posedge areset)
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  logic tx_er, tx_en;
  logic [ 7: 0] tx_d;
  
  logic fcs_ena;
  logic [ 3: 0][ 7: 0] fcs_qi;

  always_comb begin
    next_state = state;
    
    counter_set = '0;
    counter_d   = 'x;
    counter_dec = !counter_done;
    
    tx_en       = '0;
    tx_er       = '0;
    tx_d        = 'x;
    
    fcs_ena     = '0;
    
    case (state)
    stIDLE : begin
      if (counter_done && frame_ena) begin
        counter_set = '1;
        counter_d   = counter_t'(PREAMBLE_LENGTH);
    
        next_state = stPREAMBLE;
      end
    end
    
    stPREAMBLE : begin
      
      tx_en       = '1;
      tx_d        = PREAMBLE_OCTET;

      if (counter_done) begin
      
        tx_d        = SOF_OCTET;
      
        counter_set = '1;
        counter_d   = counter_t'(PADDING_LENGTH);

        next_state = stDATA;
      end
    end

    stDATA : begin
      tx_en       = '1;
      
      tx_d        = frame_ena ? frame_d : 8'h00;
      
      fcs_ena     = '1;
      
      if (!frame_ena && counter_done) begin
        tx_d      = fcs_qi[0];
        
        fcs_ena   = '0;
        
        counter_set = '1;
        counter_d   = counter_t'(FCS_LENGTH - 1);
        
        next_state = stFCS;
      end
    end
    
    stFCS : begin
      tx_en       = '1;
      case (counter_q[ 1: 0])
        2'b11   : tx_d = fcs_qi[1];
        2'b10   : tx_d = fcs_qi[2];
        default : tx_d = fcs_qi[3];
      endcase

      if (counter_done) begin
        tx_en = '0;
        counter_set = '1;
        counter_d   = counter_t'(IFG_LENGTH - 2);
        
        next_state = stIDLE;
      end
    end

    endcase
  end

  crc_32_type
    fcs
    (
      .clk      (clk),
      .reset_n  (!areset),
      
      .init     (state == stPREAMBLE),
      .d        (tx_d),
      .ena      (fcs_ena && ena),
      
      .q        (fcs_qi)
    );

  assign gmii_tx_en = tx_en;
  assign gmii_tx_er = tx_er;
  assign gmii_tx_d  = tx_d;

  assign frame_busy = !((state == stIDLE) && counter_done);
  assign frame_ack  = (state == stDATA) && frame_ena;
  
endmodule
*/