module hi157x_fsm
#(
  parameter CLKFREQ   = 50_000_000,
  parameter AR708FREQ =  1_000_000
)
(
  input  wire     areset,
  input  wire     clk,
  
  input  wire     line_p,
  input  wire     line_n,
  
  output wire     idle,
  output wire     sow,
  output wire     next_bit,
  output wire     eow,
  output wire     timeout
);

/*
  logic line_idle;
  logic line_value;
  
  always_comb begin
    line_idle   = line_p == line_n;
    line_value  = line_p;
  end
    
  localparam CDIV = ( CLKFREQ / (2 * AR708FREQ) );
  
  localparam SP0  = 1;
  localparam SP1  = SP0 + CDIV;
  localparam SP2  = SP1 + CDIV;
  
  localparam TO   = SP2 + CDIV - 1;

  
  typedef logic [$clog2(4 * CDIV) - 1: 0] cnt_t;
  
  cnt_t dcnt;
  
  always_ff@(posedge areset or posedge clk)
    if (areset)
      dcnt <= '0;
    else begin
      if (line_idle)
        dcnt <= '0;
      else
        dcnt <= dcnt + 1'b1;
    end

  logic sample_point;
  always_comb begin
    sample_point = (dcnt == SP0) || (dcnt == SP1) || (dcnt == SP2);
    timeout_point = (dcnt == TO)
  end

  logic [ 5: 0] spipe;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      spipe <= '0;
    else
      if (sample_point)
        spipe <= { spipe[ 4: 0], line_value };

  logic is_sof_pattern;
  logic is_eof_pattern;
  always_comb begin
    is_sof_pattern = (spipe == 6'b111_000);
    is_eof_pattern = (spipe == 6'b000_111);
  end
  
  case (state)
    stIDLE : begin
      if (is_sof_pattern && sample_point)
        next_state = stSOW;
    end
    
    stSOW : begin
      next_state = stBIT_H0;
    end
    
    stBIT_H0 : begin
      if (timeout)
        next_state = stTIMEOUT;
      else if (sample_point && ^bb)
        next_state = stNEXT_BIT;
    end
    
    stNEXT_BIT : begin
      next_state = stBIT_H0;
    end
    
    stTIMEOUT: begin
      next_state = stIDLE;
    end
    
    stEOW : begin
      next_state = stIDLE;
    end

  endcase
*/
endmodule

