module ar708_mmap
(
  input  wire         areset,
  input  wire         clk,

  input  wire         ena,
  input  wire [ 9: 0] address,
  input  wire [ 3: 0] byteenable,
  input  wire         read,
  output wire [31: 0] readdata,
  input  wire         write,
  input  wire [31: 0] writedata

);

  logic [ 7: 0] ad;
  logic [ 1: 0] sel;
  always_comb begin
    ad  = address[ 7: 0];
    sel = address[ 9: 8];
  end

  logic [ 3: 0]         rd;
  logic [ 3: 0][31: 0]  rd_q;
  logic [ 3: 0]         wr;
  logic [ 3: 0][31: 0]  wr_d;
  logic [ 3: 0][ 3: 0]  be;

  logic [ 3: 0] ce;
  
  always_comb begin
    case (sel)
      2'b00 : ce = 4'b0001;
      2'b01 : ce = 4'b0010;
      2'b10 : ce = 4'b0100;
      2'b11 : ce = 4'b1000;
    endcase
    
    rd    = { 4 { read  } } & ce;
    wr    = { 4 { write } } & ce;
    wr_d  = { 4 { writedata } };
    be    = { 4 { byteenable } };
  end
    
  logic [ 1: 0] rd_sel;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      rd_sel <= '0;
    else
      if (ena)
        rd_sel <= sel;
        
  assign readdata = rd_q[rd_sel];

  //
  assign rd_q[ 3: 1] = 'x;

  ar708_rx_mmap
    ar708_rx_mmap_i0
    (
      .areset           (areset),
      .clk              (clk),
      
      .mmap_ena         (ena),
      .mmap_address     (ad),
      .mmap_byteenable  (be[0]),
      .mmap_read        (rd[0]),
      .mmap_readdata    (rd_q[0]),
      .mmap_write       (wr[0]),
      .mmap_writedata   (wr_d[0])
    );

  
endmodule

