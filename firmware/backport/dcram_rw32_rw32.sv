`default_nettype none

module dcram_rw32_rw32
#(
  parameter SIZE  = 8192,
  parameter AW    = $clog2(SIZE),
  parameter DEPTH = "AUTO",
  parameter INIT_FILE = ""
)
(
  input  wire             mem_ws_areset,
  input  wire             mem_ws_clk,
  input  wire             mem_ws_ena,
  input  wire  [AW-1: 0]  mem_ws_addr,
  input  wire  [ 3: 0]    mem_ws_be,
  input  wire  [31: 0]    mem_ws_d,
  input  wire             mem_ws_write,
  input  wire             mem_ws_read,
  output wire  [31: 0]    mem_ws_q,

  input  wire             mem_rs_areset,
  input  wire             mem_rs_clk,
  input  wire             mem_rs_ena,
  input  wire  [AW-1: 0]  mem_rs_addr,
  input  wire  [ 3: 0]    mem_rs_be,
  input  wire  [31: 0]    mem_rs_d,
  input  wire             mem_rs_write,
  input  wire             mem_rs_read,
  output wire  [31: 0]    mem_rs_q
);

  localparam  UNINITIALIZED = (INIT_FILE == "") ? "TRUE" : "FALSE";

  localparam  RAM_BW    = 4;
  localparam  RAM_SIZE  = SIZE;
  localparam  RAM_AW    = AW;
  localparam  RAM_DW    = 32;

  altsyncram#(
    .init_file              (INIT_FILE),
    .power_up_uninitialized (UNINITIALIZED),
    .operation_mode         ("BIDIR_DUAL_PORT"),
    .read_during_write_mode_mixed_ports("DONT_CARE"),
    
    .byte_size        (8),

    .width_a          (RAM_DW),
    .widthad_a        (RAM_AW),
    .numwords_a       (RAM_SIZE),
    .width_byteena_a  (RAM_BW),
    .address_reg_a    ("CLOCK0"),
    .outdata_reg_a    ("UNREGISTERED"),

    .width_b          (RAM_DW),
    .widthad_b        (RAM_AW),
    .numwords_b       (RAM_SIZE),
    .width_byteena_b  (RAM_BW),
    .address_reg_b    ("CLOCK1"),
    .outdata_reg_b    ("UNREGISTERED")
  )
    ram_buffer(
      .clock0     (mem_ws_clk),
      .clocken0   (mem_ws_ena),
      .address_a  (mem_ws_addr),
      .byteena_a  (mem_ws_be),
      .wren_a     (mem_ws_write),
      .data_a     (mem_ws_d),
      .rden_a     (mem_ws_read),
      .q_a        (mem_ws_q),
      
      .clock1     (mem_rs_clk),
      .clocken1   (mem_rs_ena),
      .address_b  (mem_rs_addr),
      .byteena_b  (mem_rs_be),
      .wren_b     (mem_rs_write),
      .data_b     (mem_rs_d),
      .rden_b     (mem_rs_read),
      .q_b        (mem_rs_q),

      // make simulator happy
      .aclr0          (1'b0),
      .aclr1          (1'b0),
      .clocken2       (1'b1),
      .clocken3       (1'b1),
      .addressstall_a (1'b0),
      .addressstall_b (1'b0),
      .eccstatus      ()
    );

endmodule
