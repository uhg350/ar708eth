interface rmii_i
(
  input  wire         areset,
  input  wire         clk,
  input  wire         mode,
  input  wire         rx_active,
  input  wire         rx_dv,
  input  wire         rx_er,
  input  wire [ 1: 0] rx_d,
  output wire         tx_en,
  output wire         tx_er,
  output wire [ 1: 0] tx_d
);

endinterface
