module crc_32_type
#(
  parameter DATA_WIDTH    = 8
)
(
  input  logic                   reset_n,
  input  logic                   clk,
  
  input  logic                   init,
  input  logic [DATA_WIDTH-1:0]  d,
  input  logic                   ena,
  
  output logic [31:0]   q
);
  generic_crc#
  (
    .CRC_WIDTH(32),
    .CRC_POLYNOME(32'h04C11DB7),
    .CRC_INITIAL(32'hFFFFFFFF),
    .CRC_XOR(32'hFFFFFFFF),

    .DATA_WIDTH(DATA_WIDTH),

    .DATA_REFLECT(1),
    .CRC_REFLECT(1)
  )
    crc32 
    (
      .reset_n(reset_n),
      .clk(clk),
      
      .init(init),
      .d(d),
      .ena(ena),    
      .q(q)
    );
endmodule
