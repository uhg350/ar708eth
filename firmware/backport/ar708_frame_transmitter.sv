module ar708_sow_data_eow
(
  input  wire         areset,
  input  wire         clk,
  input  wire         ena,

  input  wire         send,
  input  wire         d,
  output wire         data,
  output wire         idle,

  output wire [ 1: 0] q
);

  enum int unsigned
  {
    stIDLE,
    stSOW0,
    stSOW1,
    stSOW2,
    stDATA,
    stEOW0,
    stEOW1,
    stEOW2
  } state, next_state;

  always_ff@(posedge areset or posedge clk) 
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  logic [ 1: 0] tx_d;
  
  always_comb begin
    next_state = state;

    case (state)
      stIDLE: begin
        if (send)
          next_state = stSOW0;
      end
      
      stSOW0: begin
        tx_d  = 2'b11;
        next_state = stSOW1;
      end
      
      stSOW1: begin
        tx_d  = 2'b10;
        next_state = stSOW2;
      end
      
      stSOW2: begin
        tx_d  = 2'b00;
        next_state = stDATA;
      end
      
      stDATA: begin
        tx_d  = { d, !d };
        if (!send)
          next_state = stEOW0;
      end
      
      stEOW0: begin
        tx_d  = 2'b00;
        next_state = stEOW1;
      end
      
      stEOW1: begin
        tx_d  = 2'b01;
        next_state = stEOW2;
      end
      
      stEOW2: begin
        tx_d  = 2'b11;
        next_state = stIDLE;
      end

    endcase
  end
  
  assign data = (state == stDATA);
  assign idle = (state == stIDLE);
  assign q    = tx_d;

endmodule


module ar708_frame_transmitter
#(
  parameter CLK_FREQ   = 50_000_000,
  parameter AR708_FREQ =  1_000_000
)
(
  input  wire         areset,
  input  wire         clk,
  
  input  wire         start,
  output wire         busy,
  
  output wire         acq,
  input  wire         d,
  
  output wire         tx_oe,
  output wire         tx_p,
  output wire         tx_n
);
  
  localparam CDIV   = CLK_FREQ / (2 * AR708_FREQ);
  localparam CSTEPS = (3 + 1600 + 3) * 2;
  
  logic process_q;
  logic next_div_q;
  logic next_step_q;
  logic [$clog2(CDIV)-1: 0] div_q;
  logic [$clog2(CSTEPS)-1: 0] step_q;
  
  process_steps#(
    .DIV    (CDIV),
    .STEPS  (CSTEPS)
  )
    process_steps_i0
    (
      .areset       (areset),
      .clk          (clk),
      .ena          (1'b1),

      .start        (start),
      .busy         (busy),

      .process_q    (process_q),

      .next_div_q   (next_div_q),
      .div_q        (div_q),

      .next_step_q  (next_step_q),
      .step_q       (step_q)
    );

  logic q;
  always_comb begin
    case (step_q)
            0: q = '1;
            1: q = '1;
            2: q = '1;
            3: q = '0;
            4: q = '0;
            5: q = '0;
            
         3206: q = '0;
         3207: q = '0;
         3208: q = '0;
         3209: q = '1;
         3210: q = '1;
         3211: q = '1;
      default: q = step_q[0] ? ~d : d;
    endcase
  end
  
  logic oe;
  logic q_p;
  logic q_n;
  
  always_ff@(posedge areset or posedge clk)
    if (areset) begin
      oe  <= '0;
      q_p <= '0;
      q_n <= '0;
    end
    else begin
      oe  <= process_q;
      q_p <=  q && process_q;
      q_n <= ~q && process_q;
    end


  assign tx_oe = oe;
  assign tx_p  = q_p;
  assign tx_n  = q_n;
  assign acq   = next_div_q && step_q[0] && (step_q > 5) && (step_q < 3206);

endmodule
