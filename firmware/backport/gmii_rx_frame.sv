`default_nettype none

module gmii_rx_frame
#(
  parameter SW = 11
)
(
  input  wire           gmii_rx_areset,
  input  wire           gmii_rx_clk,
  input  wire           gmii_rx_acq,
                        
  input  wire           gmii_rx_dv,
  input  wire           gmii_rx_er,
  input  wire [ 7: 0]   gmii_rx_d,

  input  wire           drop,
  output wire [ 7: 0]   q,
  output wire [SW-1: 0] csize,
  output wire           is_idle,
  output wire           is_preamble,
  output wire           is_sfd,
  output wire           is_data,
  output wire           is_eof,
  output wire           is_eoferr,
  output wire           is_crcerr,
  output wire           is_drop
);
  localparam PREAMBLE_OCTET   = 8'h55;
  localparam SFD_OCTET        = 8'hD5;

  wire areset = gmii_rx_areset;
  wire clk    = gmii_rx_clk;
  wire ena    = gmii_rx_acq;

  enum int unsigned
  {
    stIDLE,
    stPREAMBLE,
    stSFD,
    stDATA,
    stEOF,
    stEOFERR,
    stDROP
  } state, next_state;

  always_ff@(posedge areset or posedge clk) 
    if (areset)
      state <= stIDLE;
    else
      if (ena)
        state <= next_state;

  typedef logic [SW-1: 0] counter_t;
  counter_t counter_d, counter_q;
  logic counter_set, counter_inc;

/*
  lpm_counter#(.LPM_WIDTH(SW), .LPM_DIRECTION("UP"))
    counter
    (
      .aclr   (areset),
      .clock  (clk),
      .clk_en (ena),
      
      .sclr   (counter_set),
      .cnt_en (counter_inc),
      
      .q      (counter_q)
    );
*/

  ud_counter#(.W(SW), .DIR("UP"))
    counter
    (
      .areset (areset),
      .clk    (clk),
      .ena    (ena),
      
      .step   (counter_inc),
      .set    (counter_set),
      .d      (counter_d),
      .q      (counter_q)
    );

  assign csize = counter_q;

  always_comb begin
    counter_set = (state == stSFD);
    counter_d   = '0;
    
    counter_inc = (state == stDATA);
  end
  
  wire          rx_dv = gmii_rx_dv;
  wire          rx_er = gmii_rx_er;
  wire [ 7: 0]  rx_d  = gmii_rx_d;

  always_comb begin
    next_state = state;

    case (state)
      stIDLE: begin
        if (rx_dv && !rx_er && rx_d == PREAMBLE_OCTET)
          next_state = stPREAMBLE;
      end
      
      stPREAMBLE: begin
        if      (!rx_dv)
          next_state = stIDLE;
        else if (rx_er)
          next_state = stDROP;
        else if (drop)
          next_state = stDROP;
        else if (rx_d == SFD_OCTET)
          next_state = stSFD;
        else if (rx_d != PREAMBLE_OCTET)
          next_state = stDROP;
      end 
      
      stSFD: begin
        if      (!rx_dv)
          next_state = stEOFERR;
        else if (rx_er)
          next_state = stDROP;
        else
          next_state = stDATA;
      end
      
      stDATA: begin
        if      (!rx_dv)
          next_state = stEOF;
        else if (rx_er)
          next_state = stDROP;
        else if (drop)
          next_state = stDROP;
      end
      
      stEOF : begin
        next_state = stIDLE;
      end
      
      stEOFERR : begin
        next_state = stIDLE;
      end

      stDROP: begin
        if (!rx_dv)
          next_state = stEOFERR;
      end

    endcase
  end

  logic [31: 0] fcs;

  assign is_idle      = (state == stIDLE);
  assign is_preamble  = (state == stPREAMBLE);
  assign is_sfd       = (state == stSFD);
  assign is_data      = (state == stDATA);
  assign is_eof       = (state == stEOF);
  assign is_eoferr    = (state == stEOFERR);
  assign is_crcerr    = (fcs != 32'h2144df1c);
  assign is_drop      = (state == stDROP);
  
  logic [7:0] r_q;
  always_ff@(posedge areset or posedge clk)
    if (areset)
      r_q <= '0;
    else
      if (ena)
        r_q <= rx_d;
        
  assign q = r_q;

  crc32_ieee_802_3
    frame_check_sum
    (
      .areset   (areset),
      .clk      (clk),
      .ena      (ena),
      
      .init     (is_sfd),
      .next     (is_data),
      
      .d        (r_q),
      .crc      (fcs)
    );

endmodule
